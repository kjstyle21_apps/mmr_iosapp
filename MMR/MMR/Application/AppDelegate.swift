//
//  AppDelegate.swift
//  MMR
//
//  Created by Mindbowser on 10/12/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import CoreLocation
import AWSCore
import AWSS3
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate, MMRMediaUploaderDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var isanAuthUser = false 
    var beaconRegion: CLBeaconRegion!
    var locationManager: CLLocationManager!
    var lastFoundBeacon: CLBeacon!
    var lastProximity: CLProximity! = CLProximity.unknown
//    var uploadReqArrayAppDel = [AWSS3TransferManagerUploadRequest]()
//    var serialArray = [String]()
    var timerSet = false
  
    var timer = Timer()
    var surveyTimer = Timer()
    var surveyTimerSet = false
    var netWorkStatus = false
    let uploader = MMRMediaUploader.sharedInstance
    var s3UploadRequestProgressDict = [String : Double]()
    var s3UploadRequestNameArray = [String]()
    var s3UploadProgress = 0.0
    var totalNotUploadedRecords = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        sleep(2)

        // Checking for if database is already created
        let databaseExists = FileManager.default.fileExists(atPath: CreateDB.getDataBasePath())
        
        if !databaseExists {
            CreateDB.createAndCheckDatabaseOfQuestionTable()
        }
        else {
          let dbVer1 = UserDefaults.standard.value(forKey: DATABASE_VERSION_UD)
          if let dbVer = UserDefaults.standard.value(forKey: DATABASE_VERSION_UD) as? NSInteger {
            if dbVer < 3{
              CreateDB.createAndCheckUpdateDatabaseOfQuestionTable()
            }
          }
          else {
            CreateDB.createAndCheckUpdateDatabaseOfQuestionTable()
          }
      }

        // Login valdiation check...If  no auth token take to Login screen else Home screen of app
      if UserDefaults.standard.object(forKey: authToken_UD) != nil{
        
        let navigationController: UINavigationController = self.window?.rootViewController as! UINavigationController
        //root view controller as home view controller
        let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
//        let nav = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationViewController") as! UINavigationController
        
        let home = storyboard.instantiateViewController(withIdentifier: "SSASideMenu") as! SSASideMenu
        navigationController.pushViewController(home, animated: false)
        
        print("naviga : \(navigationController.viewControllers)")
        //self.window?.rootViewController = nav
//        let arr = [1,2]
//        print(arr[3])
    }
      
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        
      UIApplication.shared.registerUserNotificationSettings(settings)
      UIApplication.shared.registerForRemoteNotifications()
        IQKeyboardManager.shared().isEnabled = true
      if let noSurveyStatus = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
        if !noSurveyStatus {
          if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
          self.startDiscovery()
          }
        }
      }
//        self.startDiscovery()
//      LManager.instance.startDiscovery()

        // Override point for customization after application launch.
      NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
      Reach().monitorReachabilityChanges()
      
      if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound], completionHandler: { (accepted, error) in
          print("Notification access denied.")
        })
      } else {
        // Fallback on earlier versions
      }
      
      let currentLangStrArr = Locale.preferredLanguages
      let currentLangStr = currentLangStrArr[0]
      print(currentLangStr)
      let tempArr = currentLangStr.components(separatedBy: "-")
      let currentLang = tempArr[0]
      if currentLang == "en" {
        Localize.setCurrentLanguage(currentLang)
      } else if currentLang == "es" || currentLang == "it" || currentLang == "de" || currentLang == "nl" || currentLang == "fr" || currentLang == "tl" || currentLang == "id" || currentLang == "th" {
        Localize.setCurrentLanguage(currentLang)
      } else if currentLang == "zh" {
        Localize.setCurrentLanguage("zh-Hant")
      } else if currentLang == "pt" {
        Localize.setCurrentLanguage("pt-BR")
      } else {
        Localize.setCurrentLanguage("en")
      }
//      let languageCodes = ["en","zh-Hant","es","it","de","nl","pt-BR","fr","tl","id","ty"]

      let notificationSettings = UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
      UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        
        if (launchOptions != nil) {
        if (launchOptions![UIApplicationLaunchOptionsKey.localNotification] != nil) {
            if launchOptions![UIApplicationLaunchOptionsKey.localNotification].debugDescription.contains(INTERVAL) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NAVIGATE_TO_INTERVAL"), object: nil)
            }
        }
    }
        return true
    }
  
  @objc func update() {
    self.timerSet = false
    UserDefaults.standard.setValue("YES", forKey: DONE_WITH_INTERVAL_UD)
    NotificationCenter.default.post(name: Notification.Name(rawValue: "DONE_WITH_INTERVAL_NOTIFY"), object: nil)
  }

    @objc func updateForSurveyTimer() {
        //hit notification for triggering the survey
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TRIGGER_SURVEY"), object: nil)
        self.surveyTimerSet = false
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_STOP_DATE_UD)
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_START_DATE_UD)
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_COMPLETION_SECs_UD)
    }

  @objc func networkStatusChanged(_ notification: Notification) {
    let userInfo = notification.userInfo
    print(userInfo)
     if (UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD) != nil)
    {
      
//      if let tempFlag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//      if tempFlag {
    if userInfo!["Status"] as! String == "Offline" {
      print("Offline")
        self.netWorkStatus = false
      UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
      UserDefaults.standard.synchronize()
    }
    else {
      print("Online")
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        self.totalNotUploadedRecords = UploadRecordSharedClass.sharedInstance.getCountOfNotUploadedRecords(userID! as AnyObject, type: "")
        self.netWorkStatus = true
      if UserDefaults.standard.bool(forKey: UPLOADING_IN_PROCESS_UD) == false {
        uploader.delegate = self
        let status = uploader.startUploadProcess("")
        if status {
          UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
          UserDefaults.standard.synchronize()
          NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOADING_IN_PROCESS_NOTIFY"), object: ["Status" : "true"])
        }
        else {
          UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
          UserDefaults.standard.synchronize()
          NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOADING_IN_PROCESS_NOTIFY"), object: ["Status" : "false"])
        }
      }
    }
//    }
//  }
  }
  }

  func didCompleteImageUpload(_ status: Bool) {
    if status {
      UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
      UserDefaults.standard.synchronize()
    }
    else {
      UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
      UserDefaults.standard.synchronize()
    }
  }
    func applicationWillResignActive(_ application: UIApplication) {
      
      if self.timerSet {
        self.timerSet = false
        UserDefaults.standard.set(Date(), forKey: TIMER_STOP_DATE_UD)
        UserDefaults.standard.synchronize()
      }
      self.timer.invalidate()

        if self.surveyTimerSet {
            UserDefaults.standard.set(Date(), forKey: SURVEY_TIMER_STOP_DATE_UD)
            UserDefaults.standard.synchronize()
        }
        self.surveyTimer.invalidate()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
      print("Foreground")
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
      self.clearAPNSNotifications()
      if (UserDefaults.standard.value(forKey: TIMER_STOP_DATE_UD) != nil) {
         let killDate = UserDefaults.standard.value(forKey: TIMER_STOP_DATE_UD) as! Date
        print("kill date : \(killDate)")
        let startDate = UserDefaults.standard.value(forKey: TIMER_START_DATE_UD) as! Date
        print("start date : \(startDate)")
        let differ = killDate.timeIntervalSince(startDate)
        let differ2 = Date().timeIntervalSince(killDate)
        print("current date : \(Date())")
        let completeInterval = UserDefaults.standard.value(forKey: TIMER_COMPLETION_SECs_UD) as! TimeInterval
        
        let differ3 = completeInterval - (differ2 + differ)
        print(differ3)
        if differ3 <= 0 {
          self.update()
        }
        else {
            self.timerSet = true
          self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(differ3), target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        }
      }
        if (UserDefaults.standard.value(forKey: SURVEY_TIMER_STOP_DATE_UD) != nil) {
            let killDate = UserDefaults.standard.value(forKey: SURVEY_TIMER_STOP_DATE_UD) as! Date
            print("kill date : \(killDate)")
            let startDate = UserDefaults.standard.value(forKey: SURVEY_TIMER_START_DATE_UD) as! Date
            print("start date : \(startDate)")
            let differ = killDate.timeIntervalSince(startDate)
            let differ2 = Date().timeIntervalSince(killDate)
            print("current date : \(Date())")
            let completeInterval = UserDefaults.standard.value(forKey: SURVEY_TIMER_COMPLETION_SECs_UD) as! TimeInterval
            let differ3 = completeInterval - (differ2 + differ)
            print(differ3)
            if differ3 <= 0{
                self.updateForSurveyTimer()
            }
            else {
                self.surveyTimer = Timer.scheduledTimer(timeInterval: TimeInterval(differ3), target: self, selector: #selector(self.updateForSurveyTimer), userInfo: nil, repeats: false)
            }
        }
  NotificationCenter.default.post(name: Notification.Name(rawValue: "MMR_APP_ACTIVE_STATE_NOTIFICATION"), object: nil)

      let reachability: Reachability
      do {
        reachability = try Reachability.reachabilityForInternetConnection()
      } catch {
        print("Unable to create Reachability")
        return
      }
      
      if reachability.isReachable() {
        //internet connected
        print("Connected")
      }
      else  {
        //not connected
        NotificationCenter.default.post(name: Notification.Name(rawValue: "INTERNET_CONNECTION_FAIL"), object: nil)
      }
        if UserDefaults.standard.object(forKey: authToken_UD) != nil {
            CompressionRecordsSharedClass.sharedInstance.startCompression()
        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

  
    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Mindbowser.MMR" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "MMR", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    var deviceIdString = String(describing: deviceToken)

    deviceIdString = deviceIdString.replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)
    deviceIdString = deviceIdString.replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
    deviceIdString = deviceIdString.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
    print("deviceIdString = \(deviceIdString)")
    
    let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
    print("token: \(token)")
    UserDefaults.standard.set(token, forKey: MMRServiceRegisterRequestParameterDeviceToken)
//    UserDefaults.standard.set(deviceIdString, forKey: MMRServiceRegisterRequestParameterDeviceToken)
    UserDefaults.standard.synchronize()
  }
    
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    //Log an error for debugging purposes, user doesn't need to know
    print("didFailToRegisterForRemoteNotificationsWithError: \(error.localizedDescription)")
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    print("..... application didReceiveRemoteNotification()")
    // Worning not synced with server changes.
    print("Recived: \(userInfo)")
  }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
  func startDiscovery()
  {
    locationManager = CLLocationManager()
    locationManager.delegate = self
    if #available(iOS 8.0, *) {
      if(locationManager!.responds(to: #selector(CLLocationManager.requestAlwaysAuthorization))){
        if #available(iOS 8.0, *) {
          locationManager!.requestAlwaysAuthorization()
        } else {
          // Fallback on earlier versions
        }
      }
    } else {
      // Fallback on earlier versions
    }
    
    discoverBeacons()
  }
  
  /* Stop discovery */
  func stopBeacon()
  {
    if(locationManager != nil)
    {
      locationManager.stopMonitoring(for: beaconRegion)
      locationManager.stopRangingBeacons(in: beaconRegion)
      locationManager.stopUpdatingLocation()
    }
  }
  
  /* Start discovery of beacons */
  func discoverBeacons()
  {
    var uuid = UUID()
    if let tempUUID = UserDefaults.standard.value(forKey: CURRENT_SENSOR_UUID_UD) as? String {
      uuid = UUID(uuidString: tempUUID)!
    }
//    let uuid = NSUUID(UUIDString: "00005100-0000-1000-8000-008055aa55aa")
    
    beaconRegion = CLBeaconRegion(proximityUUID: uuid, identifier: "com.appcoda.beacondemo")
    beaconRegion.notifyOnEntry = true
    beaconRegion.notifyOnExit = true
    locationManager.startMonitoring(for: beaconRegion)
    locationManager.startUpdatingLocation()
    
  }
  
  /* Callback received after - monitoring region - request state of region */
  func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
    locationManager.requestState(for: region)
  }
  
  /* Callback received - mentioning region state */
  func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
    if state == CLRegionState.inside {
      if(beaconRegion != nil)
      {
        locationManager.startRangingBeacons(in: beaconRegion)
      }
    }
    else {
      if(beaconRegion != nil)
      {
        locationManager.stopRangingBeacons(in: beaconRegion)
      }
    }
  }
  
  
  func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
  }
  
  
  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    
  }
  
  
  /* Callback - if any beacon found  */
  // func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!)
  
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion)
  {
    _ = true
    
    print("beacons= \(beacons)")
    
    if beacons.count > 0
    {
      print(beacons.first)
      lastFoundBeacon = beacons.first
//      lastFoundBeacon = beacons[0]
      var proximityMessage: String!
      
      switch lastFoundBeacon.proximity
      {
      case CLProximity.immediate:
        
        proximityMessage = "Very close"
        
      case CLProximity.near:
        proximityMessage = "Near"
        
      case CLProximity.far:
        proximityMessage = "Far"
        
      default:
        proximityMessage = "Where's the iBeacon?" //Unknown
        
      }
      
      //Parse major and minor values
      Parser.instance.parseAndLoadBeaconDetails(beacons[0].proximityUUID,major: beacons[0].major,minor: beacons[0].minor,proximity: proximityMessage)
      
      // if beacons[0] != lastFoundBeacon || lastProximity != beacons[0].proximity
      //            {
      //                lastFoundBeacon = beacons[0]
      //
      //                print(beacons[0].proximityUUID);
      //
      //                _ = beacons[0].major;
      //                _ = beacons[0].minor;
      //                _ = beacons[0].rssi;
      //
      //                var proximityMessage: String!
      //                switch lastFoundBeacon.proximity
      //                {
      //                case CLProximity.Immediate:
      //
      //                    proximityMessage = "Very close"
      //
      //                case CLProximity.Near:
      //                    proximityMessage = "Near"
      //
      //                case CLProximity.Far:
      //                    proximityMessage = "Far"
      //
      //                default:
      //                    proximityMessage = "Where's the iBeacon?" //Unknown
      //
      //                }
      //
      //                //Parse major and minor values
      //                Parser.instance.parseAndLoadBeaconDetails(beacons[0].proximityUUID,major: beacons[0].major,minor: beacons[0].minor,proximity: proximityMessage)
      //            }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Did Fail With Error : \(error)")
  }
  
  func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
    print("Monitoring fail error : \(error)")
  }
  
  func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
    print("Ranging fail error : \(error)")
  }

  func clearAPNSNotifications() {
//    UIApplication.shared.applicationIconBadgeNumber = 0
//    UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
  }
  
  func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
    let rootControl = UIApplication.shared.keyWindow?.rootViewController
    if let payload = notification.userInfo as? [String : String] {
        if payload[PUSH_CODE] == INTERVAL {
//            self.update()
            if UIApplication.shared.applicationState == .active {
                if #available(iOS 8.2, *) {
                    AlertModel.sharedInstance.showAlert(notification.alertTitle!, message: notification.alertBody!, buttonTitles: ["Okay"], viewController: rootControl!, completionHandler: { (clickedButtonTitle, success) in
                        //navigate to interval type question
                        if (rootControl as? UINavigationController)?.viewControllers.last is BaseClassForQuestions {
                            //no need to navigate
                        } else {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "NAVIGATE_TO_INTERVAL"), object: nil)
                        }
                    })
                } else {
                    // Fallback on earlier versions
                }
            } else {
                if (rootControl as? UINavigationController)?.viewControllers.last is BaseClassForQuestions {
                    //no need to navigate
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NAVIGATE_TO_INTERVAL"), object: nil)
                }
            }
        }
    } else {
        if UIApplication.shared.applicationState == .active {
            if #available(iOS 8.2, *) {
                AlertModel.sharedInstance.showMessage(notification.alertTitle!, message: notification.alertBody!)
            } else {
                // Fallback on earlier versions
            }
        }
    }
  }
  
//  @available(iOS 10.0, *)
//  func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
//    completionHandler([.Alert])
//  }
    
//    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
//    AlertModel.sharedInstance.showErrorMessage(response.debugDescription)
//    response.notification.request.content.title
//    }
}

