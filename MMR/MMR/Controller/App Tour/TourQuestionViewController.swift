//
//  TourQuestionViewController.swift
//  MMR
//
//  Created by Mindbowser on 06/08/18.
//  Copyright © 2018 Mindbowser. All rights reserved.
//

import UIKit
import Instructions

class TourQuestionViewController: UIViewController, NVActivityIndicatorViewable,CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    @IBOutlet weak var maxDiscriptionLabel: UILabel!
    @IBOutlet weak var minDiscriptionLabel: UILabel!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonSliderType: UIButton!
    @IBOutlet weak var navigationView: Navigation_View!
    @IBOutlet weak var sliderQuestionTextView: UITextView!
    @IBOutlet weak var questionTextviewHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderQuestionScrollView: UIScrollView!

    @IBOutlet weak var compulsaryStarLabel: UILabel!

    @IBOutlet weak var heightOfMinOrMaxDiscLabel: NSLayoutConstraint!

    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var isMandatory         = true
    var questionFromServer  = NSMutableDictionary ()
    var stepOfSlider        = Float()
    var sliderForMMR        = MBSliderView()
    var tipsMsg = String()
    let coachMarksController = CoachMarksController()
    var selectedIndex = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
        navigationSetup()
        self.setText()
        selectedIndex = 0
        self.coachMarksController.dataSource = self
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.startTour), userInfo: nil, repeats: false)
    }
    
    func startTour() {
        DispatchQueue.main.async {
            self.coachMarksController.start(in: PresentationContext.viewController(self))
        }
    }

    func setText() {
    self.nextButtonSliderType.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    self.tipsMsg = MMRInstructionsTips.localized()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.coachMarksController.stop(immediately: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sliderQuestionTextView.setContentOffset(CGPoint.zero, animated: false)
        let newSize : CGSize = sliderQuestionTextView.sizeThatFits(CGSize(width: self.sliderQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextviewHeight.constant = newSize.height
        self.sliderQuestionScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height+200)
        sliderForMMR.frame = CGRect(x: sliderQuestionScrollView.frame.origin.x + 30 , y: newSize.height + 80 , width: self.sliderQuestionScrollView.frame.size.width - 60, height: 100)
        if (selectedIndex >= 4){
            if (selectedIndex == 5){
            self.nextButtonSliderType.backgroundColor = UIColor(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            }
            selectedIndex = 5
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initSetUp()
    {
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        //@kj
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
        if (currentSurveyID != nil){
            UserDefaults.standard.set(false, forKey: IS_NIL_SURVEY)
            self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        }else{
            UserDefaults.standard.set(true, forKey: IS_NIL_SURVEY)
        }
            self.minDiscriptionLabel.text = "1"
            self.maxDiscriptionLabel.text = "10"
        
            self.sliderQuestionTextView.text = Tour_Slider_Question_Text.localized()
        
            let newSize : CGSize = self.sliderQuestionTextView.sizeThatFits(CGSize(width: self.sliderQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        
        // configure sliderFromCode
        sliderForMMR.frame = CGRect(x: sliderQuestionScrollView.frame.origin.x + 30 , y: newSize.height + 80 , width: self.sliderQuestionScrollView.frame.size.width - 60, height: 100)
        
        sliderForMMR.minValue =  1.0
        sliderForMMR.maxValue =  10.0
        sliderForMMR.currentValue = sliderForMMR.minValue
        let sliderCenter = (sliderForMMR.maxValue - sliderForMMR.minValue) / 2
        sliderForMMR.currentValue = sliderCenter
        sliderForMMR.ignoreDecimals = true   // default value
        sliderForMMR.step = 1.0
        sliderForMMR.animateLabel = true      // default value
        sliderForMMR.delegate = self
        sliderForMMR.tintColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        self.sliderQuestionScrollView.addSubview(sliderForMMR)
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            self.nextButtonSliderType.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            self.nextButtonSliderType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
    }
    
    func navigationSetup() {
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
        navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        self.nextButtonSliderType.roundbutton(self.nextButtonSliderType, cornerradius: 5)
    }
    
    @objc func leftButtonAction() {
//        self.tipsMsg = "Slide to select appropriate value"
        AlertModel.sharedInstance
            .showMessage(self.tipsMsg, message: "Slide to select appropriate value")
    }
    
    @objc func rightButtonAction() {
        delegate!.goToHomeViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextButtonOfSliderTypeClicked(_ sender: UIButton) {
        delegate?.fetchTourSubmit()
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 4
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {
        switch index {
        case 0:
            let view = UIView(frame: CGRect(x: self.sliderForMMR.frame.origin.x, y: self.sliderForMMR.frame.origin.y + 20, width: self.sliderForMMR.frame.size.width, height: self.sliderForMMR.frame.size.height))
            return coachMarksController.helper.makeCoachMark(for: view)
        case 1:
            return coachMarksController.helper.makeCoachMark(for: self.navigationView.leftButton)
        case 2:
            return coachMarksController.helper.makeCoachMark(for: self.navigationView.progressView)
        case 3:
            return coachMarksController.helper.makeCoachMark(for: self.nextButtonSliderType)
        default:
            return coachMarksController.helper.makeCoachMark()
        }
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        var arrowView : CoachMarkArrowView?
        var coachMarkArrowView: TransparentCoachMarkArrowView? = nil
        if index == 0 {
            coachViews.bodyView.hintLabel.text = Tour_Slider_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        } else if index == 1 {
            coachViews.bodyView.hintLabel.text = Tour_i_Button_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        } else if index == 2 {
            coachViews.bodyView.hintLabel.text = Tour_ProgressBar_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        } else if index == 3 {
            coachViews.bodyView.hintLabel.text = Tour_Next_Button_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        }
        if let arrowOrientation = coachMark.arrowOrientation {
            coachMarkArrowView = TransparentCoachMarkArrowView(orientation: arrowOrientation)
        }
        if index == 3 {
            selectedIndex = 4
        }
        arrowView = coachMarkArrowView
        return (bodyView: coachViews.bodyView, arrowView: arrowView)
    }
}
extension TourQuestionViewController: MBSliderDelegate {
    func sliderView(_ sliderView: MBSliderView, valueDidChange value: Float) {
        if sliderView == sliderForMMR {
            self.nextButtonSliderType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            print("sliderFromCode: \(value)")
        }
    }
}
