//
//  TourSubmitViewController.swift
//  MMR
//
//  Created by Mindbowser on 06/08/18.
//  Copyright © 2018 Mindbowser. All rights reserved.
//

import UIKit

class TourSubmitViewController: UIViewController, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    @IBOutlet weak var navigationView: Navigation_View!
    @IBOutlet weak var submitSurveyButton: UIButton!
    @IBOutlet var quoteLabel: UILabel!
    @IBOutlet var memorySizeLabel: UILabel!
    @IBOutlet var congoMessageLabel: UILabel!
    @IBOutlet var internetConnectionLabel: UILabel!
    let coachMarksController = CoachMarksController()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationView.progressView.isHidden = true
        self.coachMarksController.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setText()
//        quoteCount = quotesArray.count
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        self.submitSurveyButton.roundbutton(self.submitSurveyButton, cornerradius: 5)
        
        
        self.internetConnectionLabel.text = "Make sure you have active internet connection and then submit the survey.".localized()
        self.congoMessageLabel.text = "Congratulations! \n You have completed the survey.".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.coachMarksController.overlay
        self.coachMarksController.start(in: PresentationContext.viewController(self))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.coachMarksController.stop(immediately: true)
    }
    
    @objc func setText() {
        self.navigationView.titleLabel.text = submitSurveyTitleKeyword.localized()
        self.submitSurveyButton.setTitle(submitButtonTitleKeyword.localized(), for: UIControlState())
    }
    
    @objc func rightButtonAction() {
        goToHomeScreen()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitButtonClicked(_ sender: UIButton) {
        AlertModel.sharedInstance.showAlert("MMR", message: "Survey Submitted Successfully", buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (buttonTitle, status) in
            self.goToHomeScreen()
        })
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        UserDefaults.standard.set(true, forKey: Welcome_Screen_Again_UD)
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 1
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {
        switch index {
        case 0:
            return coachMarksController.helper.makeCoachMark(for: self.submitSurveyButton)
        default:
            return coachMarksController.helper.makeCoachMark()
        }
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        var arrowView : CoachMarkArrowView?
        let coachMarkBodyView = TransparentCoachMarkBodyView()
        var coachMarkArrowView: TransparentCoachMarkArrowView? = nil
        coachViews.bodyView.hintLabel.text = Tour_Submit_Screen_Text.localized()
        coachViews.bodyView.nextLabel.text = nil
        if let arrowOrientation = coachMark.arrowOrientation {
            coachMarkArrowView = TransparentCoachMarkArrowView(orientation: arrowOrientation)
        }
        arrowView = coachMarkArrowView
        return (bodyView: coachViews.bodyView, arrowView: arrowView)
    }
}
