//
//  MMRForgotPasswordViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRForgotPasswordViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
    @IBOutlet weak var sendButton: UIButton!

  @IBOutlet var emailTextField: UITextField!
  @IBOutlet var forgotPasswordScreenTitleLabel: UILabel!
  @IBOutlet var sendButtonBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet var descMessageLabel: UILabel!
  var originalFrame = CGRect()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendButton.roundbutton(self.sendButton, cornerradius: 8)
      self.forgotPasswordScreenTitleLabel.text = "Forgot Password?".localized()
      let bounds = UIScreen.main.bounds
      let height = bounds.size.height
      
      if height == 480.0{
        self.sendButtonBottomSpaceConstraint.constant = 35.0
      }

      
      let h = emailTextField.frame.size.height
      let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: h, height: h))
      let image = UIImage(named: "newforgetPasswordIcon")
      imageView.image = image
        imageView.sizeToFit()
      emailTextField.leftView = imageView
      emailTextField.leftViewMode = UITextFieldViewMode.always
      self.descMessageLabel.text = "Enter your email address below and we’ll help you to recover your password.".localized()
      self.emailTextField.placeholder = "Email".localized()
      self.sendButton.setTitle("SEND".localized(), for: UIControlState())
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionsClicked(_ sender: UIButton) {
        switch sender .tag {
        case 0: self.navigationController?.popViewController(animated: true)
            break
        case 1: forgetPasswordButtonClicked ()
            break
        default:
            break
        }
    }
  
  func forgetPasswordButtonClicked (){
    
    if !self.emailTextField.text!.isValidEmail()
    {
      AlertModel.sharedInstance.showErrorMessage(invalidEmailMessageKeyword)
    } else if(self.emailTextField.text == "") {
      //check for textfield is nil
      AlertModel.sharedInstance.showErrorMessage(emptyEmailMessageKeyword)
    } else {
      // hit api for forgot pass
      self.forgetPasswordAPICall()
    }

  }
  
  func forgetPasswordAPICall() {
    
    let email = emailTextField.text
  
    let dict: [String:AnyObject] = [MMRUserEmail : email! as AnyObject]
    
    startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
    let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      let success = APIManager().serverForgotPassword(dict)
      DispatchQueue.main.async(execute: {
        self.stopActivityAnimating()
        if success{
          
        }
      })
      
      
    }
    
    
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
      textField.resignFirstResponder()
    return true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
      UIView.animate(withDuration: 0.25, animations: { 
        self.originalFrame = self.view.frame
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        switch height {
        case 480.0:
          print("iPhone 3,4")
          self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 70, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
        case 568.0:
          print("iPhone 5")
          self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 50, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
        case 667.0:
          print("iPhone 6")
        case 736.0:
          print("iPhone 6+")
          
        default:
          print("not an iPhone")
          
        }
        
      }) 
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
      UIView.animate(withDuration: 0.25, animations: { 
        self.view.frame = self.originalFrame
    }) 
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
