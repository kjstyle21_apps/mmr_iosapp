//
//  HelperScreenThree.swift
//  MMR
//
//  Created by Mindbowser on 10/17/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class HelperScreenThree: UIViewController {
    @IBOutlet weak var mmrLogoTopConstraint: NSLayoutConstraint!

  @IBOutlet var helpLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.helpLabel.text = "At the end, press SUBMIT button, and that’s it!".localized()
  }
  
    override func viewDidLayoutSubviews() {
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        switch height {
        case 480.0:
            print("iPhone 3,4")
            mmrLogoTopConstraint.constant = 60

        default:
            print("not an iPhone")
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
