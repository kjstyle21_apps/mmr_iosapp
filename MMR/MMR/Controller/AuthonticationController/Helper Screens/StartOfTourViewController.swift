//
//  StartOfTourViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class StartOfTourViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource,UIScrollViewDelegate {

    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!

    var isFromHome = false
  
    var pageTitles = [String]()
    var pageImages = [String]()
    
    var pageViewController = UIPageViewController()
    
    var nextPageCount : NSInteger = 0
    var currentPageCount : NSInteger = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
      pageTitles = ["GetStartedContentViewController","HelperScreenTwo","HelperScreenThree", "MMRLoginViewController"]
      // Create page view controller
      self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "GetStartedPageViewController") as! UIPageViewController
      self.pageViewController.dataSource = self
      self.pageViewController.delegate   = self
      
      // Change the size of page view controller
      self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height);
      
      currentPageCount = -1
      nextPageCount = 0
      var startingViewController = UIViewController()
      if isFromHome == false{
        startingViewController = self.viewControllerAtIndex(0)!
      }
      else{
        skipButton.isHidden = true
        startingViewController = self.viewControllerAtIndex(3)!
        pageControl.currentPage = 3
      }
      let viewControllers = [startingViewController];
      self.pageViewController.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
      
      self.addChildViewController(pageViewController)
      self.view.addSubview(pageViewController.view)
      
      self.pageViewController.didMove(toParentViewController: self)
      
      self.view.bringSubview(toFront: self.skipButton)
      self.view.bringSubview(toFront: self.pageControl)
       
        // Do any additional setup after loading the view.
    }

  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0);
  }
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        modifypageControl()
//    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        modifypageControl()
//        let currentPageIndex = pageViewController.viewControllers!.first!.view.tag
//        if currentPageIndex != 3 {
//            modifypageControl()
//        } else {
//            pageControl.currentPageIndicatorTintColor = UIColor.init(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
//            pageControl.pageIndicatorTintColor = UIColor.init(red:74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 0.4)
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      self.skipButton.setTitle("Skip".localized(), for: UIControlState())

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipButtonClicked(_ sender: UIButton) {
      skipButton.isHidden = true
      let startingViewController = self.viewControllerAtIndex(3)!
      pageControl.currentPage = 3
      let viewControllers = [startingViewController];
      
      self.pageViewController.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        modifypageControl()
    }
    
    // MARK: - PageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let identifier = viewController.restorationIdentifier
        var index = self.pageTitles.index(of: identifier!)!
        print("index back = \(index)")
        
        if index == 0 {
            currentPageCount = -1
            nextPageCount = index
        }
        
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        currentPageCount = index-1
        nextPageCount = index
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let identifier = viewController.restorationIdentifier
        var index = self.pageTitles.index(of: identifier!)!
        if index == NSNotFound {
            return nil;
        }
        index += 1
        if index == self.pageTitles.count {
            return nil;
        }
        return self.viewControllerAtIndex(index)
    }
    
    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        if self.pageTitles.count == 0 || index >= self.pageTitles.count {
            return nil
        }
        //      self.pageControl.numberOfPages = index
      self.pageControl.numberOfPages = 4
        let identifier = self.pageTitles[index]
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        
        return viewController
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if nextPageCount != -1 {
            return nextPageCount
        }
        return 0
    }

  func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    if pageControl.currentPage != 3{
        skipButton.isHidden = false
    }
      if (!completed)
      {
        return
      }
      let currentPageIndex = pageViewController.viewControllers!.first!.view.tag
    pageControl.currentPage = currentPageIndex
    if currentPageIndex == 3{
      skipButton.isHidden = true
        pageControl.currentPageIndicatorTintColor = UIColor.init(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        pageControl.pageIndicatorTintColor = UIColor.init(red:74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 0.4)
    }
    else{
       
       self.modifypageControl()
      skipButton.isHidden = false
    }
  }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        let vc = pendingViewControllers[0]
        if vc .isKind(of: MMRLoginViewController.self) {
            if pageControl.currentPage == 2{
                skipButton.isHidden = true
            }
            else{
                skipButton.isHidden = false
            }
        }
    }
    
    func modifypageControl(){
        for i in 0 ..< 4 {
            let dot : UIView = pageControl.subviews[i]
            
            if (i == pageControl.currentPage) {
                dot.backgroundColor = UIColor.white
                dot.layer.cornerRadius = dot.frame.size.height / 2;
                
            } else {
                dot.backgroundColor = UIColor.clear
                dot.layer.cornerRadius = dot.frame.size.height / 2;
                dot.layer.borderColor = UIColor.white.cgColor;
                dot.layer.borderWidth = 1;
            }
        }
        
        let currentPageIndex = pageViewController.viewControllers!.first!.view.tag
        if currentPageIndex == 3 {
            pageControl.currentPageIndicatorTintColor = UIColor.init(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            pageControl.pageIndicatorTintColor = UIColor.init(red:74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 0.4)
        }
    }
}
