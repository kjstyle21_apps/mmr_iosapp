//
//  MMRLoginViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/12/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

class MMRLoginViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var isPasswordTextSecure = false
    @IBOutlet weak var scanQRButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bottonConstraintOfScanCode: NSLayoutConstraint!
    var originalFrame = CGRect()
    @IBOutlet var topConstraintOfLogo: NSLayoutConstraint!
    @IBOutlet var forgetPasswordTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var orLabelUpperSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var orLabelLowerSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var showPasswordButton: UIButton!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet var orLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        self.loginButton.roundbutton(self.loginButton, cornerradius: 5)
        self.scanQRButton.roundbutton(self.scanQRButton, cornerradius: 5)
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        switch height {
        case 480.0:
            print("iPhone 3,4")
            self.bottonConstraintOfScanCode.constant = 35.0
            self.topConstraintOfLogo.constant = self.topConstraintOfLogo.constant - 40.0
            self.orLabelLowerSpaceConstraint.constant = 2
            self.orLabelUpperSpaceConstraint.constant = 2
            self.forgetPasswordTopSpaceConstraint.constant = -3
            
        case 568.0:
            print("iPhone 5")
            self.orLabelLowerSpaceConstraint.constant = 5
            self.orLabelUpperSpaceConstraint.constant = 5
            self.forgetPasswordTopSpaceConstraint.constant = 0
            
        case 667.0:
            print("iPhone 6")
            self.orLabelLowerSpaceConstraint.constant = 20
            self.orLabelUpperSpaceConstraint.constant = 20
            self.forgetPasswordTopSpaceConstraint.constant = 8
            
        case 736.0:
            print("iPhone 6+")
            self.orLabelLowerSpaceConstraint.constant = 30
            self.orLabelUpperSpaceConstraint.constant = 30
            self.forgetPasswordTopSpaceConstraint.constant = 16
        default:
            print("not an iPhone")
            
        }
        self.setText()
        // Do any additional setup after loading the view.
    }
    
    func setText() {
        self.loginButton.setTitle("LOGIN".localized(), for: UIControlState())
        self.scanQRButton.setTitle("SCAN QR CODE".localized(), for: UIControlState())
        self.showPasswordButton.setTitle("Show Password".localized(), for: UIControlState())
        self.emailLabel.text = "EMAIL".localized()
        self.passwordLabel.text = "PASSWORD".localized()
        self.passwordTextField.placeholder = "password".localized()
        self.forgotPasswordButton.setTitle("Forgot Password".localized(), for: UIControlState())
        self.orLabel.text = "Or".localized()
        self.showPasswordButton.setTitle(loginVCshowPasswordButtonTitleKeyword.localized(), for: UIControlState())
    }
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //      NSUserDefaults.standardUserDefaults().setBool(true, forKey: "MMRServiceLogout")
        //      NSUserDefaults.standardUserDefaults().synchronize()
        
        if let temp = UserDefaults.standard.bool(forKey: MMRServiceLogout_UD) as? Bool {
            if !temp {
                if appDelegate.isanAuthUser {
                    appDelegate.isanAuthUser = false
                    AlertModel.sharedInstance.showMessage("MMR", message: "Seems you have logged in from another device.".localized())
                }
            }
            else {
                UserDefaults.standard.removeObject(forKey: MMRServiceLogout_UD)
            }
        }
        else {
            if appDelegate.isanAuthUser {
                appDelegate.isanAuthUser = false
                AlertModel.sharedInstance.showMessage("MMR", message: "Seems you have logged in from another device.".localized())
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Make specific layout for small devices.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionsClicked(_ sender: UIButton) {
        switch sender .tag {
        case 0: self.showPassword()
            break
            
        case 1: self.goToForgotPassword()
            break
            
        case 2: self.normalLogin()
            break
            
        case 3: self.loginWithQRCode()
            break
            
        default:
            break
        }
    }
    
    func loginWithQRCode() {
        self.performSegue(withIdentifier: "MMRQRCodeScanViewController", sender: self)
    }
    
    func normalLogin() {
        self.view.endEditing(true)
        let email = self.emailTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = self.passwordTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if email == "" && password == "" {
            AlertModel.sharedInstance.showErrorMessage(allFieldsMandatoryMessageKeyword.localized())
        }
        else if email == ""{
            AlertModel.sharedInstance.showErrorMessage(emptyEmailMessageKeyword.localized())
        }
        else if password == ""
        {
            AlertModel.sharedInstance.showErrorMessage(emptyPasswordMessageKeyword.localized())
        }
        else if password?.characters.count < 6 {
            AlertModel.sharedInstance.showErrorMessage("Invalid Password".localized())
        }
        else if !self.emailTextField.text!.isValidEmail()
        {
            AlertModel.sharedInstance.showErrorMessage(invalidEmailMessageKeyword.localized())
        }
        else if !self.emailTextField.text!.isValidEmailFormat()
        {
            AlertModel.sharedInstance.showErrorMessage(invalidEmailMessageKeyword.localized())
        }
        else {
            // Hit api for login and take to home screen
            self.normalLoginAPICall()
        }
    }
    
    func goToForgotPassword() {
        self .performSegue(withIdentifier: "MMRForgotPasswordViewController", sender: self)
    }
    
    func showPassword(){
        if !isPasswordTextSecure {
            self.passwordTextField.isSecureTextEntry  = false
            isPasswordTextSecure                    = true
            self.showPasswordButton.setTitle(loginVChidePasswordButtonTitleKeyword.localized(), for: UIControlState())
        } else {
            self.passwordTextField.isSecureTextEntry  = true
            isPasswordTextSecure                    = false
            self.showPasswordButton.setTitle(loginVCshowPasswordButtonTitleKeyword.localized(), for: UIControlState())
        }
    }
    
    @IBAction func showPasswordAction(_ sender: AnyObject) {
        self.showPassword()
    }
    
    //MARK:- TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField == passwordTextField  {
            if let text = textField.text {
                let newLength = text.utf8.count + string.utf8.count - range.length
                return newLength > kMaximumPasswordCharacters ? false : true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.passwordTextField.becomeFirstResponder()
        if textField == self.passwordTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func normalLoginAPICall() {
        let email = emailTextField.text
        let password = passwordTextField.text
        
        var deviceToken = UserDefaults.standard.object(forKey: MMRServiceRegisterRequestParameterDeviceToken)
        UserDefaults.standard.setValue(email, forKey: emailId)
        if deviceToken == nil{
            deviceToken = ""
        }
        
        let md5Pass = password?.md5(string: password!)
        print("md5 Password : \(String(describing: md5Pass))")
        let dict: [String:AnyObject] = [MMRUserEmail : email! as AnyObject,
                                        MMRUserPassword  : md5Pass! as AnyObject,
                                        MMRDeviceType : MMRDeviceTypeiOS as AnyObject,
                                        MMRDeviceToken : deviceToken as! String as AnyObject]
        
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().serverLoginUser(dict)
            DispatchQueue.main.async(execute: {
                self.removeActivityIndicator()
                
                if (success != nil) {
                    var userInfo = [String:AnyObject]()
                    userInfo = (success?.value(forKey: "object"))! as! [String : AnyObject]
                    var sameUserFlag = false
                    var similarUserID = String()
                    if let userID = userInfo[userId] {
                        if let userIDPre = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD) {
                            if "\(userIDPre)" == "\(userID)" {
                                sameUserFlag = true
                                similarUserID = "\(userID)"
                            }else {
                                sameUserFlag = false
                            }
                        }
                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        //                    if let userID = userInfo[userId]
                        //                    {
                        //                      NSUserDefaults.standardUserDefaults().setValue(userID, forKey: "CURRENT_USER_ID")
                        //                      if let tempserial = NSUserDefaults.standardUserDefaults().valueForKey("\(userID)") as? NSInteger {
                        //                        NSUserDefaults.standardUserDefaults().setInteger(tempserial, forKey: serialNoID)
                        //                      }
                        //                    }
                        
                        //                    NSUserDefaults.standardUserDefaults().setValue(userID, forKey: "CURRENT_USER_ID")
                        UserDefaults.standard.setValue(sameUserFlag, forKey: SAME_USER_FLAG_STATUS_UD)
                    }
                    //                    let noSurverAssigned = userInfo[assignedSurvey]![noSurveyMessage] as? String
                    //                    if noSurverAssigned?.characters.count > 0 {
                    
                    //                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: NoSurveyForUser)
                    //                        NSUserDefaults.standardUserDefaults().synchronize()
                    //                        self.emailTextField.text = ""
                    //                        self.passwordTextField.text = ""
                    //                      if sameUserFlag {
                    //                        QuestionSharedClass.sharedInstance.deleteImagesAndVideos(similarUserID)
                    //                        QuestionSharedClass.sharedInstance.deleteQuestionDataOfUploadedSurvey(similarUserID)
                    //                        QuestionDropRecordSharedClass.sharedInstance.deleteQuestionDropDataOfUploadedSurvey(similarUserID)
                    //                        AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(similarUserID)
                    //                      }
                    //                        self.GoToAppsHomeScreen()
                    //                    }
                    //                    else  {
                    if let userID = userInfo[userId] {
                        UserDefaults.standard.setValue(userID, forKey: CURRENT_USER_ID_UD)
                        if let tempserial = UserDefaults.standard.value(forKey: "\(userID)") as? NSInteger {
                            UserDefaults.standard.set(tempserial, forKey: serialNoID)
                        }
                    }
                    
                    if let userRole = userInfo[MMRUserRole] as? String {
                        UserDefaults.standard.setValue(userRole, forKey: CURRENT_USER_ROLE_UD)
                    } else {
                        UserDefaults.standard.setValue("USER", forKey: CURRENT_USER_ROLE_UD)
                    }
                    UserDefaults.standard.synchronize()
                    
                    //                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: NoSurveyForUser)
                    //                    if let surveyID = userInfo [assignedSurvey]! [surveyId]  {
                    //                        NSUserDefaults.standardUserDefaults().setValue(surveyID, forKey: surveyId)
                    //                    }
                    
                    //                    if let nameOfSurvey = userInfo [assignedSurvey]! [surveyName]  {
                    //                        NSUserDefaults.standardUserDefaults().setValue(nameOfSurvey, forKey: surveyName)
                    //                    }
                    
                    //                    if let surveyInstructions = userInfo[assignedSurvey]![surveyInstruction] {
                    //                        NSUserDefaults.standardUserDefaults().setValue(surveyInstructions, forKey: surveyInstruction)
                    //                    }
                    
                    //                    if let surveyDescription = userInfo[assignedSurvey]![surveyBriefDescription] {
                    //                        NSUserDefaults.standardUserDefaults().setValue(surveyDescription, forKey: surveyBriefDescription)
                    //                    }
                    //                    if let endTimeOfSurvey = userInfo [assignedSurvey]! [surveyEndTime]  {
                    //                        NSUserDefaults.standardUserDefaults().setValue(endTimeOfSurvey, forKey: surveyEndTime)
                    //                    }
                    //                    if let surveyIntroQuestion = userInfo [assignedSurvey]! [surveyIntroductoryQuestion]  {
                    //                        NSUserDefaults.standardUserDefaults().setValue(surveyIntroQuestion, forKey: surveyIntroductoryQuestion)
                    //                      }
                    
                    //                    NSUserDefaults.standardUserDefaults().synchronize()
                    //                    if let questionList = userInfo[assignedSurvey]![questions] as? [AnyObject] {
                    //
                    //                        for questionInfo in questionList {
                    //
                    //                            if let info = questionInfo as? [String:AnyObject] {
                    //                                QuestionSharedClass.sharedInstance.parseDictionary(info)
                    //                            }
                    //                        }
                    //                    }
                    self.emailTextField.text = ""
                    self.passwordTextField.text = ""
                    self.GoToAppsHomeScreen()
                    //                }
                    //                    let mmrBLEControl = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewControllerWithIdentifier("MMRBLEViewController")
                    //                  self.navigationController?.presentViewController(mmrBLEControl, animated: true, completion: nil)
                }
            })
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
            self.originalFrame = self.view.frame
            if textField == self.passwordTextField{
                let bounds = UIScreen.main.bounds
                let height = bounds.size.height
                
                switch height {
                case 480.0:
                    print("iPhone 3,4")
                    self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 70, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
                case 568.0:
                    print("iPhone 5")
                    self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 50, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
                case 667.0:
                    print("iPhone 6")
                case 736.0:
                    print("iPhone 6+")
                default:
                    print("not an iPhone")
                }
            }
        }) 
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.passwordTextField{
            UIView.animate(withDuration: 0.25, animations: {
                self.view.frame = self.originalFrame
            }) 
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func removeActivityIndicator() {
        stopActivityAnimating()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func GoToAppsHomeScreen(){
        let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "SSASideMenu") as! SSASideMenu
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
}
