//
//  MMRQRCodeScanViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/18/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import AVFoundation

class MMRQRCodeScanViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate,NVActivityIndicatorViewable {
  
  @IBOutlet weak var messageLabel:UILabel!
  @IBOutlet var navigationBackButton: UIButton!
  @IBOutlet var navigationTitleLabel: UILabel!
  @IBOutlet var navigationBarView: UIView!
  
    @IBOutlet weak var navigationView: Navigation_View!
  
  
  var captureSession:AVCaptureSession?
  var videoPreviewLayer:AVCaptureVideoPreviewLayer?
  var qrCodeFrameView:UIView?
  
  // Added to support different barcodes
//  let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
  
    let supportedBarCodes = [AVMetadataObject.ObjectType.qr]
    var scanStatus = false

  override func viewDidLoad() {
    super.viewDidLoad()
    self.messageLabel.text = "Focus the camera on QR code".localized()
    self.navigationSetup()
    if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
    {
        // Already Authorized
       self.initializationOfQRCodeAndAddingViewsOnOverlay()
    }
    else {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
          let alertController = UIAlertController(title: MMRProjectName, message: "Kindly provide camera access to scan the QR code.".localized(), preferredStyle: .alert)
          
          let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
          }
          alertController.addAction(cancelAction)
          
          let OKAction = UIAlertAction(title: "Settings".localized(), style: .default) { (action:UIAlertAction!) in
            
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
              UIApplication.shared.openURL(url)
            }
          }
          alertController.addAction(OKAction)
          
          self.present(alertController, animated: true, completion:nil)

          break
        case .authorized: break
        case .restricted: break
          
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                    // User granted
                     DispatchQueue.main.async {
                    self.initializationOfQRCodeAndAddingViewsOnOverlay()
                    }
                } else {
                    print("Denied access to \(cameraMediaType)")
                    let alertController = UIAlertController(title: MMRProjectName, message: "Kindly provide camera access to scan the QR code.".localized(), preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action:UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(cancelAction)
                    
                    let OKAction = UIAlertAction(title: "Settings".localized(), style: .default) { (action:UIAlertAction!) in
                        
                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
        }
        
//        dispatch_async(dispatch_get_main_queue()) {
//            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
//                if granted == true
//                {
//                    // User granted
//                    self.initializationOfQRCodeAndAddingViewsOnOverlay()
//                }
//                else
//                {
                    // User Rejected
//                    let alertController = UIAlertController(title: MMRProjectName, message: "Kindly provide camera access to scan the QR code.", preferredStyle: .Alert)
//                    
//                    let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
//                        self.navigationController?.popViewControllerAnimated(true)
//                    }
//                    alertController.addAction(cancelAction)
//                    
//                    let OKAction = UIAlertAction(title: "Settings", style: .Default) { (action:UIAlertAction!) in
//                        
//                        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                        if let url = settingsUrl {
//                            UIApplication.sharedApplication().openURL(url)
//                        }
//                        
//                    }
//                    alertController.addAction(OKAction)
//                    
//                    self.presentViewController(alertController, animated: true, completion:nil)
//                }
//            });
//        }
    
    }
  }
    
    func navigationSetup () {
        navigationView.titleLabel.text = ScanQRCode.localized()
        navigationView.leftButton! .setImage(UIImage (named: "backButton"), for: UIControlState())
        navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        navigationView.rightButton.setImage(UIImage(named: "flashIcon"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        navigationView.progressView.isHidden = true
    }
    
    @objc func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func rightButtonAction() {
        
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
   
    
    func initializationOfQRCodeAndAddingViewsOnOverlay()  {
   
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            
            
            
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            //      videoPreviewLayer?.frame = self.overlayLayerView.layer.frame
            
            //      self.overlayLayerView.layer.addSublayer(videoPreviewLayer!)
            
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            // Move the message label to the top view
            view.bringSubview(toFront: messageLabel)
            view.bringSubview(toFront: self.navigationView)
            self.messageLabel.backgroundColor = UIColor.white
            
            
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.init(red: 84.0/255.0, green: 244.0/255.0, blue: 11.0/255.0, alpha: 1).cgColor
                qrCodeFrameView.layer.borderWidth = 5
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }

    }
   override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//    self.toggleFlash()
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if metadataObjects == nil || metadataObjects.count == 0 {
      qrCodeFrameView?.frame = CGRect.zero
      messageLabel.text = noBarcodeQRcodeDetectedKeyword
      return
    }
    if let qrCodeFrameView = qrCodeFrameView {
        qrCodeFrameView.layer.borderColor = UIColor.init(red: 84.0/255.0, green: 244.0/255.0, blue: 11.0/255.0, alpha: 1).cgColor
    }
    // Get the metadata object.
    let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
    
    // Here we use filter method to check if the type of metadataObj is supported
    // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
    // can be found in the array of supported bar codes.
    if supportedBarCodes.contains(metadataObj.type) {
      //        if metadataObj.type == AVMetadataObjectTypeQRCode {
      // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
      let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
      qrCodeFrameView?.frame = barCodeObject!.bounds
        
      if metadataObj.stringValue != nil {
        self.captureSession?.stopRunning()

        //messageLabel.text = metadataObj.stringValue
        print("Result:\(metadataObj.stringValue)")

        var deviceToken = UserDefaults.standard.object(forKey: MMRServiceRegisterRequestParameterDeviceToken)
        
        if deviceToken == nil{
            deviceToken = ""
        }
        
        
        let dict: [String:AnyObject] = [MMRQRCode      : metadataObj.stringValue as AnyObject,
                                        MMRDeviceType  : MMRDeviceTypeiOS as AnyObject,
                                        MMRDeviceToken : deviceToken as! String as AnyObject]
        
        print("Dict value:\(dict)")
        
        if !scanStatus {
          scanStatus = true
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().serverLoginUser(dict)
            DispatchQueue.main.async(execute: {
                self.removeActivityIndicator()
                
                if (success != nil){
                    var userInfo = [String:AnyObject]()
                    userInfo = (success?.value(forKey: "object"))! as! [String : AnyObject]
                  var sameUserFlag = false
                  var similarUserID = String()
                  if let userID = userInfo[userId]
                  {
                    if let userIDPre = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD) {
                      if "\(userIDPre)" == "\(userID)" {
                        sameUserFlag = true
                        similarUserID = "\(userID)"
                      }
                      else {
                        sameUserFlag = false
                      }
                    }
                    UserDefaults.standard.setValue(userID, forKey: CURRENT_USER_ID_UD)
                    UserDefaults.standard.setValue(sameUserFlag, forKey: SAME_USER_FLAG_STATUS_UD)
                  }
                    self.captureSession?.startRunning()
                      if let qrCodeFrameView = self.qrCodeFrameView {
                            qrCodeFrameView.layer.borderColor = UIColor.clear.cgColor
                        }
                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)

                  
                        if let userID = userInfo[userId] {
                          UserDefaults.standard.setValue(userID, forKey: CURRENT_USER_ID_UD)
                          if let tempserial = UserDefaults.standard.value(forKey: "\(userID)") as? NSInteger {
                            UserDefaults.standard.set(tempserial, forKey: serialNoID)
                          }
                        }
                  
                  if let userRole = userInfo[MMRUserRole] as? String {
                    UserDefaults.standard.setValue(userRole, forKey: CURRENT_USER_ROLE_UD)
                  }
                  else {
                    UserDefaults.standard.setValue("USER", forKey: CURRENT_USER_ROLE_UD)
                  }
                    UserDefaults.standard.synchronize()

                    self.GoToAppsHomeScreen()
                  
                } else {
                    self.captureSession?.startRunning()
                    if let qrCodeFrameView = self.qrCodeFrameView {
                        qrCodeFrameView.layer.borderColor = UIColor.clear.cgColor
                    }
                }
            })
        }
      }
      }
    }
  }
  
    func GoToAppsHomeScreen() {
        let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "SSASideMenu") as! SSASideMenu
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
    func removeActivityIndicator() {
        stopActivityAnimating()
    }
}
