//
//  BaseClassForQuestions.swift
//  MMR
//
//  Created by Mindbowser on 11/10/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

enum QuestionType : String {
     case Radio             = "oneOption"
     case Slider            = "slider"
     case CheckBox          = "multipleChoice"
     case Textbox           = "textBox"
     case TrueFalse         = "trueFalse"
     case MultipleTextBox   = "multipleTextBox"
     case Bullseye          = "bullseye"
     case DatePicker        = "datePicker"
     case TimePicker        = "timePicker"
     case DropDown          = "dropDown"
     case Picture           = "pictureChoice"
     case Video             = "videoChoice"
     case DateTimePicker    = "dateTimePicker"
     case Cards    = "cards"
      case Thermometer = "thermometer"
      case Info = "info"
      case Interval = "interval"
    case TourQuestion = "TourQuestion"
}

protocol BaseClassMethodToFetchDataFromDB : class {
    
    func fetchNextQuestionType()
    func fetchNextQuestionTypeWithSerialID(_ serialID : Int)
    func removeChildFromParticularController(_ child:UIViewController)
    func updateInTimeForQuestion(_ inTime : String, serialNo : String)
    func updateOutTimeForQuestion(_ outTime : String, serialNo : String)
    func updateAnswerTextForQuestion(_ answerText : String, serialNo : String)
    func updateOptionTextIDSeqForQuestion(_ answerText : String, serialNo : String)
    func goToHomeViewController()
    func fetchTourSubmit()
}

class BaseClassForQuestions: UIViewController, BaseClassMethodToFetchDataFromDB {
    static var serialNo_ID = 0
    var typeOfCurrentQuestion : QuestionType?
    var questionAndOptionsDict = NSMutableDictionary ()
    var currentChildViewController = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Fetch the question as per saved SerialNo in user Defaults
        let serilID = UserDefaults.standard.value(forKey: serialNoID)
        
        if serilID != nil {
            BaseClassForQuestions.serialNo_ID = serilID as! Int
        }
        else
        {
            BaseClassForQuestions.serialNo_ID = 1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            UserDefaults.standard.synchronize()
        }
            self.fetchTheTypeOfQuestionAndProceed()
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
    func fetchTheTypeOfQuestionAndProceed() {
      let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        if self.typeOfCurrentQuestion == QuestionType.TourQuestion {
            if typeOfCurrentQuestion != nil {
                addChildControllerAsPerTypeOfQuestion()
            }
        } else {
        if (UserDefaults.standard.value(forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)") != nil) {
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
          typeOfCurrentQuestion = QuestionType(rawValue: QuestionSharedClass.sharedInstance.fetchQuestionType(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) )
          
          // Checking if there are no more question in DB then move to Submit Survey screen
          if typeOfCurrentQuestion != nil {
            addChildControllerAsPerTypeOfQuestion()
          }
          else {
            let storyboard : UIStoryboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
            let submitAction = storyboard.instantiateViewController(withIdentifier: "MMRSubmitSurvey") as! MMRSubmitSurvey
//            addChildViewController(submitAction)
            // always keep animated false to avoid the black vertical strip 
            self.navigationController?.pushViewController(submitAction, animated: false)
          }
//        }
    }
      else {
        // Fetching Type of question
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
        typeOfCurrentQuestion = QuestionType(rawValue: QuestionSharedClass.sharedInstance.fetchQuestionType(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) )
        
        // Checking if there are no more question in DB then move to Submit Survey screen
        if typeOfCurrentQuestion != nil {
            addChildControllerAsPerTypeOfQuestion()
        }
        else {
            let storyboard : UIStoryboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
            let submitAction = storyboard.instantiateViewController(withIdentifier: "MMRSubmitSurvey") as! MMRSubmitSurvey
          // always keep animated false to avoid the black vertical strip
            self.navigationController?.pushViewController(submitAction, animated: false)
        }
    }
    }
    }
    
    func goToHomeViewController () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
    func fetchNextQuestionType()
    {
        removeChildFromParticularController(currentChildViewController)
        BaseClassForQuestions.serialNo_ID += 1
        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
        UserDefaults.standard.synchronize()
        self.fetchTheTypeOfQuestionAndProceed()

    }
  
  func fetchNextQuestionTypeWithSerialID(_ serialID : Int)
  {
    removeChildFromParticularController(currentChildViewController)
    BaseClassForQuestions.serialNo_ID = serialID
    UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
    UserDefaults.standard.synchronize()
    self.fetchTheTypeOfQuestionAndProceed()
    
  }
  
  func updateInTimeForQuestion(_ inTime: String, serialNo: String) {
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    QuestionSharedClass.sharedInstance.updateInTimeForQuestion(inTime, serialNo: serialNo, userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
  }
  
  func updateOutTimeForQuestion(_ outTime: String, serialNo: String) {
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    QuestionSharedClass.sharedInstance.updateOutTimeForQuestion(outTime, serialNo: serialNo, userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
  }
  
  func updateAnswerTextForQuestion(_ answerText: String, serialNo: String) {
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)")
    let surveyID = UserDefaults.standard.value(forKey: surveyId)
    QuestionSharedClass.sharedInstance.updateAnswerTextForQuestion(answerText, serialNo: serialNo,userID: userID! as AnyObject,uploadSeqNum: seqNum! as AnyObject,surveyID: surveyID! as AnyObject)
  }
    
    func updateOptionTextIDSeqForQuestion(_ answerText: String, serialNo: String) {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        QuestionSharedClass.sharedInstance.updateOptionTextIDSeqForQuestion(answerText, serialNo: serialNo,userID: userID! as AnyObject,uploadSeqNum: seqNum! as AnyObject,surveyID: surveyID! as AnyObject)
    }
  
    func addChildControllerAsPerTypeOfQuestion()  {
        if let typeOfCurrentQuestion = typeOfCurrentQuestion {
            let storyboard : UIStoryboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
            switch typeOfCurrentQuestion {
                
            case .Radio     :
                
                let radioController = storyboard.instantiateViewController(withIdentifier: "MMRRadioTypeQuestions") as! MMRRadioTypeQuestions
                radioController.delegate = self
                addChildViewController(radioController)
                
                print("Radio")
                break
                
            case .Slider    :
                let sliderController = storyboard.instantiateViewController(withIdentifier: "MMRSliderTypeQuestion") as! MMRSliderTypeQuestion
                sliderController.delegate = self
                addChildViewController(sliderController)
                print("Slider")
                
                break
                
            case .CheckBox  :
                let radioController = storyboard.instantiateViewController(withIdentifier: "MMRRadioTypeQuestions") as! MMRRadioTypeQuestions
                radioController.delegate = self
                addChildViewController(radioController)
                print("Checkbox")
                
                break
                
            case .Textbox   :
                print("Textbox")
                let textBoxController =  storyboard.instantiateViewController(withIdentifier: "MMRDiscriptiveTypeViewController") as! MMRDiscriptiveTypeViewController
                textBoxController.delegate = self
                addChildViewController(textBoxController)
                break
                
            case .TrueFalse :
                
                let radioController =
                    storyboard.instantiateViewController(withIdentifier: "MMRRadioTypeQuestions") as!MMRRadioTypeQuestions
//                let radioController = storyboard.instantiateViewControllerWithIdentifier("MMRThermometerTypeQuestionController") as! MMRThermometerTypeQuestionController
                radioController.delegate = self
                addChildViewController(radioController)
                
                print("TrueFalse")
                break
            
            case .DropDown :
                let dropDownController =
                    storyboard.instantiateViewController(withIdentifier: "MMRDropDownOrDatePickerController") as!MMRDropDownOrDatePickerController
                dropDownController.delegate = self
                addChildViewController(dropDownController)
                
                print("DropDown")
                break
                
            case .DatePicker :
                self.goToDateTimePickerController(storyboard)
                break
                
            case .TimePicker :
                self.goToDateTimePickerController(storyboard)
                break

            case .DateTimePicker :
                self.goToDateTimePickerController(storyboard)
                break
                
            case .Bullseye   :
                let bullsEyeController =
                    storyboard.instantiateViewController(withIdentifier: "BullseyeWithTableviewViewController") as!BullseyeWithTableviewViewController
                bullsEyeController.delegate = self
                addChildViewController(bullsEyeController)
                
                print("BullsEye")
                break
                
            case .MultipleTextBox :
                
                let multipleTextBoxController =
                    storyboard.instantiateViewController(withIdentifier: "MMRMultipleTextboxController") as!MMRMultipleTextboxController
                multipleTextBoxController.delegate = self
                addChildViewController(multipleTextBoxController)
                
                print("BullsEye")
                break
            case .Picture,.Video :
                let imageController = storyboard.instantiateViewController(withIdentifier: "MMRImageTypeQuestionViewController") as! MMRImageTypeQuestionViewController
                imageController.delegate = self
                addChildViewController(imageController)
            case .Cards   :
              let cardsController = storyboard.instantiateViewController(withIdentifier: "CardDragQuestionWithTableViewController") as! CardDragQuestionWithTableViewController
              cardsController.delegate = self
              addChildViewController(cardsController)
              break
              
            case .Thermometer :
            let radioController = storyboard.instantiateViewController(withIdentifier: "MMRThermometerTypeQuestionController") as! MMRThermometerTypeQuestionController
              radioController.delegate = self
              addChildViewController(radioController)
              print("Thermometer")
              break
              
            case .Info :
              let infoController = storyboard.instantiateViewController(withIdentifier: "MMRSimpleTextTypeQuestionController") as! MMRSimpleTextTypeQuestionController
              infoController.delegate = self
              addChildViewController(infoController)
              print("Info")
              break
              
            case .Interval :
              let infoController = storyboard.instantiateViewController(withIdentifier: "MMRSurveyPauseController") as! MMRSurveyPauseController
              infoController.delegate = self
              addChildViewController(infoController)
              print("Info")
              break
            case .TourQuestion :
                let tourQuestion = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TourQuestionViewController") as? TourQuestionViewController
                tourQuestion?.delegate = self
                addChildViewController(tourQuestion!)
            default         :
                print("default")
                break
            }
        }
        
    }
    
    func goToDateTimePickerController ( _ storyboard : UIStoryboard ) {
        let dropDownController =
            storyboard.instantiateViewController(withIdentifier: "MMRDatePickerViewController") as!MMRDatePickerViewController
        dropDownController.delegate = self
        addChildViewController(dropDownController)
        
        print("DatePicker/TimePicker")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func addChildViewController(_ childController: UIViewController) {
        self.currentChildViewController = childController
        childController.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view .addSubview(childController.view)
        childController.didMove(toParentViewController: self)
    }

  
    func removeChildFromParticularController(_ child: UIViewController)
    {
        child.didMove(toParentViewController: nil)
        child.view.removeFromSuperview()
        child.removeFromParentViewController()
    }
    
    func fetchTourSubmit() {
        self.typeOfCurrentQuestion = nil
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let submitAction = storyboard.instantiateViewController(withIdentifier: "TourSubmitViewController") as! TourSubmitViewController
        self.navigationController?.pushViewController(submitAction, animated: false)
    }
}
