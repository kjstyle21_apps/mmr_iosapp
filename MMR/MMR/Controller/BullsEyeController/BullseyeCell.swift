//
//  BullseyeCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 10/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class BullseyeCell: UITableViewCell {
    
  @IBOutlet var layerView0: UIView!
  @IBOutlet var layerView1: UIView!
  @IBOutlet var layerView2: UIView!
  @IBOutlet var layerView3: UIView!
  @IBOutlet var layerView4: UIView!
  @IBOutlet var layerView5: UIView!
  @IBOutlet var layerView6: UIView!
  @IBOutlet var layerView7: UIView!
  @IBOutlet var layerView8: UIView!
  @IBOutlet var layerView9: UIView!
  @IBOutlet var layerView10: UIView!
  @IBOutlet var targetAreaView: UIView!
  @IBOutlet var targetLocation: UIView!
  
  @IBOutlet var backLayerView0: UIView!
  @IBOutlet var backLayerView1: UIView!
  @IBOutlet var backLayerView2: UIView!
  
  @IBOutlet var targetLocationCenterXConstraint: NSLayoutConstraint!
  
  @IBOutlet var targetLocationCenterYConstraint: NSLayoutConstraint!
  
  var originalFrame : CGRect!
  var originalYConstantofTargetLocation : CGFloat!
//  var newTempTextLabel = UILabel()
  @IBOutlet var tempLabel: UILabel!
    @IBOutlet var temporaryLabel: UILabel!
    @IBOutlet var temporaryLabel3: UILabel!
    @IBOutlet var temporaryLabel2: UILabel!
  @IBOutlet var nextButton: UIButton!
  var count = 1
//  var dataDict = [String : String]()
  var dataDict = [MMRAnsModel]()
  var totalCount = Int()
  
  var questionArray = [String]()
  var questionOptionArr = [String]()
  var questionOptionSeqArr = [String]()

  @IBOutlet var heightConstraintofBackLayerView0: NSLayoutConstraint!
  
  @IBOutlet var widthConstraintofBackLayerView0: NSLayoutConstraint!
  
    override func awakeFromNib() {
        super.awakeFromNib()
             // Initialization code
        nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
        nextButton.roundbutton(self.nextButton, cornerradius: 5)
    }
  
  //Method for each layer of bullseye view

  func setUpView(_ layerView : UIView, bg1R : CGFloat, bg1G : CGFloat, bg1B : CGFloat, bg2R : CGFloat, bg2G : CGFloat, bg2B : CGFloat, borderR : CGFloat, borderG : CGFloat, borderB : CGFloat) {
    
    let borderWidth = CGFloat(0.3)
    
    layerView.layer.cornerRadius = layerView.frame.size.width/2 + 5
    layerView.clipsToBounds = true
    layerView.frame = layerView.frame.insetBy(dx: -borderWidth, dy: -borderWidth)
    layerView.layer.borderColor = UIColor.init(red: borderR/255.0, green: borderG/255.0, blue: borderB/255.0, alpha: 1).cgColor
    layerView.layer.borderWidth = borderWidth
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = layerView.bounds
//    if #available(iOS 10.0, *) {
//        let color11 = UIColor(displayP3Red: bg1R / 255.0, green: bg1G / 255.0, blue: bg1B / 255.0, alpha: 1).cgColor
//        let color22 = UIColor(displayP3Red: bg2R / 255.0, green: bg2G / 255.0, blue: bg2B / 255.0, alpha: 1).cgColor
//    } else {
//        // Fallback on earlier versions
//    }
    let color1 = UIColor(red: bg1R/255.0, green: bg1G/255.0, blue: bg1B/255.0, alpha: 1).cgColor
    let color2 = UIColor(red: bg2R/255.0, green: bg2G/255.0, blue: bg2B/255.0, alpha: 1).cgColor
    gradientLayer.colors = [color1, color2]
//    gradientLayer.locations = [0.0, 1.0]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0)
    gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.2)
    layerView.layer.addSublayer(gradientLayer)
  }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

  override func layoutSubviews() {
    super.layoutSubviews()
    originalFrame = backLayerView0.frame
//    print("original frame : \(originalFrame)")
    originalYConstantofTargetLocation = self.targetLocationCenterYConstraint.constant
    
    setUpBullsEye()
    print(questionArray.count)
  }
  
  //Method for setup of BullsEye View with gradient pattern
  
  func setUpBullsEye() {
    
    //code for setup of back layer views of cards
    
    targetLocation.layer.cornerRadius = targetLocation.frame.size.width/2
    targetLocation.clipsToBounds = true
    
    nextButton.layer.cornerRadius = 5.0
    
    backLayerView1.layer.cornerRadius = 10.0
    backLayerView1.frame = backLayerView1.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView1.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView1.layer.borderWidth = 0.5
    
    backLayerView0.layer.cornerRadius = 10.0
    backLayerView0.frame = backLayerView0.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView0.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView0.layer.borderWidth = 0.5
    
    backLayerView2.layer.cornerRadius = 10.0
    backLayerView2.frame = backLayerView2.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView2.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView2.layer.borderWidth = 0.5
    
    //code for setup of bullseye view
    self.setUpView(layerView10, bg1R: 225.0, bg1G: 225.0, bg1B: 226.0, bg2R: 140.0, bg2G: 142.0, bg2B: 145.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView9, bg1R: 225.0, bg1G: 225.0, bg1B: 226.0, bg2R: 140.0, bg2G: 142.0, bg2B: 145.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView8, bg1R: 62.0, bg1G: 62.0, bg1B: 64.0, bg2R: 26.0, bg2G: 24.0, bg2B: 24.0, borderR: 225.0, borderG: 226.0, borderB: 228.0)
    
    self.setUpView(layerView7, bg1R: 62.0, bg1G: 62.0, bg1B: 64.0, bg2R: 26.0, bg2G: 24.0, bg2B: 24.0, borderR: 225.0, borderG: 226.0, borderB: 228.0)
    
    self.setUpView(layerView6, bg1R: 88.0, bg1G: 165.0, bg1B: 220.0, bg2R: 24.0, bg2G: 118.0, bg2B: 170.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView5, bg1R: 88.0, bg1G: 165.0, bg1B: 220.0, bg2R: 24.0, bg2G: 118.0, bg2B: 170.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView4, bg1R: 212.0, bg1G: 30.0, bg1B: 35.0, bg2R: 166.0, bg2G: 25.0, bg2B: 36.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView3, bg1R: 212.0, bg1G: 30.0, bg1B: 35.0, bg2R: 166.0, bg2G: 25.0, bg2B: 36.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView2, bg1R: 245.0, bg1G: 166.0, bg1B: 22.0, bg2R: 214.0, bg2G: 134.0, bg2B: 12.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView1, bg1R: 245.0, bg1G: 166.0, bg1B: 22.0, bg2R: 214.0, bg2G: 134.0, bg2B: 12.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
    
    self.setUpView(layerView0, bg1R: 245.0, bg1G: 166.0, bg1B: 22.0, bg2R: 214.0, bg2G: 134.0, bg2B: 12.0, borderR: 56.0, borderG: 56.0, borderB: 58.0)
  }
  
  //Method for setup of initial question card
  func setUpInitialQuestionLabel() {
    
    tempLabel.text = questionArray[0]
    tempLabel.numberOfLines = 0

    backLayerView0.layer.shadowColor = UIColor.black.cgColor
    backLayerView0.layer.shadowOpacity = 1
    backLayerView0.layer.shadowOffset = CGSize.zero
    backLayerView0.layer.shadowRadius = 1.5

    backLayerView1.layer.shadowColor = UIColor.black.cgColor
    backLayerView1.layer.shadowOpacity = 1
    backLayerView1.layer.shadowOffset = CGSize.zero
    backLayerView1.layer.shadowRadius = 1.5

    backLayerView2.layer.shadowColor = UIColor.black.cgColor
    backLayerView2.layer.shadowOpacity = 1
    backLayerView2.layer.shadowOffset = CGSize.zero
    backLayerView2.layer.shadowRadius = 1.5

    tempLabel.textAlignment = .center
    tempLabel.adjustsFontSizeToFitWidth = true
    
    tempLabel.isUserInteractionEnabled = true
    
    self.contentView.addSubview(tempLabel)
    tempLabel.layer.zPosition = 1
    
    let pan = UIPanGestureRecognizer(target: self, action: #selector(handleStackTextPan))
    pan.delegate = self
    tempLabel.addGestureRecognizer(pan)
  }

  //method for handling the pan gesture
  
  @objc func handleStackTextPan(_ sender: UIPanGestureRecognizer) {
    
    //code for animation of the target location pointer
    let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
    scaleAnimation.duration = 0.4
    scaleAnimation.repeatCount = 100.0
    scaleAnimation.autoreverses = true
    scaleAnimation.fromValue = Int(2.0)
    scaleAnimation.toValue = Int(0.2)
    
    //Began State
    if sender.state == .began{

      sender.view?.frame = CGRect(x: ((sender.view?.frame.origin.x)! + 20),y: ((sender.view?.frame.origin.y)! + 20), width: sender.view!.frame.size.width, height: sender.view!.frame.size.height)
      
//        targetLocation.frame = CGRectMake(targetLocation.frame.origin.x, (sender.view?.frame.size.width)! / 2, targetLocation.frame.size.width, targetLocation.frame.size.height)
//      targetLocation.center = (sender.view?.center)!
        sender.view?.backgroundColor = UIColor.white
      
        sender.view?.layer.zPosition = 1
        sender.view?.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0).cgColor
        sender.view?.layer.borderWidth = 0.5
        sender.view?.layer.cornerRadius = 10
        sender.view?.clipsToBounds = true
        
        targetLocation.isHidden = false
//        targetLocation.layer.add(scaleAnimation, forKey: "scale")
        
        if count ==  totalCount{
            self.backLayerView2.isHidden = true
        }
        
        print("Count : \(count)")
        if count < totalCount - 2{
            print("Show text on first label")
            temporaryLabel.text = questionArray[count]
        }
        else if count == totalCount - 2{
            print("Show text on second label")
            temporaryLabel.text = questionArray[count]
        }
        else if count == totalCount - 1{
            print("Show text on third label")
            temporaryLabel.isHidden = true
            temporaryLabel2.text = questionArray[count]
        }
        else{
            print("Hide third label")
            temporaryLabel2.isHidden = true
        }
    }
    
    //Change State
    
    let translation = sender.translation(in: self.contentView)
    
    sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
    
    self.targetLocationCenterXConstraint.constant = self.targetLocationCenterXConstraint.constant + translation.x
    self.targetLocationCenterYConstraint.constant = self.targetLocationCenterYConstraint.constant + translation.y
    
    
    var touchLocation = CGPoint()
    touchLocation = sender.location(in: self.contentView)
    print("\(targetLocation.frame.origin.x) , \(targetAreaView.frame.size.height - targetLocation.frame.origin.y)")
    
    
    if sender.state == .changed{
      
      //code for constraint change of target loaction view while dragging
      
        sender.view?.frame = CGRect(x: ((sender.view?.frame.origin.x)!),y: ((sender.view?.frame.origin.y)!), width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
      
        let lb = sender.view as! UILabel
        lb.textColor = UIColor.black
      
      sender.view!.backgroundColor = UIColor.white
      
    }
    
    //End State
    if sender.state == .ended{
      
      let pt = targetLocation.superview!.convert(targetLocation.frame.origin, to: nil)
      
      self.targetLocationCenterXConstraint.constant = 0
      self.targetLocationCenterYConstraint.constant = originalYConstantofTargetLocation
      targetLocation.isHidden = true
      targetLocation.layer.removeAllAnimations()
      

//        let w = layerView1.frame.size.width
//        let h = layerView1.frame.size.height
      
        let w = CGFloat(20)
        let h = CGFloat(20)
      
        let rad = self.targetAreaView.frame.size.width / 2
        let xpt = self.targetLocation.frame.origin.x
        let ypt = self.targetLocation.frame.origin.y
        
        let x = abs(rad - xpt)
        let y = abs(rad - ypt)
      
      //code for checking whether the target loaction after dropping is in the circle or not
        if (x * x + y * y) < (rad * rad){
          
          //code for placing the arrow on the dart
//        let arrowImgView = UIImageView(frame: CGRectMake(targetLocation.frame.origin.x + 30, targetLocation.frame.origin.y - (h + 8), w, h))
          let arrowImgView = UIImageView(frame: CGRect(x: targetLocation.frame.origin.x, y: targetLocation.frame.origin.y, width: w, height: h))
          arrowImgView.image = UIImage(named: "bullseye_arrow")
                    self.targetAreaView.addSubview(arrowImgView)
          
          count = count + 1

          UIView.animate(withDuration: 0.5, animations: { 
            arrowImgView.frame = CGRect(x: self.targetLocation.frame.origin.x, y: self.targetLocation.frame.origin.y - (h - 12), width: w, height: h)
            }, completion: { (status) in
              if self.count >  self.totalCount{
                self.nextButton.isHidden = false
              }
          })
          
//          UIView.animateWithDuration(0.5, animations: {
//            arrowImgView.frame = CGRectMake(self.targetLocation.frame.origin.x - 7, self.targetLocation.frame.origin.y - (h - 8), w, h)
//          })
          
          arrowImgView.layer.zPosition = 1

          print(targetLocation.frame)
          print("\(targetLocation.frame.origin.x) , \(targetAreaView.frame.size.height - targetLocation.frame.origin.y)")
          let originalX = targetLocation.frame.origin.x
          let originalY = targetAreaView.frame.size.height - targetLocation.frame.origin.y
          
          let newX = (100 * originalX) / targetAreaView.frame.size.width
          let newY = (100 * originalY) / targetAreaView.frame.size.width
          
          print("View width : \(targetAreaView.frame.size.width) | View height : \(targetAreaView.frame.size.height)")
          
          let lb = sender.view as!  UILabel
//          self.dataDict[lb.text!] = "\(targetLocation.frame.origin.x),\(targetAreaView.frame.size.height - targetLocation.frame.origin.y)"
//          self.dataDict[lb.text!] = "\(newX),\(newY)"
          let ans = MMRAnsModel()
          ans.labelSelected = "\(Int(newX)):\(Int(newY))"
          print("ANSWER:= \(ans.labelSelected)")
          let indexofOpt = self.questionArray.index(of: lb.text!)
          ans.optionIdAns = Int(self.questionOptionArr[indexofOpt!])!
          ans.answeredOptionSeq = Int(self.questionOptionSeqArr[indexofOpt!])!
          self.dataDict.append(ans)

          sender.view!.removeFromSuperview()
          
          
          if count == totalCount - 1{
            self.backLayerView0.isHidden = true
          }
          else if count == totalCount
          {
            self.backLayerView1.isHidden = true
          }
          
          if count >  totalCount{
            self.backLayerView2.isHidden = true
            self.nextButton.isHidden = false
          }
          else{
          
          //If successful plotting of the arrow next question card is created
            
          let newStackTextLabel = UILabel()
           let newTempTextLabel = UILabel()
            if count < totalCount - 1{
              newStackTextLabel.frame = CGRect(x: self.backLayerView0.frame.origin.x, y: self.backLayerView0.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
              self.targetLocationCenterXConstraint.constant = 20

            }
            else if count == totalCount - 1{
              newStackTextLabel.frame = CGRect(x: self.backLayerView1.frame.origin.x, y: self.backLayerView1.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
              self.targetLocationCenterXConstraint.constant = 30
            }
            else if count == totalCount {
              newStackTextLabel.frame = CGRect(x: self.backLayerView2.frame.origin.x, y: self.backLayerView2.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
              self.targetLocationCenterXConstraint.constant = 40
            }

            print("newStackTextLabel:-\(newStackTextLabel.frame)")
            print("newTempTextLabel:-\(newTempTextLabel.frame)")
            
          temporaryLabel.text = ""
          temporaryLabel2.text = ""
            
          newStackTextLabel.text = questionArray[count - 1]
          newStackTextLabel.numberOfLines = 0
          newStackTextLabel.backgroundColor = UIColor.clear

          newStackTextLabel.textAlignment = .center
          newStackTextLabel.adjustsFontSizeToFitWidth = true
          newStackTextLabel.isUserInteractionEnabled = true
          
            self.contentView.addSubview(newStackTextLabel)
//            self.contentView.addSubview(newTempTextLabel)
          
          newStackTextLabel.layer.zPosition = 1
          
          let pan = UIPanGestureRecognizer(target: self, action: #selector(handleStackTextPan))
          pan.delegate = self
          newStackTextLabel.addGestureRecognizer(pan)
      }
        }
      else{
            temporaryLabel.text = ""
            temporaryLabel2.text = ""
          // If out of the Circle Place at original position
          
          if count < totalCount - 1{
            sender.view?.frame = CGRect(x: self.backLayerView0.frame.origin.x, y: self.backLayerView0.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
          }
          else if count == totalCount - 1{
            sender.view?.frame = CGRect(x: self.backLayerView1.frame.origin.x, y: self.backLayerView1.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
          }
          else if count == totalCount{
            sender.view?.frame = CGRect(x: self.backLayerView2.frame.origin.x, y: self.backLayerView2.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
            backLayerView2.isHidden = false
          }
          
          if nextButton.isHidden == false{
            nextButton.isHidden = true
          }

        let lb = sender.view as! UILabel
        lb.textColor = UIColor.black

          sender.view!.backgroundColor = UIColor.clear
      }
    }
    
    sender.setTranslation(CGPoint.zero, in: self.contentView)
  }
  
  @IBAction func nextClicked(_ sender: AnyObject) {
    print(dataDict)
  }
  
}
