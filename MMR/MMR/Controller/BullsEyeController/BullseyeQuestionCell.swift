//
//  BullseyeQuestionCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 10/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class BullseyeQuestionCell: UITableViewCell {

  @IBOutlet var questionTextView: UITextView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
