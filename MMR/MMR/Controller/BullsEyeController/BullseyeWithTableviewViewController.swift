//
//  BullseyeWithTableviewViewController.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 10/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class BullseyeWithTableviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {

    @IBOutlet weak var navigationView: Navigation_View!
    @IBOutlet var bullseyeTableView: UITableView!
    weak var delegate : BaseClassMethodToFetchDataFromDB?

    var otionsArray         =  NSMutableArray ()
    var optionsIdArray = NSMutableArray()
    var optionsOrgSeqArray = NSMutableArray()
    var optionsTextIdArray = NSMutableArray()
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var isAnswerGiven       = Bool ()
    var questionTextString  = String ()
    var question = String()
    var attrStr = NSAttributedString()
    var newSize = CGSize()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
        navigationSetup()
        self.setText()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      }
      bullseyeTableView.estimatedRowHeight = 400.0
      bullseyeTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
   
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }

  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
//    let mmrBLEControlNav = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewControllerWithIdentifier("MMRBLEViewController") as? MMRBLEViewController
//    mmrBLEControlNav?.startBLEFunctionality()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
    AlertModel.sharedInstance.showErrorMessage(bluetoothePowerOffMessage)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  @objc func setText() {
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
  }
    func navigationSetup() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
//        navigationView.titleLabel.text = "Question " + "\(serialID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)
//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! NSNumber
//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)", userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! NSNumber
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }

      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)
//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

      print(inTime)
        let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists" {
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        self.otionsArray = []
        self.optionsIdArray = []
      
        let optionsTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue, userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      
        self.otionsArray = optionsTuple.optionText
        self.optionsIdArray = optionsTuple.optionId
        self.optionsOrgSeqArray = optionsTuple.originalSeqNo
        self.optionsTextIdArray = optionsTuple.textOptionId
      
        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(self.otionsArray)")
        print("optionsForQuestionIDFromServer\(self.optionsIdArray)")
        print("optionsTextIdFromServer : \(self.optionsTextIdArray)")

        self.questionTextString   = questionFromServer[questionText] as! String
        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        self.bullseyeTableView.reloadData()
      
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
        
    }


  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.row == 0{
      
      let cell = self.bullseyeTableView.dequeueReusableCell(withIdentifier: "BullseyeQuestionCell", for: indexPath) as! BullseyeQuestionCell

      cell.questionTextView.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(self.questionTextString)
//            let quesString = questionFromServer[questionText] as! String
//            quesString.attributedStringFromHTML { (resultString) in
//              print(resultString?.debugDescription)
//              cell.questionTextView.attributedText = resultString
//            }
      cell.questionTextView.delegate = self
      newSize = cell.questionTextView.contentSize
      return cell
    }
    else {
      let cell1 = self.bullseyeTableView.dequeueReusableCell(withIdentifier: "BullseyeCell", for: indexPath) as! BullseyeCell
        cell1.questionArray = self.otionsArray.copy() as! [String]
        cell1.questionOptionArr = self.optionsIdArray.copy() as! [String]
        cell1.questionOptionSeqArr = self.optionsOrgSeqArray.copy() as! [String]
        cell1.totalCount = cell1.questionArray.count
        cell1.setUpInitialQuestionLabel()
        bullseyeTableView.beginUpdates()
        cell1.setUpBullsEye()
        bullseyeTableView.endUpdates()
        return cell1
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if indexPath.row == 1{
      let bounds = UIScreen.main.bounds
      let height = bounds.size.height
      
      switch height {
      case 480.0:
        print("iPhone 3,4")
        return  UIScreen.main.bounds.size.height
        
      case 568.0:
        print("iPhone 5")
        return  UIScreen.main.bounds.size.height - 100

      case 667.0:
        print("iPhone 6")
        return  UIScreen.main.bounds.size.height - 100

      case 736.0:
        print("iPhone 6+")
        return  UIScreen.main.bounds.size.height - 100

      default:
        print("not an iPhone")
        return  UIScreen.main.bounds.size.height - 100

      }
    }
    else{
      return bullseyeTableView.rowHeight
    }
  }

    @IBAction func nextButtonClicked(_ sender: UIButton) {
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let cell = bullseyeTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! BullseyeCell
        print(cell.dataDict)
        
      /*var ansArray = [String]()
      for option in self.otionsArray {
        let coordinatesArray = (cell.dataDict[option as! String])!.componentsSeparatedByString(",")
        let tempAnsStr = "\(option)-\(coordinatesArray[0]):\(coordinatesArray[1])"
        ansArray.append(tempAnsStr)
      }
      
      let ansString = ansArray.joinWithSeparator("^]`,")
      print(ansString)*/
      
      var ansString = String()
      let sortedAnsArray = cell.dataDict.sorted(by: { $0.answeredOptionSeq < $1.answeredOptionSeq })
      
      for temp in sortedAnsArray {
        if ansString == "" {
          ansString = "\(temp.labelSelected)"
        }
        else {
          ansString = "\(ansString)^]`,\(temp.labelSelected)"
        }
      }
      let optionTextIdString = self.optionsTextIdArray.componentsJoined(by: ";")
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
      }
        self.delegate?.updateAnswerTextForQuestion(ansString, serialNo: "\(val1)")
        self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
        self.delegate?.updateOptionTextIDSeqForQuestion(optionTextIdString, serialNo: "\(val1)")

      let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
      if isTimeLeftForSurvey {
        UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
        delegate?.fetchNextQuestionType()
      } else {
        UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
        BaseClassForQuestions.serialNo_ID = -1
        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
        delegate?.fetchNextQuestionType()
      }
    }

  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
}
