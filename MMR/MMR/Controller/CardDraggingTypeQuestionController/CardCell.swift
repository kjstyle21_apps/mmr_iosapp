//
//  CardCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 10/12/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
  @IBOutlet var labelBackgroundView: UIView!
  @IBOutlet var optionLabel: UILabel!

  @IBOutlet var cardSlotSubtitleLabel: UILabel!
}
