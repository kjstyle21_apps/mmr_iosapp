//
//  CardDragQuestionWithTableViewController.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 11/7/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class CardDragQuestionWithTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,NVActivityIndicatorViewable,MMRSubmitSurveyDelegate {

  @IBOutlet var dragCardTableview: UITableView!
  @IBOutlet weak var navigationView: Navigation_View!

  weak var delegate : BaseClassMethodToFetchDataFromDB?
  var optionsArray         =  NSMutableArray ()
  var optionIdArray = NSMutableArray()
  var optionsOrgSeqArray = NSMutableArray()
  var questionFromServer  = NSMutableDictionary ()
  var isHintAvailable     = Bool ()
  var isMandatory         = Bool ()
  var isAvarerGiven       = Bool ()
  var questionTextString  = String ()
  var question = String()
  var labelArray = [String]()
  var inTime = Date()
  var outTime = Date()
  var dropTime = Date()
  
  var finalOutTimeStr = String()
  var finalInTimeStr = String()
  var finalDropTime = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
      navigationSetup()
      self.setText()
      dragCardTableview.estimatedRowHeight = 400.0
      dragCardTableview.rowHeight = UITableViewAutomaticDimension
      NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {

      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
//    AlertModel.sharedInstance.showErrorMessage(bluetoothePowerOffMessage)
  }

  
  @objc func setText() {
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
  }
  
  func navigationSetup() {
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
    
//    navigationView.titleLabel.text = "Question " + "\(serialID!)"
    navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
    navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
    
    if isHintAvailable {
      navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
      navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
    }
  }
  
  @objc func leftButtonAction() {
    AlertModel.sharedInstance
      .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
  }
  
  @objc func rightButtonAction() {
    self.dropTime = Date()
    self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//    let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)

    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
    }
    else if status == "No_Exists" {
      //no exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
    }
    delegate!.goToHomeViewController()
  }
  
  func initSetUp()
  {
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)

    self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//    if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//      if mflag {
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//        if seqNum > 1 {
          questionFromServer.setValue("", forKey: answerStartTime)
          questionFromServer.setValue("", forKey: answerEndTime)
          questionFromServer.setValue("", forKey: isAnswered)
          questionFromServer.setValue("", forKey: answer_text)
          questionFromServer.setValue(serialID, forKey: serial_id)
          questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
          questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
          questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
          let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
          if resultArray.count == 0 {
            QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
          }
//        }
//      }
//    }
    
    self.inTime = Date()
    self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//    let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    print(inTime)
//    let serialID = NSUserDefaults.standardUserDefaults().valueForKey(serialNoID) as! NSNumber
    let val1 = String(format: "%d", serialID)
    let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)", userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
    if status == "No_Exists"{
      self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
    }

    
    self.optionsArray = []
    self.optionIdArray = []
    
    let optionTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
    
    self.optionsArray = optionTuple.optionText
    self.optionIdArray = optionTuple.optionId
    self.optionsOrgSeqArray = optionTuple.originalSeqNo
    
    print("questionFromServer\(questionFromServer)")
    print("optionsForQuestionFromServer\(self.optionsArray)")
    print("optionsIDForQuestionFromServer\(self.optionIdArray)")

    let labelText = questionFromServer["labelText"] as! String
    labelArray = labelText.components(separatedBy: "-")
    print(labelArray)
    self.questionTextString   = questionFromServer[questionText] as! String
    isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
    self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
    self.dragCardTableview.reloadData()
  }

  func goToHomeScreen () {
    var viewController: UIViewController?
    for aViewController in self.navigationController!.viewControllers {
      if aViewController.isKind(of: SSASideMenu.self) {
        viewController = aViewController
        break
      }
    }
    
    if let viewController = viewController {
      self.navigationController!.popToViewController(viewController, animated: true)
    }
    
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if self.questionTextString == "" && self.optionsArray.count == 0{
     return 0
    }
    return 2
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.row == 0{
      
      let cell = self.dragCardTableview.dequeueReusableCell(withIdentifier: "DragCardQuestionCell", for: indexPath) as! DragCardQuestionCell
      cell.questionTextview.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(self.questionTextString)
//            let quesString = questionFromServer[questionText] as! String
//            quesString.attributedStringFromHTML { (resultString) in
//              cell.questionTextview.attributedText = resultString
//            }
      cell.questionTextview.delegate = self
//      cell.isMandatory = self.isMandatory
      if isMandatory {
        cell.compulsaryStarLabel.isHidden = false
      }
      else{
        cell.compulsaryStarLabel.isHidden = true
      }
      return cell
    }
    else{
      let cell = self.dragCardTableview.dequeueReusableCell(withIdentifier: "DragCardCell", for: indexPath) as! DragCardCell
      cell.questionArray = self.optionsArray.copy() as! [String]
      cell.questionOptionArr = self.optionIdArray.copy() as! [String]
      cell.questionOptionSeqArr = self.optionsOrgSeqArray.copy() as! [String]
      cell.totalCount = cell.questionArray.count
      cell.cardSlotCount = self.labelArray.count
      cell.cardsSubtitleLabelArray = self.labelArray
      dragCardTableview.beginUpdates()
      dragCardTableview.endUpdates()
      cell.setUpInitialQuestion()
      return cell
    }

  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if indexPath.row == 1{
      return  UIScreen.main.bounds.size.height  - 130
    }
    else{
      return dragCardTableview.rowHeight
    }
    
  }

  @IBAction func nextClicked(_ sender: UIButton) {
    self.outTime = Date()
    self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
//    let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let cell = self.dragCardTableview.cellForRow(at: IndexPath(row: 1, section: 0)) as! DragCardCell
    print(cell.dataDict)
    var answerString = String()
    let sortedAnsArray = cell.dataDict.sorted(by: { $0.answeredOptionSeq < $1.answeredOptionSeq })
    
    for temp in sortedAnsArray {
      if answerString == "" {
        //Changes are done for issue 103
//        answerString = "\(temp.labelSelected)"
        let val1 = String(format: "%d", temp.seqNum)
        answerString = "\(val1)"
      }
      else {
        //Changes are done for issue 103
//        answerString = "\(answerString)^]`,\(temp.labelSelected)"
        let val1 = String(format: "%d", temp.seqNum)
        answerString = "\(answerString)^]`,\(val1)"
      }
    }
   /* for lbl in labelArray {
      if let arr = cell.dataDict[lbl] as? NSArray {
        let str  = (arr as! [String]).joinWithSeparator(":")
        if answerString == ""{
          answerString = "\(lbl)-\(str)"
        }
        else {
          answerString = "\(answerString)^]`,\(lbl)-\(str)"
        }
      }
    }*/
    
    print(answerString)
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
    self.delegate?.updateAnswerTextForQuestion(answerString, serialNo: "\(val1)")
    
    self.delegate?.updateOutTimeForQuestion(finalOutTimeStr, serialNo: "\(val1)")

    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
    }
    
    let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
    if isTimeLeftForSurvey {
      UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
      delegate?.fetchNextQuestionType()
    } else {
      UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
      BaseClassForQuestions.serialNo_ID = -1
      UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
      delegate?.fetchNextQuestionType()
      
    }

  }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
    stopActivityAnimating()
  }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
