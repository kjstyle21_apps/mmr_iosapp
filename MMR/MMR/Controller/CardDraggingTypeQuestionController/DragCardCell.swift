//
//  DragCardCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 11/7/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class DragCardCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource{

  @IBOutlet var cardsCollectionView: UICollectionView!
  @IBOutlet var backLayerView0: UIView!
  @IBOutlet var backLayerView1: UIView!
  @IBOutlet var backLayerView2: UIView!
  @IBOutlet var tempLabel: UILabel!
  @IBOutlet var nextButton: UIButton!
  @IBOutlet var temporaryLabel1: UILabel!
  @IBOutlet var temporaryLabel2: UILabel!
  
  var originalFrame : CGRect!
  var enableOriginalPlacing = false
  var cardSlotCount = Int()
  var count = 1
//  var dataDict = [String : [String]]()
  var dataDict = [MMRAnsModel]()
  var origFont = CGFloat()
  var totalCount = Int()
  var questionArray = [String]()
  var questionOptionArr = [String]()
  var questionOptionSeqArr = [String]()
  var cardsSubtitleLabelArray = [String]()

  var twoFlag = false
  
  @IBOutlet var heightConstraintofBackLayerView0: NSLayoutConstraint!
  @IBOutlet var widthConstraintofBackLayerView0: NSLayoutConstraint!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

      cardsCollectionView.delegate = self
      cardsCollectionView.dataSource = self
      self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
      let layout = KTCenterFlowLayout()
      layout.minimumInteritemSpacing = 10.0
      layout.minimumLineSpacing = 10.0
      cardsCollectionView.collectionViewLayout = layout
    }

  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.cardsCollectionView.reloadData()
    //Setting up the UI of the backLayer views of cards
    
    originalFrame = tempLabel.frame
    nextButton.layer.cornerRadius = 5.0
    
    backLayerView1.layer.cornerRadius = 10.0
    backLayerView1.frame = backLayerView1.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView1.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView1.layer.borderWidth = 0.5
    
    backLayerView0.layer.cornerRadius = 10.0
    backLayerView0.frame = backLayerView0.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView0.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView0.layer.borderWidth = 0.5
    
    backLayerView2.layer.cornerRadius = 10.0
    backLayerView2.frame = backLayerView2.frame.insetBy(dx: 0.5, dy: 0.5)
    backLayerView2.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1).cgColor
    backLayerView2.layer.borderWidth = 0.5
    
  }
  
  //method for setup the initial card in the stack
  func setUpInitialQuestion() {
    
    if totalCount == 2 {
      self.backLayerView2.isHidden = true
      self.twoFlag = true
    }
    
    let newStackTextLabel = UILabel(frame: CGRect(x: self.backLayerView0.frame.origin.x, y: self.backLayerView0.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier))

    self.tempLabel.isHidden = true
    
    newStackTextLabel.text = questionArray[0]
    newStackTextLabel.numberOfLines = 0
    
    newStackTextLabel.backgroundColor = UIColor.clear
    newStackTextLabel.textAlignment = .center
//    newStackTextLabel.adjustsFontSizeToFitWidth = true
    newStackTextLabel.font = newStackTextLabel.font.withSize(17.0)
    newStackTextLabel.isUserInteractionEnabled = true
    self.contentView.addSubview(newStackTextLabel)
    newStackTextLabel.layer.zPosition = 1

    let pan = UIPanGestureRecognizer(target: self, action: #selector(handleStackTextPan))
    pan.delegate = self
    newStackTextLabel.addGestureRecognizer(pan)

    backLayerView0.layer.shadowColor = UIColor.black.cgColor
    backLayerView0.layer.shadowOpacity = 1
    backLayerView0.layer.shadowOffset = CGSize.zero
    backLayerView0.layer.shadowRadius = 1.5
    
    backLayerView1.layer.shadowColor = UIColor.black.cgColor
    backLayerView1.layer.shadowOpacity = 1
    backLayerView1.layer.shadowOffset = CGSize.zero
    backLayerView1.layer.shadowRadius = 1.5
    
    backLayerView2.layer.shadowColor = UIColor.black.cgColor
    backLayerView2.layer.shadowOpacity = 1
    backLayerView2.layer.shadowOffset = CGSize.zero
    backLayerView2.layer.shadowRadius = 1.5

  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return cardSlotCount
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.cardsCollectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCell
    cell.tag = indexPath.item
    cell.cardSlotSubtitleLabel.text = self.cardsSubtitleLabelArray[indexPath.item]
    cell.cardSlotSubtitleLabel.tag = indexPath.item + 1
    return cell
  }
  
  @objc func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                             sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
    
    let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
    let totalSpace = flowLayout.sectionInset.left
      + flowLayout.sectionInset.right
      + (flowLayout.minimumInteritemSpacing * CGFloat(5))
    let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(5))
    let sizeNew = CGSize(width: size, height: Int(cardsCollectionView.frame.size.height))
    return sizeNew
    
  }
  
  
  //method for handling the pan gesture
  
  @objc func handleStackTextPan(_ sender: UIPanGestureRecognizer) {
    
    //Began State
    
    if sender.state == .began{
      
      //code for resizing the card while dragging
      
      let indexPath = IndexPath(item: 0, section: 0)
      let cell = self.cardsCollectionView.cellForItem(at: indexPath) as! CardCell
      
      sender.view?.frame = CGRect(x: ((sender.view?.frame.origin.x)! + 20),y: ((sender.view?.frame.origin.y)! + 20), width: cell.labelBackgroundView.frame.size.width - 2, height: cell.labelBackgroundView.frame.size.height - 2)
      
      sender.view!.layer.shadowPath = UIBezierPath(rect: sender.view!.bounds).cgPath
      sender.view?.backgroundColor = UIColor.white
      sender.view?.layer.zPosition = 1
      sender.view?.layer.borderColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0).cgColor
      sender.view?.layer.borderWidth = 0.5
      sender.view?.layer.cornerRadius = 5
      sender.view?.clipsToBounds = true
      
      let lb1 = sender.view as! UILabel
      origFont = lb1.font.pointSize
      lb1.font = lb1.font.withSize(10.0)
     
      
      if count ==  totalCount{
        self.backLayerView2.isHidden = true
      }
      
      if count < totalCount - 2{
        print("Show text on first label")
        temporaryLabel1.text = questionArray[count]
      }
      else if count == totalCount - 2{
        print("Show text on second label")
        temporaryLabel1.text = questionArray[count]
      }
      else if count == totalCount - 1{
        print("Show text on third label")
        temporaryLabel1.isHidden = true
        temporaryLabel2.text = questionArray[count]
      }
      else{
        print("Hide third label")
        temporaryLabel2.isHidden = true
      }
    }
    
    //Change State
    
    let translation = sender.translation(in: self.contentView)
    
    sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
    
    var touchLocation = CGPoint()
    
    touchLocation = sender.location(in: self.contentView)
//    print("TouchLocation : \(touchLocation)")
    
    
    var removeHighlighter = false
    var cell = CardCell()
    
    //code for highlighting the cardslot while dragging over any slot
    for i in 0..<cardSlotCount {

   // for var i = 0; i < cardSlotCount ; i = i + 1 {
      let indexPath = IndexPath(item: i, section: 0)
      cell = self.cardsCollectionView.cellForItem(at: indexPath) as! CardCell
      
//      if CGRectContainsPoint(cell.frame, touchLocation) {
      if cell.frame.contains((sender.view?.center)!) {

        removeHighlighter = true
        
        if #available(iOS 10.0, *) {
            cell.labelBackgroundView.backgroundColor = UIColor(displayP3Red: 0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1)
        } else {
            // Fallback on earlier versions
            cell.labelBackgroundView.backgroundColor = UIColor(red: 255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1)
        } //UIColor(colorLiteralRed: 0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1)
        cell.selectedBackgroundView = UIView(frame: cell.frame)
        cell.selectedBackgroundView?.backgroundColor = UIColor(red: 0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1)
        cell.selectedBackgroundView?.alpha = 0.5
      }
      else{
        if cell.optionLabel.text == ""{
        cell.labelBackgroundView.backgroundColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 0.45)
        }
        else{
          cell.labelBackgroundView.backgroundColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
        cell.selectedBackgroundView?.removeFromSuperview()
        if i == cardSlotCount{
          if removeHighlighter != true {
            removeHighlighter = false
          }
        }
        
      }
      
    }
    if removeHighlighter == false{
      cell.selectedBackgroundView?.removeFromSuperview()
      
      if cell.optionLabel.text == ""{
      cell.labelBackgroundView.backgroundColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 0.45)
      }
      else{
        cell.labelBackgroundView.backgroundColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
      }
    }
    
    //End State
    
    if sender.state == .ended {
      
      //Code for checking the card slot selected or not
      //If selected then which one is selected
      
      self.enableOriginalPlacing = false
        
        for i in 0..<cardSlotCount {
      //for var i = 0; i < cardSlotCount ; i = i + 1 {
        let indexPath = IndexPath(item: i, section: 0)
        let cell = self.cardsCollectionView.cellForItem(at: indexPath) as! CardCell
        
        let cellFrameInView = self.cardsCollectionView.convert(cell.frame, to: self.contentView)
//        if CGRectContainsPoint(cell.frame, touchLocation) {
        if cell.frame.contains((sender.view?.center)!) {

          
          self.enableOriginalPlacing = true

          count = count + 1
          
          let lb = sender.view as!  UILabel

          UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            
            switch height {
            case 480.0:
              print("iPhone 3,4")
              sender.view?.center = CGPoint(x: cell.center.x + 8, y: cell.center.y + 3)

            case 568.0:
              print("iPhone 5")
              sender.view?.center = CGPoint(x: cell.center.x + 8, y: cell.center.y)

            case 667.0:
              print("iPhone 6")
              sender.view?.center = CGPoint(x: cell.center.x + 8, y: cell.center.y - 5)

            case 736.0:
              print("iPhone 6+")
              sender.view?.center = CGPoint(x: cell.center.x + 8, y: cell.center.y - 6)

            default:
              print("not an iPhone")
              sender.view?.center = CGPoint(x: cell.center.x + 8, y: cell.center.y - 6)
            }

            }, completion: { (true) in
              
              let ans = MMRAnsModel()
              ans.labelSelected = cell.cardSlotSubtitleLabel.text!
                ans.seqNum = cell.cardSlotSubtitleLabel.tag
              let indexofOpt = self.questionArray.index(of: lb.text!)
              ans.optionIdAns = Int(self.questionOptionArr[indexofOpt!])!
              ans.answeredOptionSeq = Int(self.questionOptionSeqArr[indexofOpt!])!
              self.dataDict.append(ans)
              
              /*if let arr = self.dataDict[cell.cardSlotSubtitleLabel.text!] {
                self.dataDict[cell.cardSlotSubtitleLabel.text!]?.append(lb.text!)
              }
              else{
                let array = [lb.text!]
                self.dataDict[cell.cardSlotSubtitleLabel.text!] = array
              }*/
              
              cell.optionLabel.text = ""
              cell.optionLabel.text = lb.text
              
              UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                sender.view?.alpha = 0
                }, completion: { (true) in
                  sender.view!.removeFromSuperview()
                  cell.selectedBackgroundView?.removeFromSuperview()
                  cell.labelBackgroundView.backgroundColor = UIColor.init(red: 72.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                  if self.count >  self.totalCount {
                    self.nextButton.isHidden = false
                  }
              })
              
          })
          
          
          if count == totalCount - 1{
            self.backLayerView0.isHidden = true
          }
          else if count == totalCount
          {
            self.backLayerView1.isHidden = true
          }
          
          if count >  totalCount{
            self.backLayerView2.isHidden = true
//            self.nextButton.hidden = false
          }
          else{
            
            //If card is dropped at selected slot then a next card is created for dragging
            
            let newStackTextLabel = UILabel()
            
            if count < totalCount - 1{
              newStackTextLabel.frame = CGRect(x: self.backLayerView0.frame.origin.x, y: self.backLayerView0.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
            }
            else if count == totalCount - 1{
              newStackTextLabel.frame = CGRect(x: self.backLayerView1.frame.origin.x, y: self.backLayerView1.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
            }
            else if count == totalCount{
              newStackTextLabel.frame = CGRect(x: self.backLayerView2.frame.origin.x, y: self.backLayerView2.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
            }
            
            
            self.tempLabel.isHidden = true
            temporaryLabel1.text = ""
            temporaryLabel2.text = ""

            
            newStackTextLabel.text = questionArray[count - 1]
            newStackTextLabel.numberOfLines = 0
            
            newStackTextLabel.backgroundColor = UIColor.clear
            newStackTextLabel.textAlignment = .center
//            newStackTextLabel.adjustsFontSizeToFitWidth = true
            newStackTextLabel.font = newStackTextLabel.font.withSize(17.0)

            newStackTextLabel.isUserInteractionEnabled = true
            self.contentView.addSubview(newStackTextLabel)
            newStackTextLabel.layer.zPosition = 1
            
            let pan = UIPanGestureRecognizer(target: self, action: #selector(handleStackTextPan))
            pan.delegate = self
            newStackTextLabel.addGestureRecognizer(pan)
            
          }
          
        }
        else{
          
          if i == cardSlotCount - 1{
            if enableOriginalPlacing != true {
              enableOriginalPlacing = false
            }
          }
          
        }
      }
      
      //If no appropriate slot selected then place it at original position
      //code for placing at original position
      if enableOriginalPlacing == false{
        sender.view?.alpha = 1
        temporaryLabel1.text = ""
        temporaryLabel2.text = ""

        if count < totalCount - 1{
          sender.view?.frame = CGRect(x: self.backLayerView0.frame.origin.x, y: self.backLayerView0.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
        }
        else if count == totalCount - 1{
          sender.view?.frame = CGRect(x: self.backLayerView1.frame.origin.x, y: self.backLayerView1.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
        }
        else if count == totalCount{
          sender.view?.frame = CGRect(x: self.backLayerView2.frame.origin.x, y: self.backLayerView2.frame.origin.y, width: self.contentView.frame.size.width * widthConstraintofBackLayerView0.multiplier, height: self.contentView.frame.size.height * heightConstraintofBackLayerView0.multiplier)
        }
      
        
        let lb1 = sender.view as! UILabel
        lb1.font = lb1.font.withSize(origFont)

        
        sender.view?.layer.zPosition = 1
        if self.twoFlag == true {
          self.backLayerView2.isHidden = true
        }
        else {
        self.backLayerView2.isHidden = false
        }
        self.nextButton.isHidden = true
        sender.view?.backgroundColor = UIColor.clear
        sender.view?.layer.borderColor = UIColor.clear.cgColor

      }
      else{
//        print("Do NOthing")
      }
    }
    
    if self.nextButton.isHidden == false {
      self.backLayerView0.isHidden = true
      self.backLayerView1.isHidden = true
      self.backLayerView2.isHidden = true
    }
    
    sender.setTranslation(CGPoint.zero, in: self)
    
  }

  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
  @IBAction func printClicked(_ sender: AnyObject) {
    
    print(dataDict)
    
  }
  
  
  
}
