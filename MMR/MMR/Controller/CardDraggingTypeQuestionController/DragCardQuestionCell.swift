//
//  DragCardQuestionCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 11/7/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class DragCardQuestionCell: UITableViewCell {

  @IBOutlet var questionTextview: UITextView!
  @IBOutlet weak var compulsaryStarLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
