//
//  MMRAnsModel.swift
//  MMR
//
//  Created by Mindbowser on 7/11/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRAnsModel: NSObject {
  var optionIdAns = Int()
  var labelSelected = String()
  var answeredOptionSeq = Int()
  var seqNum = Int()
}
