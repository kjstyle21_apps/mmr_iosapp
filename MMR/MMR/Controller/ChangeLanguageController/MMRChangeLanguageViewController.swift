//
//  MMRChangeLanguageViewController.swift
//  MMR
//
//  Created by Mindbowser on 11/11/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRChangeLanguageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  @IBOutlet var languageTableViewController: UITableView!
  @IBOutlet var navigationView: Navigation_View!
  
  @IBOutlet var selectLanguageLabel: UILabel!
  
    let availableLanguages = Localize.availableLanguages()
    var selectedIndex = Int()
    var selectedRow = Int()
    let languageNames : NSArray = ["English", "Chinese", "Spanish", "Italian", "German", "Dutch", "Portuguese", "French", "Tagalog", "Bahasa", "Tahi"];
    let languageCodes = ["en","zh-Hant","es","it","de","nl","pt-BR","fr","tl","id","th"]
    //
//    let languageNames : NSArray = ["English", "Chinese", "Spanish", "Italian", "German", "Dutch", "Portuguese", "French", "Tagalog", "Bahasa", "Tahi", "Polish", "Russian", "Romanian"];
//    let languageCodes = ["en","zh-Hant","es","it","de","nl","pt-BR","fr","tl","id","th", "pl", "ru", "ro"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.navigationView.statusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.navigationView.statusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
      
        print("Selected Row :-\(self.selectedRow)")
        print("Available Langs : \(availableLanguages)")
        self.selectedRow = UserDefaults.standard.integer(forKey: SelectedRow_UD)
        print("User Defaults Selected Row :-\(self.selectedRow)")

        self.languageTableViewController.delegate = self
        self.languageTableViewController.dataSource = self
        navigationSetup()
        self.setText()
    }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }

  @objc func setText() {
    print("Select Language".localized())
    if Localize.currentLanguage() == "nl" {
      self.selectLanguageLabel.text = "Selecteer Taal".localized()
    }
    else {
    self.selectLanguageLabel.text = "Select Language".localized()
    }
    navigationView.titleLabel.text = changeLanguageMenuKeyword.localized()
  }
  
  func navigationSetup() {
    navigationView.leftButton! .setImage(UIImage (named: "backButton"), for: UIControlState())
    navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
    navigationView.progressView.isHidden = true
  }
  
  @objc func leftButtonAction() {
    self.navigationController!.popViewController(animated: true)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.languageNames.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = languageTableViewController.dequeueReusableCell(withIdentifier: "changeLanguageCell", for: indexPath)
    cell.textLabel?.text = self.languageNames[indexPath.row] as? String
    cell.selectionStyle = UITableViewCellSelectionStyle.none

    if(indexPath.row == selectedRow)
    {
        cell.imageView?.image = UIImage(named: "selecteddotsImageForLanguageSelection")
    }
    else
    {
        cell.imageView?.image = UIImage(named: "unselecteddotsImageForLanguageSelection")
    }
    return cell
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
      let language = self.languageCodes[indexPath.row]
      Localize.setCurrentLanguage(language)
      print(Localize.currentLanguage())
      
        self.selectedRow = indexPath.row
        UserDefaults.standard.set(self.selectedRow, forKey: SelectedRow_UD)
        UserDefaults.standard.synchronize()
        tableView.reloadData()
        print("Selected Row :-\(self.selectedRow)")
    }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}
