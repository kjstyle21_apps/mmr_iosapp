//
//  MMRChangePasswordController.swift
//  MMR
//
//  Created by Mindbowser on 1/3/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MMRChangePasswordController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable {

    @IBOutlet var oldPasswordTextfield: UITextField!
    @IBOutlet var newPasswordTextfield: UITextField!
    @IBOutlet var confirmPasswordTextfield: UITextField!
    @IBOutlet var updateButton: UIButton!

    @IBOutlet var updateButtonBottomSpaceConstraint: NSLayoutConstraint!
    var originalFrame = CGRect()
  
  @IBOutlet var changePasswordTitleLabel: UILabel!
  
  @IBOutlet var bluetoothStatusLabel: UILabel!
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.updateButton.roundbutton(self.updateButton, cornerradius: 8)
        self.textfieldSetup()

        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        if height == 480.0{
            self.updateButtonBottomSpaceConstraint.constant = 35.0
        }

      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.bluetoothStatusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.bluetoothStatusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.changePasswordTitleLabel.text = changePasswordMenuKeyword.localized()
    self.updateButton.setTitle("UPDATE".localized(), for: UIControlState())
    oldPasswordTextfield.placeholder = oldPasswordTextfieldPlaceholderKeyword.localized()
    newPasswordTextfield.placeholder = newPasswordTextfieldPlaceholderKeyword.localized()
    confirmPasswordTextfield.placeholder = confirmPasswordTextfieldPlaceholderKeyword.localized()
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.bluetoothStatusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.bluetoothStatusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Textfield Setup
    func textfieldSetup() {
        let h = oldPasswordTextfield.frame.size.height
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: h, height: h))
        let image = UIImage(named: "Lock")
        imageView.image = image
        imageView.sizeToFit()
        oldPasswordTextfield.leftView = imageView
        oldPasswordTextfield.leftViewMode = UITextFieldViewMode.always
      
        let h1 = newPasswordTextfield.frame.size.height
        let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: h1, height: h1))
        let image1 = UIImage(named: "Lock")
        imageView1.image = image1
        imageView1.sizeToFit()
        newPasswordTextfield.leftView = imageView1
        newPasswordTextfield.leftViewMode = UITextFieldViewMode.always
      
        let h2 = confirmPasswordTextfield.frame.size.height
        let imageView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: h2, height: h2))
        let image2 = UIImage(named: "Lock")
        imageView2.image = image2
        imageView2.sizeToFit()
        confirmPasswordTextfield.leftView = imageView2
        confirmPasswordTextfield.leftViewMode = UITextFieldViewMode.always

    }

    // MARK: - Button Action
    @IBAction func updateButtonClicked(_ sender: AnyObject)
    {
        if(self.oldPasswordTextfield.text == "" || self.newPasswordTextfield.text == "" || self.confirmPasswordTextfield.text == "") {
            AlertModel.sharedInstance.showErrorMessage(emptyPasswordMessageKeyword.localized())
        }
        else if self.newPasswordTextfield.text?.utf16.count < kMinimumPasswordCharacters
        {
            AlertModel.sharedInstance.showErrorMessage(passwordTooShortMessageKeyword.localized())
        }
        else if self.newPasswordTextfield.text?.utf16.count > kMaximumPasswordCharacters
        {
          AlertModel.sharedInstance.showErrorMessage(passwordTooLongMessageKeyword.localized())
        }
        else if (self.newPasswordTextfield.text != self.confirmPasswordTextfield.text)
        {
            AlertModel.sharedInstance.showErrorMessage(passwordDoesNotMatchMessageKeyword.localized())
        }
        else if (self.newPasswordTextfield.text == self.oldPasswordTextfield.text)
        {
            AlertModel.sharedInstance.showErrorMessage(samePasswordMessageKeyword.localized())
        }
        else
        {
            self.changePasswordAPICall()
        }

    }
    
    // MARK: - API Call
    func changePasswordAPICall() {
        
        let email = UserDefaults.standard.object(forKey: emailId)
        print(email)
        
        let oldPassword = self.oldPasswordTextfield.text?.md5(string: self.oldPasswordTextfield.text!)
        print(oldPassword)

        let newPassword = self.newPasswordTextfield.text?.md5(string: self.newPasswordTextfield.text!)
        print(newPassword)
        
        let dict: [String:AnyObject] = [MMRUserEmail : email! as AnyObject , MMROldPassword : oldPassword! as AnyObject, MMRNewPassword : newPassword! as AnyObject]
        print(dict)
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().changePassword(dict)
            DispatchQueue.main.async(execute: {
                self.stopActivityAnimating()
                if success{
                    AlertModel.sharedInstance.showAlert("MMR", message: passwordChangeSuccessMessageKeyword.localized(), buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (clickedButtonTitle, success) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            })
            
        }
        
    }
    
    @IBAction func backButtonClicked(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
            self.originalFrame = self.view.frame
            
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            
            switch height {
            case 480.0:
                print("iPhone 3,4")
                self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 70, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
            case 568.0:
                print("iPhone 5")
                self.view.frame = CGRect(x: self.originalFrame.origin.x, y: self.originalFrame.origin.y - 50, width: self.originalFrame.size.width, height: self.originalFrame.size.height)
            case 667.0:
                print("iPhone 6")
            case 736.0:
                print("iPhone 6+")
                
            default:
                print("not an iPhone")
                
            }
            
        }) 
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame = self.originalFrame
        })
        
    }


}
