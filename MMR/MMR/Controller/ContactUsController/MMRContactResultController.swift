//
//  MMRContactResultController.swift
//  MMR
//
//  Created by Mindbowser on 1/4/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRContactResultController: UIViewController {

  @IBOutlet var contactUsTitleLabel: UILabel!
  @IBOutlet var contactUsMessageLabel: UILabel!
  @IBOutlet var bluetoothStatusLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.bluetoothStatusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.bluetoothStatusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    contactUsTitleLabel.text = contactUsMenuKeyword.localized()
    contactUsMessageLabel.text = contactUsMessageLabelKeyword.localized()
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.bluetoothStatusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.bluetoothStatusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonClicked(_ sender: AnyObject)
    {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }

}
