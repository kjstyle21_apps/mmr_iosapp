//
//  MMRContactUsController.swift
//  MMR
//
//  Created by Mindbowser on 1/3/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRContactUsController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,NVActivityIndicatorViewable,UITextViewDelegate {

    @IBOutlet var regardingTextField: UITextField!
    @IBOutlet var phoneTypeTextField: UITextField!
    
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var problemTextView: UITextView!
    @IBOutlet var emailTextField: UITextField!
   
    @IBOutlet var issueTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet var phoneTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet var emailTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet var firstLabelUpperConstraint: NSLayoutConstraint!
    @IBOutlet var secondLabelUpperConstraint: NSLayoutConstraint!
    @IBOutlet var thirdLabelUpperConstraint: NSLayoutConstraint!
    @IBOutlet var fourthLabelUpperConstraint: NSLayoutConstraint!
    @IBOutlet var submitButtonUpperConstraint: NSLayoutConstraint!
    @IBOutlet var submitButtonBottomSpaceConstraint: NSLayoutConstraint!
  
  @IBOutlet var bluetoothStatusLabel: UILabel!
  
    var activeTextField = UITextField()
    var issueArray = ["Make Selection","Issue","Make a suggestion","Help / Support"]
    var phoneArray = ["Make Selection","Android","iOS"];
    var selectedIssue    = String ()
    var selectedPhone    = "iOS"
    var pickerView = UIPickerView()

  @IBOutlet var contactUsTitleLabel: UILabel!
  
  @IBOutlet var regardingWhatQLabel: UILabel!
  @IBOutlet var phoneQLabel: UILabel!
  @IBOutlet var emailAddressQLabel: UILabel!
  @IBOutlet var problemQLabel: UILabel!
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTextfieldAndTextView()
        self.submitButton.roundbutton(self.submitButton, cornerradius: 8)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        if height == 480.0
        {
            self.issueTextFieldHeightConstraint.constant = 30
            self.phoneTextFieldHeightConstraint.constant = 30
            self.emailTextFieldHeightConstraint.constant = 30
            self.firstLabelUpperConstraint.constant = 10
            self.secondLabelUpperConstraint.constant = 10
            self.thirdLabelUpperConstraint.constant = 10
            self.fourthLabelUpperConstraint.constant = 10
            self.submitButtonUpperConstraint.constant = 10
            self.submitButtonBottomSpaceConstraint.constant = 35.0
        }

      let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(_:)))
      singleTapGestureRecognizer.numberOfTapsRequired = 1
      singleTapGestureRecognizer.isEnabled = true
      singleTapGestureRecognizer.cancelsTouchesInView = false
      self.view.addGestureRecognizer(singleTapGestureRecognizer)
      
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.bluetoothStatusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.bluetoothStatusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
    }

  @objc func singleTap(_ sender: UITapGestureRecognizer) {
    self.view.endEditing(true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      contactUsTitleLabel.text = contactUsMenuKeyword.localized()
      regardingWhatQLabel.text = regardingQLabelKeyword.localized()
      phoneQLabel.text = phoneTypeQLabelKeyword.localized()
      emailAddressQLabel.text = emailQLabelKeyword.localized()
      problemQLabel.text = problemQLabelKeyword.localized()
      self.submitButton.setTitle(submitButtonTitleKeyword.localized(), for: UIControlState())
      regardingTextField.placeholder = makeSelectionPlaceholderKeyword.localized()
      phoneTypeTextField.placeholder = makeSelectionPlaceholderKeyword.localized()

  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.bluetoothStatusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.bluetoothStatusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  
    func setupTextfieldAndTextView()
    {
        self.submitButton.backgroundColor = UIColor.lightGray
        self.submitButton.isEnabled = false
      

        let borderColor = UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
        self.problemTextView.layer.borderColor = borderColor.cgColor
        self.problemTextView.layer.borderWidth = 1.0
        self.problemTextView.layer.cornerRadius = 5.0

        
        self.problemTextView.text = explainProblemInDetailMessageKeyword.localized()
        self.problemTextView.textColor = UIColor.init(red: 199/255.0, green: 199/255.0, blue: 205/255.0, alpha: 1.0)
        
        self.regardingTextField.tag = 1
        self.phoneTypeTextField.tag = 2
        
        pickerView.delegate = self
        self.regardingTextField.inputView = pickerView
        self.phoneTypeTextField.inputView = pickerView
        
        let h = regardingTextField.frame.size.height
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: h, height: h))
        let image = UIImage(named: "ContactUsDropDown")
        imageView.image = image
        imageView.sizeToFit()
        regardingTextField.rightView = imageView
        regardingTextField.rightViewMode = UITextFieldViewMode.always
      
        let h1 = phoneTypeTextField.frame.size.height
        let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: h1, height: h1))
        let image1 = UIImage(named: "ContactUsDropDown")
        imageView1.image = image1
        imageView1.sizeToFit()
//        phoneTypeTextField.rightView = imageView1
        phoneTypeTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    // MARK:- Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if (self.activeTextField.tag == 1)
        {
            return self.issueArray.count
        }
        else if (self.activeTextField.tag == 2)
        {
            return self.phoneArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if (self.activeTextField.tag == 1)
        {
            return self.issueArray[row]
        }
        else if (self.activeTextField.tag == 2)
        {
            return self.phoneArray[row]
        }
      
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if self.activeTextField.tag == 1
        {
            self.selectedIssue = (self.issueArray[row])
            self.regardingTextField.text = selectedIssue
            print("Selected Issue:-\(self.selectedIssue)")
        }
        else if self.activeTextField.tag == 2
        {
            self.selectedPhone = (self.phoneArray[row])
            self.phoneTypeTextField.text = selectedPhone
            print("Selected Phone:-\(self.selectedPhone)")
        }
        
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeTextField = textField
        print(activeTextField.tag)
        if activeTextField.tag == 1 || activeTextField.tag == 2
        {
            self.pickerView.reloadAllComponents()
        }
//        if  selectedIssue == "Make Selection" && activeTextField.tag == 1
//        {
//            self.pickerView.selectRow(0, inComponent: 0, animated: true)
//        }
//        if  selectedPhone == "Make Selection" && activeTextField.tag == 2
//        {
//            self.pickerView.selectRow(0, inComponent: 0, animated: true)
//        }

        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.regardingTextField.text != "" && self.regardingTextField.text != "Make Selection" && self.phoneTypeTextField.text != "" && self.phoneTypeTextField.text != "Make Selection" && self.emailTextField.text != "" && self.problemTextView.text != "" && self.problemTextView.text != explainProblemInDetailMessageKeyword.localized()
        {
            self.submitButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            self.submitButton.isEnabled = true
        }
        else
        {
            self.submitButton.backgroundColor = UIColor.lightGray
            self.submitButton.isEnabled = false
        }
//        if self.regardingTextField.text == "Make Selection"
//        {
//            self.regardingTextField.textColor = UIColor.init(colorLiteralRed: 199/255.0, green: 199/255.0, blue: 205/255.0, alpha: 1.0)
//        }
//        else
//        {
//            self.regardingTextField.textColor = UIColor.blackColor()
//        }
//        if self.phoneTypeTextField.text == "Make Selection"
//        {
//            self.phoneTypeTextField.textColor = UIColor.init(colorLiteralRed: 199/255.0, green: 199/255.0, blue: 205/255.0, alpha: 1.0)
// 
//        }
//        else
//        {
//            self.phoneTypeTextField.textColor = UIColor.blackColor()
//        }


    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK:- TextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.textColor == UIColor.init(red: 199/255.0, green: 199/255.0, blue: 205/255.0, alpha: 1.0)
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if self.regardingTextField.text != "" && self.regardingTextField.text != "Make Selection" && self.phoneTypeTextField.text != "" && self.phoneTypeTextField.text != "Make Selection" && self.emailTextField.text != "" && self.problemTextView.text != "" && self.problemTextView.text != explainProblemInDetailMessageKeyword.localized()
        {
            self.submitButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            self.submitButton.isEnabled = true
        }
        else
        {
            self.submitButton.backgroundColor = UIColor.lightGray
            self.submitButton.isEnabled = false
        }
        
    }
    
    // MARK:- Button Actions
    @IBAction func backButtonClicked(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonClicked(_ sender: AnyObject)
    {
        if (self.regardingTextField.text == "") || (self.phoneTypeTextField.text == "") || (self.emailTextField.text == "")
        {
            AlertModel.sharedInstance.showErrorMessage(allFieldsMandatoryMessageKeyword.localized())
        }
        else if !self.emailTextField.text!.isValidEmail()
        {
            AlertModel.sharedInstance.showErrorMessage(invalidEmailMessageKeyword.localized())
        }
        else
        {
            self.contactUsAPICall()
        }
    }
    
    // MARK:- API Function
    func contactUsAPICall()
    {
        let userId = UserDefaults.standard.object(forKey: CURRENT_USER_ID_UD)
        
        let dict: [String:AnyObject] = [MMRUserId : userId! as AnyObject , MMRRegardingType : self.selectedIssue as AnyObject, MMRPhoneType : self.selectedPhone as AnyObject, MMRUserEmail : self.emailTextField.text! as AnyObject, MMRUserQuery : self.problemTextView.text as AnyObject]
        print(dict)
        
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().contactUs(dict)
            DispatchQueue.main.async(execute: {
                self.stopActivityAnimating()
                if success{
                    let resultScreen = self.storyboard!.instantiateViewController(withIdentifier: "MMRContactResultController") as! MMRContactResultController
                    self.navigationController?.pushViewController(resultScreen, animated: true)
                }
            })
        }
    }
  
  func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
    if textView.text == ""{
      self.problemTextView.text = explainProblemInDetailMessageKeyword.localized()
      self.problemTextView.textColor = UIColor.init(red: 199/255.0, green: 199/255.0, blue: 205/255.0, alpha: 1.0)
    }
        return true
  }
  
}
