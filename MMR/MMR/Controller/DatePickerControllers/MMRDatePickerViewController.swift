//
//  MMRDatePickerViewController.swift
//  MMR
//
//  Created by Mindbowser on 12/6/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

enum PickerType : String {
    
    case DatePicker        = "datePicker"
    case TimePicker        = "timePicker"
    case DateTimePicker    = "dateTimePicker"
    
}

class MMRDatePickerViewController: UIViewController,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {
    @IBOutlet weak var distanceBetweenQuestionAndDatePickerButton: NSLayoutConstraint!
   
    @IBOutlet weak var timePickerButton: UIButton!
    @IBOutlet weak var datePickerButton: UIButton!
    @IBOutlet weak var navigationView: Navigation_View!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var datePickerScrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerScrollView: UIScrollView!
    @IBOutlet weak var questionTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var questionTextView: UITextView!
    
    @IBOutlet weak var compulsaryStarLabel: UILabel!
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var optionsArray         =  NSMutableArray ()
    var optionsIdArray         =  NSMutableArray ()
    var TypeOfPicker        = PickerType(rawValue: "")
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var isDateSelected       = Bool ()
    var isAnswerGiven        = Bool ()
    var pickerBackgroundView:UIView?
    var datePicker:UIDatePicker?
    var dateString           = ""
    var timeString           = ""
   // var timeString           = String()

    var isDateChanged        = false
    //var selectedDate         =
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
    override func viewDidLoad() {
        super.viewDidLoad()
            initSetUp()
        navigationSetup()
        self.setText()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
    }
  
  @objc func setText() {
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    self.timePickerButton.setTitle("Select Time".localized(), for: UIControlState())
    self.datePickerButton.setTitle("Select Date".localized(), for: UIControlState())
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: "BLE_HISTORY_NOTIFY_FLAG")
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func navigationSetup() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
        
//        navigationView.titleLabel.text = "Question " + "\(serialID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
        
        // Handled dismiss of keyboard on clicking on scroll view
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MMRDatePickerViewController.singleTap(_:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.datePickerScrollView.addGestureRecognizer(singleTapGestureRecognizer)
        
        self.datePickerButton.layer.borderColor = UIColor.lightGray.cgColor
        self.datePickerButton.layer.borderWidth = 2.0
        self.timePickerButton.layer.borderColor = UIColor.lightGray.cgColor
        self.timePickerButton.layer.borderWidth = 2.0
        
    }

    @objc func singleTap(_ sender: UITapGestureRecognizer) {
        if (pickerBackgroundView != nil) {
            pickerBackgroundView!.removeFromSuperview()
        }
    }
    
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.questionTextView.sizeThatFits(CGSize(width: self.questionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextViewHeight.constant = newSize.height
        
        self.datePickerScrollViewHeight.constant = self.view.frame.size.height - 64
        self.datePickerScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height)
        
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }

      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      print(inTime)
        let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists"{
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        self.optionsArray = []
        self.optionsIdArray = []
        let optionsTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      
        self.optionsArray = optionsTuple.optionText
        self.optionsIdArray = optionsTuple.optionId
        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(self.optionsArray)")
        
//        self.questionTextView.text   = questionFromServer[questionText] as! String
//        self.questionTextView.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
            let quesString = questionFromServer[questionText] as! String
            quesString.attributedStringFromHTML { (resultString) in
              self.questionTextView.attributedText = resultString
            }
        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        TypeOfPicker            = PickerType(rawValue: QuestionSharedClass.sharedInstance.fetchQuestionType(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject))
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButton.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            
        }
        if  TypeOfPicker?.rawValue == "datePicker" {
            // hide time picker button
            self.timePickerButton.isHidden = true
            
        }
        if  TypeOfPicker?.rawValue == "timePicker" {
            // hide date picker button and set constraint value to 0
            self.datePickerButton.isHidden = true
            self.distanceBetweenQuestionAndDatePickerButton.constant = 0

        }
        if  TypeOfPicker?.rawValue == "dateTimePicker" {
            // dont hide any button
        }
        
        
        
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func timeButtonClicked(_ sender: UIButton) {
        self.createDatepickerController(UIDatePickerMode.time)

    }
    @IBAction func dateButtonClicked(_ sender: UIButton) {
    self.createDatepickerController(UIDatePickerMode.date)
    
    }
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
//        let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//        self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)

        if isAnswerGiven {
          var ansStr = String()
            if self.timeString != nil && self.dateString != nil {
              let tempstrArray = self.timeString.components(separatedBy: " ")
              let newTimeString = tempstrArray[0]
              let tempStrArray2 = "\(Date())".components(separatedBy: "+")
              ansStr = "\(self.dateString)T\(newTimeString):00+\(tempStrArray2[1])"
          }
            else if self.timeString == nil && self.dateString != nil {
              ansStr = self.dateString
          }
          
            else if self.timeString != nil && self.dateString == nil {
              let tempstrArray = self.timeString.components(separatedBy: " ")
              let newTimeString = tempstrArray[0]
              let tempStrArray2 = "\(Date())".components(separatedBy: "+")
              ansStr = "\(newTimeString):00+\(tempStrArray2[1])"
          }
          
          print(ansStr)
            let val1 = String(format: "%d", serialID)
          let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
          if status == "Exists" {
            //Exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
          }
          
          if ansStr == ""{
            delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
            self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
          }
          else {
            delegate?.updateAnswerTextForQuestion(ansStr, serialNo: "\(val1)")
            self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
          }
          
          let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
          if isTimeLeftForSurvey {
            UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
            delegate?.fetchNextQuestionType()
          } else {
            UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
            BaseClassForQuestions.serialNo_ID = -1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            delegate?.fetchNextQuestionType()
          }
        }
    }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
  
    func createDatepickerController(_ contentMode:UIDatePickerMode) {
        if UIScreen.main.bounds.size.height < 568 {
            pickerBackgroundView = UIView(frame: CGRect(x: 0, y: 240, width: self.view.frame.size.width, height: 240))
            datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 196))
        }else {
            pickerBackgroundView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height - 260, width: self.view.frame.size.width, height: 260))
            datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 216))
        }
        pickerBackgroundView?.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1.0)
        datePicker?.datePickerMode = contentMode
        datePicker!.locale = Locale(identifier: "en_US")
        datePicker!.addTarget(self, action: #selector(MMRDatePickerViewController.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
        let titleLabel = UILabel(frame: CGRect(x: 70, y: 7, width: 180, height: 30))
        if contentMode == UIDatePickerMode.date {
            titleLabel.text = "Select Date"
            isDateSelected = true
        } else {
            titleLabel.text = "Select Time"
            isDateSelected = false
        }
        titleLabel.textAlignment = .center
        
        if contentMode == UIDatePickerMode.date {
            if isDateChanged{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                if dateString != nil
                {
                    let strDate = dateFormatter.date(from: dateString)
                    datePicker?.setDate(strDate!, animated: false)
                }
                
            }
        }
        //        let doneButton = UISegmentedControl(items: ["Done"])
        //        doneButton.momentary = true
        //        doneButton.frame = CGRect(x: (pickerBackgroundView?.frame.size.width)!-70, y: 7, width: 50, height: 30)
        //        doneButton.tintColor = UIColor.blueColor()
        //        doneButton.addTarget(self, action:#selector(ASDatePickerTool.doneButtonclick), forControlEvents: .TouchUpInside)
        //
        //        let cancelutton = UISegmentedControl(items: ["Cancel"])
        //        cancelutton.momentary = true
        //        cancelutton.frame = CGRect(x:10, y: 7, width: 50, height: 30)
        //        cancelutton.tintColor = UIColor.blueColor()
        //        cancelutton.addTarget(self, action: #selector(ASDatePickerTool.cancelButtonclick), forControlEvents: .TouchUpInside)
        
        let doneButton = UIButton(type:.system)
        doneButton.frame =  CGRect(x: (pickerBackgroundView?.frame.size.width)!-70, y: 7, width: 50, height: 30)
        doneButton.setTitle("Done".localized(), for: UIControlState())
        
        doneButton.addTarget(self, action:#selector(ASDatePickerTool.doneButtonclick), for: .touchUpInside)
        let cancelutton = UIButton(type:.system)
        cancelutton.setTitle("Cancel".localized(), for: UIControlState())
        
        cancelutton.frame =  CGRect(x:10, y: 7, width: 50, height: 30)
        cancelutton.addTarget(self, action: #selector(ASDatePickerTool.cancelButtonclick), for: .touchUpInside)
        
        pickerBackgroundView?.addSubview(datePicker!)
        pickerBackgroundView?.addSubview(titleLabel)
        pickerBackgroundView?.addSubview(doneButton)
        pickerBackgroundView?.addSubview(cancelutton)
        self.view.addSubview(pickerBackgroundView!)
    }
    @objc func datePickerValueChanged (_ sender : UIDatePicker) {
        if sender.datePickerMode == .date {
            isDateChanged = true
        }
        else
        {
            
        }
    }
    func doneButtonclick() {
        
        if isDateSelected {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: datePicker!.date)
            self.datePickerButton.setTitle(strDate, for: UIControlState())
            pickerBackgroundView?.removeFromSuperview()
            print("Selected Time :\(strDate)")
            dateString = strDate
        }
        if isDateSelected == false {
            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat =  "HH:mm:ss"
            datePicker?.locale = Locale(identifier: "en_US")
            dateFormatter.timeStyle = .short
            let strDate = dateFormatter.string(from: datePicker!.date)
            self.timePickerButton.setTitle(strDate, for: UIControlState())
            pickerBackgroundView?.removeFromSuperview()
            print("Selected Time :\(strDate)")
            timeString = strDate
        }
       
        self.CheckIfCompulsaryQuestionIsAnswered()
        
    }
    
    func CheckIfCompulsaryQuestionIsAnswered (){
        if isMandatory == true {
            if  TypeOfPicker?.rawValue == "datePicker" {
                self.stringContainsValue(dateString)
            }
                
            else if TypeOfPicker?.rawValue == "timePicker" {
                
                self.stringContainsValue(timeString)
                
            } else {
                if timeString != nil && dateString != nil{
                    isAnswerGiven = true
                    self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                } else {
                    isAnswerGiven = false
                    self.nextButton.backgroundColor = UIColor.lightGray
                }
            }
        }
        else {
            isAnswerGiven = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
    }
    
    func stringContainsValue(_ ansString : String?) {
        if ansString != nil {
            isAnswerGiven = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        } else {
            isAnswerGiven = false
            self.nextButton.backgroundColor = UIColor.lightGray
        }
    }
    
    /*             self.nextButton.backgroundColor = UIColor.lightGrayColor()
     self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0) */
    func cancelButtonclick() {
        print("Cancel Clicked")
        pickerBackgroundView!.removeFromSuperview()
        self.CheckIfCompulsaryQuestionIsAnswered()
    }
}
