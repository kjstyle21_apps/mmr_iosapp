//
//  MMRDiscriptiveTypeViewController.swift
//  MMR
//
//  Created by Mindbowser on 11/4/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRDiscriptiveTypeViewController: UIViewController,UITextViewDelegate,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {

    @IBOutlet weak var answerTextView: UITextView!
    @IBOutlet weak var nextButtonDiscriptiveType: UIButton!
    @IBOutlet weak var questionTextviewHeight: NSLayoutConstraint!
    @IBOutlet weak var discriptionScrollView: UIScrollView!
    @IBOutlet weak var discriptionQuestionTextView: UITextView!
    @IBOutlet weak var navigationView: Navigation_View!
    
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var compulsaryStarLabel: UILabel!
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var questionFromServer  = NSMutableDictionary ()
    let placeholderlabel    = UILabel ()
    var isAnswerGiven       = Bool ()
    var characterLimit      = Int ()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
        self.setText()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        navigationSetup()
        
        self.discriptionScrollView.delaysContentTouches = false
        self.discriptionScrollView.canCancelContentTouches = false
        
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }

  
  @objc func setText() {
    self.nextButtonDiscriptiveType.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    let questionID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(questionID!)"
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

    @objc func bluetoothStatus(_ notify : Notification) {
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationSetup() {
        
        let questionID = UserDefaults.standard.value(forKey: serialNoID)
//        navigationView.titleLabel.text = "Question " + "\(questionID!)"
      
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        
        self.nextButtonDiscriptiveType.roundbutton(self.nextButtonDiscriptiveType, cornerradius: 5)
        self.answerTextView.layer.cornerRadius = 5
        
        self.placeholderlabel.frame = CGRect(x: 5.0, y: 0.0,width: self.answerTextView.frame.size.width - 10.0, height: 34.0);
        self.placeholderlabel.font = UIFont (name: "tahoma", size: 14)
        self.placeholderlabel.text = kMMRDescriptionPlaceholder
        self.placeholderlabel.backgroundColor = UIColor.clear
        self.placeholderlabel.textColor = UIColor.lightGray
        self.answerTextView.delegate = self;
        self.answerTextView.addSubview(self.placeholderlabel)
        
    }
    
    @objc func leftButtonAction() {
        
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
        
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.discriptionQuestionTextView.sizeThatFits(CGSize(width: self.discriptionQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextviewHeight.constant = newSize.height
        
        self.scrollViewHeightConstraint.constant = self.view.frame.size.height - 64
        
        self.discriptionScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height + 200)
    }
    
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }
      
      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      print(inTime)
        let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists"{
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

         self.characterLimit = AnswerSharedClass.sharedInstance.fetchMaxCharForDiscriptiveType((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        

//        discriptionQuestionTextView.text = questionFromServer[questionText] as? String
//        discriptionQuestionTextView.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
            let quesString = questionFromServer[questionText] as! String
            quesString.attributedStringFromHTML { (resultString) in
              self.discriptionQuestionTextView.attributedText = resultString
            }
        self.isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButtonDiscriptiveType.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButtonDiscriptiveType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
        
        print("questionFromServer\(questionFromServer)")
//        print("optionsForQuestionFromServer\(optionsForQuestionFromServer)")
        
        // Handled dismiss of keyboard on clicking on scroll view
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MMRDiscriptiveTypeViewController.singleTap(_:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.discriptionScrollView.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    @objc func singleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    
    func validateText (_ answerText : String) -> Bool {
        if self.isMandatory {
            let descriptionText = self.answerTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            if descriptionText == "" {
                self.nextButtonDiscriptiveType.backgroundColor = UIColor.lightGray
                isAnswerGiven                   = true
                return false
            }
        } else {
            isAnswerGiven                   = true
            self.nextButtonDiscriptiveType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
        return true
    }
    @IBAction func nextButtonOfDisciptiveClicked(_ sender: UIButton) {
        let validation = validateText(self.answerTextView.description)
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
//      let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
        self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
      if validation {
            if self.answerTextView.text == "" {
              delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
              delegate?.updateAnswerTextForQuestion(self.answerTextView.text!, serialNo: "\(val1)")
            }
            else {
              delegate?.updateAnswerTextForQuestion(self.answerTextView.text!, serialNo: "\(val1)")
              }
        let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
        if status == "Exists" {
          //Exists
          QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
        }
        
        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
        if isTimeLeftForSurvey {
          UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
          delegate?.fetchNextQuestionType()
        } else {
          UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
          BaseClassForQuestions.serialNo_ID = -1
          UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
          delegate?.fetchNextQuestionType()

      }
    }
      
  }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
  
    // MARK: -  Textview Delegates
    //    func textView(textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    //        if textView == self.answerTextView {
    //
    //
    //        // Combine the textView text and the replacement text to
    //        // create the updated text string
    //        let currentText = textView.text
    //
    //        // If updated text view will be empty, add the placeholder
    //        // and set the cursor to the beginning of the text view
    //        if currentText.isEmpty {
    //
    //            textView.text = "Placeholder"
    //            textView.textColor = UIColor.lightGrayColor()
    //
    //
    //
    //            return false
    //        }
    //
    //            // Else if the text view's placeholder is showing and the
    //            // length of the replacement string is greater than 0, clear
    //            // the text view and set its color to black to prepare for
    //            // the user's entry
    //        else if textView.textColor == UIColor.lightGrayColor() && !text.isEmpty {
    //            textView.text = nil
    //            textView.textColor = UIColor.blackColor()
    //        }
    //    }
    //        return true
    //    }
    //    func textViewDidChangeSelection(textView: UITextView) {
    //        if self.view.window != nil {
    //            if textView.textColor == UIColor.lightGrayColor() {
    //            }
    //        }
    //    }
    //
    //    func textViewDidBeginEditing(textView: UITextView) {
    //        if textView == self.answerTextView {
    //        if textView.textColor == UIColor.lightGrayColor() {
    //            textView.text = nil
    //            textView.textColor = UIColor.blackColor()
    //        }
    //    }
    //    }
    //
    //    func textViewDidEndEditing(textView: UITextView) {
    //        if textView == self.answerTextView {
    //        if textView.text.isEmpty {
    //            textView.text = "Placeholder"
    //            textView.textColor = UIColor.lightGrayColor()
    //        }
    //    }
    //    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.answerTextView {
            if !textView.hasText {
                self.placeholderlabel.isHidden = false
                isAnswerGiven                   = false
              if self.isMandatory {
                self.nextButtonDiscriptiveType.backgroundColor = UIColor.lightGray
              }
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == self.answerTextView {
            if !textView.hasText {
                self.placeholderlabel.isHidden    = false
                isAnswerGiven                   = false
              if self.isMandatory {
                self.nextButtonDiscriptiveType.backgroundColor = UIColor.lightGray
              }
            } else
            {
                self.placeholderlabel.isHidden    = true
                isAnswerGiven                   = true
                self.nextButtonDiscriptiveType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        return numberOfChars <= self.characterLimit;
    }
  
  func goToHomeScreen () {
    var viewController: UIViewController?
    
    for aViewController in self.navigationController!.viewControllers {
      if aViewController.isKind(of: SSASideMenu.self) {
        viewController = aViewController
        break
      }
    }
    
    if let viewController = viewController {
      self.navigationController!.popToViewController(viewController, animated: true)
    }
    
  }
  
}



