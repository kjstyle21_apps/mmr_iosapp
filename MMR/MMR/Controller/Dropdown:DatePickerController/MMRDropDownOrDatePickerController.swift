//
//  MMRDropDownOrDatePickerController.swift
//  MMR
//
//  Created by Mindbowser on 11/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit


class MMRDropDownOrDatePickerController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var dropDownButtonText: UIButton!
    @IBOutlet weak var CompulsaryTypeOfLabel: UILabel!
    @IBOutlet weak var heightOfScrollView: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var dropDownScrollView: UIScrollView!
    @IBOutlet weak var heightOfQuestionView: NSLayoutConstraint!
    @IBOutlet weak var questionTextViewDropDown: UITextView!
    @IBOutlet weak var navigationView: Navigation_View!
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var optionsArray        =  NSMutableArray ()
    var optionsIdArray = NSMutableArray()
    var TypeOfPicker        = PickerType(rawValue: "")
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var isAnswerGiven       = Bool ()
    var labelString         = String ()
    var selected            = String()
   // var picker              = UIPickerView ()
    var selectedAnswer     = String ()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
    var orActionArray = [[String]]()
    var singleActionArray = [[String]]()
    var andActionArray = [[String]]()
    var orNextQuestion = [Int]()
    var andNextQuestion = [Int]()
    var singleNextQuestion = [Int]()
    var notNextQuestion = [Int]()
    var notActionArray = [[String]]()
  
    override func viewDidLoad() {
      super.viewDidLoad()
      initSetUp()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        navigationSetup()
        self.setText()
        // Do any additional setup after loading the view.
    }

  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  @objc func setText() {
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationSetup() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
//        navigationView.titleLabel.text = "Question " + "\(serialID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        let val1 = String(format: "%d", serialID)
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.questionTextViewDropDown.sizeThatFits(CGSize(width: self.questionTextViewDropDown.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.heightOfQuestionView.constant = newSize.height
        
        self.heightOfScrollView.constant = self.view.frame.size.height - 64
        self.dropDownScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height)
        
    }
    
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }
      
      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

      print(inTime)
        let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists" {
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        self.optionsArray = []
        self.optionsIdArray = []
      
        let optionsTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      
        self.optionsArray = optionsTuple.optionText
        self.optionsIdArray = optionsTuple.optionId
      
        print("\(questionFromServer[labelText])")
        self.labelString = questionFromServer[labelText] as! String
      if self.labelString == "" {
        self.labelString = "Make Selection"
      }
        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(self.optionsArray)")
        
//        self.questionTextViewDropDown.text   = questionFromServer[questionText] as! String
      
        self.dropDownButtonText.setTitle(self.labelString, for: UIControlState())

        self.optionsArray.insert(self.labelString, at: 0)
        print(optionsArray)
//        self.questionTextViewDropDown.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
            let quesString = questionFromServer[questionText] as! String
            quesString.attributedStringFromHTML { (resultString) in
              self.questionTextViewDropDown.attributedText = resultString
            }
        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        
        if self.isMandatory {
            self.CompulsaryTypeOfLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButton.backgroundColor = UIColor.lightGray
        } else {
            self.CompulsaryTypeOfLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            
        }
        
        self.dropDownButtonText.layer.borderColor = UIColor.lightGray.cgColor
        self.dropDownButtonText.layer.borderWidth = 2.0

      let routingQuestionID = (questionFromServer["Question_ID"] as! NSString).integerValue
        let routeArray = QuestionSharedClass.sharedInstance.fetchRoutingForQuestionID("\(userID!)", questionID: NSNumber(value: routingQuestionID),fetchedSurveyID: currentSurveyID! as AnyObject) as[[String : String]]
      if routeArray.count > 0
      {
        for tempRoute in routeArray {
          let neededArrayString = tempRoute[selectedOptions]
          let neededArray = neededArrayString?.components(separatedBy: ",")
          let action = tempRoute[actionName]!
          
          switch action {
          case "OR":
            self.orActionArray.append(neededArray!)
//            self.orActionArray = neededArray!
            let nextQuestion = tempRoute[nextQuestionID]!
//            self.orNextQuestion = Int(nextQuestion)!
            self.orNextQuestion.append(Int(nextQuestion)!)
            break
            
          case "AND":
            self.andActionArray.append(neededArray!)
//            self.andActionArray = neededArray!
            let nextQuestion = tempRoute[nextQuestionID]!
//            self.andNextQuestion = Int(nextQuestion)!
            self.andNextQuestion.append(Int(nextQuestion)!)
            break
            
          case "SINGLE":
            self.singleActionArray.append(neededArray!)
//            self.singleActionArray = neededArray!
            let nextQuestion = tempRoute[nextQuestionID]!
//            self.singleNextQuestion = Int(nextQuestion)!
            self.singleNextQuestion.append(Int(nextQuestion)!)
            break
            
          case "NOT":
            self.notActionArray.append(neededArray!)
//            self.notActionArray = neededArray!
            let nextQuestion = tempRoute[nextQuestionID]!
//            self.notNextQuestion = Int(nextQuestion)!
            self.notNextQuestion.append(Int(nextQuestion)!)
            break
          default:
            break
          }
        }
        
        print(self.singleActionArray)
        print(self.singleActionArray.reversed())
      }
    }
  
  
    // Mark:- Picker Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.optionsArray.count
    }
      
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.optionsArray[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Selected Answer:-\(self.selectedAnswer)")
        self.selectedAnswer = (self.optionsArray[row] as? String)!
        print("Selected Answer 2:-\(self.selectedAnswer)")

    }

    func cancelPicker() {
        print("Cancel clicked")
       // picker.hidden = true
       // self.dropDownButtonText.setTitle("Make Selection",forState: UIControlState.Normal)

    }
    func donePicker() {
        
      print("Done clicked")
       // picker.hidden = true

    }
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
        
    }
    @IBAction func nextButtonClicked(_ sender: UIButton) {
      var goflag = false
      if self.isMandatory {
        if self.selected == self.labelString || self.selected == "" {
          goflag = false
          self.isAnswerGiven = false
          self.nextButton.backgroundColor = UIColor.lightGray
        }
        else {
          goflag = true
        }
      }
      else {
        goflag = true
      }
      
      if goflag {
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
//        let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//        self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)

          self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
        if isAnswerGiven {
            if self.selected == ""{
              delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
          }
            else {
            delegate?.updateAnswerTextForQuestion(self.selected, serialNo: "\(val1)")
          }
          let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)", userID: userID! as AnyObject)
          if status == "Exists" {
            //Exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
          }
          
          let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
          if isTimeLeftForSurvey {
            
            let arr = [self.selected]
            
            var matchFlag = false
            if matchFlag == false {
              var cnt1 = 0
            for temp in self.andActionArray {
              cnt1 = cnt1 + 1
              if self.containSameElements(arr, temp) {
                matchFlag = true
                delegate?.fetchNextQuestionTypeWithSerialID(andNextQuestion[cnt1 - 1])
                break
              }
            }
            }
            
            if matchFlag == false {
              var cnt2 = 0
              for temp in self.orActionArray {
                cnt2 = cnt2 + 1
                if self.containSomeSimilarElements([self.selected], array2: temp) {
                  matchFlag = true
                  delegate?.fetchNextQuestionTypeWithSerialID(orNextQuestion[cnt2 - 1])
                  break
                }
              }
            }
            
            if matchFlag == false {
              var cnt3 = 0
              for temp in self.singleActionArray {
                cnt3 = cnt3 + 1
                if self.containSomeSimilarElements([self.selected], array2: temp) {
                  matchFlag = true
                  delegate?.fetchNextQuestionTypeWithSerialID(singleNextQuestion[cnt3 - 1])
                  break
                }
              }
            }
            
            if matchFlag == false {
              var cnt4 = 0
              for temp in self.notActionArray {
                cnt4 = cnt4 + 1
                if !self.containSomeSimilarElements([self.selected], array2: temp) {
                  matchFlag = true
                  delegate?.fetchNextQuestionTypeWithSerialID(notNextQuestion[cnt4 - 1])
                  break
                }
              }
            }
            
            if matchFlag == false {
              delegate?.fetchNextQuestionType()
            }
            
            UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)

//            if self.selected == "" {
//              delegate?.fetchNextQuestionType()
//            }
//            else if self.containSameElements(arr, self.andActionArray) {
//              delegate?.fetchNextQuestionTypeWithSerialID(andNextQuestion)
//            }
//            else if self.containSomeSimilarElements([self.selected], array2: self.orActionArray) {
//              delegate?.fetchNextQuestionTypeWithSerialID(orNextQuestion)
//            }
//            else if self.containSomeSimilarElements([self.selected], array2: self.singleActionArray) {
//              delegate?.fetchNextQuestionTypeWithSerialID(singleNextQuestion)
//            }
//            else if self.containSomeSimilarElements([self.selected], array2: self.notActionArray) {
//              delegate?.fetchNextQuestionTypeWithSerialID(notNextQuestion)
//            }
//            else {
//              delegate?.fetchNextQuestionType()
//            }
            
            
            ///delegate?.fetchNextQuestionType()
          } else {
            UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
            BaseClassForQuestions.serialNo_ID = -1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            delegate?.fetchNextQuestionType()
          }
        }
      }
    }

  
  func containSameElements<T: Comparable>(_ array1: [T], _ array2: [T]) -> Bool {
    guard array1.count == array2.count else {
      return false // No need to sorting if they already have different counts
    }
    return array1.sorted() == array2.sorted()
  }
  
  func containSomeSimilarElements(_ array1: [String],array2: [String]) -> Bool {
    //    guard array1.count == array2.count else {
    //      return false // No need to sorting if they already have different counts
    //    }
    
    for temp in array2 {
      if array1.contains(temp) {
        return true
      }
    }
    return false
  }
  
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
  
    @IBAction func dropDownButtonClicked(_ sender: UIButton) {
        
        PickerTools.shareModel().showPickerView(with: self, withContent: self.optionsArray as [AnyObject],andSelectedString: sender.titleLabel?.text) { (selected, status) in
            if status == true {
                if self.isMandatory == true {
                    print(selected)
                    let index = self.optionsArray.index(of: selected)
                  
                  if index == 0{
                    sender.setTitle(selected, for: UIControlState())
                    self.isAnswerGiven = false
                    self.nextButton.backgroundColor = UIColor.lightGray
                  }
                  else {
                  
                    self.selected = self.optionsIdArray.object(at: index - 1) as! String
                        if selected != self.labelString {
                            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                            print("selected: \(selected)")
                            self.isAnswerGiven = true
                            sender.setTitle(selected, for: UIControlState())

                        }
                        else {
                            sender.setTitle(selected, for: UIControlState())
                            self.isAnswerGiven = false
                            self.nextButton.backgroundColor = UIColor.lightGray
                        }
                      }
                    }
                else {
                  print(selected)
                  let index = self.optionsArray.index(of: selected)
                  if index == 0 {
                    sender.setTitle(selected, for: UIControlState())
                    self.isAnswerGiven = true
//                    self.nextButton.backgroundColor = UIColor.lightGrayColor()
                  }
                  else {
                    
                    self.selected = self.optionsIdArray.object(at: index - 1) as! String
                    if selected != self.labelString {
                      self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                      print("selected: \(selected)")
                      self.isAnswerGiven = true
                      sender.setTitle(selected, for: UIControlState())
                      
                    }
                    else {
                      sender.setTitle(selected, for: UIControlState())
                      self.isAnswerGiven = false
                      self.nextButton.backgroundColor = UIColor.lightGray
                    }
                  }

              }
                  }
                else {
                    self.isAnswerGiven = true
                    self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                }
               
                
            }
        }
        
//        if sender.titleLabel?.text == "Make Selection" {
//            // save the date for your need
//            picker.hidden = false
//            self.dropDownButtonText.setTitle(selectedAnswer,forState: UIControlState.Normal)
//        } else {
//            picker.hidden = true
//            self.dropDownButtonText.setTitle("Make Selection",forState: UIControlState.Normal)
//        }
        
        
    
}
