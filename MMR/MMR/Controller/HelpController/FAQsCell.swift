//
//  FAQsCell.swift
//  MMR
//
//  Created by Mindbowser on 11/14/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class FAQsCell: UITableViewCell {

  @IBOutlet var questionNumberLabel: UILabel!
  
  @IBOutlet var questionLabel: UILabel!
  @IBOutlet var answerLabel: UILabel!
  @IBOutlet var answerTextLabel: UILabel!
  
//  @IBOutlet var answerTextview: UITextView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
//      questionLabel.sizeToFit()
//      answerLabel.sizeToFit()
        // Initialization code
    }

  override func layoutSubviews() {
    super.layoutSubviews()
    answerLabel.sizeToFit()
    questionLabel.sizeToFit()
  }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
