//
//  MMRHelpMenuViewController.swift
//  MMR
//
//  Created by Mindbowser on 11/14/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

//class MMRHelpMenuViewController: UIViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {

class MMRHelpMenuViewController: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    
    @IBOutlet var tutorialsButton: UIButton!
    @IBOutlet var faqsButton: UIButton!
    @IBOutlet var tutorialButtonUnderlineView: UIView!
    @IBOutlet var faqButtonUnderlineView: UIView!
    
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet var navigationView: Navigation_View!
    
    @IBOutlet var faqTableView: UITableView!
    
    var pageTitles = [String]()
    var pageImages = [String]()
    var pageViewController = UIPageViewController()
    var nextPageCount : NSInteger = 0
    var currentPageCount : NSInteger = 0
    
    var questionArray = [String]()
    var answerArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //oldInitialSetup()
        initialSetup()
        navigationSetup()
        self.setText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getfaqsAPICall()
    }
    
    func initialSetup()  {
        tutorialsButton.isHidden = true
        tutorialsButton.isUserInteractionEnabled = false
        tutorialButtonUnderlineView.isHidden = false
        faqButtonUnderlineView.isHidden = false
        pageControl.isHidden = true
        pageViewController.view.removeFromSuperview()
        
        faqTableView.isHidden = false
        faqTableView.estimatedRowHeight = 400.0
        faqTableView.rowHeight = UITableViewAutomaticDimension
        faqsButton.isUserInteractionEnabled = false
    }
    
    /*
    func oldInitialSetup()  {
        tutorialButtonUnderlineView.isHidden = false
        faqButtonUnderlineView.isHidden = true
        faqTableView.isHidden = true
        tutorialsButton.layer.borderWidth = 1.0
        tutorialsButton.layer.borderColor = UIColor.lightGray.cgColor
        faqsButton.layer.borderWidth = 1.0
        faqsButton.layer.borderColor = UIColor.lightGray.cgColor
        tutorialButtonUnderlineView.backgroundColor = UIColor.lightGray
    }
    */
 
    @objc func setText() {
        navigationView.titleLabel.text = helpMenuKeyword.localized()
        //        self.tutorialsButton.setTitle(tutorialsButtonTitleKeyword.localized(), for: UIControlState())
        self.faqsButton.setTitle(FAQsButtonTitleKeyword.localized(), for: UIControlState())
    }
    
    func navigationSetup() {
        navigationView.leftButton! .setImage(UIImage (named: "backButton"), for: UIControlState())
        navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        navigationView.progressView.isHidden = true
    }
    
    @objc func leftButtonAction() {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func tutorialsClicked(_ sender: AnyObject) {
        /*
         pageViewController.view.removeFromSuperview()
         tutorialButtonUnderlineView.isHidden = false
         tutorialButtonUnderlineView.backgroundColor = UIColor.lightGray
         faqButtonUnderlineView.isHidden = true
         faqTableView.isHidden = true
         pageControl.isHidden = false
         pageViewControllSetup()
         */
    }
    
    @IBAction func faqClicked(_ sender: AnyObject) {
        /*
         tutorialButtonUnderlineView.isHidden = true
         faqButtonUnderlineView.isHidden = false
         pageControl.isHidden = true
         pageViewController.view.removeFromSuperview()
         
         faqTableView.isHidden = false
         faqTableView.estimatedRowHeight = 400.0
         faqTableView.rowHeight = UITableViewAutomaticDimension
         faqTableView.reloadData()
         */
    }
    
    /*
     
     //UIPageViewController for Tutorials
     func pageViewControllSetup() {
     pageTitles = ["GetStartedContentViewController","HelperScreenTwo","HelperScreenThree"]
     // Create page view controller
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     self.pageViewController = storyboard.instantiateViewController(withIdentifier: "GetStartedPageViewController") as! UIPageViewController
     self.pageViewController.dataSource = self
     self.pageViewController.delegate   = self
     
     // Change the size of page view controller
     let y = faqsButton.frame.size.height + 64
     self.pageViewController.view.frame = CGRect(x: 0, y: y, width: self.view.frame.size.width, height: self.view.frame.size.height - y);
     
     currentPageCount = -1
     nextPageCount = 0
     var startingViewController = UIViewController()
     startingViewController = self.viewControllerAtIndex(0)!
     
     let viewControllers = [startingViewController];
     self.pageViewController.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
     
     
     self.addChildViewController(pageViewController)
     self.view.addSubview(pageViewController.view)
     
     self.pageViewController.didMove(toParentViewController: self)
     
     pageControl.currentPage = 0
     self.view.bringSubview(toFront: self.pageControl)
     
     modifypageControl()
     
     }
     
     override func didReceiveMemoryWarning() {
     super.didReceiveMemoryWarning()
     // Dispose of any resources that can be recreated.
     }
     
     
     // MARK: - PageViewControllerDataSource
     
     func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
     
     let identifier = viewController.restorationIdentifier
     var index = self.pageTitles.index(of: identifier!)!
     print("index back = \(index)")
     
     if index == 0 {
     currentPageCount = -1
     nextPageCount = index
     }
     
     if index == 0 || index == NSNotFound {
     return nil
     }
     
     index -= 1
     currentPageCount = index-1
     nextPageCount = index
     return self.viewControllerAtIndex(index)
     }
     
     func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
     
     let identifier = viewController.restorationIdentifier
     var index = self.pageTitles.index(of: identifier!)!
     
     if index == NSNotFound {
     return nil;
     }
     
     index += 1
     if index == self.pageTitles.count {
     return nil;
     }
     return self.viewControllerAtIndex(index)
     }
     
     func viewControllerAtIndex(_ index: Int) -> UIViewController? {
     if self.pageTitles.count == 0 || index >= self.pageTitles.count {
     return nil
     }
     self.pageControl.numberOfPages = 3
     let identifier = self.pageTitles[index]
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
     
     return viewController
     }
     
     func presentationIndex(for pageViewController: UIPageViewController) -> Int {
     if nextPageCount != -1 {
     return nextPageCount
     }
     return 0
     }
     
     func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
     
     if (!completed)
     {
     return
     }
     let currentPageIndex = pageViewController.viewControllers!.first!.view.tag
     pageControl.currentPage = currentPageIndex
     self.modifypageControl()
     
     }
     
     func modifypageControl(){
     for i in 0 ..< 3 {
     let dot : UIView = pageControl.subviews[i]
     
     if (i == pageControl.currentPage) {
     dot.backgroundColor = UIColor.white
     dot.layer.cornerRadius = dot.frame.size.height / 2;
     
     } else {
     dot.backgroundColor = UIColor.clear
     dot.layer.cornerRadius = dot.frame.size.height / 2;
     dot.layer.borderColor = UIColor.white.cgColor;
     dot.layer.borderWidth = 1;
     }
     }
     }
     */
    
    //UITableViewController for FAQs
    //Tableview Delegate and Datasource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = faqTableView.dequeueReusableCell(withIdentifier: "FAQsCell", for: indexPath) as! FAQsCell
        cell.questionLabel.text = questionArray[indexPath.row]
        cell.answerLabel.text = answerArray[indexPath.row]
        cell.questionNumberLabel.text = "Q\(indexPath.row + 1)."
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return faqTableView.rowHeight
    }
    
    //Get FAQs API call
    func getfaqsAPICall() {
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().getFaqs()
            print("FAQs Response : \(success)")
            DispatchQueue.main.async(execute: {
                self.stopActivityAnimating()
                
                if success.isKind(of: NSDictionary.self){
                    //self.pageViewControllSetup()
                    let faqsArray = success.object(forKey: MMRRServerObject) as! [NSDictionary]
                    self.questionArray.removeAll()
                    self.answerArray.removeAll()
                    for faq in faqsArray{
                        self.questionArray.append(faq.object(forKey: MMRFaqsQuestion) as! String)
                        self.answerArray.append(faq.object(forKey: MMRFaqsAnswer) as! String)
                    }
                    self.faqTableView.reloadData()
                }
            })
        }
    }
}
