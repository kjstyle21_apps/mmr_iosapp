//
//  HomeViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/18/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import Instructions

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

enum NSComparisonResult : Int {
    case orderedAscending
    case orderedSame
    case orderedDescending
}

class HomeViewController: UIViewController,NVActivityIndicatorViewable, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    @IBOutlet weak var areYouUsingProduct: UILabel!
    @IBOutlet weak var surveyNameLabel: UILabel!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    let coachMarksController = CoachMarksController()
    
    @IBOutlet var heightOfSurveyTitleLabel: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfHomeTextView: NSLayoutConstraint!
    @IBOutlet weak var homeTextView: UITextView!
    @IBOutlet var noButtonBottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet var areYouUsingHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var discriptionBoldlabel: UILabel!
    var labelForNoSurvey = UILabel ()
    @IBOutlet var refreshButton: UIButton!
    
    @IBOutlet var bluetoothStatusLabel: UILabel!
    var localNotification:UILocalNotification?
    
    @IBOutlet var descriptionLabelHeight: NSLayoutConstraint!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet weak var mmrLogoImageView: UIImageView!
    
    var currentTimeStr = String()
    
    //MARK:- ViewController LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setText()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        if let userRole = UserDefaults.standard.value(forKey: CURRENT_USER_ROLE_UD) as? String {
            if userRole == "ADMIN" {
                self.refreshButton.isHidden = false
            }
            else {
                //commenting for disabling sync
                self.refreshButton.isHidden = false
                self.refreshButton.tag = 2
                
                //            self.refreshButton.hidden = true
            }
        } else {
            self.refreshButton.isHidden = false
            self.refreshButton.tag = 2
            
            //        self.refreshButton.hidden = true
        }
        
        labelForNoSurvey = UILabel(frame: CGRect(x: 0, y: ((self.view.frame.size.height - 12) / 2) + 21, width: self.view.frame.size.width - 12, height: 42))
        labelForNoSurvey.textAlignment = NSTextAlignment.center
        labelForNoSurvey.numberOfLines = 0
        labelForNoSurvey.font = UIFont(name: TAHOMA_REGULAR, size: 14.0)
        self.view.addSubview(labelForNoSurvey)
        self.coachMarksController.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Code Needed once Push implementation done.
        
        //      let curDate = NSDate()
        //      let dateFormatter = NSDateFormatter()
        //      dateFormatter.dateFormat = "h:mm a"
        //      let date = dateFormatter.stringFromDate(curDate)
        //      print(date)
        //      dateFormatter.dateFormat = "HH:mm"
        //      let date24 = dateFormatter.stringFromDate(curDate)
        //      print(date24)
        
        
        labelForNoSurvey.text = ""
        if Localize.currentLanguage() == "nl" {
            labelForNoSurvey.text = "Geen enquêtes worden aan u toegewezen"
        }
        else {
            labelForNoSurvey.text = "Currently no surveys are assigned to you".localized()
        }
        
        
        labelForNoSurvey.isHidden = true
        
        self.homeSetUp()
        self.homeScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        if let noSurvey  = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
            if noSurvey == false {
                //Uncomment after the ble integration is used
                //          (UIApplication.sharedApplication().delegate as! AppDelegate).startDiscovery()
            }
        }
    }
    
    func homeSetUp() {
        if let noSurvey  = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
            if  noSurvey == true {
                self.areYouUsingProduct.isHidden  = true
                self.surveyNameLabel.isHidden     = true
                self.yesButton.isHidden           = true
                self.noButton.isHidden            = true
                self.homeTextView.isHidden        = true
                self.discriptionBoldlabel.isHidden = true
                labelForNoSurvey.isHidden = false
                
            } else {
                labelForNoSurvey.isHidden = true
                self.areYouUsingProduct.isHidden  = false
                self.surveyNameLabel.isHidden     = false
                self.yesButton.isHidden           = false
                self.noButton.isHidden            = false
                self.homeTextView.isHidden        = false
                self.discriptionBoldlabel.isHidden = false
                self.yesButton.roundbutton(self.yesButton, cornerradius: 5)
                self.noButton.roundbutton(self.noButton, cornerradius: 5)
                
                if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
                    if status {
                        self.surveyNameLabel.text = Tour_Survey_Title.localized()
                    } else {
                        if let productName = UserDefaults.standard.value(forKey: surveyName) as? String {
                            self.surveyNameLabel.text = productName
                        }
                    }
                } else {
                    if let productName = UserDefaults.standard.value(forKey: surveyName) as? String {
                        self.surveyNameLabel.text = productName
                    }
                }
                
                if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
                    if status {
                        //                self.areYouUsingProduct.text = Tour_Welcome_Question_Text.localized()
                        //                self.areYouUsingProduct.text = Tour_Desc_Text.localized()
                        self.areYouUsingProduct.text = Tour_Desc_New_Text.localized()
                    } else {
                        if let areYouUsingText = UserDefaults.standard.value(forKey: surveyIntroductoryQuestion) as? String {
                            self.areYouUsingProduct.text = areYouUsingText
                        }
                    }
                } else {
                    if let areYouUsingText = UserDefaults.standard.value(forKey: surveyIntroductoryQuestion) as? String {
                        self.areYouUsingProduct.text = areYouUsingText
                    }
                }
                if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
                    if status {
                        self.homeTextView.text = Tour_Desc_Text.localized()
                    } else {
                        if let descriptionText = UserDefaults.standard.value(forKey: surveyBriefDescription) as? String {
                            self.homeTextView.text = descriptionText
                            if descriptionText == "" {
                                self.discriptionBoldlabel.isHidden = true
                            }
                        }
                    }
                } else {
                    if let descriptionText = UserDefaults.standard.value(forKey: surveyBriefDescription) as? String {
                        self.homeTextView.text = descriptionText
                        if descriptionText == "" {
                            self.discriptionBoldlabel.isHidden = true
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let currentTime = Date()
        currentTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(currentTime)
        NotificationCenter.default.addObserver(self, selector: #selector(updateSurveyApicall), name: NSNotification.Name(rawValue: "MMR_APP_ACTIVE_STATE_NOTIFICATION"), object: nil)
        //    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateEndTime), name: "UPDATE_INTERVAL_END_TIME", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(triggerSurvey), name: NSNotification.Name(rawValue: "TRIGGER_SURVEY"), object: nil)
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            
            if BMCentralManager.sharedInstance().isCentralReady == true {
                self.bluetoothStatusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.bluetoothStatusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
            NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        }
        let userId = UserDefaults.standard.value(forKey: currentUserId)
        if !SurveyRecordsSharedClass.sharedInstance.isAnySurveyPresent(userId! as AnyObject) {
            UserDefaults.standard.setValue(true, forKey: NoSurveyForUser)
            if let noSurvey  = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
                if noSurvey {
                    if let status = UserDefaults.standard.bool(forKey: API_EXECUTION_ON_UD) as? Bool {
                        if !status {
                            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                            let flag = SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID! as AnyObject)
                            if !flag {
                                self.getSurveyApiCall(false)
                            }
                                //MULTIPLE_ONLINE CHANGE
                            else {
                                //handle else part
                                UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                if (UserDefaults.standard.value(forKey: localPushTime) != nil) && (UserDefaults.standard.value(forKey: maxPushNo) != nil) && (UserDefaults.standard.value(forKey: pushInterval) != nil) && (UserDefaults.standard.value(forKey: pushNotificationText) != nil) {
                                    self.fireUILocalnotificationWithMessage()
                                }
                                NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
                                self.homeSetUp()
                            }
                        }
                    }
                    else {
                        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                        let flag = SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID! as AnyObject)
                        if !flag {
                            self.getSurveyApiCall(false)
                        }
                        else {
                            //handle else part
                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                            if (UserDefaults.standard.value(forKey: localPushTime) != nil) && (UserDefaults.standard.value(forKey: maxPushNo) != nil) && (UserDefaults.standard.value(forKey: pushInterval) != nil) && (UserDefaults.standard.value(forKey: pushNotificationText) != nil) {
                                self.fireUILocalnotificationWithMessage()
                            }
                            NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
                            self.homeSetUp()
                        }
                    }
                }
            }
        } else {
            //***
            if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                if interval > 0 {
                    //start timer
                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    if !appDel.surveyTimerSet {
                        if (UserDefaults.standard.value(forKey: SURVEY_TIMER_STOP_DATE_UD) == nil) {
                            appDel.surveyTimerSet = true
                            appDel.surveyTimer = Timer.scheduledTimer(timeInterval: interval, target: appDel, selector: #selector(appDel.updateForSurveyTimer), userInfo: nil, repeats: false)
                            UserDefaults.standard.set(Date(), forKey: SURVEY_TIMER_START_DATE_UD)
                            UserDefaults.standard.set(TimeInterval(interval), forKey: SURVEY_TIMER_COMPLETION_SECs_UD)
                        }
                    }
                    self.homeSetUp()
                } else {
                    //@kj
                     if let userSurveyIntervalTime = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                     if (userSurveyIntervalTime <= 0){
                     self.refreshActionCall()
                     }
                     }else{
                     self.refreshActionCall()
                     }
                    /*
                    //start trigger
                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                    let flag = SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID! as AnyObject)
                    self.homeSetUp()
                    */
                }
            }
        }
        //***
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Start_Tour"), object: nil, queue: OperationQueue.main) { (notify) in
            UserDefaults.standard.set(true, forKey: Start_Tour_UD)
            //@kj
            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
//            if let isNilSurvey = UserDefaults.standard.value(forKey: IS_NIL_SURVEY) as? Bool{
//                if (isNilSurvey){
//                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
//                }else{
//                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
//                }
//            }
            self.homeSetUp()
            self.coachMarksController.start(in: PresentationContext.viewController(self))
        }
        
        if let againFlag = UserDefaults.standard.value(forKey: Welcome_Screen_Again_UD) as? Bool {
            //@kj
            if let isNilSurvey = UserDefaults.standard.value(forKey: IS_NIL_SURVEY) as? Bool{
                if (isNilSurvey){
                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                }else{
                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                }
            }
            self.homeSetUp()
            self.coachMarksController.start(in: PresentationContext.viewController(self))
        }
    }
    
    @objc func triggerSurvey() {
        UserDefaults.standard.set(false, forKey: NoSurveyForUser)
        self.homeSetUp()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MMR_APP_ACTIVE_STATE_NOTIFICATION"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
        self.bluetoothStatusLabel.isHidden = true
        self.coachMarksController.stop(immediately: true)
    }
    
    @objc func bluetoothStatus(_ notify : Notification) {
        let userinfo = notify.userInfo as! NSDictionary
        
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            if userinfo.value(forKey: "state") as! String == "ON" {
                self.bluetoothStatusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.bluetoothStatusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
        }
    }
    
    @objc func onFunc() {
        print("Bluetooth is on reconnect the device")
        UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
        UserDefaults.standard.synchronize()
        MMRBLEModule.sharedInstance.startBLEFunctionality()
    }
    
    func offFunc() {
        print("Bluetooth off device disconnected")
    }
    
    
    @objc func updateSurveyApicall() {
        let currentTime = Date()
        currentTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(currentTime)
        //check do we have any survey in db
        let userId = UserDefaults.standard.value(forKey: currentUserId)
        if !SurveyRecordsSharedClass.sharedInstance.isAnySurveyPresent(userId! as AnyObject) {
            if let noSurvey  = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
                if noSurvey {
                    if let status = UserDefaults.standard.bool(forKey: API_EXECUTION_ON_UD) as? Bool {
                        if !status {
                            self.getSurveyApiCall(false)
                        }
                    }
                    else {
                        self.getSurveyApiCall(false)
                    }
                }
            }
        } else {
            if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                if interval > 0 {
                    //start timer
                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    if !appDel.surveyTimerSet {
                        if (UserDefaults.standard.value(forKey: SURVEY_TIMER_STOP_DATE_UD) == nil) {
                            appDel.surveyTimerSet = true
                            appDel.surveyTimer = Timer.scheduledTimer(timeInterval: interval, target: appDel, selector: #selector(appDel.updateForSurveyTimer), userInfo: nil, repeats: false)
                            UserDefaults.standard.set(Date(), forKey: SURVEY_TIMER_START_DATE_UD)
                            UserDefaults.standard.set(TimeInterval(interval), forKey: SURVEY_TIMER_COMPLETION_SECs_UD)
                        }
                    }
                    self.homeSetUp()
                } else {
                    //start trigger
                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                    let flag = SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID! as AnyObject)
                    
                    self.homeSetUp()
                }
            }
        }
    }
    
    //    func updateEndTime() {
    //        let currentTime = NSDate()
    //        currentTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(currentTime)
    //    }
    
    
    @objc func setText() {
        discriptionBoldlabel.text = homeVCDescriptionLabelKeyword.localized()
        self.yesButton.setTitle(homeVCYesButtonTitleKeyword.localized(), for: UIControlState())
        self.noButton.setTitle(homeVCNOButtonTitleKeyword.localized(), for: UIControlState())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.homeTextView.sizeThatFits(CGSize(width: self.homeTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.heightOfHomeTextView.constant = newSize.height
        if self.homeTextView.text == "" {
            self.heightOfHomeTextView.constant = 0.0
            self.descriptionLabelHeight.constant = 0.0
        }
        
        let newSize3 : CGSize = self.surveyNameLabel.sizeThatFits(CGSize(width: self.surveyNameLabel.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize3)
        self.heightOfSurveyTitleLabel.constant = newSize3.height
        
        
        let newSize2 : CGSize = self.areYouUsingProduct.sizeThatFits(CGSize(width: self.areYouUsingProduct.frame.size.width, height: CGFloat(FLT_MAX)))
        self.areYouUsingHeightConstraint.constant = newSize2.height
        self.homeScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height+200)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showSideMenu(_ sender: AnyObject) {
        sideMenuViewController?._presentLeftMenuViewController()
    }
    
    func getSurveyApiCall(_ isRefreshFlag : Bool) {
        startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            UserDefaults.standard.set(true, forKey: API_EXECUTION_ON_UD)
            UserDefaults.standard.synchronize()
            let success = APIManager().getUserSurvey()
            DispatchQueue.main.async(execute: {
                self.stopActivityAnimating()
                print("Get Survey Response : \(String(describing: success))")
                
                if (success != nil) {
                    if isRefreshFlag && self.refreshButton.tag != 2 {
                        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.removeObject(forKey: "\(userID!)")
                        UserDefaults.standard.synchronize()
                        QuestionSharedClass.sharedInstance.deleteQuestionDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
                        QuestionDropRecordSharedClass.sharedInstance.deleteQuestionDropDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
                        AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
                        QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: "" as AnyObject)
                        QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
                        SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID! as AnyObject,surveyID: "" as AnyObject)
                        if let tempFlag = UserDefaults.standard.value(forKey: MULTIPLE_FLAG_UD) as? Bool {
                            if !tempFlag {
                                QuestionSharedClass.sharedInstance.deleteImagesAndVideos("\(userID)")
                            }
                            else {
                                MMRMediaUploader.sharedInstance.cancelAll()
                                UploadRecordSharedClass.sharedInstance.deleteImagesAndVideosUploadRecord("\(userID!)")
                            }
                        }
                        else {
                            QuestionSharedClass.sharedInstance.deleteImagesAndVideos("\(userID)")
                        }
                        BaseClassForQuestions.serialNo_ID = 1
                        BLERecordsSharedClass.sharedInstance.deleteBLERecord(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
                        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
                        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
                        MMRBLEModule.sharedInstance.disconnectMMRDevice()
                        (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
                        UIApplication.shared.cancelAllLocalNotifications()
                        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID!)")
                    } else if isRefreshFlag && self.refreshButton.tag == 2 {
                        //sync process begins
                        self.syncProcess()
                    }
                    
                    var userInfo = [String:AnyObject]()
                    
                    userInfo = (success?.value(forKey: "object"))! as! [String : AnyObject]
                    
                    //          if let temp = userInfo[assignedSurvey]![noSurveyMessage] as? String {
                    //           print(temp)
                    //          }
                    
                    //          let noSurverAssigned = userInfo[assignedSurvey]![noSurveyMessage] as? String
                    let noSurverAssigned = userInfo[noSurveyMessage] as? String
                    if noSurverAssigned?.characters.count > 0 {
                        
                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        UserDefaults.standard.synchronize()
                        
                        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                        
                        if let sameUserFlagStatus = UserDefaults.standard.value(forKey: SAME_USER_FLAG_STATUS_UD) as? Bool {
                            if sameUserFlagStatus {
                                UploadRecordSharedClass.sharedInstance.deleteImagesAndVideosUploadRecord("\(userID)")
                                QuestionSharedClass.sharedInstance.deleteQuestionDataOfUploadedSurvey("\(userID)" as AnyObject,fetchedSurveyID: "" as AnyObject)
                                QuestionDropRecordSharedClass.sharedInstance.deleteQuestionDropDataOfUploadedSurvey("\(userID)" as AnyObject,fetchedSurveyID: "" as AnyObject)
                                AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey("\(userID)" as AnyObject, surveyID: "" as AnyObject)
                            }
                        }
                        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
                    }
                    else  {
                        //MULTIPLE_ONLINE CHANGE
                        SurveyRecordsSharedClass.sharedInstance.parseSurveyDetails(userInfo)
                        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                        if self.refreshButton.tag != 2 {
                            UserDefaults.standard.set(1, forKey: "SURVEY_SEQ_NUM_\(userID!)")
                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                            UserDefaults.standard.synchronize()
                        }
                        if let arrayOfSurveys = userInfo[assignedSurvey] as? [[String : AnyObject]] {
                            for fetchedSurvey in arrayOfSurveys {
                                if let questionList = fetchedSurvey[questions] as? [AnyObject] {
                                    for questionInfo in questionList {
                                        var surveyIdFetched : AnyObject?
                                        if let surveyID = fetchedSurvey[surveyId]  {
                                            surveyIdFetched = surveyID
                                        }
                                        if let info = questionInfo as? [String:AnyObject] {
                                            QuestionSharedClass.sharedInstance.parseDictionary(info,surveyIDFetched: surveyIdFetched!)
                                        }
                                    }
                                }
                                
                                if let tempPushArray = fetchedSurvey[surveyLocalPush] as? [[String : AnyObject]] {
                                    for tempPushObj in tempPushArray {
                                        self.basicLocalPushHandler(tempPushObj)
                                    }
                                }
                            }
                        }
                        
                        if (UserDefaults.standard.value(forKey: localPushTime) != nil) && (UserDefaults.standard.value(forKey: maxPushNo) != nil) && (UserDefaults.standard.value(forKey: pushInterval) != nil) && (UserDefaults.standard.value(forKey: pushNotificationText) != nil) {
                            self.fireUILocalnotificationWithMessage()
                        }
                        if let surveyIntervalObj = userInfo[surveyIntervalData] as? [[String : AnyObject]] {
                            if surveyIntervalObj.count > 0 {
                                let intervalMins = surveyIntervalObj.first![userSurveyIntervalTime]
                                UserDefaults.standard.setValue(intervalMins, forKey: surveyIntervalData)
                            } else {
                                UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                            }
                        } else {
                            UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                        }
                    }
                    //compare the survey starttime with current time
                    // if its interval is true then startTimer upto SurveyStartTime
                    //else start the survey
                    
                    if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                        let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                        if interval > 0 {
                            //start timer
                            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            let appDel = UIApplication.shared.delegate as! AppDelegate
                            //                    appDel.surveyTimer = NSTimer.scheduledTimerWithTimeInterval(interval, target: appDel, selector: #selector(appDel.updateForSurveyTimer), userInfo: nil, repeats: false)
                            
                            if (UserDefaults.standard.value(forKey: SURVEY_TIMER_STOP_DATE_UD) == nil) {
                                appDel.surveyTimerSet = true
                                appDel.surveyTimer = Timer.scheduledTimer(timeInterval: interval, target: appDel, selector: #selector(appDel.updateForSurveyTimer), userInfo: nil, repeats: false)
                                UserDefaults.standard.set(Date(), forKey: SURVEY_TIMER_START_DATE_UD)
                                UserDefaults.standard.set(TimeInterval(interval), forKey: SURVEY_TIMER_COMPLETION_SECs_UD)
                            }
                            
                            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                            if let lastques = UserDefaults.standard.value(forKey: "\(userID!)_Tour_Flag") as? Bool {
                                //No need to show Tour
                            } else {
                                UserDefaults.standard.set(true, forKey: "\(userID!)_Tour_Flag")
                                UserDefaults.standard.set(true, forKey: Start_Tour_UD)
                                self.coachMarksController.start(in: PresentationContext.viewController(self))
                            }
                            self.homeSetUp()
                        } else {
                            //start trigger
                            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                            if let lastques = UserDefaults.standard.value(forKey: "\(userID!)_Tour_Flag") as? Bool {
                                //No need to show Tour
                            } else {
                                UserDefaults.standard.set(true, forKey: "\(userID!)_Tour_Flag")
                                UserDefaults.standard.set(true, forKey: Start_Tour_UD)
                                //@kj
                                UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                self.coachMarksController.start(in: PresentationContext.viewController(self))
                            }
                            self.homeSetUp()
                        }
                    }
                }
                UserDefaults.standard.set(false, forKey: API_EXECUTION_ON_UD)
                UserDefaults.standard.synchronize()
            })
        }
    }
    
    func fireUILocalnotificationWithMessage() {
        var localPushTimeUD = String()
        var localPushInterval = String()
        var localPushMessage = String()
        var numberOfPush = NSNumber()
        if let templocalPushTime = UserDefaults.standard.value(forKey: localPushTime) {
            localPushTimeUD = templocalPushTime as! String
        }
        if let templocalPushInterval = UserDefaults.standard.value(forKey: pushInterval) {
            localPushInterval = templocalPushInterval as! String
        }
        if let templocalPushMessage = UserDefaults.standard.value(forKey: pushNotificationText) {
            localPushMessage = templocalPushMessage as! String
        }
        if let tempnumberOfPush = UserDefaults.standard.value(forKey: maxPushNo) {
            numberOfPush = tempnumberOfPush as! NSNumber
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone.autoupdatingCurrent
        let datetime = String(format: "%@",formatter.string(from: Date()))
        print("date:\(datetime)")
        self.logger(msg: "\n\(localPushMessage) : \(datetime)\n")
        let dtComp = datetime.components(separatedBy: " ")
        let newdateStr = String(format: "%@ %@:00", dtComp[0],localPushTimeUD)
        //    let sec = self.HoursMinutesToSeconds(localPushTimeUD)
        //    let date = NSDate().dateByAddingTimeInterval(Double(sec))
        let date = formatter.date(from: newdateStr)
        let localNotification = UILocalNotification()
        localNotification.alertBody = localPushMessage
        if #available(iOS 8.2, *) {
            localNotification.alertTitle = "MMR"
        } else {
            // Fallback on earlier versions
        }
        let diff = date!.timeIntervalSince(Date())
        localNotification.fireDate = Date(timeIntervalSinceNow: Double(diff))
        var firstFireDate = localNotification.fireDate
        localNotification.repeatInterval = .day
        UIApplication.shared.scheduleLocalNotification(localNotification)
        if numberOfPush.intValue > 1 {
            for index in 2...numberOfPush.intValue {
                let localNotification = UILocalNotification()
                localNotification.alertBody = localPushMessage
                if #available(iOS 8.2, *) {
                    localNotification.alertTitle = "MMR"
                } else {
                    // Fallback on earlier versions
                }
                let extraSeconds = Int(localPushInterval)! * 60
                let dateNew = firstFireDate!.addingTimeInterval(Double(extraSeconds))
                
                let diff = dateNew.timeIntervalSince(Date())
                localNotification.fireDate = Date(timeIntervalSinceNow: Double(diff))
                firstFireDate = localNotification.fireDate!
                localNotification.repeatInterval = .day
                UIApplication.shared.scheduleLocalNotification(localNotification)
            }
        }
    }
    
    func basicLocalPushHandler(_ pushObj : [String:AnyObject]) {
        if let temppushtime = pushObj[pushTime] as? String {
            if QuestionSharedClass.sharedInstance.compareTwoDatesforPush(temppushtime) && QuestionSharedClass.sharedInstance.compareBeforeDatesforPush(temppushtime) {
                let pushTime = temppushtime
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                let actualPushdate = dateFormatter.date(from: pushTime)
                
                if let tempPushText = pushObj[pushNotificationText] as? String {
                    let pushText = tempPushText
                    self.logger(msg: "\n\(pushText) : \(pushTime)\n")
                    let localNotification = UILocalNotification()
                    localNotification.alertBody = pushText
                    if #available(iOS 8.2, *) {
                        localNotification.alertTitle = "MMR"
                    } else {
                        // Fallback on earlier versions
                    }
                    let diff = actualPushdate!.timeIntervalSince(Date())
                    localNotification.fireDate = Date(timeIntervalSinceNow: Double(diff))
                    UIApplication.shared.scheduleLocalNotification(localNotification)
                }
            }
        }
    }
    
    func HoursMinutesToSeconds (_ timeStr : String) -> Int {
        let timeStrArr = timeStr.components(separatedBy: ":")
        var hr = 0
        var min = 0
        var sec = 0
        if timeStrArr.count > 2 {
            hr = Int(timeStrArr[0])!
            min = Int(timeStrArr[1])!
            sec = hr * 3600 + min * 60
        }
        return sec
    }
    
    @IBAction func refreshAction(_ sender: AnyObject) {
//        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//        let surveyID = UserDefaults.standard.value(forKey: surveyId)
//        SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID as AnyObject, surveyID: surveyID as AnyObject)
        
        //@kj
        if let userSurveyIntervalTime = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
            if (userSurveyIntervalTime <= 0){
               self.refreshActionCall()
            }
        }else{
            self.refreshActionCall()
        }
    }
    
    func refreshActionCall() {
        //@kj
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID as AnyObject, surveyID: surveyID as AnyObject)
 
        if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
            if !status {
                self.getSurveyApiCall(true)
            } else {
                coachMarksController.flow.showNext()
            }
        } else {
            self.getSurveyApiCall(true)
        }
    }
    
    @IBAction func noAction(_ sender: AnyObject) {
        if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
            if !status {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "GOTO_CONTACT_US"), object: nil)
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "GOTO_CONTACT_US"), object: nil)
        }
    }
    
    @IBAction func yesAction(_ sender: UIButton) {
        if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
            if status {
                //Tour
                UserDefaults.standard.set(true, forKey: Start_Tour_UD)
                let base = BaseClassForQuestions()
                base.typeOfCurrentQuestion = QuestionType.TourQuestion
                self.navigationController?.pushViewController(base, animated: true)
            } else {
                let instructionControl = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewController(withIdentifier: "MMRInstructionsAndTips") as? MMRInstructionsAndTips
                self.navigationController?.pushViewController(instructionControl!, animated: true)
            }
        } else {
            let instructionControl = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewController(withIdentifier: "MMRInstructionsAndTips") as? MMRInstructionsAndTips
            self.navigationController?.pushViewController(instructionControl!, animated: true)
        }
    }
    
    func syncProcess() {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        
        //If the survey is Multiple and submitted online atleast once and yet to end so dont delete the questions, options, routing details and push details for that particular survey.
     /*
        if UserDefaults.standard.bool(forKey: "CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE") {
         AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurveyExcludingCurrentSurvey(userID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingExcludingCurrentSurvey(userID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteQuestionData_CopyExcludingCurrentSurvey(userID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetailsExcludingCurrentSurvey(userID! as AnyObject)
                } else {
       */ AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
        QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: "" as AnyObject)
        QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
        SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetails(userID! as AnyObject, surveyID: "" as AnyObject)
        //        }
        //        MMRMediaUploader.sharedInstance.cancelAll()
        UIApplication.shared.cancelAllLocalNotifications()
        (UIApplication.shared.delegate as? AppDelegate)!.surveyTimerSet = false
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_STOP_DATE_UD)
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_START_DATE_UD)
        UserDefaults.standard.removeObject(forKey: SURVEY_TIMER_COMPLETION_SECs_UD)
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        if (UserDefaults.standard.value(forKey: Welcome_Screen_Again_UD) as? Bool) != nil {
            return 1
        } else {
            return 4
        }
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {
        switch index {
        case 0:
            return coachMarksController.helper.makeCoachMark(for: self.mmrLogoImageView)
        case 1:
            return coachMarksController.helper.makeCoachMark(for: self.menuButton)
        case 2:
            return coachMarksController.helper.makeCoachMark(for: self.refreshButton)
        case 3:
            return coachMarksController.helper.makeCoachMark(for: self.yesButton)
        default:
            return coachMarksController.helper.makeCoachMark()
        }
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        var arrowView : CoachMarkArrowView?
        let coachMarkBodyView = TransparentCoachMarkBodyView()
        var coachMarkArrowView: TransparentCoachMarkArrowView? = nil
        if index == 0 {
            if let againFlag = UserDefaults.standard.value(forKey: Welcome_Screen_Again_UD) as? Bool {
                coachViews.bodyView.hintLabel.text = Tour_Welcome_Screen_Text_again.localized()
                UserDefaults.standard.set(false, forKey: Start_Tour_UD)
                UserDefaults.standard.removeObject(forKey: Welcome_Screen_Again_UD)
                self.homeSetUp()
            } else {
                coachViews.bodyView.hintLabel.text = Tour_Text1.localized()
            }
        } else if index == 1 {
            coachViews.bodyView.hintLabel.text = Tour_Menu_Button_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        } else if index == 2 {
            coachViews.bodyView.hintLabel.text = Tour_Update_Button_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        } else if index == 3 {
            coachViews.bodyView.hintLabel.text = Tour_Yes_Button_Text.localized()
            coachViews.bodyView.nextLabel.text = nil
        }
        if let arrowOrientation = coachMark.arrowOrientation {
            coachMarkArrowView = TransparentCoachMarkArrowView(orientation: arrowOrientation)
        }
        arrowView = coachMarkArrowView
        return (bodyView: coachViews.bodyView, arrowView: arrowView)
    }
    
    func logger(msg: String) {
        //Get the file path
        var documentsDirectory: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        var fileName: String = (URL(string: "\(NSHomeDirectory())/Documents/mmrLog.txt")?.absoluteString)!
        //create file if it doesn't exist
        if !FileManager.default.fileExists(atPath: fileName) {
            FileManager.default.createFile(atPath: fileName, contents: nil, attributes: nil)
        }
        //append text to file (you'll probably want to add a newline every write)
        var file: FileHandle? = FileHandle(forUpdatingAtPath: fileName)
        file?.seekToEndOfFile()
        file?.write(msg.data(using: String.Encoding.utf8)!)
        file?.closeFile()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
