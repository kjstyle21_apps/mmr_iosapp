//
//  SideMenuViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/19/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import MessageUI

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable, MFMailComposeViewControllerDelegate {
  
  var titles = [String]()

  lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    let bounds = UIScreen.main.bounds
    let height = bounds.size.height
    
    switch height {
    case 480.0:
        print("iPhone 3,4")
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 40 * 3) / 2.0, width: self.view.frame.size.width, height: 40 * 8)
    case 568.0:
        print("iPhone 5")
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 40 * 4) / 2.0, width: self.view.frame.size.width, height: 40 * 8)
    case 667.0:
        print("iPhone 6")
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 40 * 4) / 2.0, width: self.view.frame.size.width, height: 40 * 8)
    case 736.0:
        print("iPhone 6+")
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 40 * 4) / 2.0, width: self.view.frame.size.width, height: 40 * 8)
    default:
        print("not an iPhone")
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 40 * 4) / 2.0, width: self.view.frame.size.width, height: 40 * 8)
    }
    
    
    tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.isOpaque = false
    tableView.backgroundColor = UIColor.clear
    tableView.backgroundView = nil
    tableView.bounces = false
    return tableView
  }()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setText()
    view.backgroundColor = UIColor.clear
    view.addSubview(tableView)
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "GOTO_CONTACT_US"), object: nil, queue: OperationQueue.main) { (notify) in
      print("GOTO_CONTACT_US")
//      self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0), animated: true, scrollPosition: UITableViewScrollPosition.None)
      let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
      let contactUs = storyboard.instantiateViewController(withIdentifier: "MMRContactUsController") as! MMRContactUsController
      self.navigationController?.pushViewController(contactUs, animated: true)
    }
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "NAVIGATE_TO_INTERVAL"), object: nil, queue: OperationQueue.main) { (notify) in
        let base = BaseClassForQuestions()
        self.navigationController?.pushViewController(base, animated: true)
    }
    
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)

  }
  
  @objc func setText() {
//    self.titles = [changeLanguageMenuKeyword.localized(), changePasswordMenuKeyword.localized(), helpMenuKeyword.localized(), contactUsMenuKeyword.localized(), logoutMenuKeyword.localized(), Tour.localized(),"Send Log"]
    self.titles = [changeLanguageMenuKeyword.localized(), changePasswordMenuKeyword.localized(), Tour.localized(), helpMenuKeyword.localized(), contactUsMenuKeyword.localized(), logoutMenuKeyword.localized(),"Send Log"]
    
    tableView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 7
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 44
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
    
    cell.backgroundColor = UIColor.clear
    cell.textLabel?.font = UIFont(name: "tahoma", size: 16)
    cell.textLabel?.textColor = UIColor.init(red: 29.0/255.0, green: 29.0/255.0, blue: 38.0/255.0, alpha: 1)
    cell.textLabel?.text  = self.titles[indexPath.row]
    cell.selectionStyle = .none
//    cell.imageView?.image = UIImage(named: images[indexPath.row])
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    tableView.deselectRow(at: indexPath, animated: true)
    
    switch indexPath.row {
      
    case 0: //Change Language
      sideMenuViewController?.hideMenuViewController()
      if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
        if !status {
            let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
            let changeLanguageViewController = storyboard.instantiateViewController(withIdentifier: "MMRChangeLanguageViewController") as! MMRChangeLanguageViewController
            self.navigationController?.pushViewController(changeLanguageViewController, animated: true)
        }
      } else {
        let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
        let changeLanguageViewController = storyboard.instantiateViewController(withIdentifier: "MMRChangeLanguageViewController") as! MMRChangeLanguageViewController
        self.navigationController?.pushViewController(changeLanguageViewController, animated: true)
      }
      break
        
    case 1: //Change Password
      sideMenuViewController?.hideMenuViewController()
      if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
        if !status {
            let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
            let changePasswordController = storyboard.instantiateViewController(withIdentifier: "MMRChangePasswordController") as! MMRChangePasswordController
            self.navigationController?.pushViewController(changePasswordController, animated: true)
        }
      } else {
        let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
        let changePasswordController = storyboard.instantiateViewController(withIdentifier: "MMRChangePasswordController") as! MMRChangePasswordController
        self.navigationController?.pushViewController(changePasswordController, animated: true)
      }
      break
     
    case 2: //Tour
        if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
            if !status {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Start_Tour"), object: nil, userInfo: nil)
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Start_Tour"), object: nil, userInfo: nil)
        }
        sideMenuViewController?.hideMenuViewController()
        break
        
    case 3: //Help
      sideMenuViewController?.hideMenuViewController()
      if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
        if !status {
            let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
            let helpMenuViewController = storyboard.instantiateViewController(withIdentifier: "MMRHelpMenuViewController") as! MMRHelpMenuViewController
            self.navigationController?.pushViewController(helpMenuViewController, animated: true)
        }
      } else {
        let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
        let helpMenuViewController = storyboard.instantiateViewController(withIdentifier: "MMRHelpMenuViewController") as! MMRHelpMenuViewController
        self.navigationController?.pushViewController(helpMenuViewController, animated: true)
      }
      break
      
    case 4: //Contact Us
      sideMenuViewController?.hideMenuViewController()
      if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
        if !status {
            let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
            let contactUs = storyboard.instantiateViewController(withIdentifier: "MMRContactUsController") as! MMRContactUsController
            self.navigationController?.pushViewController(contactUs, animated: true)
        }
      } else {
        let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
        let contactUs = storyboard.instantiateViewController(withIdentifier: "MMRContactUsController") as! MMRContactUsController
        self.navigationController?.pushViewController(contactUs, animated: true)
      }
      break
      
    case 5: // Logout
        if let status = UserDefaults.standard.value(forKey: Start_Tour_UD) as? Bool {
            if !status {
                let alertController = UIAlertController(title: MMRProjectName, message: "Are you sure you want to logout ?".localized(), preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "OK".localized(), style: .default) { (action:UIAlertAction!) in
                    self.sideMenuViewController?.hideMenuViewController()
                    self.logoutAPICall()
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            } else {
                sideMenuViewController?.hideMenuViewController()
            }
        } else {
            let alertController = UIAlertController(title: MMRProjectName, message: "Are you sure you want to logout ?".localized(), preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "OK".localized(), style: .default) { (action:UIAlertAction!) in
                self.sideMenuViewController?.hideMenuViewController()
                self.logoutAPICall()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        break
        
    case 6:
        self.emailAction()
        break
    default:
      break
    }
  }
  
  func logoutAPICall() {
    self.view.isUserInteractionEnabled = false
    startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : false)
    
        let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      let success = APIManager().logoutUser()
      DispatchQueue.main.async(execute: {
        self.stopActivityAnimating()
        self.view.isUserInteractionEnabled = true

        if success{
          //NSUserDefaults.standardUserDefaults().setBool(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY")
          MMRMediaUploader.sharedInstance.cancelAll()
          UserDefaults.standard.set(nil, forKey: MMRServiceUserInfoAuthToken)
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
          if let lastques = UserDefaults.standard.value(forKey: serialNoID) as? NSInteger {
            UserDefaults.standard.set(lastques, forKey: "\(userID!)")
            UserDefaults.standard.set(1, forKey: serialNoID)
          }
//          let deleteResult = AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID!)
//          if deleteResult {0
//            NSUserDefaults.standardUserDefaults().setValue("", forKey: "CURRENT_USER_ID")
            UserDefaults.standard.removeObject(forKey: CURRENT_USER_ID_UD)
            UserDefaults.standard.removeObject(forKey: authToken_UD)
//          }
          
//          Localize.resetCurrentLanguageToDefault()
//          Localize.setCurrentLanguage("en")
          if let tempNoSurveyFlag = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
            if !tempNoSurveyFlag {
              UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
              UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
              MMRBLEModule.sharedInstance.disconnectMMRDevice()
              (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
            }
          }
          
          UserDefaults.standard.removeObject(forKey: SelectedRow_UD)
          UserDefaults.standard.set(false, forKey: API_EXECUTION_ON_UD)
          UserDefaults.standard.synchronize()

          var loginViewController: UIViewController?
          
          if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
              if viewController.isKind(of: StartOfTourViewController.self) {
                
                (viewController as! StartOfTourViewController).isFromHome = true
                loginViewController = viewController
                
                break
              }
            }
          }
          
          if let loginViewController = loginViewController {
            
            (loginViewController as! StartOfTourViewController).isFromHome = true
            
            self.navigationController?.popToViewController(loginViewController, animated: true)
          } else {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let mmrloginViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "StartOfTourViewController")
            (mmrloginViewController as! StartOfTourViewController).isFromHome = true
            self.navigationController?.setViewControllers([mmrloginViewController], animated: true)
          }
          (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
          UIApplication.shared.cancelAllLocalNotifications()
        }
      })
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }

    func readLog() -> String? {
        var documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        var fileName: String = (URL(string: "\(NSHomeDirectory())/Documents/mmrLog.txt")?.absoluteString)!
        //read the whole file as a single string
        var content = try? String(contentsOfFile: fileName, encoding: .utf8)
        return content
    }
    
    func emailAction() {
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
    }

    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["gaurav.thorat@mindbowser.com"])
        mailComposerVC.setSubject("Log file for MMR iOS")
        if let tempStr = self.readLog() as? String {
            mailComposerVC.setMessageBody(tempStr, isHTML: false)
        }
        return mailComposerVC
    }
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
