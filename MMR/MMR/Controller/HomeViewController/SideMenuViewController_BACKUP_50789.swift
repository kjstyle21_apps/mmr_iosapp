//
//  SideMenuViewController.swift
//  MMR
//
//  Created by Mindbowser on 10/19/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

import UIKit

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
  
  
  lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .None
    
    let bounds = UIScreen.mainScreen().bounds
    let height = bounds.size.height
    
    switch height {
    case 480.0:
        print("iPhone 3,4")
        tableView.frame = CGRectMake(20, (self.view.frame.size.height - 40 * 3) / 2.0, self.view.frame.size.width, 40 * 6)
    case 568.0:
        print("iPhone 5")
        tableView.frame = CGRectMake(20, (self.view.frame.size.height - 40 * 4) / 2.0, self.view.frame.size.width, 40 * 6)
    case 667.0:
        print("iPhone 6")
        tableView.frame = CGRectMake(20, (self.view.frame.size.height - 40 * 4) / 2.0, self.view.frame.size.width, 40 * 6)
    case 736.0:
        print("iPhone 6+")
        tableView.frame = CGRectMake(20, (self.view.frame.size.height - 40 * 4) / 2.0, self.view.frame.size.width, 40 * 6)
    default:
        print("not an iPhone")
        tableView.frame = CGRectMake(20, (self.view.frame.size.height - 40 * 4) / 2.0, self.view.frame.size.width, 40 * 6)
    }
    
    
    tableView.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleWidth]
    tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.opaque = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.backgroundView = nil
    tableView.bounces = false
    return tableView
  }()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.clearColor()
    view.addSubview(tableView)
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 44
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
    
<<<<<<< HEAD
    let titles: [String] = [homeMenuKeyword,changeLanguageMenuKeyword, changePasswordMenuKeyword, helpMenuKeyword, contactUsMenuKeyword, logoutMenuKeyword]
=======
    let titles: [String] = ["Change Language", "Change Password", "Help", "Contact Us", "Logout"]
>>>>>>> AW_MMR-225-ios-local-database
    
//    let images: [String] = ["IconHome", "IconCalendar", "IconProfile", "IconSettings", "IconEmpty"]
    
    cell.backgroundColor = UIColor.clearColor()
    cell.textLabel?.font = UIFont(name: "Tahoma", size: 16)
    cell.textLabel?.textColor = UIColor.init(red: 29.0/255.0, green: 29.0/255.0, blue: 38.0/255.0, alpha: 1)
    cell.textLabel?.text  = titles[indexPath.row]
    cell.selectionStyle = .None
//    cell.imageView?.image = UIImage(named: images[indexPath.row])
    
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    
    switch indexPath.row {
//    case 0:
//      sideMenuViewController?.hideMenuViewController()
//      break
      
    case 0:
      sideMenuViewController?.hideMenuViewController()
      break
      
    case 1:
      //change language menu
      
      sideMenuViewController?.hideMenuViewController()
      let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
      let changeLanguageViewController = storyboard.instantiateViewControllerWithIdentifier("MMRChangeLanguageViewController") as! MMRChangeLanguageViewController
      self.navigationController?.pushViewController(changeLanguageViewController, animated: true)

      
      break
      
    case 2:
      sideMenuViewController?.hideMenuViewController()
      break
      
    case 3:
      sideMenuViewController?.hideMenuViewController()
      let storyboard = UIStoryboard(name: "MMRSecondaryStoryboard", bundle: nil)
      let helpMenuViewController = storyboard.instantiateViewControllerWithIdentifier("MMRHelpMenuViewController") as! MMRHelpMenuViewController
      self.navigationController?.pushViewController(helpMenuViewController, animated: true)

      break
      
    case 4:   //hit logout api
        
                  let alertController = UIAlertController(title: MMRProjectName, message: "Are you sure you want to logout ?", preferredStyle: .Alert)
                  
                  let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
                  }
                  alertController.addAction(cancelAction)
                  
                  let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in

                    
                    self.logoutAPICall()
                  }
                  alertController.addAction(OKAction)
                  self.presentViewController(alertController, animated: true, completion:nil)

                  break
    default:
      break
    }

  }
  
  
  func logoutAPICall() {
    self.view.userInteractionEnabled = false
    startActivityAnimating(CGSize(width: 50,height: 50), message: "", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor())

        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
    dispatch_async(dispatch_get_global_queue(priority, 0)) {
      let success = APIManager().logoutUser()
      dispatch_async(dispatch_get_main_queue(), {
        self.stopActivityAnimating()
        self.view.userInteractionEnabled = true

        if success{
          
          NSUserDefaults.standardUserDefaults().setObject(nil, forKey: MMRServiceUserInfoAuthToken)
          
          var loginViewController: UIViewController?
          
          if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
              if viewController.isKindOfClass(StartOfTourViewController) {
                
                (viewController as! StartOfTourViewController).isFromHome = true
                loginViewController = viewController
                
                break
              }
            }
          }
          
          if let loginViewController = loginViewController {
            
            (loginViewController as! StartOfTourViewController).isFromHome = true
            
            self.navigationController?.popToViewController(loginViewController, animated: true)
          } else {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let mmrloginViewController: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("StartOfTourViewController")
            (mmrloginViewController as! StartOfTourViewController).isFromHome = true
            self.navigationController?.setViewControllers([mmrloginViewController], animated: true)
          }
          
        }
      })
      
      
    }
    
    
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
