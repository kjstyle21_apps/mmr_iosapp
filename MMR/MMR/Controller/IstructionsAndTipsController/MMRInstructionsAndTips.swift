//
//  MMRInstructionsAndTips.swift
//  MMR
//
//  Created by Mindbowser on 10/24/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRInstructionsAndTips: UIViewController,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {
    @IBOutlet weak var navigationView: Navigation_View!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var instructionAndTipsTextView: UITextView!
    @IBOutlet weak var instructionsAndTipsScollView: UIScrollView!
    @IBOutlet weak var heightOfInstructionTextView: NSLayoutConstraint!
    @IBOutlet weak var verticalDistanceBetweenTextViewAndButton: NSLayoutConstraint!
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var navFlag = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationSetup()
        navigationView.titleLabel.text = "Instructions & Tips".localized()
        self.startButton.setTitle("START".localized(), for: UIControlState())
        self.startButton.roundbutton(self.startButton, cornerradius: 5)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            
            if BMCentralManager.sharedInstance().isCentralReady == true {
                self.navigationView.statusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.navigationView.statusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
            NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        }
        navigationView.progressView.isHidden = true
        
        //      NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setText), name: LCLLanguageChangeNotification, object: nil)
        //              self.setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        if (UserDefaults.standard.value(forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)") as? Bool) != nil {
            self.navFlag = UserDefaults.standard.value(forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)") as! Bool
        }
        else {
            self.navFlag = false
        }
        self.setText()
        //    if BMCentralManager.sharedInstance().centralReady == true {
        //      self.navigationView.statusLabel.hidden = true
        //      dispatch_async(dispatch_get_main_queue(), {
        //        UIApplication.sharedApplication().statusBarHidden = false
        //      })
        //    }
        //    else {
        //      self.navigationView.statusLabel.hidden = false
        //      dispatch_async(dispatch_get_main_queue(), {
        //        UIApplication.sharedApplication().statusBarHidden = true
        //      })
        //    }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
    }
    
    @objc func bluetoothStatus(_ notify : Notification) {
        let userinfo = notify.userInfo as! NSDictionary
        if userinfo.value(forKey: "state") as! String == "ON" {
            self.navigationView.statusLabel.isHidden = true
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isStatusBarHidden = false
            })
        }
        else {
            self.navigationView.statusLabel.isHidden = false
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isStatusBarHidden = true
            })
        }
    }
    
    @objc func onFunc() {
        print("Bluetooth is on reconnect the device")
        UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
        UserDefaults.standard.synchronize()
        //    let mmrBLEControlNav = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewControllerWithIdentifier("MMRBLEViewController") as? MMRBLEViewController
        //    mmrBLEControlNav?.startBLEFunctionality()
        MMRBLEModule.sharedInstance.startBLEFunctionality()
    }
    
    func offFunc() {
        print("Bluetooth off device disconnected")
        //    AlertModel.sharedInstance.showErrorMessage(bluetoothePowerOffMessage)
    }
    
    
    func setText() {
        //    self.startButton.setTitle("START".localized(), forState: .Normal)
        self.startButton.titleLabel?.text = "START".localized()
        //    navigationView.titleLabel.text = "Instructions & Tips".localized()
        
    }
    // MARK: - *** Navigation View Setup ***
    func navigationSetup() {
        navigationView.rightButton .setImage(UIImage (named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        instructionAndTipsTextView.text = UserDefaults.standard.value(forKey: surveyInstruction) as? String
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.instructionAndTipsTextView.sizeThatFits(CGSize(width: self.instructionAndTipsTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.heightOfInstructionTextView.constant = newSize.height + 20
        
        self.instructionsAndTipsScollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height+200)
    }
    // MARK: - *** Pop on close button ***
    
    @objc func rightButtonAction() {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func startAction(_ sender: AnyObject) {
        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
        if isTimeLeftForSurvey {
            //@kj
            let currentTime = Date()
            let dateFormatter2 = DateFormatter()
//            dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // 2018-12-07T09:33:13.766+0000
            dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // 2018-12-07T09:34:54
//            dateFormatter2.timeZone = TimeZone(identifier: "UTC")
            let strDate = dateFormatter2.string(from: currentTime)
            print("strDate: \(strDate)");
            UserDefaults.standard.set(strDate, forKey: FIRST_QUESTION_SEEN_TIME)
            
            if self.navFlag == true {
                let storyboard : UIStoryboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
                let submitAction = storyboard.instantiateViewController(withIdentifier: "MMRSubmitSurvey") as! MMRSubmitSurvey
                self.navigationController?.pushViewController(submitAction, animated: false)
            }
            else {
                if let isEntryDoneFlag = UserDefaults.standard.value(forKey: isEntryDoneFlag_UD) as? String {
                    if isEntryDoneFlag == "FALSE" {
                        let userID = UserDefaults.standard.value(forKey: currentUserId)
                        let surveyID = UserDefaults.standard.value(forKey: surveyId)
                        let questionArray = QuestionSharedClass.sharedInstance.copyAllQuestionsIntoQuestionsRecord(userID! as AnyObject, fetchedSurveyID: surveyID! as AnyObject)
                        if questionArray.count > 0 {
                            QuestionSharedClass.sharedInstance.checkIsAvailableAndThenInsert(questionArray)
                        }
                        let optionsArray = AnswerSharedClass.sharedInstance.copyAllOptionsIntoAnswerRecordsCopy(userID! as AnyObject, fetchedSurveyID: surveyID! as AnyObject)
                        if optionsArray.count > 0 {
                            AnswerSharedClass.sharedInstance.checkIsOptionAvailableAndThenInsert(optionsArray)
                        }
                        let routingArray = QuestionSharedClass.sharedInstance.copyAllRoutingDataIntoRoutingRecordsCopy(userID! as AnyObject, fetchedSurveyID: surveyID! as AnyObject)
                        if routingArray.count > 0 {
                            QuestionSharedClass.sharedInstance.checkIsRoutingAvailableAndThenInsert(routingArray)
                        }
                        UserDefaults.standard.set("TRUE", forKey: isEntryDoneFlag_UD)
                    }
                } else {
                    let userID = UserDefaults.standard.value(forKey: currentUserId)
                    let surveyID = UserDefaults.standard.value(forKey: surveyId)
                    let questionArray = QuestionSharedClass.sharedInstance.copyAllQuestionsIntoQuestionsRecord(userID! as AnyObject, fetchedSurveyID: surveyID! as AnyObject)
                    if questionArray.count > 0 {
                        QuestionSharedClass.sharedInstance.checkIsAvailableAndThenInsert(questionArray)
                    }
                    let optionsArray = AnswerSharedClass.sharedInstance.copyAllOptionsIntoAnswerRecordsCopy(userID! as AnyObject, fetchedSurveyID: surveyID! as AnyObject)
                    if optionsArray.count > 0 {
                        AnswerSharedClass.sharedInstance.checkIsOptionAvailableAndThenInsert(optionsArray)
                    }
                    UserDefaults.standard.set("TRUE", forKey: isEntryDoneFlag_UD)
                }
                let base = BaseClassForQuestions()
                self.navigationController?.pushViewController(base, animated: true)
            }
        } else {
            // Upload survey to server and return to Home
            UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
            BaseClassForQuestions.serialNo_ID = -1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            let storyboard : UIStoryboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
            let submitAction = storyboard.instantiateViewController(withIdentifier: "MMRSubmitSurvey") as! MMRSubmitSurvey
            self.navigationController?.pushViewController(submitAction, animated: false)
        }
    }
    
    func uploadCompletion(_ status: Bool) {
        if status {
            print("Success")
            AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
                let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                UserDefaults.standard.synchronize()
                //self.delegate!.goToHomeViewController()
                self.goToHomeViewController()
            })
        }
        else {
            print("Failure")
        }
        stopActivityAnimating()
    }
    
    func goToHomeViewController () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
}
