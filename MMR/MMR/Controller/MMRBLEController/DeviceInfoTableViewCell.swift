//
//  DeviceInfoTableViewCell.swift
//  MMRBLEDemo
//
//  Created by Mindbowser on 13/12/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class DeviceInfoTableViewCell: UITableViewCell {
    
    @IBOutlet var deviceName:UILabel?
    @IBOutlet var macAddress:UILabel?
    @IBOutlet var weight:UILabel?
    @IBOutlet var updatedTime:UILabel?
    @IBOutlet var batteryLevel:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
