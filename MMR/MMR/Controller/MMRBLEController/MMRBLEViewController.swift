//
//  MMRBLEViewController.swift
//  MMR
//
//  Created by Mindbowser on 1/18/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import CoreBluetooth

class MMRBLEViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
  var  connectedPeripheral:BMPeripheral?
  var scanningTimer:Timer?
  var localNotification:UILocalNotification?
  @IBOutlet var deviceListTableview:UITableView?
  var devicelistArray:NSArray = NSArray()
  var beaconObj = Beacon();
  var dateFormatter:DateFormatter = DateFormatter()
  var discoveredCharacteristics:NSMutableArray = NSMutableArray()
//  var deviceList:NSMutableArray = NSMutableArray()
  var deviceList = NSMutableArray()
  var peripheral = BMPeripheral()
  var createdDate : Date?
	var isHidtoryDone = false
  var historyRecordArray = [Beacon]()
  var sensorName = String()
  
  @IBOutlet var connectButton: UIButton!
  
  @IBOutlet var SensorNameLabel: UILabel!
  @IBOutlet var updatedTimeLabel: UILabel!
  @IBOutlet var weightLabel: UILabel!
  @IBOutlet var batteryStatusLabel: UILabel!
  @IBOutlet var macAddressLabel: UILabel!
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    deviceList = NSMutableArray()
    UserDefaults.standard.setValue(false, forKey: IS_HISTORY_SAVED_UD)
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    self.connectButton.roundbutton(self.connectButton, cornerradius: 5)

    // Do any additional setup after loading the view, typically from a nib.
    //
//    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MMRBLEViewController.handleDeviceDisconnected(_:)), name: "disconnected", object: nil)
    if BMCentralManager.sharedInstance().isCentralReady == true
    {
      
    }
//    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MMRBLEViewController.updateState(_:)), name: "update", object: nil)
    
//    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MMRBLEViewController.handleDeviceDisconnected(_:)), name: "disconnected", object: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    print("DeviceList in didAppear: \(deviceList)")
//    print(deviceList?.count)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.deviceListTableview?.delegate = self
    self.deviceListTableview?.dataSource = self
    if let tempSensorName = UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) as? String {
      
      let tempArr = tempSensorName.components(separatedBy: "|")
      self.sensorName = tempArr[1]
      print("Current Sensor Name : \(self.sensorName)")
    }
    
    ScanNewMMRDevice()
    
//    if connectedPeripheral != nil {
//      connectedPeripheral?.connectWithCompletion({ (error) in
//        self.fireUILocalnotificationWithMessage("Device Connected with \(self.connectedPeripheral!.name)")
//        
//        self.discoverServicesAndcharacteristicsForSelectedDevice(self.connectedPeripheral!)
//      })
//    }
//    else {
//      ScanNewMMRDevice()
//    }

    NotificationCenter.default.addObserver(self, selector: #selector(MMRBLEViewController.updateState(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)

    
    if connectButton.tag == 0{
      self.connectButton.setTitle("Connect", for: UIControlState())
    }
    else if connectButton.tag == 1{
      self.connectButton.setTitle("Disconnect", for: UIControlState())
    }
  }
  
  func startBLEFunctionality() {
    UserDefaults.standard.setValue(false, forKey: IS_HISTORY_SAVED_UD)
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    if let tempSensorName = UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) as? String {
      let tempArr = tempSensorName.components(separatedBy: "|")
      self.sensorName = tempArr[1]
      print("Current Sensor Name : \(self.sensorName)")
    }
    ScanNewMMRDevice()
    NotificationCenter.default.addObserver(self, selector: #selector(MMRBLEViewController.updateState(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @IBAction func connectClicked(_ sender: AnyObject) {
//    if connectButton.tag == 0 {
//      connectButton.tag = 1
//      connectButton.setTitle("Disconnect", forState: .Normal)
//      if BMCentralManager.sharedInstance().centralReady == true
//      {
//        ScanNewMMRDevice()
//        
//      }
////      NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MMRBLEViewController.updateState(_:)), name: "update", object: nil)
//    }
//    else if connectButton.tag == 1 {
//      connectButton.tag = 0
////      ScanNewMMRDevice()
//      connectButton.setTitle("Connect", forState: .Normal)
//     self.disconnectMMRDevice()
//    }
  }
  
  @IBAction func closeClicked(_ sender: AnyObject) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  @objc func updateState(_ notification:Notification) {
    let centralStateInfo = notification.userInfo! as NSDictionary
    if centralStateInfo.value(forKey: "state") as! String == "ON" {
      NotificationCenter.default.post(name: Notification.Name(rawValue: BLUTOOTH_POWER_STATUS_ON), object: nil)
//      ScanNewMMRDevice()
    }
    else if centralStateInfo.value(forKey: "state") as! String == "OFF" {
      print(centralStateInfo.value(forKey: "state"))
//      self.disconnectMMRDevice()
      NotificationCenter.default.post(name: Notification.Name(rawValue: BLUTOOTH_POWER_STATUS_OFF), object: nil)
    } else {
      
    }
  }
  
  func handleDeviceDisconnected(_ notification:Notification) {
    let centralStateInfo = notification.userInfo! as NSDictionary
    self.fireUILocalnotificationWithMessage("Device Disconnected with \(centralStateInfo.value(forKey: "peripheralName"))")
    if connectedPeripheral != nil {
      connectedPeripheral?.connect(completion: { (error) in
        self.fireUILocalnotificationWithMessage("Device Connected with \(self.connectedPeripheral!.name)")
        
        self.discoverServicesAndcharacteristicsForSelectedDevice(self.connectedPeripheral!)
      })
    }
    else {
      ScanNewMMRDevice()
    }
  }
  
  // scan new MMR device for connection.
  func ScanNewMMRDevice() {
    BMCentralManager.sharedInstance().scanForPeripherals(withServices: nil, options:[CBCentralManagerScanOptionAllowDuplicatesKey: false])
    
    if (scanningTimer != nil) {
      scanningTimer?.invalidate()
      scanningTimer = nil;
    }
    
    scanningTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getScannedPeripheralslist), userInfo: nil, repeats: true)
  }
  
  @objc func getScannedPeripheralslist() {
    
    if BMCentralManager.sharedInstance().scannedPeripheralsArray.count > 0 {
      
      for device in BMCentralManager.sharedInstance().scannedPeripheralsArray {
        //print("deviceName:\(device.name)")
        let peripheralName = (device as? BMPeripheral)?.name
        print("deviceName:\(peripheralName)")
        
        if let peripheral_name = peripheralName {
//          if peripheral_name == self.sensorName {
          if peripheral_name == "MMR-USAGE MONITOR" {
            scanningTimer?.invalidate()
            deviceList.add(device)
//            dispatch_async(dispatch_get_main_queue(), { 
            if deviceList.count > 0 {
              self.deviceListTableview?.reloadData()
            }
//            })
            self.peripheral = device as! BMPeripheral
            self.setupPeripheralwithSelectedDevice(device as! BMPeripheral)
          }
        }
        
      }
    }
  }
  
  // device connection setup.
  func setupPeripheralwithSelectedDevice(_ selectedDevice:BMPeripheral) {
    
    selectedDevice.connect { (error) in
      
      if error != nil {
        
      }
      else {
        self.fireUILocalnotificationWithMessage("Device Connected with \(selectedDevice.name)")
        self.connectedPeripheral = selectedDevice
        print("device name:\(self.connectedPeripheral?.name)")
        print("Device is connected")
        self.discoverServicesAndcharacteristicsForSelectedDevice(selectedDevice)
      }
    }
  }
  
  func discoverServicesAndcharacteristicsForSelectedDevice(_ selectedPeripheral:BMPeripheral) {
    
    selectedPeripheral.discoverServices(completion: { (services, error) in
      if (services?.count)! > 0 {
        print("Total services:\(services)")
        for service in services! {
          
          if (service as AnyObject).uuidString == "00005100-0000-1000-8000-008055aa55aa" {
            (service as AnyObject).discoverCharacteristics(completion: { (characteristics, error) in
              if (characteristics?.count)! > 0{
                print("characteristics: \(characteristics) for service:\(service)")
                self.getRealTimeServiceCharacteristics(characteristics as! [BMCharacteristics])
              }
            })
          }
          if (service as AnyObject).uuidString == "00006100-0000-1000-8000-008055aa55aa" {
            
            (service as AnyObject).discoverCharacteristics(completion: { (characteristics, error) in
              if (characteristics?.count)! > 0{
                print("characteristics: \(characteristics) for service:\(service)")
                // self.getHistoryServiceCharacteristics(characteristics as! [BMCharacteristics])
              }
            })
          }
          if (service as AnyObject).uuidString == "1805" {
            (service as AnyObject).discoverCharacteristics(completion: { (characteristics, error) in
              if (characteristics?.count)! > 0 {
                print("characteristics: \(characteristics) for service:\(service)")
                
                self.writeCurrentDeviceTimeOnDevice(characteristics as! [BMCharacteristics])
              }
            })
          }
          
        }
      }
    })
  }
  
  func writeCurrentDeviceTimeOnDevice(_ characteristics:[BMCharacteristics]) {
    for characteristic in characteristics {
      if characteristic.uuidString == "2a2b" {
        characteristic.readValue({ (data, error) in
          print("read success")
          print("date data:\(data)")
          
          characteristic.writeValue(self.checkAndUpdateDeviceTime(), completion: { (error) in
            if error == nil {
              print("Write success")
              self.fetchHistorydata()
              
            }
          })
        })
        
      }
    }
  }
  
  func fetchHistorydata() {
    var historyWeightValue:UInt32 = 0
    let historyCharacteristic = self.getHistoryCharacteristicsUUID()
    historyCharacteristic.setNotifyValue(true, completion: { (error) in
      
      }, onUpdate: { (data, error) in
        print("history weightdata:\(data)")
        let receivedDataString:NSString = NSString(format: "%@",data as! CVarArg).replacingOccurrences(of: " ", with: "") as NSString
        print("receivedDataString:\(receivedDataString)")
        
        let weightHexString = self.reverseStringFrom(receivedDataString.substring(with: self.setcustomRange(8, location: 1)) as NSString)
        print("weightHexString:\(weightHexString)")
        
        historyWeightValue = CommonUtility.sharedInstance().int(fromHexString: weightHexString as String)
        historyWeightValue = historyWeightValue / 100;
        print("historyWeightValue:\(historyWeightValue)")
        
        
        let historyDateString = self.reverseStringFrom(receivedDataString.substring(with: self.setcustomRange(8, location: 9)) as NSString)
        print("historyDateString:\(historyDateString)")
        
        let  historyDatevalue = CommonUtility.sharedInstance().int(fromHexString: historyDateString as String)
        print("historyDatevalue:\(historyDatevalue)")
        let historyDateIntervalStr = NSString(format: "%d", historyDatevalue);
        let date = Date(timeIntervalSince1970: (historyDateIntervalStr.doubleValue))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        print("strDate:\(strDate)")
        
        
        if self.createdDate == nil {
          self.createdDate = date
        }
        print("------------------- History fetched --------------------")
        
        print("Weight : \(historyWeightValue), Update Date : \(strDate), Create Date : \(self.createdDate), Battery Level : \(self.beaconObj.batteryLevel)")
        let beacon = Beacon()
        beacon.weight = "\(historyWeightValue)"
        beacon.updatedTime = strDate
        beacon.createdTime = dateFormatter.string(from: self.createdDate!)
        beacon.batteryLevel = self.beaconObj.batteryLevel
        self.historyRecordArray.append(beacon)
        
    })
    
  }
  
  
  // function to handle real time weight values
  func getRealTimeServiceCharacteristics(_ characteristics:[BMCharacteristics]) {
    var weightValue:UInt32 = 0
    var batteryLevel:UInt32 = 0
//    self.SensorNameLabel.text = "MMR-USAGE_Monitor";
//    self.macAddressLabel.text = connectedPeripheral?.UUIDString;
    
    
    for characteristic in characteristics {
      if (characteristic).uuidString == "00005103-0000-1000-8000-008055aa55aa" {
        characteristic.setNotifyValue(true, completion: { (error) in
          print("notify with error:\(error)")
          }, onUpdate: { (data, error) in
            if error == nil {
              print("data: \(data)")
              var receivedDataString:NSString = NSString(format: "%@",data as! CVarArg).replacingOccurrences(of: " ", with: "") as NSString
              receivedDataString = receivedDataString.substring(with: self.setcustomRange(8, location: 3)) as NSString
              // 000403020100000075
              let weightHexString = self.reverseStringFrom(receivedDataString)
              weightValue = CommonUtility.sharedInstance().int(fromHexString: weightHexString as String)
              weightValue = weightValue / 100
              self.beaconObj.weight = String(format: "%d", weightValue)
              let currentDateTime: Date = Date()
              let currentDateTimeString:String = self.dateFormatter.string(from: currentDateTime)
              self.beaconObj.updatedTime = currentDateTimeString
//              dispatch_async(dispatch_get_main_queue(), { 
              if self.deviceList.count > 0 {
                self.deviceListTableview?.reloadData()
              }
//              })
//              if self.beaconObj.weight == "0" {
//                self.fireUILocalnotificationWithMessage("You have not placed a product weight: \(self.beaconObj.weight)")
//              }
//              else {
//              self.fireUILocalnotificationWithMessage("you are inside region with current weight: \(self.beaconObj.weight)")
//              }
              print("weight:\(weightValue)")
//              self.weightLabel.text = self.beaconObj.weight
              self.isHidtoryDone = true
              self.saveHistoryIntoDB()
            } else {
              
            }
        })
      }
      if characteristic.uuidString == "00005101-0000-1000-8000-008055aa55aa" {
        
        characteristic.setNotifyValue(true, completion: { (error) in
          print("notify with error:\(error)")
          }, onUpdate: { (data, error) in
            if error == nil {
              print("data: \(data)")
              
              let totalReceivedString = NSString(format: "%@", data as! CVarArg)
              batteryLevel = CommonUtility.sharedInstance().int(fromHexString: totalReceivedString.substring(with: self.setcustomRange(2, location: 1)))
              print("batteryLevel: \(batteryLevel)")
              self.beaconObj.batteryLevel = String(format: "%d", batteryLevel)
//              self.batteryStatusLabel.text = self.beaconObj.batteryLevel;
              let currentDateTime: Date = Date()
              let currentDateTimeString:String = self.dateFormatter.string(from: currentDateTime)
              self.beaconObj.updatedTime = currentDateTimeString
//              self.updatedTimeLabel.text = self.beaconObj.updatedTime;
              if self.deviceList.count > 0 {
              self.deviceListTableview?.reloadData()
              }
              
            } else {
              
            }
        })
      }
      
      //**************************************************************
//       self.deviceListTableview?.reloadData()
      
    }
    
  }
  
  
  func saveHistoryIntoDB() {
    
    if let flag = UserDefaults.standard.value(forKey: IS_HISTORY_SAVED_UD) {
      if !(flag as! Bool) {
        UserDefaults.standard.setValue(true, forKey: IS_HISTORY_SAVED_UD)
        //Check the history condition and then save it into database
        for beacon in self.historyRecordArray {
          let status = BLERecordsSharedClass.sharedInstance.isRecordAvailable(beacon.updatedTime)
          if status == "No_Exists" {
            let details = [bleWeight : beacon.weight, bleUpdatedDate : beacon.updatedTime , bleCreatedDate : beacon.createdTime, bleBatteryLevel : beacon.batteryLevel]
            if UserDefaults.standard.value(forKey: surveyId) != nil {
              BLERecordsSharedClass.sharedInstance.insertbleData(details as [String : AnyObject])
            }
          }
        }
        self.clearHistory()
//        self.disconnectMMRDevice()
      }
    }
  }
  
  
  // function to handle history weight values
  //    func getHistoryServiceCharacteristics(characteristics:[BMCharacteristics]) {
  //        for characteristic in characteristics {
  //
  //            if characteristic.UUIDString == "00006104-0000-1000-8000-008055aa55aa" {
  //                //                characteristic.setNotifyValue(true, completion: { (error) in
  //                //                    print("notify with error:\(error)")
  //                //                    }, onUpdate: { (data, error) in
  //                //                        if error == nil {
  //                //                            print("data: \(data)")
  //                //                            let receivedDataString:NSString = NSString(format: "%@",data).stringByReplacingOccurrencesOfString(" ", withString: "")
  //                //                            let weightHexString = self.reverseStringFrom(receivedDataString.substringWithRange(self.setcustomRange(8, location: 0)))
  //                //                            let historyWeightValue = CommonUtility.sharedInstance().intFromHexString(weightHexString as String)
  //                //                            print("historyWeightValue:\(historyWeightValue)")
  //                //                        } else {
  //                //
  //                //                        }
  //                //                })
  //
  //              //  NSMutableData *boundData = [[NSMutableData alloc]initWithData:];
  //
  //
  //                characteristic.writeValue(sendData, completion: { (error) in
  //                    if error == nil {
  //
  //                        let historyCharacteristic = self.getHistoryCharacteristicsUUID()
  //                        historyCharacteristic.setNotifyValue(true, completion: { (error) in
  //
  //                            }, onUpdate: { (data, error) in
  //                                if error == nil {
  //                                    let receivedDataString:NSString = NSString(format: "%@",data).stringByReplacingOccurrencesOfString(" ", withString: "")
  //                            let weightHexString = self.reverseStringFrom(receivedDataString.substringWithRange(self.setcustomRange(8, location: 0)))
  //                           let historyWeightValue = CommonUtility.sharedInstance().intFromHexString(weightHexString as String)
  //                          print("historyWeightValue:\(historyWeightValue)")
  //
  //                                }
  //                        })
  //                    }
  //                })
  //            }
  //
  //        }
  //    }
  
  
  
  func getHistoryCharacteristicsUUID()-> BMCharacteristics {
    var historyCharacteristic:BMCharacteristics?
    for service in (connectedPeripheral?.services)! {
      if (service as! BMServices).uuidString == "00006100-0000-1000-8000-008055aa55aa" {
        for characteristic in (service as! BMServices).characteristics {
          if (characteristic as! BMCharacteristics).uuidString == "00006103-0000-1000-8000-008055aa55aa" {
            historyCharacteristic = (characteristic as? BMCharacteristics)!
          }
        }
      }
    }
    return historyCharacteristic!
    
  }
  
  //    func readDeviceTime()
  //    {
  //
  //
  //
  //        for service in (connectedPeripheral?.services)! {
  //        }
  //        /* For each characteristic */
  //        for charateristic in charaList {
  //            let eachCharacteristic = charateristic as! CBCharacteristic;
  //
  //            //If UUID characteristic found
  //            if eachCharacteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID {
  //
  //                CBManager.instance.readData(selectedPeripheral, characteristic: eachCharacteristic);
  //                break;
  //            }
  //        }
  //    }
  
  
  @IBAction func clearHistory()
  {
    
    for service in (connectedPeripheral?.services)! {
      if (service as! BMServices).uuidString == "00006100-0000-1000-8000-008055aa55aa" {
        for characteristic in ((service as? BMServices)?.characteristics)! {
          if (characteristic as! BMCharacteristics).uuidString == "00006101-0000-1000-8000-008055aa55aa" {
            let clearFlag = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([3])), count: 1)
            (characteristic as! BMCharacteristics).writeValue(clearFlag, completion: { (error) in
              if error == nil {
                self.disconnectMMRDevice()
//                self.fetchHistorydata()
                
              }
            })
            
          }
        }
      }
    }
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
  }
  
  //  00006101-0000-1000-8000-008055aa55aa
  func updateTableValues(_ weight:String,batteryLevel:String) {
    let currentDateTime: Date = Date()
    let currentDateTimeString:String = dateFormatter.string(from: currentDateTime)
    beaconObj.updatedTime = currentDateTimeString
    if deviceList.count > 0 {
    self.deviceListTableview?.reloadData()
    }
  }
  
  func setcustomRange(_ length:Int,location:Int) -> NSRange {
    var customRange:NSRange = NSRange()
    customRange.length = length
    customRange.location = location
    return customRange
  }
  
  func reverseStringFrom(_ string:NSString) -> NSMutableString {
    let reverseString:NSMutableString = NSMutableString()
    string.replacingOccurrences(of: " ", with: "")
    var i = string.length
    
    while i > 0 {
      let subString = string.substring(with: NSMakeRange(i-2, 2))
      reverseString.append(subString)
      i-=2
    }
    return reverseString
  }
  
  func checkAndUpdateDeviceTime() -> Data?
  {
    _ = self.connectedPeripheral
    let date = Date();
    var calendar = Calendar.current;
    calendar.timeZone = TimeZone(abbreviation: "UTC")!
    let unitFlags: NSCalendar.Unit = [.hour,.minute,.second,.month,.year,.day]
    
    //        let components = calendar.components(.Hour | .Minute | .Month | .Year | .Day, fromDate: date);
    let components:DateComponents = (calendar as NSCalendar).components(unitFlags, from: date)
    
//    //Define each date time components
//    let year = Data(bytes: UnsafePointer<UInt8>([components.year! % 256]), count: 1);
//    let year1 = Data(bytes: UnsafePointer<UInt8>([components.year!/256]), count: 1);
//    let month = Data(bytes: UnsafePointer<UInt8>([components.month]), count: 1);
//    let day = Data(bytes: UnsafePointer<UInt8>([components.day]), count: 1);
//    let hour = Data(bytes: UnsafePointer<UInt8>([components.hour]), count: 1);
//    let minute = Data(bytes: UnsafePointer<UInt8>([components.minute]), count: 1);
//    let second = Data(bytes: UnsafePointer<UInt8>([components.second]), count: 1);
//    let weekday = Data(bytes: UnsafePointer<UInt8>([components.weekday]), count: 1);
//    var Zero_int = 0;
//    let data = Data(bytes: UnsafePointer<UInt8>(&Zero_int), count:1)
//    
//    //Append bytes
//    var totalTime = NSData(data: year) as Data;
//    totalTime.append(year1);
//    totalTime.append(month);
//    totalTime.append(day);
//    totalTime.append(hour);
//    totalTime.append(minute);
//    totalTime.append(second);
//    totalTime.append(weekday);
//    totalTime.append(data);
//    totalTime.append(data);
    
    var year = components.year
    let yearData:Data = Data(bytes: &year, count: MemoryLayout.size(ofValue: year))
    let year1:Data = yearData.subdata(in: 0..<1)
    let year2:Data = yearData.subdata(in: 1..<2)
    let settingArray = [UInt8]([
        UInt8(year1[0])
        , UInt8(year2[0])
        , UInt8(components.month!)
        , UInt8(components.day!)
        , UInt8(components.hour!)
        , UInt8(components.minute!)
        , UInt8(components.second!)
        ])
    let settingData:Data = Data(bytes: settingArray, count: MemoryLayout.size(ofValue: settingArray))
    return settingData
  }
  
  /* Subtract 1 month for each received date time from device to get exact date time(as per standard) */
  func subtractMonth(_ date:  Date) -> Date
  {
    var dateComponent = DateComponents();
    dateComponent.month = -1;
    let cal = Calendar.current;
    //        return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendarOptions(rawValue: 0))!
    return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
  }
  
  /* Add 1 month from date time device to get exact date time(as per standard) */
  func addMonth( _ date:  Date) -> Date
  {
    var dateComponent = DateComponents();
    dateComponent.month = 1;
    let cal = Calendar.current;
    //  return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendarOptions(rawValue: 0))!
    return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
  }
  
  /* Subctract 1 hour from specified date time */
  func subtractHour(_ date:  Date) -> Date
  {
    var dateComponent = DateComponents();
    dateComponent.hour = -1;
    let cal = Calendar.current;
    //return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendarOptions(rawValue: 0))!
    return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
  }
  
  
  
  func fireUILocalnotificationWithMessage(_ message:String) {
    let previousNotification = localNotification
    if previousNotification != nil {
      // UIApplication.sharedApplication().cancelLocalNotification(localNotification!)
    }
    localNotification = UILocalNotification()
    localNotification?.alertBody = message
    if #available(iOS 8.2, *) {
      localNotification?.alertTitle = "MMR"
    } else {
      // Fallback on earlier versions
    }
    localNotification?.fireDate = Date(timeIntervalSinceNow: 0)
    UIApplication.shared.scheduleLocalNotification(localNotification!)
  }
  
  /* Return the number of sections in table */
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  /* Return the number of rows in the section */
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    return 1
    return deviceList.count
  }
  
  /* Return actual cell contents */
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = "DeviceTableCell"
    
//    let cell = self.deviceListTableview?.cellForRowAtIndexPath(indexPath) as! DeviceInfoTableViewCell
    let cell = deviceListTableview!.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DeviceInfoTableViewCell
    
    // Configure the cell...
//    cell.deviceName!.text = "MMR-USAGE_Monitor";
//    cell.macAddress!.text = connectedPeripheral?.UUIDString;
//    cell.weight!.text = self.beaconObj.weight
//    cell.batteryLevel!.text = self.beaconObj.batteryLevel;
//    cell.updatedTime!.text = self.beaconObj.updatedTime;
    return cell
  }
  
  
  // Device disconnected.
  func disconnectMMRDevice() {
//    self.deviceList.removeAllObjects()
//    self.deviceListTableview?.reloadData()
    self.connectedPeripheral?.disconnect(completion: nil)
    UserDefaults.standard.setValue(false, forKey: IS_HISTORY_SAVED_UD)
//    self.dismissViewControllerAnimated(true, completion: nil)
    
//    self.connectedPeripheral = nil
//    BMCentralManager.sharedInstance().stopScanForPeripherals()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
}
