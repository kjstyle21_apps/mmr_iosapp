//
//  MMRImageBrowserController.swift
//  MMR
//
//  Created by Mindbowser on 2/10/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import Photos


@objc protocol MMRImageBrowserDelegate : class {
  @objc optional func mediaSelected(_ selectedAsset : PHAsset)
}


class MMRImageBrowserController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable {

  // MARK:- Properties
  var assets = NSArray()
  var selectedAssest:PHAsset?
  var allPhotos: PHFetchResult<AnyObject>!
  let itemsPerRow: CGFloat = 4
  let sectionInsets = UIEdgeInsets(top: 2.0, left: 2.0, bottom: 2.0, right: 2.0)
  weak var mediaDelegate : MMRImageBrowserDelegate?

  @IBOutlet var browserCollectionView: UICollectionView!
  @IBOutlet weak var navigationView: Navigation_View!

    override func viewDidLoad() {
        super.viewDidLoad()
        browserCollectionView.delegate = self
        browserCollectionView.dataSource = self
        self.loadDataSource()
        self.navigationSetup()

    }
  
  func navigationSetup() {
//    let serialID = NSUserDefaults.standardUserDefaults().valueForKey(serialNoID)
    
    navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
    navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
  }
  
  @objc func rightButtonAction() {
    self.dismiss(animated: true, completion: nil)
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(loadDataSource), name: NSNotification.Name(rawValue: "MMR_APP_ACTIVE_STATE_NOTIFICATION"), object: nil)
  }

  //MARK:- Get Image Thumbnail
  func getAssetThumbnail(_ asset: PHAsset) -> UIImage? {
    let manager = PHImageManager.default()
    let options = PHImageRequestOptions()
    options.deliveryMode = .opportunistic
    let retinaScale = UIScreen.main.scale
    let retinaSquare = CGSize(width: 200 * retinaScale, height: 200 * retinaScale)
    var thumbnail: UIImage?
    options.isSynchronous = true
    manager.requestImage(for: asset, targetSize: retinaSquare, contentMode: .aspectFit, options: options) { (result, info) in
      thumbnail = result
    }
    return thumbnail
  }
  
  @objc func loadDataSource() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Fetching Data...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    PHAssetsManager.sharedInstance.fetchAssets {  (assets: NSMutableArray) -> Void in
      self.assets = assets as NSArray
      self.browserCollectionView!.reloadData()
    }
      self.stopActivityAnimating()
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.assets.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = browserCollectionView.dequeueReusableCell(withReuseIdentifier: "MMRImageOrVideoCell", for: indexPath) as! MMRImageOrVideoCell
    cell.imageView.image = self.getAssetThumbnail(assets[indexPath.row] as! PHAsset)
    let phAssest = self.assets[indexPath.row] as! PHAsset
    cell.sbSetAsset(phAssest)
    return cell
  }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = browserCollectionView.dequeueReusableCell(withReuseIdentifier: "MMRImageOrVideoCell", for: indexPath) as! MMRImageOrVideoCell
        let phAssest = self.assets[indexPath.row] as! PHAsset
        
        if phAssest.mediaType == .video {
            let options = PHVideoRequestOptions()
            options.version = .original
            options.deliveryMode = .fastFormat
            if phAssest.duration > 60.0 {
                AlertModel.sharedInstance.showAlert("Video File Length Excedding", message: "Kindly select video file max 60 secs", buttonTitles: ["Okay"], viewController: self, completionHandler: { (buttonTitle, status) in
                })
            } else {
                self.selectedAssest = self.assets[indexPath.row] as? PHAsset
                self.mediaDelegate?.mediaSelected!(self.selectedAssest!)
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            self.selectedAssest = self.assets[indexPath.row] as? PHAsset
            self.mediaDelegate?.mediaSelected!(self.selectedAssest!)
            dismiss(animated: true, completion: nil)
        }
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
