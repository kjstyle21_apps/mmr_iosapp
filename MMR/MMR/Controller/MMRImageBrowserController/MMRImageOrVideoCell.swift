//
//  MMRImageOrVideoCell.swift
//  MMR
//
//  Created by Mindbowser on 2/10/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import Photos

class MMRImageOrVideoCell: UICollectionViewCell {
  
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var videoIconImageView: UIImageView!
  @IBOutlet var durationLabel: UILabel!
  
  var assetProp: PHAsset!

  override func awakeFromNib() {
    super.awakeFromNib()
    self.videoIconImageView.isHidden = true
  }
  
  func sbSetAsset(_ asset: PHAsset)
  {
    assetProp = asset
    if assetProp.mediaType == PHAssetMediaType.video
    {
      self.videoIconImageView.isHidden = false
      let i = Int(assetProp.duration)
      let duration = self.getTimeString(i)
      self.durationLabel.text = duration
    }
    else if assetProp.mediaType == PHAssetMediaType.image
    {
      self.videoIconImageView.isHidden = true
      self.durationLabel.text = nil
    }
  }
  
  func getTimeString(_ time: Int) -> String
  {
    let timeInterval = CDouble(time)
    let formatter = DateFormatter()
    formatter.dateFormat = "mm:ss"
    formatter.timeZone = TimeZone.init(secondsFromGMT: 0) as TimeZone!
    let date = Date(timeIntervalSinceReferenceDate: timeInterval)
    return formatter.string(from: date)
  }
  
}
