//
//  PHAssetsManager.swift
//  MMR
//
//  Created by Mindbowser on 2/10/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AssetsLibrary

class PHAssetsManager: NSObject {
  typealias CompletionHandler = (_ assests:NSMutableArray) -> Void
  typealias ALAssetsLibraryGroupsEnumerationResultsBlock = (ALAssetsGroup?, UnsafeMutablePointer<ObjCBool>) -> Void

  var completionHandler : CompletionHandler?
  
  var allPhotosResult: PHFetchResult<AnyObject>!
  var assetLibrary = ALAssetsLibrary()
  
  class var sharedInstance :PHAssetsManager {
    struct Singleton {
      static let instance = PHAssetsManager()
    }
    return Singleton.instance
  }
  
  
  func fetchAssets(withCompletion completion: @escaping (CompletionHandler)) {
    completionHandler = completion
    var items = NSMutableArray()
    let allPhotosOptions = PHFetchOptions()
     allPhotosOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.video.rawValue)
    allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]

    allPhotosResult = PHAsset.fetchAssets(with: allPhotosOptions) as! PHFetchResult<AnyObject>
    //    if allPhotosResult != nil
    //    {
    allPhotosResult.enumerateObjects({ (obj, idx, bool) in
        items.add(obj)
    })
    //    }
    if items.count > 0
    {
      completion(items)
    }
             print("Items Count:-\(items.count)")
    
//    var enumerate = {(group: ALAssetsGroup, stop: Bool) -> Void in
//      if (group.valueForProperty(ALAssetsGroupPropertyType)) as! UInt32 == ALAssetsGroupSavedPhotos {
//              group.setAssetsFilter(ALAssetsFilter.allVideos())
//              group.enumerateAssetsUsingBlock({ (assetfetched, index, stop) in
//                if assetfetched != nil {
//                  items.addObject(assetfetched)
//                }
//              })
//      }
//      else {
//            completion(assests: items)
//      }
//    }
    
   
  }
  
  class func defaultAssetsLibrary() -> PHPhotoLibrary {
    struct Singleton {
      static let instance = PHPhotoLibrary()
    }
    return Singleton.instance
  }
}
