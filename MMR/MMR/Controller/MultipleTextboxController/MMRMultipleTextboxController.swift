//
//  MMRMultipleTextboxController.swift
//  MMR
//
//  Created by Mindbowser on 11/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRMultipleTextboxController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable  {
    
    @IBOutlet weak var compulsaryStarLabel: UILabel!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var questionTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var multipleTextBoxScrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var multipleTextBoxTableView: UITableView!
    @IBOutlet weak var multipleTextBoxQuestionTextView: UITextView!
    @IBOutlet weak var multipleTextBoxScrollView: UIScrollView!
    @IBOutlet weak var navigationView: Navigation_View!
   
    weak var delegate : BaseClassMethodToFetchDataFromDB?

    var optionsArray         = NSMutableArray ()
    var optionIdArray = NSMutableArray()
    var optionOrgSeqArray = NSMutableArray()
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var isAnswerGiven       = Bool ()
    var characterLimit      = Int ()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var textFieldDataDict = [MMRAnsModel]()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        self.multipleTextBoxTableView.estimatedRowHeight = 400.0
        self.multipleTextBoxTableView.rowHeight = UITableViewAutomaticDimension
        initSetUp()
        navigationSetup()
        self.setText()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        // Handled dismiss of keyboard on clicking on scroll view
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MMRMultipleTextboxController.singleTap(_:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    @objc func singleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }

  @objc func setText() {
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    let questionID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(questionID!)"

  }
  

    func navigationSetup() {
        let questionID = UserDefaults.standard.value(forKey: serialNoID)
        
//        navigationView.titleLabel.text = "Question " + "\(questionID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
        
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.multipleTextBoxQuestionTextView.sizeThatFits(CGSize(width: self.multipleTextBoxQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextViewHeightConstraint.constant = newSize.height
        
        self.multipleTextBoxScrollViewHeight.constant = self.view.frame.size.height - 64
        
        self.multipleTextBoxScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height + self.multipleTextBoxTableView.contentSize.height)
        
        tableViewHeightConstraint.constant = self.multipleTextBoxTableView.contentSize.height
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
    }
  
    
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }
      
      self.multipleTextBoxTableView.backgroundColor = UIColor.lightGray
      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
        let val1 = String(format: "%d", serialID)

            let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists" {
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        let optionsTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue, userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        self.optionsArray = optionsTuple.optionText
        self.optionIdArray = optionsTuple.optionId
        self.optionOrgSeqArray = optionsTuple.originalSeqNo
        self.characterLimit = AnswerSharedClass.sharedInstance.fetchMaxCharForDiscriptiveType((questionFromServer["Question_ID"] as! NSString).integerValue, userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(self.optionsArray)")
      var index = 0
      for temp in self.optionIdArray {
        let tempAns = MMRAnsModel()
        tempAns.labelSelected = ""
        tempAns.optionIdAns = Int("\(temp)")!
        tempAns.answeredOptionSeq = Int("\(self.optionOrgSeqArray[index])")!
        self.textFieldDataDict.append(tempAns)
        index = index + 1
      }
//        multipleTextBoxQuestionTextView.text   = questionFromServer[questionText] as! String
//        multipleTextBoxQuestionTextView.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
            let quesString = questionFromServer[questionText] as! String
            quesString.attributedStringFromHTML { (resultString) in
              self.multipleTextBoxQuestionTextView.attributedText = resultString
            }

        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer["answerRequired"] as! NSString).boolValue
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButton.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            
        }
        
        
        self.multipleTextBoxTableView.reloadData()
        
        
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
        
    }
    // MARK: - Table View Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MultipleTextBoxCell = tableView.dequeueReusableCell(withIdentifier: "MultipleTextBoxCell")! as! MultipleTextBoxCell
        cell.selectionStyle = .none
        cell.multipleTextBoxLabel.text = self.optionsArray.object(at: indexPath.row) as? String
        cell.optionIdForTextBox = (self.optionIdArray.object(at: indexPath.row) as? String)!
        cell.optionSeqNo = (self.optionOrgSeqArray.object(at: indexPath.row) as? String)!
          if self.textFieldDataDict.indices.contains(indexPath.row) {
            let temp = self.textFieldDataDict[indexPath.row]
            cell.multipleTextBoxTextfield.text = temp.labelSelected
          }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.multipleTextBoxTableView.rowHeight;
    }

    // Mark:- Textfield Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if self.isMandatory {
            for i : Int in 0  ..< self.optionsArray.count  {
                let cell  = self.multipleTextBoxTableView.cellForRow(at: IndexPath(item: i, section: 0)) as! MultipleTextBoxCell
                let answerText = cell.multipleTextBoxTextfield.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let newLength1 = answerText!.characters.count + string.characters.count - range.length
                if  newLength1 < 1 {
                    isAnswerGiven                   = false
                    self.nextButton.backgroundColor = UIColor.lightGray
                } else {
                  let temp = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  if temp.characters.count > 0 {
                    self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                    isAnswerGiven                   = true
                  }
                    break
                }
                print("Print rows: \(cell.multipleTextBoxTextfield.text)")
            }
        } else
        {
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            isAnswerGiven                   = true
        }
       return newLength <= self.characterLimit // Bool
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if self.isMandatory {
        for i : Int in 0  ..< self.optionsArray.count  {
            let cell  = self.multipleTextBoxTableView.cellForRow(at: IndexPath(item: i, section: 0)) as! MultipleTextBoxCell
          if textField == cell.multipleTextBoxTextfield{
            let ans = MMRAnsModel()
            ans.labelSelected = cell.multipleTextBoxTextfield.text!
            ans.optionIdAns = Int(cell.optionIdForTextBox)!
            let answerText = cell.multipleTextBoxTextfield.text!
            self.textFieldDataDict.remove(at: i)
            self.textFieldDataDict.insert(ans, at: i)
//            self.textFieldDataDict[i] = ans
//            if  answerText == "" {
//                
//                self.nextButton.backgroundColor = UIColor.lightGrayColor()
//                isAnswerGiven                   = false
//                break
//                
//
//            } else {
//                 self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
//                isAnswerGiven                   = true
//
//            }
//            print("Print rows: \(cell.multipleTextBoxTextfield.text)")
//            
        }
      }
//        }
    }
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
//      let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        print(self.textFieldDataDict)
        let val1 = String(format: "%d", serialID)

      if self.textFieldDataDict.count > 0 {
      
//      var answerString = String()
      /*var cnt = 0
      for option in optionsArray {
        if self.textFieldDataDict[cnt] as? String != nil {
              if answerString != ""{
                answerString = "\(answerString)^]`,\(self.textFieldDataDict[cnt]!)"
          }
              else{
                answerString = "\(self.textFieldDataDict[cnt]!)"
          }
        }
        else {
          if answerString != ""{
            answerString = "\(answerString)^]`,NOT ANSWERED"
          }
          else{
            answerString = "NOT ANSWERED"
          }
        }
        cnt = cnt + 1
      }*/
        var answerString = String()
        let sortedAnsArray = self.textFieldDataDict.sorted(by: { $0.answeredOptionSeq < $1.answeredOptionSeq })
        
        for temp in sortedAnsArray {
          if answerString == "" {
            if temp.labelSelected == "" {
              answerString = "NOT ANSWERED"
            }
            else {
            answerString = "\(temp.labelSelected)"
            }
          }
          else {
            if temp.labelSelected == "" {
              answerString = "\(answerString)^]`,NOT ANSWERED"
            }
            else {
              answerString = "\(answerString)^]`,\(temp.labelSelected)"
            }
          }
        }
        
        print(answerString)
          self.delegate?.updateAnswerTextForQuestion(answerString, serialNo: "\(val1)")
      }
      else {
        self.delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
      }
          self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
      }
        if isAnswerGiven {
          let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
          if isTimeLeftForSurvey {
            UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
            delegate?.fetchNextQuestionType()
          } else {
            // Upload survey to server and return to Home
            
//            var compressionStatus = true
//            if let compCount = NSUserDefaults.standardUserDefaults().integerForKey("VIDEO_COMPRESSION_COUNT") as? Int {
//              
//              if compCount == 0 {
//                compressionStatus = true
//              }
//              else {
//                compressionStatus = false
//                startActivityAnimating(CGSize(width: 50,height: 50), message: "Compressing...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
//                NSNotificationCenter.defaultCenter().addObserverForName("VIDEO_COMPRESSION_COUNT_NOTIFICATION", object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (notification) in
//                  self.stopActivityAnimating()
//                  self.uploadAction()
//                })
//              }
//            }
//            
//            if compressionStatus == true {
//              uploadAction()
//            }
//            delegate!.goToHomeViewController()
            UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
            BaseClassForQuestions.serialNo_ID = -1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            delegate?.fetchNextQuestionType()
            
          }
        }
    }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
}
