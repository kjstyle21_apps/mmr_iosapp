//
//  MultipleTextBoxCell.swift
//  MMR
//
//  Created by Mindbowser on 12/6/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MultipleTextBoxCell: UITableViewCell {

    @IBOutlet weak var multipleTextBoxTextfield: UITextField!
    @IBOutlet weak var multipleTextBoxLabel: UILabel!
    var optionIdForTextBox = String()
    var optionSeqNo = String()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
