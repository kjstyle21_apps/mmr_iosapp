//
//  MMRRadioTypeQuestions.swift
//  MMR
//
//  Created by Mindbowser on 11/4/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

enum TypeOfQuestion : String {
    case Radio     = "oneOption"
    case CheckBox  = "multipleChoice"
    case TrueFalse = "trueFalse"
}

class MMRRadioTypeQuestions: UIViewController,UITableViewDelegate,UITableViewDataSource,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var scrollViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonRadioType: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var questionTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionTextView: UITextView!
    
    @IBOutlet weak var radioScrollView: UIScrollView!
    @IBOutlet weak var navigationView: Navigation_View!
    
    @IBOutlet weak var compulsaryStarLabel: UILabel!
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var optionsArray         = NSArray ()
    var optionsIDArray         = NSArray ()
    var QuestionType        = TypeOfQuestion(rawValue: "")
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var isAnswerGiven       = Bool ()
    var isNoneSelected      = Bool ()
    var selectedIndex       = Int ()
    var noneLabel           = String()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
    var orActionArray = [[String]]()
    var singleActionArray = [[String]]()
    var andActionArray = [[String]]()
    var orNextQuestion = [Int]()
    var andNextQuestion = [Int]()
    var singleNextQuestion = [Int]()
    var notNextQuestion = [Int]()
    var notActionArray = [[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.optionsTableView.delegate = self
        self.optionsTableView.dataSource = self
        self.optionsTableView.estimatedRowHeight = 400.0
        self.optionsTableView.rowHeight = UITableViewAutomaticDimension
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            if BMCentralManager.sharedInstance().isCentralReady == true {
                self.navigationView.statusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.navigationView.statusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
            NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        }
        initSetUp()
        navigationSetup()
        self.setText()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
    }
    
    @objc func bluetoothStatus(_ notify : Notification) {
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            let userinfo = notify.userInfo as! NSDictionary
            if userinfo.value(forKey: "state") as! String == "ON" {
                self.navigationView.statusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.navigationView.statusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
        }
    }
    
    @objc func onFunc() {
        print("Bluetooth is on reconnect the device")
        UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
        UserDefaults.standard.synchronize()
        //    let mmrBLEControlNav = UIStoryboard.init(name: "MMRSurvey", bundle: nil).instantiateViewControllerWithIdentifier("MMRBLEViewController") as? MMRBLEViewController
        //    mmrBLEControlNav?.startBLEFunctionality()
        MMRBLEModule.sharedInstance.startBLEFunctionality()
    }
    
    func offFunc() {
        print("Bluetooth off device disconnected")
        //    AlertModel.sharedInstance.showErrorMessage(bluetoothePowerOffMessage)
    }
    
    
    @objc func setText() {
        nextButtonRadioType.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
        let val1 = String(format: "%d", serialID as! CVarArg)
        //    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
        navigationView.titleLabel.text = ""
    }
    
    func navigationSetup() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
        
        //        navigationView.titleLabel.text = "Question " + "\(serialID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButtonRadioType.roundbutton(self.nextButtonRadioType, cornerradius: 5)
        
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
        self.dropTime = Date()
        self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)
        
        //      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
        //      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
        
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        //      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
        let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
        
        if status == "Exists" {
            //Exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
        }
        else if status == "No_Exists" {
            //no exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
        }
        delegate!.goToHomeViewController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.questionTextView.sizeThatFits(CGSize(width: self.questionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextViewHeightConstraint.constant = newSize.height + 12
        
        self.scrollViewheightConstraint.constant = self.view.frame.size.height - 64
        self.radioScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height)
        
        tableViewHeightConstraint.constant = self.optionsTableView.contentSize.height
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.optionsTableView.backgroundColor = UIColor.lightGray
    }
    
    func initSetUp() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
        self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        
        //      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
        //        if mflag {
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
        //          if seqNum > 1 {
        questionFromServer.setValue("", forKey: answerStartTime)
        questionFromServer.setValue("", forKey: answerEndTime)
        questionFromServer.setValue("", forKey: isAnswered)
        questionFromServer.setValue("", forKey: answer_text)
        questionFromServer.setValue(serialID, forKey: serial_id)
        questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
        questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
        questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
        questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
        questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
        let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        if resultArray.count == 0 {
            QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
        }
        //          }
        //        }
        //      }
        
        self.inTime = Date()
        self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)
        
        //      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
        //      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
        let val1 = String(format: "%d", serialID)
        let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        if status == "No_Exists"{
            self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
        }
        
        self.optionsArray = []
        self.optionsIDArray = []
        let optionsTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        
        self.optionsArray = optionsTuple.optionText
        self.optionsIDArray = optionsTuple.optionId
        
        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(self.optionsArray)")
        
        noneLabel = AnswerSharedClass.sharedInstance.none_label
        print(noneLabel)
        
        let quesString = questionFromServer[questionText] as! String
        //      let ques = "u82f1"
        quesString.attributedStringFromHTML { (resultString) in
            self.questionTextView.attributedText = resultString
        }
        
        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        QuestionType            = TypeOfQuestion(rawValue: QuestionSharedClass.sharedInstance.fetchQuestionType(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject))
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButtonRadioType.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButtonRadioType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
        
        if QuestionType?.rawValue == "multipleChoice" {
            self.optionsTableView.allowsMultipleSelection = true
        } else {
            self.optionsTableView.allowsMultipleSelection = false
            self.optionsTableView.allowsSelection = true
        }
        self.optionsTableView.reloadData()
        
        let routingQuestionID = (questionFromServer["Question_ID"] as! NSString).integerValue
        let routeArray = QuestionSharedClass.sharedInstance.fetchRoutingForQuestionID("\(userID!)", questionID: NSNumber(integerLiteral: routingQuestionID),fetchedSurveyID: currentSurveyID! as AnyObject) as[[String : String]]
        if routeArray.count > 0
        {
            for tempRoute in routeArray {
                let neededArrayString = tempRoute[selectedOptions]
                let neededArray = neededArrayString?.components(separatedBy: ",")
                let action = tempRoute[actionName]!
                
                switch action {
                case "OR":
                    //              self.orActionArray = neededArray!
                    //              let nextQuestion = tempRoute[nextQuestionID]!
                    //              self.orNextQuestion = Int(nextQuestion)!
                    self.orActionArray.append(neededArray!)
                    //            self.orActionArray = neededArray!
                    let nextQuestion = tempRoute[nextQuestionID]!
                    //            self.orNextQuestion = Int(nextQuestion)!
                    self.orNextQuestion.append(Int(nextQuestion)!)
                    break
                    
                case "AND":
                    //              self.andActionArray = neededArray!
                    //              let nextQuestion = tempRoute[nextQuestionID]!
                    //              self.andNextQuestion = Int(nextQuestion)!
                    self.andActionArray.append(neededArray!)
                    //            self.andActionArray = neededArray!
                    let nextQuestion = tempRoute[nextQuestionID]!
                    //            self.andNextQuestion = Int(nextQuestion)!
                    self.andNextQuestion.append(Int(nextQuestion)!)
                    
                    break
                    
                case "SINGLE":
                    //              self.singleActionArray = neededArray!
                    //              let nextQuestion = tempRoute[nextQuestionID]!
                    //              self.singleNextQuestion = Int(nextQuestion)!
                    self.singleActionArray.append(neededArray!)
                    //            self.singleActionArray = neededArray!
                    let nextQuestion = tempRoute[nextQuestionID]!
                    //            self.singleNextQuestion = Int(nextQuestion)!
                    self.singleNextQuestion.append(Int(nextQuestion)!)
                    break
                    
                case "NOT":
                    //            self.notActionArray = neededArray!
                    //            let nextQuestion = tempRoute[nextQuestionID]!
                    //            self.notNextQuestion = Int(nextQuestion)!
                    self.notActionArray.append(neededArray!)
                    //            self.notActionArray = neededArray!
                    let nextQuestion = tempRoute[nextQuestionID]!
                    //            self.notNextQuestion = Int(nextQuestion)!
                    self.notNextQuestion.append(Int(nextQuestion)!)
                    break
                default:
                    break
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK :- Tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:RadioTypeCell = tableView.dequeueReusableCell(withIdentifier: "RadioTypeCell")! as! RadioTypeCell
        self.setUpCellForIndexPath(cell, indexPath: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.optionsTableView.rowHeight;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : RadioTypeCell = tableView.cellForRow(at: indexPath) as! RadioTypeCell
        let totalRows = optionsTableView.numberOfRows(inSection: 0)
        
        selectedIndex = indexPath.row
        print("Selected Row:-\(selectedIndex)")
        if QuestionType?.rawValue == "multipleChoice" {
            if cell.isSelected {
                if noneLabel == "none" && selectedIndex != self.optionsArray.count - 1 {
                    let lastIndexPath = IndexPath(row: totalRows - 1 , section: 0)
                    let lastCell : RadioTypeCell = tableView.cellForRow(at: lastIndexPath) as! RadioTypeCell
                    tableView.deselectRow(at: lastIndexPath, animated: true)
                    lastCell.radioImageView.image = UIImage(named: "radioImage")
                }
                cell.radioImageView.image = UIImage(named: "checkboxImageSelected")
                isAnswerGiven = true
                self.nextButtonRadioType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            }
            else
            {
                cell.radioImageView.image = UIImage(named: "checkboxImage")
            }
            if noneLabel == "none" && selectedIndex == self.optionsArray.count - 1
            {
                for  index  in tableView.indexPathsForSelectedRows! /*where index != indexPath*/ {
                    print("index:\(index.row)")
                    if index != indexPath
                    {
                        let currentCell : RadioTypeCell = tableView.cellForRow(at: index) as! RadioTypeCell
                        currentCell.radioImageView.image = UIImage(named: "checkboxImage")
                        tableView.deselectRow(at: index, animated: true)
                    }
                }
                cell.radioImageView.image   = UIImage(named: "radioImageSelected")
            }
        }
        else
        {
            if cell.isSelected {
                isAnswerGiven               = true
                self.nextButtonRadioType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                
                cell.radioImageView.image   = UIImage(named: "radioImageSelected")
            } else {
                cell.radioImageView.image   = UIImage(named: "radioImage")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : RadioTypeCell = tableView.cellForRow(at: indexPath) as! RadioTypeCell
        
        let totalRows = optionsTableView.numberOfRows(inSection: 0)
        print(totalRows)
        print(indexPath.row)
        if QuestionType?.rawValue == "multipleChoice" {
            
            if indexPath.row == self.optionsArray.count - 1{
                if noneLabel == "none"{
                    cell.radioImageView.image = UIImage(named: "radioImage")
                }
                else
                {
                    cell.radioImageView.image = UIImage(named: "checkboxImage")
                }
            }
            else
            {
                cell.radioImageView.image = UIImage(named: "checkboxImage")
            }
            if self.isMandatory {
                let selectedIndexPaths = self.optionsTableView.indexPathsForSelectedRows
                if (selectedIndexPaths != nil) {
                    
                } else
                {
                    self.isAnswerGiven = false
                    self.nextButtonRadioType.backgroundColor = UIColor.lightGray
                }
            }
            else if noneLabel == "none" && indexPath.row == totalRows - 1
            {
                cell.radioImageView.image = UIImage(named: "radioImage")
            }
            else {
                self.nextButtonRadioType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
                self.isAnswerGiven = true
            }
            
        } else {
            isAnswerGiven             = false
            cell.radioImageView.image = UIImage(named: "radioImage")
            self.nextButtonRadioType.backgroundColor = UIColor.lightGray
        }
    }
    
    func setUpCellForIndexPath(_ cell : RadioTypeCell, indexPath : IndexPath)  {
        
        if var QuestionType = QuestionType {
            switch QuestionType {
            case .Radio     :
                
                cell.optionsLabel.text   = self.optionsArray.object(at: indexPath.row) as? String
                cell.radioImageView.image = UIImage(named: "radioImage")
                
                break
                
            case .CheckBox  :
                let totalRows = optionsTableView.numberOfRows(inSection: 0)
                
                cell.optionsLabel.text    = self.optionsArray.object(at: indexPath.row) as? String
                if self.isNoneSelected && indexPath.row == selectedIndex
                {
                    self.isNoneSelected = false
                    cell.radioImageView.image = UIImage(named: "radioImageSelected")
                    let rowToSelect:IndexPath = IndexPath(row: selectedIndex, section: 0)
                    
                    self.optionsTableView.selectRow(at: rowToSelect, animated: false, scrollPosition: .none)
                    print("selected row:\(self.optionsTableView.indexPathForSelectedRow)")
                    
                }
                else
                {
                    print("Total rows:-\(totalRows)")
                    print("IndexPath Row:-\(indexPath.row)")
                    
                    if noneLabel == "none" && indexPath.row == totalRows - 1
                    {
                        cell.radioImageView.image = UIImage(named: "radioImage")
                    }
                    else
                    {
                        cell.radioImageView.image = UIImage(named: "checkboxImage")
                    }
                }
                break
                
            case .TrueFalse :
                
                cell.optionsLabel.text    = self.optionsArray.object(at: indexPath.row) as? String
                cell.radioImageView.image = UIImage(named: "radioImage")
                
                break
                
            default:
                break
            }
        }
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        self.outTime = Date()
        self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
        //    let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
        //    self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
        
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        var answerArray = [String]()
        let val2 = String(format: "%d", serialID)
        
        let selectedIndexPaths = self.optionsTableView.indexPathsForSelectedRows
        
        if selectedIndexPaths?.count > 0 {
            for indexP in selectedIndexPaths! {
                let selectedOptionId = self.optionsIDArray.object(at: indexP.row)
                let val1 = "\(selectedOptionId)"
                answerArray.append(val1)
            }
            
            let answerString = answerArray.joined(separator: "^]`,")
            print("answerString: \(answerString)")
            self.delegate?.updateAnswerTextForQuestion(answerString, serialNo: "\(val2)")
            
        }else{
            self.delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val2)")
        }
        
        self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val2)")
        let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val2)",userID: userID! as AnyObject)
        
        if status == "Exists" {
            //Exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val2)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
        }
        
        if isAnswerGiven {
            let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
            if isTimeLeftForSurvey {
                print("Answer Array : \(answerArray)")
                //        let answerArrayNewString = answerArray.joinWithSeparator(",")
                let reachability: Reachability
                do {
                    reachability = try Reachability.reachabilityForInternetConnection()
                } catch {
                    print("Unable to create Reachability")
                    return
                }
                var matchFlag = false
                if matchFlag == false {
                    var cnt1 = 0
                    for temp in self.andActionArray {
                        cnt1 = cnt1 + 1
                        if self.containSameElements(answerArray, temp) {
                            matchFlag = true
                            if reachability.isReachable() {
                                //                    MMRUploadMidSurvey.sharedInstance.getMultipleSubmitAnswerJson()
                                //                    self.getMultipleSubmitAnswerJson()
                            }
                            delegate?.fetchNextQuestionTypeWithSerialID(andNextQuestion[cnt1 - 1])
                            break
                        }
                    }
                }
                
                if matchFlag == false {
                    var cnt2 = 0
                    for temp in self.orActionArray {
                        cnt2 = cnt2 + 1
                        if self.containSomeSimilarElements(answerArray, array2: temp) {
                            matchFlag = true
                            if reachability.isReachable() {
                                //                    MMRUploadMidSurvey.sharedInstance.getMultipleSubmitAnswerJson()
                                //                    self.getMultipleSubmitAnswerJson()
                            }
                            delegate?.fetchNextQuestionTypeWithSerialID(orNextQuestion[cnt2 - 1])
                            break
                        }
                    }
                }
                
                if matchFlag == false {
                    var cnt3 = 0
                    for temp in self.singleActionArray {
                        cnt3 = cnt3 + 1
                        if self.containSomeSimilarElements(answerArray, array2: temp) {
                            matchFlag = true
                            if reachability.isReachable() {
                                //                    MMRUploadMidSurvey.sharedInstance.getMultipleSubmitAnswerJson()
                                //                    self.getMultipleSubmitAnswerJson()
                            }
                            delegate?.fetchNextQuestionTypeWithSerialID(singleNextQuestion[cnt3 - 1])
                            break
                        }
                    }
                }
                
                if matchFlag == false {
                    var cnt4 = 0
                    for temp in self.notActionArray {
                        cnt4 = cnt4 + 1
                        if !self.containSomeSimilarElements(answerArray, array2: temp) {
                            matchFlag = true
                            if reachability.isReachable() {
                                //                    MMRUploadMidSurvey.sharedInstance.getMultipleSubmitAnswerJson()
                                //                    self.getMultipleSubmitAnswerJson()
                            }
                            delegate?.fetchNextQuestionTypeWithSerialID(notNextQuestion[cnt4 - 1])
                            break
                        }
                    }
                }
                
                if matchFlag == false {
                    if reachability.isReachable() {
                        //                MMRUploadMidSurvey.sharedInstance.getMultipleSubmitAnswerJson()
                    }
                    delegate?.fetchNextQuestionType()
                }
                
                UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
            } else {
                UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
                BaseClassForQuestions.serialNo_ID = -1
                UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                delegate?.fetchNextQuestionType()
            }
        }
    }
    
    func uploadAction() {
        startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
        let uploadSurvey = MMRUploadSurvey()
        uploadSurvey.uploadDelegate = self
        uploadSurvey.surveySubmissionStatus = false
        uploadSurvey.uploadSurvey()
    }
    
    func containSameElements<T: Comparable>(_ array1: [T], _ array2: [T]) -> Bool {
        guard array1.count == array2.count else {
            return false // No need to sorting if they already have different counts
        }
        return array1.sorted() == array2.sorted()
    }
    
    func containSomeSimilarElements(_ array1: [String],array2: [String]) -> Bool {
        //    guard array1.count == array2.count else {
        //      return false // No need to sorting if they already have different counts
        //    }
        
        for temp in array2 {
            if array1.contains(temp) {
                return true
            }
        }
        return false
    }
    
    func uploadCompletion(_ status: Bool) {
        if status {
            print("Success")
            AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
                let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                UserDefaults.standard.synchronize()
                self.delegate!.goToHomeViewController()
            })
        }
        else {
            print("Failure")
        }
        stopActivityAnimating()
    }
}

