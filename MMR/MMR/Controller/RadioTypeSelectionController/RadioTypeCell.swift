//
//  RadioTypeCell.swift
//  MMR
//
//  Created by Mindbowser on 11/8/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class RadioTypeCell: UITableViewCell {

    @IBOutlet weak var radioImageView: UIImageView!
    @IBOutlet weak var optionsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
       
//        if selected {
//            self.radioImageView.image = UIImage(named: "radioImageSelected")
//        } else {
//            self.radioImageView.image = UIImage(named: "radioImage")
//        }
        // Configure the view for the selected state
    }

}
