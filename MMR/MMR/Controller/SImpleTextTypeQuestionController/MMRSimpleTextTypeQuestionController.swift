//
//  MMRSimpleTextTypeQuestionController.swift
//  MMR
//
//  Created by Mindbowser on 4/7/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRSimpleTextTypeQuestionController: UIViewController,UITextViewDelegate,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {
  
  @IBOutlet weak var navigationView: Navigation_View!
  @IBOutlet weak var nextButton: UIButton!
  
  @IBOutlet weak var instructionAndTipsTextView: UITextView!
  @IBOutlet weak var instructionsAndTipsScollView: UIScrollView!
  @IBOutlet weak var heightOfInstructionTextView: NSLayoutConstraint!
  @IBOutlet weak var verticalDistanceBetweenTextViewAndButton: NSLayoutConstraint!
  
  @IBOutlet var scrollViewHeightConstraint: NSLayoutConstraint!
  
  weak var delegate : BaseClassMethodToFetchDataFromDB?
  var isHintAvailable     = Bool ()
  var isMandatory         = Bool ()
  var questionFromServer  = NSMutableDictionary ()
//  let placeholderlabel    = UILabel ()
  var isAnswerGiven       = Bool ()
//  var characterLimit      = Int ()
  var inTime = Date()
  var outTime = Date()
  var dropTime = Date()
  var finalOutTimeStr = String()
  var finalInTimeStr = String()
  var finalDropTime = String()

    override func viewDidLoad() {
        super.viewDidLoad()
      initSetUp()
      self.setText()
      navigationSetup()
      self.instructionsAndTipsScollView.delaysContentTouches = false
      self.instructionsAndTipsScollView.canCancelContentTouches = false
      
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
        
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let newSize : CGSize = self.instructionAndTipsTextView.sizeThatFits(CGSize(width: self.instructionAndTipsTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
    print(newSize)
    self.heightOfInstructionTextView.constant = newSize.height
    
    self.scrollViewHeightConstraint.constant = self.view.frame.size.height - 64
    
    self.instructionsAndTipsScollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height + 200)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  func initSetUp()
  {
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
//    if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//      if mflag {
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//        if seqNum > 1 {
          questionFromServer.setValue("", forKey: answerStartTime)
          questionFromServer.setValue("", forKey: answerEndTime)
          questionFromServer.setValue("", forKey: isAnswered)
          questionFromServer.setValue("", forKey: answer_text)
          questionFromServer.setValue(serialID, forKey: serial_id)
          questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
          questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
          questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
          let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
          if resultArray.count == 0 {
            QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
          }
//        }
//      }
//    }
    
    self.inTime = Date()
    self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//    let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    print(inTime)
    let val1 = String(format: "%d", serialID)
    let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
    if status == "No_Exists"{
      self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
    }

    let quesString = questionFromServer[questionText] as! String
//    let quesString = "<span style=\"font-family:tahoma;font-size:14px\"><i>Thanks for your help. You're all done for today!</i><div><i><br/></i></div><div><i><b>Please click next and submit your data.</b></i></div></sapn>"
    quesString.attributedStringFromHTML { (resultString) in
      self.instructionAndTipsTextView.attributedText = resultString
    }
      isAnswerGiven                   = true
      self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    print("questionFromServer\(questionFromServer)")
  }

  @objc func setText() {
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
//    let questionID = NSUserDefaults.standardUserDefaults().valueForKey(serialNoID)
    //    navigationView.titleLabel.text = "Question ".localized() + "\(questionID!)"
  }
  
  func navigationSetup() {
    
    let questionID = UserDefaults.standard.value(forKey: serialNoID)
    //        navigationView.titleLabel.text = "Question " + "\(questionID!)"
    
    navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
    navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)

    self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
    self.instructionAndTipsTextView.delegate = self;
    
  }
  
  
  @objc func rightButtonAction() {
    self.dropTime = Date()
    self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//    let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
    }
    else if status == "No_Exists" {
      //no exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
    }
    delegate!.goToHomeViewController()
  }
  

  
  @IBAction func nextClicked(_ sender: AnyObject) {
    self.outTime = Date()
    self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)

//    let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
    self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
    
    delegate?.updateAnswerTextForQuestion("info", serialNo: "\(val1)")
    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
    }
    
    let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
    if isTimeLeftForSurvey {
      UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
      delegate?.fetchNextQuestionType()
    } else {
      UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
      BaseClassForQuestions.serialNo_ID = -1
      UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
      delegate?.fetchNextQuestionType()
    }
    
  }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
    stopActivityAnimating()
  }
  
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
