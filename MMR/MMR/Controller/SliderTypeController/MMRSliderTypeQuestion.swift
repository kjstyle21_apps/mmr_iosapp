//
//  MMRSliderTypeQuestion.swift
//  MMR
//
//  Created by Mindbowser on 10/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class MMRSliderTypeQuestion: UIViewController,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable {

    @IBOutlet weak var maxDiscriptionLabel: UILabel!
    @IBOutlet weak var minDiscriptionLabel: UILabel!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonSliderType: UIButton!
    @IBOutlet weak var navigationView: Navigation_View!
    @IBOutlet weak var sliderQuestionTextView: UITextView!
    @IBOutlet weak var questionTextviewHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderQuestionScrollView: UIScrollView!
    
    @IBOutlet weak var compulsaryStarLabel: UILabel!
    
    @IBOutlet weak var heightOfMinOrMaxDiscLabel: NSLayoutConstraint!
  
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var questionFromServer  = NSMutableDictionary ()
    var isAnswerGiven       = Bool ()
    var stepOfSlider        = Float()
    var sliderForMMR        = MBSliderView()
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
    var tipsMsg = String()
    var sliderValueChanged : Float!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
        initSetUp()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {

      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        navigationSetup()
        self.setText()
    }
  
  func setText() {
    print(NextButtonTitleKeyword.localized())
    print(Localize.currentLanguage())
    self.nextButtonSliderType.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    let questionID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(questionID!)"
    self.tipsMsg = MMRInstructionsTips.localized()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }

  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }

  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
        sliderQuestionTextView.setContentOffset(CGPoint.zero, animated: false)

        let newSize : CGSize = sliderQuestionTextView.sizeThatFits(CGSize(width: self.sliderQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
//        self.questionTextviewHeight.constant = 0
//        self.sliderQuestionTextView.contentSize.height = newSize.height
//        newSize.height = newSize.height - 50
        self.questionTextviewHeight.constant = newSize.height
        print("Textview text : \(self.sliderQuestionTextView.text) :text")
//        self.sliderQuestionTextView.contentInset = UIEdgeInsetsMake(-70.0,0.0,0,0.0)
//      self.sliderQuestionTextView.scrollRangeToVisible(NSMakeRange(0, 0))

        self.sliderQuestionScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height+200)
        
        sliderForMMR.frame = CGRect(x: sliderQuestionScrollView.frame.origin.x + 30 , y: newSize.height + 80 , width: self.sliderQuestionScrollView.frame.size.width - 60, height: 100)
//        sliderForMMR.slider.
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: serial_id)
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }
      
      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      print(inTime)
    let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists" {
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        let optionTuple = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      
//       let optionsForQuestionFromServer = AnswerSharedClass.sharedInstance.fetchDataForOptions((questionFromServer["Question_ID"] as! NSString).integerValue)
      let optionsForQuestionFromServer = optionTuple.optionText
      let optionsIDForQuestionFromServer = optionTuple.optionId
      
        if optionsForQuestionFromServer.count > 0 {
            
            var fullString: String = optionsForQuestionFromServer[0] as! String
            let fullNameArr = fullString.components(separatedBy: "-")
            
            self.minDiscriptionLabel.text = fullNameArr[0]
            self.maxDiscriptionLabel.text = fullNameArr[1]
        }
        
         let rangeOfSlider = AnswerSharedClass.sharedInstance.fetchMinMaxAndFreqForSlider((questionFromServer["Question_ID"] as! NSString).integerValue,userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        
//        sliderQuestionTextView.text = questionFromServer [questionText] as! String
//         sliderQuestionTextView.attributedText = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
            let quesString = questionFromServer[questionText] as! String
            quesString.attributedStringFromHTML { (resultString) in
              self.sliderQuestionTextView.attributedText = resultString
            }
      
        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
        
        let newSize : CGSize = self.sliderQuestionTextView.sizeThatFits(CGSize(width: self.sliderQuestionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        
        // configure sliderFromCode
        sliderForMMR.frame = CGRect(x: sliderQuestionScrollView.frame.origin.x + 30 , y: newSize.height + 80 , width: self.sliderQuestionScrollView.frame.size.width - 60, height: 100)
        
        sliderForMMR.minValue =  (rangeOfSlider["Min_Value"] as? NSString)!.floatValue
        sliderForMMR.maxValue =  (rangeOfSlider["Max_Value"] as? NSString)!.floatValue
        sliderForMMR.currentValue = sliderForMMR.minValue
        let sliderCenter = (sliderForMMR.maxValue - sliderForMMR.minValue) / 2
        sliderForMMR.currentValue = sliderCenter
        let freq = (rangeOfSlider["Frequency"] as! NSString).floatValue
        //sliderForMMR.currentValue = freq
        if freq.truncatingRemainder(dividingBy: 1) == 0 {
            sliderForMMR.ignoreDecimals = true   // default value

        } else {
            sliderForMMR.ignoreDecimals = false   // default value
        }

        sliderForMMR.step = (rangeOfSlider["Frequency"] as! NSString).floatValue
//      sliderForMMR.step = 0.5
        sliderForMMR.animateLabel = true      // default value
        sliderForMMR.delegate = self
        sliderForMMR.tintColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        self.sliderQuestionScrollView.addSubview(sliderForMMR)
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButtonSliderType.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButtonSliderType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }        
        print("questionFromServer\(questionFromServer)")
        print("optionsForQuestionFromServer\(optionsForQuestionFromServer)")
    }
    
    func navigationSetup() {
        
        let questionID = UserDefaults.standard.value(forKey: serialNoID)
//        navigationView.titleLabel.text = "Question " + "\(questionID!)"
      
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButtonSliderType.roundbutton(self.nextButtonSliderType, cornerradius: 5)
        
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(self.tipsMsg, message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)

//      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
    let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
        delegate!.goToHomeViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextButtonOfSliderTypeClicked(_ sender: UIButton) {
      self.outTime = Date()
      self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)

//      let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")

      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      var ansStr = String()
    let val1 = String(format: "%d", serialID)
      if self.sliderValueChanged == nil {
        self.delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
      }
      else {
        if sliderForMMR.step.truncatingRemainder(dividingBy: 1)  != 0 {
          ansStr = NSString(format: "%.01f", self.sliderValueChanged) as String
        }
        else {
          let integer = Int(self.sliderValueChanged)
          ansStr = "\(integer)"
        }
        print(ansStr)
        self.delegate?.updateAnswerTextForQuestion(ansStr, serialNo: "\(val1)")
      }
      

      self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
      }
        if isAnswerGiven {
          
          let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
          if isTimeLeftForSurvey {
            let reachability: Reachability
            do {
                reachability = try Reachability.reachabilityForInternetConnection()
            } catch {
                print("Unable to create Reachability")
                return
            }
            
            if reachability.isReachable() {
//                self.getMultipleSubmitAnswerJson()
            }
            
            UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
            delegate?.fetchNextQuestionType()
          } else {
            // Upload survey to server and return to Home
            
            
//            var compressionStatus = true
//            if let compCount = NSUserDefaults.standardUserDefaults().integerForKey("VIDEO_COMPRESSION_COUNT") as? Int {
//              
//              if compCount == 0 {
//                compressionStatus = true
//              }
//              else {
//                compressionStatus = false
//                startActivityAnimating(CGSize(width: 50,height: 50), message: "Compressing...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
//                NSNotificationCenter.defaultCenter().addObserverForName("VIDEO_COMPRESSION_COUNT_NOTIFICATION", object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (notification) in
//                  self.stopActivityAnimating()
//                  self.uploadAction()
//                })
//              }
//            }
//            
//            if compressionStatus == true {
//              uploadAction()
//            }
//            
            UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
            BaseClassForQuestions.serialNo_ID = -1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            delegate?.fetchNextQuestionType()

          }
        }
    }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
  
  func goToHomeScreen () {
    var viewController: UIViewController?
    
    for aViewController in self.navigationController!.viewControllers {
      if aViewController.isKind(of: SSASideMenu.self) {
        viewController = aViewController
        break
      }
    }
    
    if let viewController = viewController {
      self.navigationController!.popToViewController(viewController, animated: true)
    }
  }
    
    func getMultipleSubmitAnswerJson() {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        var answerJson: [NSMutableDictionary] = QuestionSharedClass.sharedInstance.fetchQuestionData(userID! as AnyObject, seqNum : 1 as AnyObject, surveySubmitStatus: true)
        if answerJson == [["":""]] {
            answerJson.removeAll()
            answerJson = QuestionSharedClass.sharedInstance.getDummyJsonifFailsToCreate()
        }
        print("Answer Json : \(answerJson)")
    }
  
}



extension MMRSliderTypeQuestion: MBSliderDelegate {
    func sliderView(_ sliderView: MBSliderView, valueDidChange value: Float) {
        if sliderView == sliderForMMR {
            isAnswerGiven                   = true
            self.nextButtonSliderType.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            print("sliderFromCode: \(value)")
            self.sliderValueChanged = Float()
            self.sliderValueChanged = value
        }
    }
}
