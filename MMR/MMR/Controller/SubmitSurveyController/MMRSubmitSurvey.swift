//
//  MMRSubmitSurvey.swift
//  MMR
//
//  Created by Mindbowser on 11/17/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l >= r
    default:
        return !(lhs < rhs)
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}



class MMRSubmitSurvey: UIViewController,NVActivityIndicatorViewable,MMRSubmitSurveyDelegate, MMRMediaUploaderDelegate
    
{
    
    @IBOutlet weak var navigationView: Navigation_View!
    
    @IBOutlet weak var submitSurveyButton: UIButton!
    //    let aws = AWSUploadManager()
    var uploadedDataPathArray = [String]()
    var uploadedDataSrNoArray = [String]()
    var counter = 0
    var uploadDoneCounter = 0
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    let uploadSurvey = MMRUploadSurvey()
    @IBOutlet var quoteLabel: UILabel!
    //  let quotesArray = ["Quote 1", "Quote 2", "Quote 3", "Quote 4", "Quote 5"]
    let quotesArray = [""]
    var quoteCount = 0
    var quoteCounter = 0
    var timer = Timer()
    var uploadProgressTimer = Timer()
    
    @IBOutlet var memorySizeLabel: UILabel!
    
    @IBOutlet var congoMessageLabel: UILabel!
    @IBOutlet var internetConnectionLabel: UILabel!
    let aws = AWSUploadManager()
    let uploader = MMRMediaUploader.sharedInstance
    var hud = MBProgressHUD()
    var blackView = UIView()
    var isSubmitClicked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        aws.delegate = self
        // Do any additional setup after loading the view.
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            if BMCentralManager.sharedInstance().isCentralReady == true {
                self.navigationView.statusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.navigationView.statusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
        }
        navigationView.progressView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setText()
        quoteCount = quotesArray.count
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        self.submitSurveyButton.roundbutton(self.submitSurveyButton, cornerradius: 5)
        NotificationCenter.default.addObserver(self, selector: #selector(self.connectionUpdate(_:)) , name: NSNotification.Name(rawValue: "INTERNET_CONNECTION_FAIL"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        self.internetConnectionLabel.text = "Make sure you have active internet connection and then submit the survey.".localized()
        
        if let temp =  UserDefaults.standard.bool(forKey: SURVEY_COMPLETE_STATUS_UD) as? Bool {
            if !temp {
                self.congoMessageLabel.text = "Survey end time reached. \n Kindly submit the survey by tapping the Submit button below.".localized()
            }
            else {
                self.congoMessageLabel.text = "Congratulations! \n You have completed the survey.".localized()
            }
        }
        else {
            self.congoMessageLabel.text = "Congratulations! \n You have completed the survey.".localized()
        }
        
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        }
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if !reachability.isReachable() {
            AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.uploadProgressTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
    }
    
    @objc func bluetoothStatus(_ notify : Notification) {
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
            let userinfo = notify.userInfo as! NSDictionary
            if userinfo.value(forKey: "state") as! String == "ON" {
                self.navigationView.statusLabel.isHidden = true
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = false
                })
            }
            else {
                self.navigationView.statusLabel.isHidden = false
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isStatusBarHidden = true
                })
            }
        }
    }
    
    @objc func onFunc() {
        print("Bluetooth is on reconnect the device")
        UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
        UserDefaults.standard.synchronize()
        MMRBLEModule.sharedInstance.startBLEFunctionality()
    }
    
    @objc func connectionUpdate(_ notify : Notification) {
        DispatchQueue.main.async {
            self.hud.hide(true)
            self.blackView.removeFromSuperview()
        }
        timer.invalidate()
        stopActivityAnimating()
        uploadSurvey.pauseAll()
        uploadSurvey.isExecutedJson = false
        AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
    }
    
    @objc func setText() {
        self.quoteLabel.isHidden = true
        self.navigationView.titleLabel.text = submitSurveyTitleKeyword.localized()
        self.submitSurveyButton.setTitle(submitButtonTitleKeyword.localized(), for: UIControlState())
    }
    
    @objc func rightButtonAction() {
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(true, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.synchronize()
        //      delegate?.goToHomeViewController()
        goToHomeScreen()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func needToPerformForMultipleSurveyIfTimeLeft() {
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        //MULTIPLE_ONLINE CHANGE
        
        if reachability.isReachable() {
            //internet connected
            print("Connected inside")
            var compressionStatus = true
            //if let compCount = NSUserDefaults.standardUserDefaults().integerForKey("VIDEO_COMPRESSION_COUNT") as? Int
            if let compCount = CompressionRecordsSharedClass.sharedInstance.countOfCompressionData(userID1! as AnyObject) as? Int {
                if compCount == 0 {
                    compressionStatus = true
                }
                else {
                    compressionStatus = false
                    if (UIApplication.shared.delegate as? AppDelegate)?.totalNotUploadedRecords == 0 {
                        (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress = 90.0
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.blackView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                        self.blackView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65)
                        self.view.addSubview(self.blackView)
                        self.hud = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate!.window)!, message: "Uploading", animated: true)
                        self.hud.mode = MBProgressHUDModeAnnularDeterminate
                        self.uploadProgressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MMRSubmitSurvey.updateProgress), userInfo: nil, repeats: true)
                    })
                    
                    //                        startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
                    self.quoteLabel.layer.zPosition = 1
                    timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(MMRSubmitSurvey.update), userInfo: nil, repeats: true)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.multipleSubmitAction), name: NSNotification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
                    uploader.delegate = self
                }
            }
            UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
            let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
            AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
            if compressionStatus == true {
                self.multipleSubmitAction()
            }
        }
        else {
            UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
            BaseClassForQuestions.serialNo_ID = 1
            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
            UserDefaults.standard.synchronize()
            
            let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID1!)") as! Int
            UserDefaults.standard.set(seqNum + 1, forKey: "SURVEY_SEQ_NUM_\(userID1!)")
            let surveyid = UserDefaults.standard.value(forKey: surveyId)
            SurveyRecordsSharedClass.sharedInstance.updateSeqNumOfSurvey(seqNum + 1 as AnyObject, surveyID: surveyid! as AnyObject, userID: userID1! as AnyObject)
            self.goToHomeScreen ()
        }
        
        UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
        let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
        AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
        QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
        
        //remove this while multiple online
        //            self.goToHomeScreen ()
        
    }
    
    func needToPerformForMultipleSurveyIfTimeNotLeft() {
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable() {
            var compressionStatus = true
            // if let compCount = NSUserDefaults.standardUserDefaults().integerForKey("VIDEO_COMPRESSION_COUNT") as? Int {
            if let compCount = CompressionRecordsSharedClass.sharedInstance.countOfCompressionData(userID1! as AnyObject) as? Int {
                if compCount == 0 {
                    compressionStatus = true
                }
                else {
                    compressionStatus = false
                    if (UIApplication.shared.delegate as? AppDelegate)?.totalNotUploadedRecords == 0 {
                        (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress = 90.0
                    }
                    
                    DispatchQueue.main.async(execute: { 
                        self.blackView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                        self.blackView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65)
                        self.view.addSubview(self.blackView)
                        self.hud = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate!.window)!, message: "Uploading", animated: true)
                        self.hud.mode = MBProgressHUDModeAnnularDeterminate
                        self.uploadProgressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MMRSubmitSurvey.updateProgress), userInfo: nil, repeats: true)
                    })
                    
                    //                    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
                    self.quoteLabel.layer.zPosition = 1
                    timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(MMRSubmitSurvey.update), userInfo: nil, repeats: true)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.multipleSubmitAction), name: NSNotification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
                    uploader.delegate = self
                }
            }
            let userID = UserDefaults.standard.value(forKey: currentUserId)
            let surveyID = UserDefaults.standard.value(forKey: surveyId)
            UserDefaults.standard.removeObject(forKey: "CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE")
            AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetails(userID! as AnyObject, surveyID: surveyID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.updateFlagOfLocalSubmit(surveyID! as AnyObject, userID: userID! as AnyObject, flag: true)
            UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
            AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
            if compressionStatus == true {
                self.multipleSubmitAction()
            }
        } else {
            //Start next survey
            //delete records from answer_records, question_records_copy, routing_records, survey_push_records, survey_records
            let userID = UserDefaults.standard.value(forKey: currentUserId)
            let surveyID = UserDefaults.standard.value(forKey: surveyId)
            AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetails(userID! as AnyObject, surveyID: surveyID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.updateFlagOfLocalSubmit(surveyID! as AnyObject, userID: userID! as AnyObject, flag: true)
            UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
            //                SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID!,surveyID: surveyID!)
            if SurveyRecordsSharedClass.sharedInstance.isAnySurveyPresentWithLocalSubmitFlagFalse(userID! as AnyObject) {
                QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID! as AnyObject, surveyID: surveyID! as AnyObject)
                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                BaseClassForQuestions.serialNo_ID = 1
                UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
                AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID! as AnyObject, surveyID: surveyID! as AnyObject)
                QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
                UserDefaults.standard.synchronize()
                
                self.goToHomeScreen ()
            } else {
                AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
            }
            
        }
    }
    
    func needToPerformIfItsSingleSurvey() {
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable() {
            print("Connected inside")
            var compressionStatus = true
            // if let compCount = NSUserDefaults.standardUserDefaults().integerForKey("VIDEO_COMPRESSION_COUNT") as? Int {
            if let compCount = CompressionRecordsSharedClass.sharedInstance.countOfCompressionData(userID1! as AnyObject) as? Int {
                
                if compCount == 0 {
                    compressionStatus = true
                }
                else {
                    compressionStatus = false
                    if (UIApplication.shared.delegate as? AppDelegate)?.totalNotUploadedRecords == 0 {
                        (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress = 90.0
                    }
                    DispatchQueue.main.async(execute: { 
                        self.blackView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                        self.blackView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65)
                        self.view.addSubview(self.blackView)
                        self.hud = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate!.window)!, message: "Uploading", animated: true)
                        self.hud.mode = MBProgressHUDModeAnnularDeterminate
                        self.uploadProgressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MMRSubmitSurvey.updateProgress), userInfo: nil, repeats: true)
                    })
                    
                    //                    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
                    self.quoteLabel.layer.zPosition = 1
                    timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(MMRSubmitSurvey.update), userInfo: nil, repeats: true)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.multipleSubmitAction), name: NSNotification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
                    uploader.delegate = self
                }
            }
            
            if compressionStatus == true {
                let userID = UserDefaults.standard.value(forKey: currentUserId)
                let surveyID = UserDefaults.standard.value(forKey: surveyId)
                AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: surveyID! as AnyObject)
                QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: surveyID! as AnyObject)
                SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetails(userID! as AnyObject, surveyID: surveyID! as AnyObject)
                SurveyRecordsSharedClass.sharedInstance.updateFlagOfLocalSubmit(surveyID! as AnyObject, userID: userID! as AnyObject, flag: true)
                //                SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID!,surveyID: surveyID!)
                self.multipleSubmitAction()
            }
            
        } else {
            //SINGLE SURVEY OFFLINE
            let userID = UserDefaults.standard.value(forKey: currentUserId)
            let surveyID = UserDefaults.standard.value(forKey: surveyId)
            AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: surveyID! as AnyObject)
            SurveyRecordsSharedClass.sharedInstance.deleteSurveyBasicPushDetails(userID! as AnyObject, surveyID: surveyID! as AnyObject)
            //            SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID!,surveyID: surveyID!)
            SurveyRecordsSharedClass.sharedInstance.updateFlagOfLocalSubmit(surveyID! as AnyObject, userID: userID! as AnyObject, flag: true)
            if SurveyRecordsSharedClass.sharedInstance.isAnySurveyPresentWithLocalSubmitFlagFalse(userID! as AnyObject) {
                QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID! as AnyObject, surveyID: surveyID! as AnyObject)
                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                BaseClassForQuestions.serialNo_ID = 1
                UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                UserDefaults.standard.synchronize()
                SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID! as AnyObject)
                if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                    let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                    if interval > 0 {
                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                    } else {
                        if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                            let calendar = Calendar.current
                            let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                            //update survey start time
                            let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                            let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                            SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID! as AnyObject)
                            UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        } else {
                            UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                        }
                    }
                } else {
                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                }
                
                self.goToHomeScreen ()
            } else {
                AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
            }
            
        }
    }
    
    @IBAction func submitButtonClicked(_ sender: UIButton) {
        isSubmitClicked = true
        //@kj
        let currentTime = Date()
        let dateFormatter2 = DateFormatter()
        //dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // 2018-12-07T09:33:13.766+0000
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // 2018-12-07T09:34:54
//        dateFormatter2.timeZone = TimeZone(identifier: "UTC")
        let strDate = dateFormatter2.string(from: currentTime)
        print("strDate submitButtonClicked: \(strDate)");
        UserDefaults.standard.set(strDate, forKey: LAST_QUESTION_ANSWER_TIME)
        
        if let mflag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
            let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            if mflag {
                let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
                
                if isTimeLeftForSurvey {
                    self.needToPerformForMultipleSurveyIfTimeLeft()
                }
                else {
                    self.needToPerformForMultipleSurveyIfTimeNotLeft()
                }
            }
            else {
                UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
                let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
                AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
                QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
                self.needToPerformIfItsSingleSurvey()
            }
        }
        else {
            UserDefaults.standard.set("FALSE", forKey: isEntryDoneFlag_UD)
            let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
            let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            AnswerSharedClass.sharedInstance.deleteOptionsFromAnswerCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
            QuestionSharedClass.sharedInstance.deleteRoutingCopy(userID1! as AnyObject, surveyID: curSurveyID! as AnyObject)
            self.needToPerformIfItsSingleSurvey()
        }
    }
    
    
    func notificationAction(_ notify : Notification) {
        let userinfo = notify.userInfo
        
    }
    
    //  func didCompleteImageUpload(imageName: String, serialID: String, doneFlag: Bool, contentType: String) {
    //    //Check whether anything remain or not
    //    //If remain then wait else create json and hit api
    //    let userID1 = NSUserDefaults.standardUserDefaults().valueForKey("CURRENT_USER_ID")
    //     let resultArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID1!, type: "")
    //    if resultArray.count > 0 {
    //      //wait do nothing
    //    }
    //    else {
    //      uploadSurvey.uploadDelegate = self
    //      if let temp =  NSUserDefaults.standardUserDefaults().boolForKey("SURVEY_COMPLETE_STATUS") as? Bool {
    //        uploadSurvey.surveySubmissionStatus = temp
    //      }
    //      else {
    //        uploadSurvey.surveySubmissionStatus = true
    //      }
    //      uploadSurvey.getMultipleSubmitAnswerJson()
    //    }
    //  }
    
    func didCompleteImageUpload(_ status: Bool) {
        if status == true {
            uploadSurvey.uploadDelegate = self
            if let temp =  UserDefaults.standard.bool(forKey: SURVEY_COMPLETE_STATUS_UD) as? Bool {
                uploadSurvey.surveySubmissionStatus = temp
            }
            else {
                uploadSurvey.surveySubmissionStatus = true
            }
            //      stopActivityAnimating()
            //      self.quoteLabel.layer.zPosition = 1
            //      timer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: "update", userInfo: nil, repeats: true)
            //      startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
            if isSubmitClicked == true {
                if !uploadSurvey.isExecutedJson {
                    uploadSurvey.getMultipleSubmitAnswerJson()
                }
            }
        }
    }
    
    @objc func multipleSubmitAction() {
        /*
         1. Check Internet is available or not
         2. If available then check whether currently uploading is in progress or not
         3. If not the check anything remain to upload
         4. If not the get json and hit api
         5. If yes the upload it and then hit the api
         6.
         */
        DispatchQueue.main.async {
            self.hud.hide(true)
            self.blackView.removeFromSuperview()
        }
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable() {
            self.quoteLabel.layer.zPosition = 1
            timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(MMRSubmitSurvey.update), userInfo: nil, repeats: true)
            //        if (UIApplication.sharedApplication().delegate as? AppDelegate)?.totalNotUploadedRecords == 0 {
            //            (UIApplication.sharedApplication().delegate as? AppDelegate)?.s3UploadProgress = 90.0
            //        }
            DispatchQueue.main.async(execute: {
                self.blackView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                self.blackView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65)
                self.view.addSubview(self.blackView)
                self.hud = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate!.window)!, message: "Uploading", animated: true)
                self.hud.mode = MBProgressHUDModeAnnularDeterminate
                self.uploadProgressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MMRSubmitSurvey.updateProgress), userInfo: nil, repeats: true)
            })
            
            //      startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.BallTrianglePath,color: UIColor.whiteColor(),isSloganEnable : true)
            if UserDefaults.standard.bool(forKey: UPLOADING_IN_PROCESS_UD) == false {
                //check is anything remain or not
                let resultArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID1! as AnyObject, type: "")
                if resultArray.count > 0 {
                    _ = uploader.startUploadProcess("")
                    uploader.delegate = self
                    //          aws.delegate = self
                }
                else {
                    uploadSurvey.uploadDelegate = self
                    if let temp =  UserDefaults.standard.bool(forKey: SURVEY_COMPLETE_STATUS_UD) as? Bool {
                        uploadSurvey.surveySubmissionStatus = temp
                    }
                    else {
                        uploadSurvey.surveySubmissionStatus = true
                    }
                    uploadSurvey.getMultipleSubmitAnswerJson()
                }
            }
            else if UserDefaults.standard.bool(forKey: UPLOADING_IN_PROCESS_UD) == true {
                let resultArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID1! as AnyObject, type: "")
                if resultArray.count == 0 {
                    UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
                    UserDefaults.standard.synchronize()
                    if let temp =  UserDefaults.standard.bool(forKey: SURVEY_COMPLETE_STATUS_UD) as? Bool {
                        uploadSurvey.surveySubmissionStatus = temp
                        //////////////////////////////////////////
                    }
                    else {
                        uploadSurvey.surveySubmissionStatus = true
                    }
                    uploadSurvey.uploadDelegate = self
                    uploadSurvey.getMultipleSubmitAnswerJson()
                }
                else {
                    uploader.delegate = self
                }
                uploader.delegate = self
            }
            uploader.delegate = self
        }
        else {
            stopActivityAnimating()
            timer.invalidate()
            AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
        }
    }
    
    func submitAction() {
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable() {
            uploadSurvey.uploadDelegate = self
            uploadSurvey.surveySubmissionStatus = true
            uploadSurvey.uploadSurvey()
        }
        else  {
            timer.invalidate()
            stopActivityAnimating()
            AlertModel.sharedInstance.showErrorMessage(kCRMessageNoInternetConnection.localized())
        }
    }
    
    @objc func update() {
        if quoteCounter == quoteCount {
            quoteCounter = 0
        }
        self.quoteLabel.text = quotesArray[quoteCounter]
        quoteCounter = quoteCounter + 1
    }
    
    @objc func updateProgress() {
        var progress = (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress
        if progress >= 0 && progress <= 100 {
            if progress > 100.0 || (progress?.isInfinite)! {
                progress = 90.0
            }
            print("Progress : \(Float(progress!))")
            hud.progress = Float(progress! / 100)
            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            if let cnt = UploadRecordSharedClass.sharedInstance.getCountOfNotUploadedRecords(userID! as AnyObject, type: "") as? Int {
                if cnt == 0 {
                    self.uploadProgressTimer.invalidate()
                }
            }
        }
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                UserDefaults.standard.set(1, forKey: serialNoID)
                UserDefaults.standard.synchronize()
                break
            }
        }
        
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
    func uploadCompletion(_ status: Bool) {
        if status {
            print("Success")
            self.uploadProgressTimer.invalidate()
            DispatchQueue.main.async(execute: {
                self.hud.progress = 100.0
                sleep(2)
                self.hud.hide(true)
                self.blackView.removeFromSuperview()
                //        })
                (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress = 0.0
                (UIApplication.shared.delegate as? AppDelegate)?.s3UploadRequestProgressDict = [String : Double]()
                (UIApplication.shared.delegate as? AppDelegate)?.s3UploadRequestNameArray = [String]()
                (UIApplication.shared.delegate as? AppDelegate)?.totalNotUploadedRecords = 0
                self.uploadProgressTimer.invalidate()
                
                let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                
                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                UserDefaults.standard.synchronize()
                AlertModel.sharedInstance.showAlert("MMR", message: "Survey Submitted Successfully".localized(), buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (clickedButtonTitle, success) in
                    self.uploader.delegate = nil
                    if let mflag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
                        if mflag {
                            let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
                            if isTimeLeftForSurvey {
                                UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                BaseClassForQuestions.serialNo_ID = 1
                                UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                                
                                let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID1!)") as! Int
                                UserDefaults.standard.set(seqNum + 1, forKey: "SURVEY_SEQ_NUM_\(userID1!)")
                                let surveyid = UserDefaults.standard.value(forKey: surveyId)
                                //            QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1!, surveyID: surveyid!)
                                SurveyRecordsSharedClass.sharedInstance.updateSeqNumOfSurvey(seqNum + 1 as AnyObject, surveyID: surveyid! as AnyObject, userID: userID1! as AnyObject)
                                UserDefaults.standard.set(true, forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                            }
                            else {
                                //                NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                                UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                                BaseClassForQuestions.serialNo_ID = 1
                                UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                                SurveyRecordsSharedClass.sharedInstance.clearUD()
                                UserDefaults.standard.synchronize()
                                SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                                if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                                    let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                                    if interval > 0 {
                                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                    } else {
                                        if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                            let calendar = Calendar.current
                                            let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                            //update survey start time
                                            let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                            let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                            SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                            UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                        } else {
                                            UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                        }
                                    }
                                } else {
                                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                }
                                UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                            }
                        }
                        else {
                            //            NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                            UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                            BaseClassForQuestions.serialNo_ID = 1
                            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                            UserDefaults.standard.synchronize()
                            let surveyID = UserDefaults.standard.value(forKey: surveyId)
                            
                            QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
                            SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                            if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                                let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                                if interval > 0 {
                                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                } else {
                                    if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                        let calendar = Calendar.current
                                        let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                        //update survey start time
                                        let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                        SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                        UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                    } else {
                                        UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                        UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                    }
                                }
                            } else {
                                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            }
                            UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                        }
                    }
                    else {
                        //            NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                        BaseClassForQuestions.serialNo_ID = 1
                        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                        UserDefaults.standard.synchronize()
                        let surveyID = UserDefaults.standard.value(forKey: surveyId)
                        QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
                        SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                        if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                            let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                            if interval > 0 {
                                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            } else {
                                if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                    let calendar = Calendar.current
                                    let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                    //update survey start time
                                    let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                    SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                    UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                } else {
                                    UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                }
                            }
                        } else {
                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                        }
                        UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                    }
                    self.goToHomeScreen ()
                })
            })
            
        }
        else {
            print("Failure")
            self.surveyUploadFailed()
        }
//        timer.invalidate()
//        stopActivityAnimating()
        
        
    }
    
    func surveyUploadFailed()  {
        DispatchQueue.main.async(execute: {
            self.hud.progress = 100.0
            sleep(2)
            self.hud.hide(true)
            self.blackView.removeFromSuperview()
            (UIApplication.shared.delegate as? AppDelegate)?.s3UploadProgress = 0.0
            (UIApplication.shared.delegate as? AppDelegate)?.s3UploadRequestProgressDict = [String : Double]()
            (UIApplication.shared.delegate as? AppDelegate)?.s3UploadRequestNameArray = [String]()
            (UIApplication.shared.delegate as? AppDelegate)?.totalNotUploadedRecords = 0
            self.uploadProgressTimer.invalidate()
            
            let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            
            UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
            UserDefaults.standard.synchronize()
            AlertModel.sharedInstance.showAlert("MMR", message: "Survey Submitted Successfully".localized(), buttonTitles: ["OK".localized()], viewController: self, completionHandler: { (clickedButtonTitle, success) in
                self.uploader.delegate = nil
                if let mflag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
                    if mflag {
                        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
                        if isTimeLeftForSurvey {
                            UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                            BaseClassForQuestions.serialNo_ID = 1
                            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                            
                            let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID1!)") as! Int
                            UserDefaults.standard.set(seqNum + 1, forKey: "SURVEY_SEQ_NUM_\(userID1!)")
                            let surveyid = UserDefaults.standard.value(forKey: surveyId)
                            //            QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1!, surveyID: surveyid!)
                            SurveyRecordsSharedClass.sharedInstance.updateSeqNumOfSurvey(seqNum + 1 as AnyObject, surveyID: surveyid! as AnyObject, userID: userID1! as AnyObject)
                            UserDefaults.standard.set(true, forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                        }
                        else {
                            //                NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                            UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                            BaseClassForQuestions.serialNo_ID = 1
                            UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                            SurveyRecordsSharedClass.sharedInstance.clearUD()
                            UserDefaults.standard.synchronize()
                            SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                            if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                                let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                                if interval > 0 {
                                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                } else {
                                    if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                        let calendar = Calendar.current
                                        let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                        //update survey start time
                                        let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                        SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                        UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                    } else {
                                        UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                        UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                    }
                                }
                            } else {
                                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            }
                            UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                        }
                    }
                    else {
                        //            NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                        BaseClassForQuestions.serialNo_ID = 1
                        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                        UserDefaults.standard.synchronize()
                        let surveyID = UserDefaults.standard.value(forKey: surveyId)
                        
                        QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
                        SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                        if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                            let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                            if interval > 0 {
                                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            } else {
                                if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                    let calendar = Calendar.current
                                    let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                    //update survey start time
                                    let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                    SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                    UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                    UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                                } else {
                                    UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                                }
                            }
                        } else {
                            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        }
                        UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                    }
                }
                else {
                    //            NSUserDefaults.standardUserDefaults().removeObjectForKey(surveyId)
                    UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
                    BaseClassForQuestions.serialNo_ID = 1
                    UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
                    UserDefaults.standard.synchronize()
                    let surveyID = UserDefaults.standard.value(forKey: surveyId)
                    QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID1! as AnyObject, surveyID: surveyID! as AnyObject)
                    SurveyRecordsSharedClass.sharedInstance.fetchDataForSurvey(userID1! as AnyObject)
                    if let surveyStTime = UserDefaults.standard.value(forKey: surveyStartTime) as? String {
                        let interval = SurveyRecordsSharedClass.sharedInstance.compareTwoDatesAndGetOInterval(surveyStTime)
                        if interval > 0 {
                            UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                        } else {
                            if let intervalMins = UserDefaults.standard.value(forKey: surveyIntervalData) as? Int {
                                let calendar = Calendar.current
                                let date = (calendar as NSCalendar).date(byAdding: .minute, value: intervalMins, to: Date(), options: [])
                                //update survey start time
                                let updatedStartDate = QuestionSharedClass.sharedInstance.getSurveyDateInUTC(date!)
                                let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                                SurveyRecordsSharedClass.sharedInstance.updateStartTimeOfSurvey(updatedStartDate as AnyObject, surveyID: currentSurveyID! as AnyObject, userID: userID1! as AnyObject)
                                UserDefaults.standard.setValue(updatedStartDate, forKey: surveyStartTime)
                                UserDefaults.standard.set(true, forKey: NoSurveyForUser)
                            } else {
                                UserDefaults.standard.removeObject(forKey: surveyIntervalData)
                                UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                            }
                        }
                    } else {
                        UserDefaults.standard.set(false, forKey: NoSurveyForUser)
                    }
                    UserDefaults.standard.removeObject(forKey: CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD)
                }
//                self.goToHomeScreen ()
            })
            self.goToHomeScreen ()
        })
    }
    
    func uploadDataSize(_ size: UInt64) {
        DispatchQueue.main.async {
            self.memorySizeLabel.text = "This is for Testing Purpose : \(size) MB"
            self.memorySizeLabel.layer.zPosition = 1
        }
    }
    
}
