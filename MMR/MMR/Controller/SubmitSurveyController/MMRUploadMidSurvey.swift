//
//  MMRUploadMidSurvey.swift
//  MMR
//
//  Created by Mindbowser on 29/08/18.
//  Copyright © 2018 Mindbowser. All rights reserved.
//

import UIKit

class MMRUploadMidSurvey: NSObject {

    class var sharedInstance :MMRUploadMidSurvey {
        struct Singleton {
            static let instance = MMRUploadMidSurvey()
        }
        return Singleton.instance
    }
    
    func getMultipleSubmitAnswerJson() {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        var answerJson: [NSMutableDictionary] = QuestionSharedClass.sharedInstance.fetchQuestionDataForAvailable(userID as AnyObject, seqNum: 1 as AnyObject, surveySubmitStatus: false)
        if answerJson == [["":""]] {
            answerJson.removeAll()
            answerJson = QuestionSharedClass.sharedInstance.getDummyJsonifFailsToCreate()
        }
        print("Answer Json : \(answerJson)")
        self.validateUserApiCallMultiple(answerJson)
    }
    
    func validateUserApiCallMultiple(_ answerJson : [NSMutableDictionary]) {
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            let success = APIManager().validateUser()
            print(success)
            if success {
                self.uploadSurveyApiCallMultiple((answerJson as? [NSMutableDictionary])!)
            }
            else {
                print("User Invalid")
            }
        }
    }
    
    func uploadSurveyApiCallMultiple(_ answerJson : [NSMutableDictionary]) {
        let success = APIManager().submitSurvey((answerJson as? [NSMutableDictionary])!,surveyStatus: false)
        if success {
            print("Survey Submitted Successfully")
        }
        else{
            print("Failed to submit survey")
        }
    }
}
