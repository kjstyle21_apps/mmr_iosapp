//
//  MMRUploadSurvey.swift
//  MMR
//
//  Created by Mindbowser on 1/6/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import AWSCore
import AWSS3

@objc protocol MMRSubmitSurveyDelegate : class {
  func uploadCompletion(_ status : Bool)
  @objc optional func uploadDataSize(_ size : UInt64)

}

class MMRUploadSurvey: NSObject,AWSS3UploadManagerDelegate {
  
  let aws = AWSUploadManager()
  var uploadedDataPathArray = [String]()
  var uploadedDataSrNoArray = [String]()
  var counter = 0
  var imgCounter = 0
  var uploadDoneCounter = 0
  var uploadDoneImageConter = 0
  weak var delegate : BaseClassMethodToFetchDataFromDB?
  weak var uploadDelegate : MMRSubmitSurveyDelegate?
  var surveySubmissionStatus = Bool()
//  var firstClickFlag = false
  var resumeNeededFlag = false
  var fileSize : UInt64 = 0
    var isExecutedJson = false

  override init() {
    super.init()
    aws.delegate = self

    if let arr = UserDefaults.standard.value(forKey: "UPLOADED_DATA_PATH_ARRAY") {
          self.uploadedDataPathArray.removeAll()
          self.uploadedDataPathArray = arr as! [String]
    }
    if let arr = UserDefaults.standard.value(forKey: "UPLOADED_DATA_SR_NO_ARRAY") {
      self.uploadedDataSrNoArray.removeAll()
      self.uploadedDataSrNoArray = arr as! [String]
    }
  }
  
  func uploadSurvey() {
    
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    
    if let firstClickFlag = UserDefaults.standard.value(forKey: "\(userID!)") as? Bool {
      if firstClickFlag == true {
        //resume upload tasks
        
       // NSUserDefaults.standardUserDefaults().setBool(true, forKey: "\(userID!)_RESUME_NEEDED FLAG")
        
        if let tempReloadStatus = UserDefaults.standard.bool(forKey: "\(userID!)_RESUME_NEEDED_FLAG") as? Bool {
          if tempReloadStatus {
            self.resumeAll()
          }
          else {
            self.newUploads()
          }
        }
       
        /*
        if resumeNeededFlag {
          self.resumeAll()
        }
        else {
          self.newUploads()
        }*/
      }
      else if firstClickFlag == false {
        //start new
        UserDefaults.standard.set(true, forKey: "\(userID!)")
        UserDefaults.standard.synchronize()
//        self.firstClickFlag = true
//        self.newUploads()
          self.validateUserApiCall()
      }
    }
    else {
      //start new
      UserDefaults.standard.set(true, forKey: "\(userID!)")
      UserDefaults.standard.synchronize()
//      self.newUploads()
      self.validateUserApiCall()
    }
    
  }
  
  
  func newUploads() {
    
    self.uploadDoneCounter = 0
    self.counter = 0
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    var videoArray = QuestionSharedClass.sharedInstance.fetchVideoOrImageURLs(userID! as AnyObject, type: "videoChoice")
    print(videoArray)
    if videoArray.count > 0 {
      UserDefaults.standard.set(true, forKey: "\(userID!)_RESUME_NEEDED FLAG")
      UserDefaults.standard.synchronize()
    //  resumeNeededFlag = true
    }
    for video in videoArray {
      let ansText = video.value(forKey: "answer_text") as! String
      let serialNo = video.value(forKey: "serial_id")
      if ansText != "" {

        let ansArr = (ansText ).components(separatedBy: "^]`,")
      for ans in ansArr {
        if ans != "" {
        //upload ans with serialNo to AWS
        aws.ext = "mov"
        aws.contentType = "video"
        let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
        let imgName = "\(userID!)/\(timestamp)"
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let imgURL = URL(fileURLWithPath: "\(documentsPath)/\(ans)")
        print("Image URL : \(imgURL)")
   //*********************************************************
//          let filePath = "your path here"
//          var fileSize : UInt64 = 0
          
          do {
            let attr : NSDictionary? = try FileManager.default.attributesOfItem(atPath: imgURL.path) as NSDictionary
            
            if let _attr = attr {
              fileSize = fileSize + _attr.fileSize();
//              fileSize = _attr.fileSize();
            }
          } catch {
            print("Error: \(error)")
          }
          
   //**************************************************************
          
          
        self.counter = self.counter + 1
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        aws.uploader(imgURL, imageName: imgName , index : "\(serialNo!)", uploadRequest1: uploadRequest!, resumeFlag: false)
        }
      }
    }
      else if ansText == "" {
        let tempIndex = videoArray.index(of: video)
        videoArray.remove(at: tempIndex!)
      }
    }
    var imageArray = QuestionSharedClass.sharedInstance.fetchVideoOrImageURLs(userID! as AnyObject, type: "pictureChoice")
    print(imageArray)
    if imageArray.count > 0 {
      resumeNeededFlag = true
    }
    for image in imageArray {
      let ansText = image.value(forKey: "answer_text") as! String
      let serialNo = image.value(forKey: "serial_id")
      if ansText != "" {
      let ansArr = (ansText ).components(separatedBy: "^]`,")
      for ans in ansArr {
        if ans != "" {
        //upload ans with serialNo to AWS
        aws.ext = "jpg"
        aws.contentType = "image"
        let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
        let imgName = "\(userID!)/\(timestamp)"
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let imgURL = URL(fileURLWithPath: "\(documentsPath)/\(ans)")
        
        //        let imgURL = NSURL(fileURLWithPath: ans)
        print("Image URL : \(imgURL)")
          
          //***************************************************
          
          do {
            let attr : NSDictionary? = try FileManager.default.attributesOfItem(atPath: imgURL.path) as NSDictionary
            
            if let _attr = attr {
              fileSize = fileSize + _attr.fileSize();
//              fileSize = _attr.fileSize();
            }
          } catch {
            print("Error: \(error)")
          }
          
//**************************************************************
          
          
        self.counter = self.counter + 1
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        aws.uploader(imgURL, imageName: imgName , index : "\(serialNo!)", uploadRequest1: uploadRequest!, resumeFlag: false)
        }
      }
    }
      else if ansText == "" {
        let tempIndex = imageArray.index(of: image)
        imageArray.remove(at: tempIndex!)
      }
    }
    
    UserDefaults.standard.setValue(self.counter, forKey: UPLOAD_COUNTER_UD)
    UserDefaults.standard.synchronize()

    if imageArray.count == 0 && videoArray.count == 0 {
      let answerJson = QuestionSharedClass.sharedInstance.fetchQuestionData(userID! as AnyObject,seqNum: 1 as AnyObject,surveySubmitStatus: surveySubmissionStatus)
      print(answerJson)
      let reachability: Reachability
      do {
        reachability = try Reachability.reachabilityForInternetConnection()
      } catch {
        print("Unable to create Reachability")
        return
      }
      
      if reachability.isReachable() {
        //internet connected
        print("Connected inside")
        self.uploadSurveyApiCall((answerJson as? [NSMutableDictionary])!)
      }
      else {
        self.uploadDelegate?.uploadCompletion(false)
      }
    }
    print("\(self.fileSize/1024/1024)")
   // uploadDelegate?.uploadDataSize!(self.fileSize/1024/1024)
  }
  
  func resumeAll() {
    self.counter = UserDefaults.standard.value(forKey: UPLOAD_COUNTER_UD) as! Int
    aws.resumeAllUploads()
  }
  
  func pauseAll() {
    aws.pauseAllUploads()
  }
  
  func cancelAllUploads() {
    aws.cancelAllUploads()
  }
  
  func validateUserApiCall() {
    let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      let success = APIManager().validateUser()
      print(success)
      if success {
        self.newUploads()
      }
      else {
        print("User Invalid")
      }
      }
  }
  
func didCompleteImageUpload(_ imageName: String, serialID: String, doneFlag: Bool,contentType: String, relatedSurveyId: AnyObject) {
    print(imageName)
    print(serialID)

    if doneFlag {
//    self.uploadDoneCounter = self.uploadDoneCounter + 1
    self.uploadedDataPathArray.append(imageName)
    self.uploadedDataSrNoArray.append(serialID)
    
    UserDefaults.standard.setValue(self.uploadedDataPathArray, forKey: "UPLOADED_DATA_PATH_ARRAY")
    UserDefaults.standard.setValue(self.uploadedDataSrNoArray, forKey: "UPLOADED_DATA_SR_NO_ARRAY")
    UserDefaults.standard.synchronize()
    print(self.uploadedDataPathArray)
    print(self.uploadedDataSrNoArray)
    self.uploadDoneCounter = self.uploadedDataPathArray.count
     self.pauseAll()
      self.resumeAll()
  }
  
  print(UserDefaults.standard.value(forKey: "UPLOADED_DATA_PATH_ARRAY"))
  print(UserDefaults.standard.value(forKey: "UPLOADED_DATA_SR_NO_ARRAY"))
  print(UserDefaults.standard.value(forKey: UPLOAD_COUNTER_UD))
  
    if self.counter == self.uploadDoneCounter {
      print("All done")
      
      var dataDict = NSMutableDictionary()
      var indexCount = 0
      for sr in  self.uploadedDataSrNoArray {
        if let arr = dataDict.object(forKey: sr) as? NSMutableArray {
          arr.add(self.uploadedDataPathArray[indexCount])
          dataDict.setValue(arr, forKey: sr)
        }
        else {
          let arr = NSMutableArray()
          arr.add(self.uploadedDataPathArray[indexCount])
          dataDict.setValue(arr, forKey: sr)
        }
        indexCount = indexCount + 1
      }
      
      print(dataDict)
      let updatedSrNoArray = self.removeDuplicates(self.uploadedDataSrNoArray)
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      for srno in updatedSrNoArray {
        let pathArr = dataDict.object(forKey: srno) as! [String]
        let str = pathArr.joined(separator: "^]`,")
        //update values to local database.
        //            delegate?.updateAnswerTextForQuestion(str, serialNo: srno)
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        QuestionSharedClass.sharedInstance.updateAnswerTextForQuestion(str, serialNo: srno,userID: userID! as AnyObject,uploadSeqNum: seqNum! as AnyObject,surveyID: relatedSurveyId)
      }
      
      
      
//      let userID = NSUserDefaults.standardUserDefaults().valueForKey("CURRENT_USER_ID")
      let answerJson = QuestionSharedClass.sharedInstance.fetchQuestionData(userID! as AnyObject, seqNum : 1 as AnyObject,surveySubmitStatus: surveySubmissionStatus)
      print(answerJson)
      let reachability: Reachability
      do {
        reachability = try Reachability.reachabilityForInternetConnection()
      } catch {
        print("Unable to create Reachability")
        return
      }
      
      if reachability.isReachable() {
        //internet connected
        print("Connected inside")
        self.uploadSurveyApiCall((answerJson as? [NSMutableDictionary])!)
      }
      else {
        self.uploadDelegate?.uploadCompletion(false)
      }
    }
  }
  
  
  func removeDuplicates(_ noramlArray : [String]) -> [String] {
    var result = [String]()
    for value in noramlArray {
      if result.contains(value) == false {
        result.append(value)
      }
    }
    return result
  }

  
  func uploadSurveyApiCall(_ answerJson : [NSMutableDictionary]) {
    let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      
    let success = APIManager().submitSurvey(answerJson,surveyStatus: self.surveySubmissionStatus)
    if success {
      UserDefaults.standard.removeObject(forKey: IS_ON_SUBMIT_SCREEN_UD)
      UserDefaults.standard.synchronize()
      
      print("Survey Submitted Successfully")
      UserDefaults.standard.removeObject(forKey: "UPLOADED_DATA_PATH_ARRAY")
      UserDefaults.standard.removeObject(forKey: "UPLOADED_DATA_SR_NO_ARRAY")
      UserDefaults.standard.removeObject(forKey: UPLOAD_COUNTER_UD)
      UserDefaults.standard.synchronize()

      let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//      self.firstClickFlag = false
      UserDefaults.standard.removeObject(forKey: "\(userID!)")
      UserDefaults.standard.synchronize()
      QuestionSharedClass.sharedInstance.deleteQuestionDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
      QuestionDropRecordSharedClass.sharedInstance.deleteQuestionDropDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
      AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
      QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID! as AnyObject,surveyID: "" as AnyObject)
      UploadRecordSharedClass.sharedInstance.deleteUploadRecord(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
      QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID! as AnyObject,surveyID: "" as AnyObject)
      SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID! as AnyObject,surveyID: "" as AnyObject)
//      self.uploadDelegate?.uploadCompletion(true)
//      let surveyID = NSUserDefaults.standardUserDefaults().valueForKey(surveyId)
//      let bleJson = BLERecordsSharedClass.sharedInstance.fetchBLEData(surveyID!, userID: userID!)
//      print(bleJson)
//      self.uploadbleDataApiCall(bleJson)
      
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
        MMRBLEModule.sharedInstance.disconnectMMRDevice()
        (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
        UIApplication.shared.cancelAllLocalNotifications()
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        let bleJson = BLERecordsSharedClass.sharedInstance.fetchBLEData(surveyID! as AnyObject, userID: userID! as AnyObject)
        print(bleJson)
        self.uploadbleDataApiCall(bleJson)
      }
      else {
        self.uploadDelegate?.uploadCompletion(true)
      }
      
    }
    else{
      print("Failed to submit survey")
      self.uploadDelegate?.uploadCompletion(false)
    }
    }
  }
  
  func uploadSurveyApiCallMultiple(_ answerJson : [NSMutableDictionary]) {
    //    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
    //    dispatch_async(dispatch_get_global_queue(priority, 0)) {
    let success = APIManager().submitSurvey((answerJson as? [NSMutableDictionary])!,surveyStatus: self.surveySubmissionStatus)
    if success {
      UserDefaults.standard.removeObject(forKey: IS_ON_SUBMIT_SCREEN_UD)
      UserDefaults.standard.synchronize()
      
      print("Survey Submitted Successfully")
      UserDefaults.standard.removeObject(forKey: "UPLOADED_DATA_PATH_ARRAY")
      UserDefaults.standard.removeObject(forKey: "UPLOADED_DATA_SR_NO_ARRAY")
      UserDefaults.standard.removeObject(forKey: UPLOAD_COUNTER_UD)
      UserDefaults.standard.synchronize()
      
      let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      //      self.firstClickFlag = false
      UserDefaults.standard.removeObject(forKey: "\(userID!)")
      UserDefaults.standard.synchronize()
      QuestionSharedClass.sharedInstance.deleteQuestionDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
      QuestionDropRecordSharedClass.sharedInstance.deleteQuestionDropDataOfUploadedSurvey(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
        SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetailsWithLocalSubmitFlagTrue(userID! as AnyObject)
      //MULTIPLE_ONLINE CHANGE
      let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
//      if !isTimeLeftForSurvey {
//      QuestionSharedClass.sharedInstance.deleteQuestionData_CopyOfUploadedSurvey(userID!,surveyID: "")
//      AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID!,surveyID: "")
//      QuestionSharedClass.sharedInstance.deleteRoutingForQuestionID(userID!, surveyID: "")
//      SurveyRecordsSharedClass.sharedInstance.deleteSurveyDetails(userID!,surveyID: "")
//      }
      
      UploadRecordSharedClass.sharedInstance.deleteUploadRecord(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)

        if let mflag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
            if mflag {
                if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
                    if !isTimeLeftForSurvey {
                        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
                        UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
                        UIApplication.shared.cancelAllLocalNotifications()
                    }
                    MMRBLEModule.sharedInstance.disconnectMMRDevice()
                    (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
                    let surveyID = UserDefaults.standard.value(forKey: surveyId)
                    let bleJson = BLERecordsSharedClass.sharedInstance.fetchBLEData(surveyID! as AnyObject, userID: userID! as AnyObject)
                    print(bleJson)
                    self.uploadbleDataApiCall(bleJson)
                }
                else {
                    self.uploadDelegate?.uploadCompletion(true)
                }
            } else {
                if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
                    UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
                    UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
                    MMRBLEModule.sharedInstance.disconnectMMRDevice()
                    (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
                    UIApplication.shared.cancelAllLocalNotifications()
                    let surveyID = UserDefaults.standard.value(forKey: surveyId)
                    let bleJson = BLERecordsSharedClass.sharedInstance.fetchBLEData(surveyID! as AnyObject, userID: userID! as AnyObject)
                    print(bleJson)
                    self.uploadbleDataApiCall(bleJson)
                }
                else {
                    self.uploadDelegate?.uploadCompletion(true)
                }
            }
        } else {
            if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
                UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
                UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
                MMRBLEModule.sharedInstance.disconnectMMRDevice()
                (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
                UIApplication.shared.cancelAllLocalNotifications()
                let surveyID = UserDefaults.standard.value(forKey: surveyId)
                let bleJson = BLERecordsSharedClass.sharedInstance.fetchBLEData(surveyID! as AnyObject, userID: userID! as AnyObject)
                print(bleJson)
                self.uploadbleDataApiCall(bleJson)
            }
            else {
                self.uploadDelegate?.uploadCompletion(true)
            }
        }

    }
    else{
      print("Failed to submit survey")
      self.uploadDelegate?.uploadCompletion(false)
    }
    //    }
  }
  
  func uploadbleDataApiCall(_ answerJson : [NSMutableDictionary]) {
    let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      let success = APIManager().submitBLEData(answerJson)
      if success {
        print(success)
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        BLERecordsSharedClass.sharedInstance.deleteBLERecord(userID! as AnyObject,fetchedSurveyID: "" as AnyObject)
        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
        if isTimeLeftForSurvey {
          (UIApplication.shared.delegate as! AppDelegate).startDiscovery()
        }
        self.uploadDelegate?.uploadCompletion(true)
      }
      else{
        print("Failed to submit survey")
        self.uploadDelegate?.uploadCompletion(false)
      }
    }
  }

  
  func getMultipleSubmitAnswerJson() {
    print("Inside - getMultipleSubmitAnswerJson")
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let resultArray1 = QuestionSharedClass.sharedInstance.fetchMediaTypeDataForMultipleSubmit(userID! as AnyObject, type: "videoChoice")
    for tempResultDict in resultArray1 {
      let resultArray2 = UploadRecordSharedClass.sharedInstance.fetchDataToMerge(userID! as AnyObject, paramsdict: tempResultDict) 
      let resultString1 = resultArray2.joined(separator: "^]`,")
      //update the answer of that question
      //kindly test this code
      QuestionSharedClass.sharedInstance.updateAnswerTextForQuestion(resultString1, serialNo: tempResultDict.value(forKey: serial_id) as! String, userID: userID! as AnyObject, uploadSeqNum: tempResultDict.value(forKey: "SeqNum")! as AnyObject,surveyID: tempResultDict.value(forKey: surveyId)! as AnyObject)
    }
    
    let resultArray3 = QuestionSharedClass.sharedInstance.fetchMediaTypeDataForMultipleSubmit(userID! as AnyObject, type: "pictureChoice")
    for tempResultDict in resultArray3 {
      let resultArray4 = UploadRecordSharedClass.sharedInstance.fetchDataToMerge(userID! as AnyObject, paramsdict: tempResultDict) 
      let resultString2 = resultArray4.joined(separator: "^]`,")
      //update the answer of that question
      //kindly test this code
      QuestionSharedClass.sharedInstance.updateAnswerTextForQuestion(resultString2, serialNo: tempResultDict.value(forKey: serial_id) as! String, userID: userID! as AnyObject, uploadSeqNum: tempResultDict.value(forKey: "SeqNum")! as AnyObject,surveyID: tempResultDict.value(forKey: surveyId)! as AnyObject)
    }

    
    var answerJson: [NSMutableDictionary] = QuestionSharedClass.sharedInstance.fetchQuestionData(userID! as AnyObject, seqNum : 1 as AnyObject, surveySubmitStatus: surveySubmissionStatus)
    if answerJson == [["":""]] {
        answerJson.removeAll()
        answerJson = QuestionSharedClass.sharedInstance.getDummyJsonifFailsToCreate()
    }
    print("Answer Json : \(answerJson)")
    self.isExecutedJson = true
//    self.uploadSurveyApiCall(answerJson as! NSMutableArray)
    self.validateUserApiCallMultiple((answerJson as? [NSMutableDictionary])!)
  }
  
  func validateUserApiCallMultiple(_ answerJson : [NSMutableDictionary]) {
    let priority = DispatchQueue.GlobalQueuePriority.default
    DispatchQueue.global(priority: priority).async {
      let success = APIManager().validateUser()
      print(success)
      if success {
        self.uploadSurveyApiCallMultiple((answerJson as? [NSMutableDictionary])!)
      }
      else {
        print("User Invalid")
      }
    }
  }
    func trackOfProgress() {
//        let appDel = UIApplication.sharedApplication().delegate as? AppDelegate
//        var totalPercent = 0
//        for obj in (appDel?.s3UploadRequestNameArray)! {
//            let singleReqProgress = appDel?.s3UploadRequestProgressDict[obj]
//            totalPercent = totalPercent + singleReqProgress!
//        }
//        let pro = totalPercent / (appDel?.s3UploadRequestNameArray.count)!
//        print("S3 progress : \(pro)")
    }
}
