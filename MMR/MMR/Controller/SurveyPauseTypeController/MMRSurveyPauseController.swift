//
//  MMRSurveyPauseController.swift
//  MMR
//
//  Created by Mindbowser on 6/27/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import UserNotifications

class MMRSurveyPauseController: UIViewController {


  @IBOutlet var navigationView: Navigation_View!
  @IBOutlet var messageLabel: UILabel!
  @IBOutlet var checkbackLaterLabel: UILabel!
  @IBOutlet var pauseScreenScrollView: UIScrollView!
  @IBOutlet var messageLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet var scrollHeightConstraint: NSLayoutConstraint!
  @IBOutlet var nextButton: UIButton!

  weak var delegate : BaseClassMethodToFetchDataFromDB?
  var isHintAvailable     = Bool ()
  var isMandatory         = Bool ()
  var questionFromServer  = NSMutableDictionary ()
  var isAnswerGiven       = Bool ()
  var inTime = Date()
  var outTime = Date()
  var dropTime = Date()
  var finalOutTimeStr = String()
  var finalInTimeStr = String()
  var finalDropTime = String()
    var liveTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
      initSetUp()
      navigationSetup()
      self.pauseScreenScrollView.delaysContentTouches = false
      self.pauseScreenScrollView.canCancelContentTouches = false
      
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.navigationView.statusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.navigationView.statusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
      NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "DONE_WITH_INTERVAL_NOTIFY"), object: nil, queue: OperationQueue.main) { (notify) in
                self.enableNext()
//                self.checkbackLaterLabel.hidden = true
      }
      // Do any additional setup after loading the view.
    }

  func enableNext() {
    self.outTime = Date()
    self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
    self.checkbackLaterLabel.isHidden = true
    self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    self.nextButton.isUserInteractionEnabled = true
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
    self.liveTimer.invalidate()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let newSize : CGSize = self.messageLabel.sizeThatFits(CGSize(width: self.messageLabel.frame.size.width,  height: CGFloat(FLT_MAX)))
    print(newSize)
    self.messageLabelHeightConstraint.constant = newSize.height
    
    self.scrollHeightConstraint.constant = self.view.frame.size.height - 64
    
//    self.pauseScreenScrollView.contentSize = CGSizeMake(self.view.frame.size.width, newSize.height)
  }
  
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      let userinfo = notify.userInfo as! NSDictionary
      if userinfo.value(forKey: "state") as! String == "ON" {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  
  func initSetUp()
  {
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
//    if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//      if mflag {
//        let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID!)") as! Int
//        if seqNum > 1 {
          questionFromServer.setValue("", forKey: answerStartTime)
          questionFromServer.setValue("", forKey: answerEndTime)
          questionFromServer.setValue("", forKey: isAnswered)
          questionFromServer.setValue("", forKey: answer_text)
          questionFromServer.setValue(serialID, forKey: serial_id)
          questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
          questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
          questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
          let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
          if resultArray.count == 0 {
            QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
          }
//        }
//      }
//    }
    
    self.inTime = Date()
    self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)
    
    print(inTime)
    let val1 = String(format: "%d", serialID)
    let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
    if status == "No_Exists" {
      self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
    }
    
    let quesString = questionFromServer[questionText] as! String
    quesString.attributedStringFromHTML { (resultString) in
        self.messageLabel.text = resultString?.string
    }
    
    let notificationText = questionFromServer[hintText] as! String
    let notificationTime = questionFromServer["questionIntervalTime"] as! String
    //Show the notification at time interval came from server
    //and pause the survey for that particular interval
    //resume once the interval is done
    //while the suvey is in pause condition dont fetch the next question untill the interval time reaches put the user on Pause controller each time on start click
    
    let localNotification = UILocalNotification()
    localNotification.alertBody = notificationText
    if #available(iOS 8.2, *) {
      localNotification.alertTitle = "MMR"
    } else {
      // Fallback on earlier versions
    }
    let checkBack = "Check back in".localized()
    let mins = Int(notificationTime)!
    if appDel.timerSet {
        print(appDel.timer.fireDate)
        print(appDel.timer.fireDate.timeIntervalSince(Date()))
        let newMins = appDel.timer.fireDate.timeIntervalSince(Date()) / 60
        let sec = (appDel.timer.fireDate as NSDate).timeIntervalSince(NSDate() as Date).truncatingRemainder(dividingBy: 60)
        /////
        if newMins > 0  {
            /////
        if mins <= 60 {
            self.checkbackLaterLabel.text = "\(checkBack) \(Int(newMins))min"
        } else {
            let hourtext = newMins/60
            let mintext = newMins.truncatingRemainder(dividingBy: 60)
                self.checkbackLaterLabel.text = "\(checkBack) \(Int(hourtext))hr : \(Int(mintext))min"
        }
            ////
            self.liveTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(self.updateLiveTimer), userInfo: nil, repeats: true)
    }
        ////
    }
    else {
//        if mins <= 60 {
//            self.checkbackLaterLabel.text = "\(checkBack) \(mins)min"
//        }
//        else {
//            let hourtext = mins/60
//            let mintext = mins%60
//            let sec = (appDel.timer.fireDate as NSDate).timeIntervalSince(NSDate() as Date).truncatingRemainder(dividingBy: 60)
//                self.checkbackLaterLabel.text = "\(checkBack) \(Int(hourtext))hr : \(Int(mintext))min"
//        }
        self.liveTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(self.updateLiveTimer), userInfo: nil, repeats: true)
    }
    let extraSeconds = mins * 60
//    let extraSeconds = 60
    let dateNew = Date().addingTimeInterval(Double(extraSeconds))
    localNotification.fireDate = dateNew
    localNotification.userInfo = [PUSH_CODE : INTERVAL]
    if (UserDefaults.standard.value(forKey: TIMER_STOP_DATE_UD) == nil) && !appDel.timerSet {
      if (UserDefaults.standard.value(forKey: DONE_WITH_INTERVAL_UD) == nil) {
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UIApplication.shared.scheduleLocalNotification(localNotification)
        appDel.timerSet = true
        appDel.timer = Timer.scheduledTimer(timeInterval: TimeInterval(extraSeconds), target: appDel, selector: #selector(appDel.update), userInfo: nil, repeats: false)
        UserDefaults.standard.set(Date(), forKey: TIMER_START_DATE_UD)
        UserDefaults.standard.set(TimeInterval(extraSeconds), forKey: TIMER_COMPLETION_SECs_UD)
      } else {
        ////
        self.checkbackLaterLabel.isHidden = true
        ////
        }
    } else {
        ///
        if self.checkbackLaterLabel.text == "Check Back in few mins" {
            self.checkbackLaterLabel.isHidden = true
        }
        ////
    }
    if !appDel.timerSet {
      self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
      self.nextButton.isUserInteractionEnabled = true
    } else {
      self.nextButton.isUserInteractionEnabled = false
    }
      print("questionFromServer\(questionFromServer)")
  }

    func updateLiveTimer() {
        let checkBack = "Check back in".localized()
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.timerSet {
        print(appDel.timer.fireDate)
        print(appDel.timer.fireDate.timeIntervalSince(Date()))
        let newMins = appDel.timer.fireDate.timeIntervalSince(Date()) / 60
        let sec = (appDel.timer.fireDate as NSDate).timeIntervalSince(NSDate() as Date).truncatingRemainder(dividingBy: 60)
        if newMins > 0 {
            if newMins == 0 && sec == 0 {
                self.checkbackLaterLabel.isHidden = true
            } else if newMins <= 60 {
                self.checkbackLaterLabel.text = "\(checkBack) \(Int(newMins))min : \(Int(sec))sec"
            } else {
                let hourtext = newMins/60
                let mintext = newMins.truncatingRemainder(dividingBy: 60)
                self.checkbackLaterLabel.text = "\(checkBack) \(Int(hourtext))hr : \(Int(mintext))min : \(Int(sec))sec"
            }
        } else {
            ///
            self.checkbackLaterLabel.isHidden = true
            ///
        }
    }
    }

  func navigationSetup() {
    navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
    navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
    self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
  }
  
  
  @objc func rightButtonAction() {
    self.dropTime = Date()
    self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    UserDefaults.standard.setValue(serialID, forKey: INTERVAL_QUESTION_SERIAL_NO_UD)
    UserDefaults.standard.synchronize()
    
//    let serialID = NSUserDefaults.standardUserDefaults().valueForKey(serialNoID) as! NSNumber
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
    }
    else if status == "No_Exists" {
      //no exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
    }
    delegate!.goToHomeViewController()
  }
  
  @IBAction func nextAction(_ sender: UIButton) {
    let currentTime = Date()
    let currentTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(currentTime)
    if let status = UserDefaults.standard.value(forKey: DONE_WITH_INTERVAL_UD) as? String {
      if status == "YES" {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
        let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let val1 = String(format: "%d", serialID)
          QuestionSharedClass.sharedInstance.updateOutTimeForQuestion(currentTimeStr, serialNo: "\(val1)", userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
        if isTimeLeftForSurvey {
          UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
          delegate?.fetchNextQuestionType()
        } else {
          UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
          BaseClassForQuestions.serialNo_ID = -1
          UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
          delegate?.fetchNextQuestionType()
        }
      }
      UserDefaults.standard.removeObject(forKey: TIMER_STOP_DATE_UD)
      UserDefaults.standard.removeObject(forKey: TIMER_START_DATE_UD)
      UserDefaults.standard.removeObject(forKey: TIMER_COMPLETION_SECs_UD)
      UserDefaults.standard.removeObject(forKey: DONE_WITH_INTERVAL_UD)
      UserDefaults.standard.removeObject(forKey: INTERVAL_QUESTION_SERIAL_NO_UD)
    }
    
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
