//
//  MMRThermometerCellCell.swift
//  MMR
//
//  Created by Mindbowser on 4/7/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRThermometerCellCell: UICollectionViewCell {
  
  @IBOutlet var checkBoxImageView: UIImageView!
    
  @IBOutlet var checkBoxLabel: UILabel!
  
  var selectedStatus = false
}
