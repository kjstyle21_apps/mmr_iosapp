//
//  MMRThermometerTypeQuestionController.swift
//  MMR
//
//  Created by Mindbowser on 4/7/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class MMRThermometerTypeQuestionController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

  @IBOutlet weak var CompulsaryTypeOfLabel: UILabel!
  @IBOutlet weak var heightOfScrollView: NSLayoutConstraint!
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var dropDownScrollView: UIScrollView!
  @IBOutlet weak var heightOfQuestionView: NSLayoutConstraint!
  @IBOutlet weak var questionTextViewDropDown: UITextView!
  @IBOutlet weak var navigationView: Navigation_View!

  @IBOutlet var thermometerCollectionView: UICollectionView!
  
  weak var delegate : BaseClassMethodToFetchDataFromDB?
  var questionFromServer  = NSMutableDictionary ()
  var isHintAvailable     = Bool ()
  var isMandatory         = Bool ()
  var isAnswerGiven       = Bool ()
  var labelString         = String ()
  var selected            = String()
  // var picker              = UIPickerView ()
  var selectedAnswer     = String ()
  var inTime = Date()
  var outTime = Date()
  var dropTime = Date()
  var finalOutTimeStr = String()
  var finalInTimeStr = String()
  var finalDropTime = String()
  var scale = String()
  var type1Array = ["1","2","3","4","5"]
  var type2Array = ["1","2","3","4","5","6","7","8","9"]
  var type3Array = ["1","2","3","4","5","6","7","8","9","10"]
  var type4Array = ["10","9","8","7","6","5","4","3","2","1"]
  var selectedAns = String()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
      
        thermometerCollectionView.delegate = self
        thermometerCollectionView.dataSource = self
        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 4.0
        layout.minimumLineSpacing = 4.0
        thermometerCollectionView.collectionViewLayout = layout
        navigationSetup()
        self.setText()
        if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
        if BMCentralManager.sharedInstance().isCentralReady == true {
          self.navigationView.statusLabel.isHidden = true
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = false
          })
        }
        else {
          self.navigationView.statusLabel.isHidden = false
          DispatchQueue.main.async(execute: {
            UIApplication.shared.isStatusBarHidden = true
          })
      }
          NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }
  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
    //    AlertModel.sharedInstance.showErrorMessage(bluetoothePowerOffMessage)
  }

  
  @objc func setText() {
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
    //    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
  }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  @IBAction func nextClicked(_ sender: AnyObject) {
    
    if self.isMandatory {
      if selectedAns == "" {
        self.nextButton.backgroundColor = UIColor.lightGray
        isAnswerGiven                   = false
      }
      else {
        isAnswerGiven                   = true
      }
    } else {
      isAnswerGiven                   = true
      self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    }
    
    if isAnswerGiven {
      self.outTime = Date()
      self.finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)

//      let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)

      self.delegate?.updateOutTimeForQuestion(self.finalOutTimeStr, serialNo: "\(val1)")
      
      if self.selectedAns == "" {
        delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
      }
      else {
        delegate?.updateAnswerTextForQuestion(self.selectedAns, serialNo: "\(val1)")
      }
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
      }
      
      let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
      if isTimeLeftForSurvey {
        UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
        delegate?.fetchNextQuestionType()
      } else {
        UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
        BaseClassForQuestions.serialNo_ID = -1
        UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
        delegate?.fetchNextQuestionType()
      }
    }
  }
  
  func navigationSetup() {
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
    
    //        navigationView.titleLabel.text = "Question " + "\(serialID!)"
    navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
    navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
    
    if isHintAvailable {
      navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
      navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
    }
    
    self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
  }
  
  @objc func leftButtonAction() {
    AlertModel.sharedInstance
      .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
  }
  
  @objc func rightButtonAction() {
    self.dropTime = Date()
    self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//    let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    //      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(serialID)", answerStartTime : "\(self.inTime)", dropEndTime : "\(self.dropTime)"])
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let val1 = String(format: "%d", serialID)
    let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
    
    if status == "Exists" {
      //Exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
    }
    else if status == "No_Exists" {
      //no exists
      QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
    }
    delegate!.goToHomeViewController()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let newSize : CGSize = self.questionTextViewDropDown.sizeThatFits(CGSize(width: self.questionTextViewDropDown.frame.size.width,  height: CGFloat(FLT_MAX)))
    print(newSize)
    self.heightOfQuestionView.constant = newSize.height
    
    self.heightOfScrollView.constant = self.view.frame.size.height - 64
    self.dropDownScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: newSize.height)
    
  }
  
  func initSetUp()
  {
    let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
    let val1 = String(format: "%d", serialID)
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)

    self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
//    if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//      if mflag {
//        let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID!)") as! Int
//        if seqNum > 1 {
          questionFromServer.setValue("", forKey: answerStartTime)
          questionFromServer.setValue("", forKey: answerEndTime)
          questionFromServer.setValue("", forKey: isAnswered)
          questionFromServer.setValue("", forKey: answer_text)
          questionFromServer.setValue(serialID, forKey: serial_id)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
          questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
          questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
          questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
          questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
          let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
          if resultArray.count == 0 {
            QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
          }
//        }
//      }
//    }
    
    self.inTime = Date()
    self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)
//    let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//    self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
    
    print(inTime)
    let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
    if status == "No_Exists" {
      self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
    }
    print("questionFromServer\(questionFromServer)")
    
//    self.questionTextViewDropDown.text   = questionFromServer[questionText] as! String
    let quesString = questionFromServer[questionText] as! String
    quesString.attributedStringFromHTML { (resultString) in
      self.questionTextViewDropDown.attributedText = resultString
    }
    isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
    self.isMandatory        = (questionFromServer[answerRequired] as! NSString).boolValue
    self.scale = questionFromServer["Scale"] as! String
    if self.isMandatory {
      self.CompulsaryTypeOfLabel.isHidden = false
      isAnswerGiven                   = false
      self.nextButton.backgroundColor = UIColor.lightGray
    } else {
      self.CompulsaryTypeOfLabel.isHidden = true
      isAnswerGiven                   = true
      self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
      
    }
    
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if self.scale == "Thermometer-1(1-5)" {
      return type1Array.count
    }
    else if self.scale == "Thermometer-2(1-9)" {
      return type2Array.count
    }
    else if self.scale == "Thermometer-3(1-10)" {
      return type3Array.count
    }
    else {
      return type4Array.count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.thermometerCollectionView.dequeueReusableCell(withReuseIdentifier: "MMRThermometerCellCell", for: indexPath) as! MMRThermometerCellCell
//    cell.tag = indexPath.item
//    cell.checkBoxLabel.text = "\(cell.tag)"
    if self.scale == "Thermometer-1(1-5)" {
      cell.checkBoxLabel.text = type1Array[indexPath.row]
    }
    else if self.scale == "Thermometer-2(1-9)" {
      cell.checkBoxLabel.text = type2Array[indexPath.row]
    }
    else if self.scale == "Thermometer-3(1-10)" {
      cell.checkBoxLabel.text = type3Array[indexPath.row]
    }
    else {
      cell.checkBoxLabel.text = type4Array[indexPath.row]
    }
    cell.checkBoxImageView.image = UIImage(named: "checkboxImage")
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                             sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
    
    let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
    let totalSpace = flowLayout.sectionInset.left
      + flowLayout.sectionInset.right
      + (flowLayout.minimumInteritemSpacing * CGFloat(10))
    let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(10))
    let sizeNew = CGSize(width: size, height: Int(thermometerCollectionView.frame.size.height))
    return sizeNew
    
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as! MMRThermometerCellCell
    if cell.selectedStatus {
      cell.checkBoxImageView.image = UIImage(named: "checkboxImage")
      cell.selectedStatus = false
      self.selectedAns = String()
      if self.isMandatory {
        isAnswerGiven = false
        self.nextButton.backgroundColor = UIColor.lightGray
      }
    }
    else {
      cell.checkBoxImageView.image = UIImage(named: "checkboxImageSelected")
      cell.selectedStatus = true
      self.selectedAns = cell.checkBoxLabel.text!
      if self.isMandatory {
        isAnswerGiven = true
        self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
      }
    }
    self.resetAllExceptIndex(indexPath)
  }
  
  func resetAllExceptIndex(_ indexP : IndexPath) {
    let num = thermometerCollectionView.numberOfItems(inSection: 0)
    for index in 0...num - 1 {
      if index != indexP.item {
        let indexP2 = IndexPath(item: index, section: 0)
        let cell = thermometerCollectionView.cellForItem(at: indexP2) as! MMRThermometerCellCell
        cell.checkBoxImageView.image = UIImage(named: "checkboxImage")
        cell.selectedStatus = false
      }
    }
  }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
