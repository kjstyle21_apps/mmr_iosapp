//
//  ImageTypeQuestionCollectionViewCell.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 11/21/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class ImageTypeQuestionCollectionViewCell: UICollectionViewCell {
    @IBOutlet var uploadImageView: UIImageView!
    
    @IBOutlet var dottedView: UIView!
    @IBOutlet var frameNameLabel: UILabel!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
  
}
