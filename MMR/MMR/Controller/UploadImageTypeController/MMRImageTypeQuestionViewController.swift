//
//  MMRImageTypeQuestionViewController.swift
//  CardDraggingDemo
//
//  Created by Mindbowser on 11/21/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import MediaPlayer
import Photos
import AssetsLibrary

enum SourcePicker : String {
    
    case Picture        = "pictureChoice"
    case Video        = "videoChoice"
}


class MMRImageTypeQuestionViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,UIAlertViewDelegate,MMRSubmitSurveyDelegate,NVActivityIndicatorViewable,MMRImageBrowserDelegate,MMRMediaUploaderDelegate {
    @IBOutlet weak var compulsaryStarLabel: UILabel!

    @IBOutlet weak var questionTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet var uploadImageCollectionView: UICollectionView!
    @IBOutlet var imageTypeScrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var navigationView: Navigation_View!
    var questionFromServer  = NSMutableDictionary ()
    var isHintAvailable     = Bool ()
    var isMandatory         = Bool ()
    var selectedCellIndex = IndexPath(item: -1, section: 0)
    let picker = UIImagePickerController()
    var isCam = false
    var imgURL = URL(string: "")
    var imgName = String()
    var libraryTitle = String()
    var uiImage:UIImage?
    var isAnswerGiven       = false
    
    var isCameraViewOpen   = false
    var inTime = Date()
    var outTime = Date()
    var dropTime = Date()
  
    var finalOutTimeStr = String()
    var finalInTimeStr = String()
    var finalDropTime = String()
  
    var videoURLArray = ["","",""]
    var imageURLArray = ["","",""]
    var compressedVideoURLArray = ["","",""]
    
    var assetUrlArray = ["","",""]
    var videoFlag = false
    var mediaSourceType        = SourcePicker(rawValue: "")
     let dashBorder : CAShapeLayer = CAShapeLayer()
    weak var delegate : BaseClassMethodToFetchDataFromDB?
    var selectedAssetsArray = [PHAsset(),PHAsset(),PHAsset()]
    let url11 = URL(string: "")
    var selectedAVAssetsAsset = [AVAsset(url: NSURL() as URL),AVAsset(url: NSURL() as URL),AVAsset(url: NSURL() as URL)]
    var avAssetIndexes = [-1,-1,-1]
    var phassetIndexes = [-1,-1,-1]
    var serialIDGlobal = Int()
  var backgroundUpdateTask : UIBackgroundTaskIdentifier?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            initSetUp()
        self.navigationSetup()
        self.setText()
      if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      if BMCentralManager.sharedInstance().isCentralReady == true {
        self.navigationView.statusLabel.isHidden = true
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = false
        })
      }
      else {
        self.navigationView.statusLabel.isHidden = false
        DispatchQueue.main.async(execute: {
          UIApplication.shared.isStatusBarHidden = true
        })
      }
      
      NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothStatus(_:)), name: NSNotification.Name(rawValue: "update"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.onFunc), name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
      }
    }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "update"), object: nil)
  }
  
  @objc func bluetoothStatus(_ notify : Notification) {
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
    let userinfo = notify.userInfo as! NSDictionary
    if userinfo.value(forKey: "state") as! String == "ON" {
      self.navigationView.statusLabel.isHidden = true
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = false
      })
    }
    else {
      self.navigationView.statusLabel.isHidden = false
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isStatusBarHidden = true
      })
    }
    }
  }

  
  @objc func onFunc() {
    print("Bluetooth is on reconnect the device")
    UserDefaults.standard.set(false, forKey: BLE_HISTORY_NOTIFY_FLAG_UD)
    UserDefaults.standard.synchronize()
    MMRBLEModule.sharedInstance.startBLEFunctionality()
  }
  
  func offFunc() {
    print("Bluetooth off device disconnected")
  }

    func initSetUp()
    {
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      self.questionFromServer = QuestionSharedClass.sharedInstance.fetchDataForQuestions(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)

//      if let mflag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if mflag {
          let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
//          if seqNum > 1 {
            questionFromServer.setValue("", forKey: answerStartTime)
            questionFromServer.setValue("", forKey: answerEndTime)
            questionFromServer.setValue("", forKey: isAnswered)
            questionFromServer.setValue("", forKey: answer_text)
            questionFromServer.setValue(serialID, forKey: "serial_id")
            questionFromServer.setValue("FALSE", forKey: IsSubmittedFlag)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_ID"), forKey: ques_id)
            questionFromServer.setValue(questionFromServer.value(forKey: "Question_Type"), forKey: quest_type)
            questionFromServer.setValue(questionFromServer.value(forKey: questionText), forKey: quest_text)
            questionFromServer.setValue(questionFromServer.value(forKey: "isHintAvailable"), forKey: isHintAvailabel)
            let resultArray = QuestionSharedClass.sharedInstance.isQuestionAvailable(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject) 
            if resultArray.count == 0 {
              QuestionSharedClass.sharedInstance.insertForQuestions(questionFromServer as! [String : AnyObject])
            }
//          }
//        }
//      }

      self.inTime = Date()
      self.finalInTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.inTime)

//      let outTimeStr = "\(self.inTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalInTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      print(inTime)
        let val1 = String(format: "%d", serialID)
      let status = QuestionSharedClass.sharedInstance.isStartTimeAvailable("\(val1)",userID: userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject)
      if status == "No_Exists" {
        self.delegate?.updateInTimeForQuestion(self.finalInTimeStr, serialNo: "\(val1)")
      }

        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing = 5.0
        picker.delegate = self
        uploadImageCollectionView.collectionViewLayout = layout
        
        mediaSourceType = SourcePicker(rawValue: QuestionSharedClass.sharedInstance.fetchQuestionType(userID! as AnyObject,fetchedSurveyID: currentSurveyID! as AnyObject))
        if mediaSourceType == SourcePicker.Video {
          self.videoFlag = true
        }
        else {
          self.videoFlag = false
        }
      
      let serial_id : Int = BaseClassForQuestions.serialNo_ID
      let seq = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        self.questionFromServer.setValue("FALSE", forKey: "Upload_Flag")
        self.questionFromServer.setValue(serial_id, forKey: "serial_id")
        self.questionFromServer.setValue(seq, forKey: "SeqNum")
//        questionTextView.text   = questionFromServer[questionText] as! String
//        self.questionTextView.attributedText   = QuestionSharedClass.sharedInstance.getHTMLTextAndRetriveNormalText(questionFromServer[questionText] as! String)
      let quesString = questionFromServer[questionText] as! String
      quesString.attributedStringFromHTML { (resultString) in
        self.questionTextView.attributedText = resultString
//        self.viewDidLayoutSubviews()
      }

        isHintAvailable         = (questionFromServer["isHintAvailable"] as! NSString).boolValue
        self.isMandatory        = (questionFromServer["answerRequired"] as! NSString).boolValue
        
        if self.isMandatory {
            self.compulsaryStarLabel.isHidden = false
            isAnswerGiven                   = false
            self.nextButton.backgroundColor = UIColor.lightGray
        } else {
            self.compulsaryStarLabel.isHidden = true
            isAnswerGiven                   = true
            self.nextButton.backgroundColor = UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        }
        
        
    }
    
    
    
    func navigationSetup() {
        let serialID = UserDefaults.standard.value(forKey: serialNoID)
        
//        navigationView.titleLabel.text = "Question " + "\(serialID!)"
        navigationView.rightButton.setImage(UIImage(named: "closeButton"), for: UIControlState())
        navigationView.rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        if isHintAvailable {
            navigationView.leftButton! .setImage(UIImage (named: "infoImage"), for: UIControlState())
            navigationView.leftButton!.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        }
        
        self.nextButton.roundbutton(self.nextButton, cornerradius: 5)
    }
    
    @objc func leftButtonAction() {
        AlertModel.sharedInstance
            .showMessage(MMRInstructionsTips.localized(), message: questionFromServer[hintText] as! String)
    }
    
    @objc func rightButtonAction() {
      self.dropTime = Date()
      self.finalDropTime = QuestionSharedClass.sharedInstance.getDateInUTC(self.dropTime)

//      let outTimeStr = "\(self.dropTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalDropTime = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let val1 = String(format: "%d", serialID)
      let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
      
      if status == "Exists" {
        //Exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 1)
      }
      else if status == "No_Exists" {
        //no exists
        QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, dropEndTime : self.finalDropTime as AnyObject], type : 0)
      }
      
      for imgTempUrl in self.imageURLArray{
        if imgTempUrl != "" {
      do {
        _ = try FileManager.default.removeItem(at: URL(fileURLWithPath: imgTempUrl))
        print("Item deleted Succesfully")
      } catch {
        print(error)
      }
      }
    }
      
      for videoTempUrl in self.videoURLArray{
        if videoTempUrl != "" {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
          let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(videoTempUrl)")
        do {
          _ = try FileManager.default.removeItem(at: dataPath1)
          print("Item deleted Succesfully")
        } catch {
          print(error)
        }
        }
      }
      
      for video in self.compressedVideoURLArray {
        if video != "" {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(video)")
        do {
          let success = try FileManager.default.removeItem(at: dataPath1)
          print("Item deleted Succesfully")
        } catch {
          print("Deletion error : \(error)")
        }
        }
      }
      
        delegate!.goToHomeViewController()
    }
    
    func goToHomeScreen () {
        var viewController: UIViewController?
        
        for aViewController in self.navigationController!.viewControllers {
            if aViewController.isKind(of: SSASideMenu.self) {
                viewController = aViewController
                break
            }
        }
        if let viewController = viewController {
            self.navigationController!.popToViewController(viewController, animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let newSize : CGSize = self.questionTextView.sizeThatFits(CGSize(width: self.questionTextView.frame.size.width,  height: CGFloat(FLT_MAX)))
        print(newSize)
        self.questionTextViewHeightConstraint.constant = newSize.height
      
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
      NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
    }
  
  @objc func setText() {
    let serialID = UserDefaults.standard.value(forKey: serialNoID)
//    navigationView.titleLabel.text = "Question ".localized() + "\(serialID!)"
    navigationView.titleLabel.text = ""
    self.nextButton.setTitle(NextButtonTitleKeyword.localized(), for: UIControlState())
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.uploadImageCollectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCollectionViewCell", for: indexPath) as! ImageTypeQuestionCollectionViewCell
            // cell.uploadImageView.tag == indexPath.row
        
        return cell
    }
    
   @objc func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(3))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
        let sizeNew = CGSize(width: size, height: size)
        return sizeNew
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCellIndex = indexPath
        let cell = self.uploadImageCollectionView.cellForItem(at: selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
        
        if cell.uploadImageView.tag == 1 {
            self.addActionSheetforSourceType(mediaSourceType!,DeleteOption: true)

        }
        else {
            self.addActionSheetforSourceType(mediaSourceType!,DeleteOption: false)
        }
        
    }
    
    func openGallery() {
        isCam = false
        picker.allowsEditing = false
        picker.sourceType = mediaSourceType == .Picture ? .savedPhotosAlbum : .photoLibrary
        if mediaSourceType == .Video {
//            picker.videoQuality = .Type640x480
////            picker.allowsEditing = true
////            picker.videoMaximumDuration = 60.0
//            picker.mediaTypes = [kUTTypeMovie as NSString as String]
          
          let storyboard = UIStoryboard(name: "MMRSurvey", bundle: nil)
          let browser = storyboard.instantiateViewController(withIdentifier: "MMRImageBrowserController") as! MMRImageBrowserController
//            let browser = storyboard.instantiateViewController(withIdentifier: "MMRImageBrowserControllerNav") as! UINavigationController
          browser.mediaDelegate = self
          present(browser, animated: true, completion: nil)
        }
        else {
          present(picker, animated: true, completion: nil)
      }
    }
    
    func openCam() {
        isCam = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            isCameraViewOpen = true
            UIImagePickerController.availableMediaTypes(for: .camera)
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            if mediaSourceType == .Video {
                picker.videoMaximumDuration = 60.0
                picker.videoQuality = .type640x480
                picker.mediaTypes = [kUTTypeMovie as NSString as String]
            } else {
                picker.cameraCaptureMode = .photo
            }

            present(picker,animated: true,completion: nil)
        } else {
           self.noCamera("Sorry, this device has no camera")
//          UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
          DispatchQueue.main.async(execute: { 
            UIApplication.shared.isStatusBarHidden = false
          })

        }
    }
    
    
    func noCamera(_ errorMessage:String) {
        let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK",style:.default,handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
  
  
  
  
    func imagePickerController(_ _picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        isCameraViewOpen = false
        var fileSize : UInt64 = 0
        let deviceSpace = self.deviceRemainingFreeSpaceInBytes()!
        var videoURL = URL(string: "")
        var strName = NSString()
        UIApplication.shared.isStatusBarHidden = false
        if isCam == false{
            let imageURL = info[UIImagePickerControllerReferenceURL] as! URL
            let imageName = imageURL.lastPathComponent
            
            print("URL : \(imageURL)")
            print("Image Name : \(imageName)")
        }
        if mediaSourceType == .Video {
          
          if let compCount = UserDefaults.standard.integer(forKey: VIDEO_COMPRESSION_COUNT_UD) as? Int {
            var tempCnt = compCount + 1
            UserDefaults.standard.set(tempCnt, forKey: VIDEO_COMPRESSION_COUNT_UD)
          }
          else {
            UserDefaults.standard.set(0, forKey: VIDEO_COMPRESSION_COUNT_UD)
          }

          
          let videoURL1 = info[UIImagePickerControllerMediaURL] as! URL

          let testAsset = AVURLAsset(url: videoURL1,options: nil)
          var durationSec = TimeInterval()
          durationSec = CMTimeGetSeconds(testAsset.duration)
          print(durationSec)
          
            self.videoFlag = true
            let mediaType = info[UIImagePickerControllerMediaType] as! NSString
            if (info[UIImagePickerControllerMediaURL] as? AnyObject) is NSURL {
                
//            }
//            if (info[UIImagePickerControllerMediaURL]! as AnyObject).isKind(of: NSURL(string: "") as! URL){
                let videoUrl = info[UIImagePickerControllerMediaURL] as! URL //2
              

              let videoData = try? Data(contentsOf: videoUrl)
              // *** Get documents directory path *** //
              let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
              // *** Append video file name *** //
              let tempDate = "\(Date())"
//              let strName = tempDate.stringByReplacingOccurrencesOfString(" ", withString: "")
                let userID = UserDefaults.standard.value(forKey: currentUserId)
                let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                strName = "\(timestamp)" as NSString
              let dataPath = URL(string: paths)!.appendingPathComponent("\(strName).mov")
              videoURL = dataPath
              
              do {
                let attr : NSDictionary? = try FileManager.default.attributesOfItem(atPath: videoUrl.path) as NSDictionary
                if let _attr = attr {
                  let fileSize = _attr.fileSize();
                  print(fileSize)
                  print(deviceSpace)
                }
              } catch {
                print("Error: \(error)")
              }
              
              if fileSize < deviceSpace {
                
              if FileManager.default.fileExists(atPath: "\(dataPath)") {
                let status = (try? videoData?.write(to: dataPath, options: [])) != nil
              }
              else {
                FileManager.default.createFile(atPath: "\(dataPath)", contents: videoData, attributes: nil)
                let status = (try? videoData?.write(to: URL(fileURLWithPath: dataPath.path), options: [])) != nil
                print(status)
                if (status != nil){
                  self.videoURLArray[selectedCellIndex.row] = "\(strName).mov"
                }
              }
                if isCam {
                let asset = AVURLAsset(url: videoUrl, options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                  
                  do {
                    let cgimage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
                    self.uiImage = UIImage(cgImage: cgimage)
                  }
                  catch let error {
                    print("failed: \((error as NSError).localizedDescription)")
                  }
                  
                let cell = self.uploadImageCollectionView.cellForItem(at: self.selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
              
                cell.uploadImageView.contentMode = .scaleAspectFill
                cell.uploadImageView.image = self.uiImage //4
                cell.dottedView.layer.borderWidth = 0.5
                cell.dottedView.clipsToBounds = true
                cell.dottedView.layer.borderColor = UIColor.gray.cgColor
                cell.uploadImageView.tag = 1
                }
                self.checkForNextbuttonUserInteraction()
                
          }
              else{
                AlertModel.sharedInstance.showErrorMessage("Insufficient Memory Space")
              }
            }
        }
        else {
            let image = UIImage()
            if (info[UIImagePickerControllerOriginalImage]! as AnyObject).isKind(of: UIImage.self){
              print(info)
              self.videoFlag = false

              var image = info[UIImagePickerControllerOriginalImage] as! UIImage
              var imgData: Data = NSData(data: UIImageJPEGRepresentation((image), 1)!) as Data
              fileSize = UInt64(imgData.count)
              
              if fileSize < deviceSpace {
                let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
                
                let resizedImg = resize(chosenImage)
              
              //save image into App Sandbox with a TimeStamp as a name
                let imageLocalPath = self.saveImageDocumentDirectory(resizedImg)
                self.imageURLArray[selectedCellIndex.row] = imageLocalPath
              
                print(selectedCellIndex.item)
                let cell = self.uploadImageCollectionView.cellForItem(at: selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
                cell.uploadImageView.contentMode = .scaleAspectFill
                cell.uploadImageView.image = chosenImage //4
                cell.dottedView.layer.borderWidth = 0.5
                cell.dottedView.clipsToBounds = true
                cell.dottedView.layer.borderColor = UIColor.gray.cgColor
                cell.uploadImageView.tag = 1
                self.checkForNextbuttonUserInteraction()
          }
              else{
                AlertModel.sharedInstance.showErrorMessage("Insufficient Memory Space")
              }
            }
        }
      dismiss(animated: true) {
        if self.mediaSourceType == .Video {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//          let dateStr = "\(NSDate()).mov"
            let userID = UserDefaults.standard.value(forKey: currentUserId)
            let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
            let dateStr = "\(strName).mov"
          let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(dateStr)")
            self.assetUrlArray.remove(at: self.selectedCellIndex.row)
          self.assetUrlArray.insert(dateStr as String, at: self.selectedCellIndex.row)
          let url = URL(fileURLWithPath: videoURL!.path)
            
          let asset1 = AVAsset(url: url)
          self.selectedAVAssetsAsset.remove(at: self.selectedCellIndex.row)
          self.selectedAVAssetsAsset.insert(asset1, at: self.selectedCellIndex.row)
          
          self.avAssetIndexes.remove(at: self.selectedCellIndex.row)
          self.avAssetIndexes.insert(self.selectedCellIndex.row, at: self.selectedCellIndex.row)
          
        }
      }
  }
  
  func convertVideoToLowQuality(_ asset : AVAsset, assetIndex : Int,outputURL : URL, outputFileName : NSString, handler:(_ session: AVAssetExportSession)-> Void) {
  
    
    if assetIndex != -1 {
    do {
      try FileManager.default.removeItem(at: outputURL)
    }
    catch let error {
      print("Compression Error : \(error)")
    }
    var exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)
    exportSession!.outputURL = outputURL
    
    let dur = CMTimeGetSeconds(asset.duration)
    if dur > 60.0 {
        let start = CMTimeMakeWithSeconds(1.0, 600)
        let duration = CMTimeMakeWithSeconds(60.0, 600)
        let range = CMTimeRangeMake(start, duration)
        exportSession?.timeRange = range
    }
    exportSession!.outputFileType = AVFileType.mov
//      self.beginBackgroundUpdateTask()
        
      exportSession?.exportAsynchronously(completionHandler: {
        switch exportSession!.status {
        case  AVAssetExportSessionStatus.failed:
          print("ERROR AKAudioFile: Export Failed!...")
          print("Error: \(exportSession!.error)")
        case AVAssetExportSessionStatus.cancelled:
          print("ERROR AKAudioFile: Export Cancelled!...")
          print("Error: \(exportSession!.error)")
        default:
          // Export succeeded !
          let url = URL(string: exportSession!.outputURL!.path)
          print("success")
          self.compressedVideoURLArray[assetIndex] = outputFileName as String
          if assetIndex == 2 {
           self.convertVideoToLowQuality(AVAsset(url: URL(string: "")!), assetIndex: -1, outputURL: URL(string: "")!, outputFileName: "", handler: { (session) in
            //do nothing
           })
          }
          else {
            
            if assetIndex + 1 == 1 || assetIndex + 1 == 2 {
              if self.avAssetIndexes[1] == -1 && self.phassetIndexes[1] == -1 {
                if self.avAssetIndexes[2] == -1 && self.phassetIndexes[2] == -1 {
                  self.convertVideoToLowQuality(AVAsset(url: URL(string: "")!), assetIndex: -1, outputURL: URL(string: "")!, outputFileName: "", handler: { (session) in
                    //do nothing
                  })
                }
                else {
                  if self.avAssetIndexes[2] != -1 && self.phassetIndexes[2] == -1 {
                    let documentsPath1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//                    let dateStr1 = "\(NSDate()).mov"
                    let userID = UserDefaults.standard.value(forKey: currentUserId)
                    let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                    let dateStr1 = "\(timestamp).mov"
                    let dataPath2 = URL(fileURLWithPath: "\(documentsPath1)/\(dateStr1)")
                    self.convertVideoToLowQuality(self.selectedAVAssetsAsset[2], assetIndex: 2, outputURL: dataPath2, outputFileName: dateStr1 as NSString, handler: { (session) in
                      if session.status == .completed {
                        print("Compression completed")
                      }
                    })
                  }
                  else if self.avAssetIndexes[2] == -1 && self.phassetIndexes[2] != -1 {
                    self.convertVideoToLowQualityPHAsset(self.selectedAssetsArray[2], index: 2)
                  }
                }
              }
              else {
                var tempIndex = 1
                if assetIndex + 1 == 1 {
                  tempIndex = 1
                }
                else if assetIndex + 1 == 2 {
                  tempIndex = 2
                }
                //self.compressedVideoURLArray[tempIndex] = outputFileName as String
                if self.avAssetIndexes[tempIndex] != -1 && self.phassetIndexes[tempIndex] == -1 {
                  let documentsPath1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//                  let dateStr1 = "\(NSDate()).mov"
                    let userID = UserDefaults.standard.value(forKey: currentUserId)
                    let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                    let dateStr1 = "\(timestamp).mov"
                  let dataPath2 = URL(fileURLWithPath: "\(documentsPath1)/\(dateStr1)")
                  self.convertVideoToLowQuality(self.selectedAVAssetsAsset[tempIndex], assetIndex: tempIndex, outputURL: dataPath2, outputFileName: dateStr1 as NSString, handler: { (session) in
                    if session.status == .completed {
                      print("Compression completed")
                    }
                  })
                }
                else if self.avAssetIndexes[tempIndex] == -1 && self.phassetIndexes[tempIndex] != -1 {
                  self.convertVideoToLowQualityPHAsset(self.selectedAssetsArray[tempIndex], index: tempIndex)
                }
                else if assetIndex + 1 == 2 {
                  if self.avAssetIndexes[2] == -1 && self.phassetIndexes[2] == -1 {
                    self.convertVideoToLowQuality(AVAsset(url: URL(string: "")!), assetIndex: -1, outputURL: URL(string: "")!, outputFileName: "", handler: { (session) in
                      //do nothing
                    })
                  }
                }
              }
            }
          }
        }

      })
  }
    else {
      print(compressedVideoURLArray)
      self.needTobeDoneINBackground(serialIDGlobal)
    }
  }
  
  
  func convertVideoToLowQualityPHAsset(_ asset : PHAsset, index : Int) {
    
    if index != -1 {
      let defaultRep = PHImageManager.default()
      defaultRep.requestExportSession(forVideo: asset, options: .none, exportPreset: AVAssetExportPreset640x480) { (exportSession, info) in
        
        guard let exportSession = exportSession
          else {
            if let error = info?[PHImageErrorKey] as? NSError {
            } else {
            }
            return
        }
        let cmtime = CMTimeMakeWithSeconds(asset.duration, 600)
        let dur = CMTimeGetSeconds(cmtime)
        if dur > 60.0 {
          let start = CMTimeMakeWithSeconds(1.0, 600)
          let duration = CMTimeMakeWithSeconds(60.0, 600)
          let range = CMTimeRangeMake(start, duration)
          exportSession.timeRange = range
        }
        
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//        let dateStr = "\(NSDate()).mov"
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
        let dateStr = "\(timestamp).mov"
        let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(dateStr)")
        self.removeFile(dataPath1)
        exportSession.outputURL = dataPath1 as URL
//        self.beginBackgroundUpdateTask()
        exportSession.exportAsynchronously(completionHandler: {
          switch exportSession.status{
          case .completed :
            print("Complete")
            print(exportSession.outputURL)
            let videoUrl = exportSession.outputURL
            let url = URL(fileURLWithPath: videoUrl!.path)
            let asset1 = AVAsset(url: url)
            self.videoURLArray[index] = dateStr
            self.selectedAVAssetsAsset.remove(at: index)
            self.selectedAVAssetsAsset.insert(asset1, at: index)

              if self.avAssetIndexes[index] == -1 && self.phassetIndexes[index] != -1 {
                let documentsPath1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//                let dateStr1 = "\(NSDate()).mov"
                let userID = UserDefaults.standard.value(forKey: currentUserId)
                let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                let dateStr1 = "\(timestamp).mov"
                let dataPath2 = URL(fileURLWithPath: "\(documentsPath1)/\(dateStr1)")
                self.convertVideoToLowQuality(asset1, assetIndex: index, outputURL: dataPath2, outputFileName: dateStr1 as NSString, handler: { (session) in
                  if session.status == .completed {
                    print("Compression completed")
                  }
                })
              }
              

            
            break
          case .cancelled:
            print("cancelled")
            if let error = exportSession.error {
              print(error)
            }
            break
          case .exporting:
            print("Exporting")
            if let error = exportSession.error {
              print(error)
            }
            break
          case .failed:
            print("Exporting")
            if let error = exportSession.error {
              print(error)
            }
            break
          case .waiting:
            print("Waiting")
            if let error = exportSession.error {
              print(error)
            }
            break
          default :
            break
          }
        })
      }
    }
    else {
      //do nothing
    }
}
  
  func removeFile(_ fileURL: URL) {
    let filePath = fileURL.path
    let fileManager = FileManager.default
    if fileManager.fileExists(atPath: filePath) {
    do{
      try fileManager.removeItem(atPath: filePath)
    }
    catch {
    print(error)
    }
    }
    }

  func deviceRemainingFreeSpaceInBytes() -> UInt64? {
    let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
    guard
      let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
      let freeSize = systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber
      else {
        // something failed
        return nil
    }
    return freeSize.uint64Value
  }
  
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isCameraViewOpen = false
        UIApplication.shared.isStatusBarHidden = false
        let cell = self.uploadImageCollectionView.cellForItem(at: selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
        
        dismiss(animated: true, completion: nil)


    }
    
    func addActionSheetforSourceType(_ source:SourcePicker,DeleteOption:Bool) {
        
        
        
        let alertcontroller = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
            alertcontroller.dismiss(animated: true, completion: nil)
        }
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (camera) in
             self.mediaAccessPermissions()
         }
        if DeleteOption == true {
          if source == .Picture{
            let deleteAction = UIAlertAction(title: "Delete Photo", style: .destructive) { (delete) in
                self.deleteSelectedImageOrVideo()
            }
            alertcontroller.addAction(deleteAction)
          }
          else{
            let deleteAction = UIAlertAction(title: "Delete Video", style: .destructive) { (delete) in
              self.deleteSelectedImageOrVideo()
            }
            alertcontroller.addAction(deleteAction)
          }

        }
        
        if source == .Picture {
            libraryTitle = "Image Library"
        }
        else {
            libraryTitle = "Video Library"
        }
        let phoneLibraryAction = UIAlertAction(title: libraryTitle, style: .default) { (library) in
            self.openGallery()
        }
        alertcontroller.addAction(cancelAction)
        alertcontroller.addAction(cameraAction)
        alertcontroller.addAction(phoneLibraryAction)
        
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    func deleteSelectedImageOrVideo() {
      
      
      if self.videoFlag == false {
        do {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
          if self.imageURLArray[self.selectedCellIndex.row] != "" {
          let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(self.imageURLArray[self.selectedCellIndex.row])")
          _ = try FileManager.default.removeItem(at: dataPath1)
          print("Item deleted Succesfully")
        }
        } catch {
          print(error)
        }
      }
      
      
      if self.videoFlag == true {
        
        if let compCount = UserDefaults.standard.integer(forKey: VIDEO_COMPRESSION_COUNT_UD) as? Int {
          var tempCnt = compCount
          if tempCnt != 0 {
            tempCnt = tempCnt - 1
            UserDefaults.standard.set(tempCnt, forKey: VIDEO_COMPRESSION_COUNT_UD)
          }
        }
        else {
          UserDefaults.standard.set(0, forKey: VIDEO_COMPRESSION_COUNT_UD)
        }

        
        do {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
          if self.videoURLArray[self.selectedCellIndex.row] != "" {
          let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(self.videoURLArray[self.selectedCellIndex.row])")
          _ = try FileManager.default.removeItem(at: dataPath1)
          print("Item deleted Succesfully")
          }
        } catch {
          print(error)
        }
        
        do {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
          if self.compressedVideoURLArray[self.selectedCellIndex.row] != "" {
            let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(self.compressedVideoURLArray[self.selectedCellIndex.row])")
            _ = try FileManager.default.removeItem(at: dataPath1)
            print("Item deleted Succesfully")
          }
        } catch {
          print(error)
        }
        
        self.assetUrlArray.remove(at: self.selectedCellIndex.row)
        self.assetUrlArray.insert("", at: self.selectedCellIndex.row)
      }
      
        let cell = self.uploadImageCollectionView.cellForItem(at: selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
              cell.uploadImageView.image = UIImage(named: "add.png")
              cell.dottedView.layer.borderWidth = 0.5
              cell.dottedView.clipsToBounds = true
              cell.dottedView.layer.borderColor = UIColor.clear.cgColor
              cell.uploadImageView.tag = 0
      
             uploadImageCollectionView.reloadItems(at: [selectedCellIndex])
             self.checkForNextbuttonUserInteraction()
    }
    
    @IBAction func nextButtonclick(_ sender: UIButton) {
      self.outTime = Date()
      finalOutTimeStr = QuestionSharedClass.sharedInstance.getDateInUTC(self.outTime)
      
//      print(tOutTime)
      
//      let outTimeStr = "\(self.outTime)".stringByReplacingOccurrencesOfString(" ", withString: "T")
//      self.finalOutTimeStr = outTimeStr.stringByReplacingOccurrencesOfString("T+", withString: "+")
      print(finalOutTimeStr)
      let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
      serialIDGlobal = UserDefaults.standard.value(forKey: serialNoID) as! Int
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)

      if self.videoFlag == false {
        
        var tempArray = [String]()
        for vid in self.imageURLArray {
          if vid != "" {
            tempArray.append(vid)
          }
        }
        
        let imageURLString = tempArray.joined(separator: "^]`,")
        let val1 = String(format: "%d", serialID)
        if imageURLString == ""{
          delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
          self.delegate?.updateOutTimeForQuestion(finalOutTimeStr, serialNo: "\(val1)")
        }
        else {
          self.delegate?.updateOutTimeForQuestion(finalOutTimeStr, serialNo: "\(val1)")
//          if let flag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//            if flag {
              var cnt = 0
              for temp in tempArray {
                self.questionFromServer.setValue(temp, forKey: answer_text)
                UploadRecordSharedClass.sharedInstance.insertuploadData(self.questionFromServer, counter: cnt,fetchedSurveyID: currentSurveyID! as AnyObject)
                cnt = cnt + 1
              }
              
              let reachability: Reachability
              do {
                reachability = try Reachability.reachabilityForInternetConnection()
              } catch {
                print("Unable to create Reachability")
                return
              }
              
              if reachability.isReachable() {
              let uploader = MMRMediaUploader.sharedInstance
                UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
              uploader.startUploadProcess("\(self.questionFromServer[quest_type])")
              }
//            }
//            else {
//              delegate?.updateAnswerTextForQuestion(imageURLString, serialNo: "\(serialID)")
//            }
//          }
//          else {
//            delegate?.updateAnswerTextForQuestion(imageURLString, serialNo: "\(serialID)")
//          }
        }
      }
      
        if self.videoFlag == true {
            /*let qualityOfServiceClass = QOS_CLASS_BACKGROUND
             let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
             dispatch_async(backgroundQueue) {
             if self.avAssetIndexes[0] == -1 && self.phassetIndexes[0] == -1 {
             if self.avAssetIndexes[1] == -1 && self.phassetIndexes[1] == -1 {
             if self.avAssetIndexes[2] == -1 && self.phassetIndexes[2] == -1 {
             //do nothing
             self.needTobeDoneINBackground(self.serialIDGlobal)
             }
             else {
             self.initialStart(2)
             }
             }
             else {
             self.initialStart(1)
             }
             }
             else {
             self.initialStart(0)
             }
             }*/
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
           // dispatch_async(backgroundQueue) {
                let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                let questionid = self.questionFromServer.value(forKey: ques_id)
                let serialID = UserDefaults.standard.value(forKey: serialNoID) as! Int
                let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
                let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
                var cnt = 0
                let val1 = String(format: "%d", serialID)
                for temp in self.assetUrlArray {
                    if temp != ""{
                        if temp.contains("/") {
                            CompressionRecordsSharedClass.sharedInstance.insertMediaDetails(currentSurveyID! as AnyObject, questionId: questionid! as AnyObject, serialId: "\(val1)_\(cnt)" as AnyObject, SurveySeqNum: seqNum as AnyObject, videoLocalPath: temp, assetType: "PH")
                            
                        } else {
                            CompressionRecordsSharedClass.sharedInstance.insertMediaDetails(currentSurveyID! as AnyObject, questionId: questionid! as AnyObject, serialId: "\(val1)_\(cnt)" as AnyObject, SurveySeqNum: seqNum as AnyObject, videoLocalPath: temp, assetType: "AV")
                        }
                        cnt = cnt + 1
                    }
                }
                if self.assetUrlArray.count > 0 {
                    if self.assetUrlArray != ["","",""] {
                        CompressionRecordsSharedClass.sharedInstance.startCompression()
                    }
                }
           // }
        }
        
      if isAnswerGiven {
        let isTimeLeftForSurvey = QuestionSharedClass.sharedInstance.compareTwoDates()
        if isTimeLeftForSurvey {
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            let val1 = String(format: "%d", serialID)
          let status = QuestionDropRecordSharedClass.sharedInstance.isDropOutTimeAvailable("\(val1)",userID: userID! as AnyObject)
          if status == "Exists" {
            //Exists
            QuestionDropRecordSharedClass.sharedInstance.insertForQuestions([serial_id : "\(val1)" as AnyObject, answerStartTime : self.finalInTimeStr as AnyObject], type : 2)
          }
          UserDefaults.standard.set(true, forKey: SURVEY_COMPLETE_STATUS_UD)
          delegate?.fetchNextQuestionType()
        } else {

          UserDefaults.standard.set(false, forKey: SURVEY_COMPLETE_STATUS_UD)
          BaseClassForQuestions.serialNo_ID = -1
          UserDefaults.standard.set(BaseClassForQuestions.serialNo_ID, forKey: serialNoID)
          delegate?.fetchNextQuestionType()
        }
        
      }
      
    }
  
  func beginBackgroundUpdateTask() {
    self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(withName: "COMPRESSION_TASK", expirationHandler: { 
      self.endBackgroundUpdateTask()
    })
  }
  
  func endBackgroundUpdateTask() {
    UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask!)
    self.backgroundUpdateTask = UIBackgroundTaskInvalid
  }
  
  func didCompleteImageUpload(_ status: Bool) {
    if status {
      UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
      UserDefaults.standard.synchronize()
    }
    else {
      UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
      UserDefaults.standard.synchronize()
    }
  }
  
  func uploadAction() {
    startActivityAnimating(CGSize(width: 50,height: 50), message: "Uploading...", type: NVActivityIndicatorType.ballTrianglePath,color: UIColor.white,isSloganEnable : true)
    let uploadSurvey = MMRUploadSurvey()
    uploadSurvey.uploadDelegate = self
    uploadSurvey.surveySubmissionStatus = false
    uploadSurvey.uploadSurvey()
  }
  
  func initialStart(_ index : Int) {
    if self.avAssetIndexes[index] != -1 {
      let documentsPath1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//      let dateStr1 = "\(NSDate()).mov"
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
        let dateStr1 = "\(timestamp).mov"
      let dataPath2 = URL(fileURLWithPath: "\(documentsPath1)/\(dateStr1)")
      self.convertVideoToLowQuality(self.selectedAVAssetsAsset[index], assetIndex: index, outputURL: dataPath2, outputFileName: dateStr1 as NSString, handler: { (session) in
        if session.status == .completed {
          print("Compression completed")
        }
      })
    }
    else {
      self.convertVideoToLowQualityPHAsset(self.selectedAssetsArray[index], index: index)
    }
  }
  
  func needTobeDoneINBackground(_ serialID : Int) {
    
    if let compCount = UserDefaults.standard.integer(forKey: VIDEO_COMPRESSION_COUNT_UD) as? Int {
      var tempCnt = compCount
      var arrayCount = 0
      if tempCnt != 0 {
        for item in self.compressedVideoURLArray {
          if item != "" {
            arrayCount = arrayCount + 1
          }
        }
        tempCnt = tempCnt - arrayCount
      UserDefaults.standard.set(tempCnt, forKey: VIDEO_COMPRESSION_COUNT_UD)
        if tempCnt == 0 {
          NotificationCenter.default.post(name: Notification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
        }
      }
      else {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
      }
    }
    else {
      NotificationCenter.default.post(name: Notification.Name(rawValue: "VIDEO_COMPRESSION_COUNT_NOTIFICATION"), object: nil)
      UserDefaults.standard.set(0, forKey: VIDEO_COMPRESSION_COUNT_UD)
    }
    
      var tempArray = [String]()
      for vid in self.compressedVideoURLArray {
        if vid != "" {
          tempArray.append(vid)
        }
      }
      
      let videoURLString = tempArray.joined(separator: "^]`,")
    let val1 = String(format: "%d", serialID)
    if videoURLString == "" {
      delegate?.updateAnswerTextForQuestion("NOT ANSWERED", serialNo: "\(val1)")
      self.delegate?.updateOutTimeForQuestion(finalOutTimeStr, serialNo: "\(val1)")
    }
    else {
      self.delegate?.updateOutTimeForQuestion(finalOutTimeStr, serialNo: "\(val1)")
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      
//      if let flag = NSUserDefaults.standardUserDefaults().boolForKey("MULTIPLE_FLAG") as? Bool {
//        if flag {
          var cnt = 0
          for temp in tempArray {
            self.questionFromServer.setValue(temp, forKey: answer_text)
            UploadRecordSharedClass.sharedInstance.insertuploadData(self.questionFromServer, counter: cnt,fetchedSurveyID: currentSurveyID! as AnyObject)
            cnt = cnt + 1
          }
          
          let reachability: Reachability
          do {
            reachability = try Reachability.reachabilityForInternetConnection()
          } catch {
            print("Unable to create Reachability")
            return
          }
          
          if reachability.isReachable() {
          let uploader = MMRMediaUploader.sharedInstance
          UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
          uploader.startUploadProcess("\(self.questionFromServer[quest_type])")
          }
//        }
//        else {
////          delegate?.updateAnswerTextForQuestion(imageURLString, serialNo: "\(serialID)")
//          delegate?.updateAnswerTextForQuestion(videoURLString, serialNo: "\(serialID)")
//        }
//      }
//      else {
////        delegate?.updateAnswerTextForQuestion(imageURLString, serialNo: "\(serialID)")
//        delegate?.updateAnswerTextForQuestion(videoURLString, serialNo: "\(serialID)")
//      }
      
    }
      for video in self.videoURLArray {
        if video != "" {
          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
          let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(video)")
          do {
            let success = try FileManager.default.removeItem(at: dataPath1)
            print("Item deleted Succesfully")
          } catch {
            print("Deletion error : \(error)")
          }
        }
      }
    
  }
  
  func uploadCompletion(_ status: Bool) {
    if status {
      print("Success")
      AlertModel.sharedInstance.showAlert("MMR", message: "Survey Sumitted Successfully".localized(), buttonTitles: ["OK"], viewController: self, completionHandler: { (clickedButtonTitle, success) in
        let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        UserDefaults.standard.set(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID1!)")
        UserDefaults.standard.set(true, forKey: NoSurveyForUser)
        UserDefaults.standard.synchronize()
        self.delegate!.goToHomeViewController()
      })
    }
    else {
      print("Failure")
    }
        stopActivityAnimating()
  }
  
    func checkForNextbuttonUserInteraction() {
        var uploadedCount = 0
        if self.isMandatory == true {
            for i in 0..<3 {
                let indexpath = IndexPath(item: i, section: 0)
                let cell = self.uploadImageCollectionView.cellForItem(at: indexpath) as! ImageTypeQuestionCollectionViewCell
                if cell.uploadImageView.tag == 1 {
                    uploadedCount += 1
                }
            }
            isAnswerGiven = uploadedCount > 0 ? true : false
            self.nextButton.backgroundColor = isAnswerGiven == true ? UIColor(red: 74.0/255.0, green: 26.0/255.0, blue: 102.0/255.0, alpha: 1.0) : UIColor.lightGray
        }
        else {
            isAnswerGiven = true
        }
       
    }
    
    
    func mediaAccessPermissions() {
        
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
     
      
      var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
      var camFlag = false

        switch authStatus {
        case .authorized:
            self.openCam()
            DispatchQueue.main.async(execute: {
              UIApplication.shared.isStatusBarHidden = true
            })
            break
        case .denied :
            self.noCamera("MMR does not have permission to access camera \n please go to settings and turn on Permissions.")
            DispatchQueue.main.async(execute: {
              UIApplication.shared.isStatusBarHidden = false
            })
          break
        case .notDetermined :
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    self.openCam()
                  camFlag = true
                }
                else {
                    self.noCamera("MMR does not have permission to access camera \n please go to settings and turn on Permissions.")
                    camFlag = false
                    DispatchQueue.main.async(execute: { 
                      UIApplication.shared.isStatusBarHidden = false
                    })
                }
            })

            break
        case .restricted :
            self.noCamera("MMR does not have permission to access camera \n please go to settings and turn on Permissions.")
            UIApplication.shared.isStatusBarHidden = false
            
            break
        default:
            break
            
        }
      
    }
  
  @objc func update() {
//    UIApplication.sharedApplication().statusBarHidden = true
  }
  
    
        func resize(_ image: UIImage) -> UIImage {
            var actualHeight: Float = Float(image.size.height)
            var actualWidth: Float = Float(image.size.width)
            let maxHeight: Float = 400.0
            let maxWidth: Float = 400.0
            var imgRatio: Float = actualWidth / actualHeight
            let maxRatio: Float = maxWidth / maxHeight
            // let compressionQuality: Float = 0.8
            if actualHeight > maxHeight || actualWidth > maxWidth {
                if imgRatio < maxRatio {
                    //adjust width according to maxHeight
                    imgRatio = maxHeight / actualHeight
                    actualWidth = imgRatio * actualWidth
                    actualHeight = maxHeight
                }
                else if imgRatio > maxRatio {
                    //adjust height according to maxWidth
                    imgRatio = maxWidth / actualWidth
                    actualHeight = imgRatio * actualHeight
                    actualWidth = maxWidth
                }
                else {
                    actualHeight = maxHeight
                    actualWidth = maxWidth
                }
            }
    
            let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            //    image.draw(in: rect)
            let img = UIGraphicsGetImageFromCurrentImageContext()!
            let imageData = UIImagePNGRepresentation(img)
            UIGraphicsEndImageContext()
            return UIImage(data: imageData!)!
        }
  
  func saveImageDocumentDirectory(_ imageNeedToSave : UIImage) -> String {
    let fileManager = FileManager.default
    let userID = UserDefaults.standard.value(forKey: currentUserId)
    let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
    let strName = "\(timestamp)"
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(strName).jpg")
    print(paths)
    let imageData = UIImageJPEGRepresentation(imageNeedToSave, 0.5)
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
//    return paths as! String
    return "\(strName).jpg"
  }
  
  
  func getDocumentsDirectory() -> URL {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            return documentsDirectory
  }

  func mediaSelected(_ selectedAsset: PHAsset) {
    
    if let compCount = UserDefaults.standard.integer(forKey: VIDEO_COMPRESSION_COUNT_UD) as? Int {
          let tempCnt = compCount + 1
          UserDefaults.standard.set(tempCnt, forKey: VIDEO_COMPRESSION_COUNT_UD)
    }
    else {
      UserDefaults.standard.set(0, forKey: VIDEO_COMPRESSION_COUNT_UD)
    }
    
    self.videoFlag = true
    
    self.selectedAssetsArray.remove(at: selectedCellIndex.row)
    self.selectedAssetsArray.insert(selectedAsset, at: selectedCellIndex.row)

    self.phassetIndexes.remove(at: self.selectedCellIndex.row)
    self.phassetIndexes.insert(self.selectedCellIndex.row, at: self.selectedCellIndex.row)

    let imgGenerator = self.getAssetThumbnail(selectedAsset, size: 100.0)
    DispatchQueue.main.async(execute: {
      let cell = self.uploadImageCollectionView.cellForItem(at: self.selectedCellIndex) as! ImageTypeQuestionCollectionViewCell
    
      cell.uploadImageView.contentMode = .scaleAspectFill
      cell.uploadImageView.image = imgGenerator //4
      cell.dottedView.layer.borderWidth = 0.5
      cell.dottedView.clipsToBounds = true
      cell.dottedView.layer.borderColor = UIColor.gray.cgColor
      cell.uploadImageView.tag = 1
      self.checkForNextbuttonUserInteraction()
    })
    
    if selectedAsset.mediaType == .video {
        let options = PHVideoRequestOptions()
        options.version = .original
        options.deliveryMode = .mediumQualityFormat
        PHImageManager.default().requestAVAsset(forVideo: selectedAsset, options: options, resultHandler: { (asset, audio, nil) in
            if let urlAsset = asset as? AVURLAsset {
                let localVideoUrl = urlAsset.url
                print(localVideoUrl);
//                let control = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrimmerViewController") as? TrimmerViewController
//                let url = URL(fileURLWithPath: localVideoUrl.path)
////                control?.path = url
//                DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
//                    control?.selectedAsset = asset!
//                    self.present(control!, animated: true, completion: nil)
//                })
                
                
//                let asset2 = AVAsset(URL: localVideoUrl)
//                print(asset2)
                self.assetUrlArray.remove(at: self.selectedCellIndex.row)
                self.assetUrlArray.insert(localVideoUrl.path, at: self.selectedCellIndex.row)
            }
        })
    }
    
  }
  
  func getAssetThumbnail(_ asset: PHAsset, size: CGFloat) -> UIImage {
    let retinaScale = UIScreen.main.scale
    let retinaSquare = CGSize(width: size * retinaScale, height: size * retinaScale)
    let cropSizeLength = min(asset.pixelWidth, asset.pixelHeight)
    let square = CGRect(x: 0, y: 0, width: CGFloat(cropSizeLength), height: CGFloat(cropSizeLength))
    let cropRect = square.applying(CGAffineTransform(scaleX: 1.0/CGFloat(asset.pixelWidth), y: 1.0/CGFloat(asset.pixelHeight)))
    
    let manager = PHImageManager.default()
    let options = PHImageRequestOptions()
    var thumbnail = UIImage()
    
    options.isSynchronous = true
    options.deliveryMode = .highQualityFormat
    options.resizeMode = .exact
    options.normalizedCropRect = cropRect
    
    manager.requestImage(for: asset, targetSize: retinaSquare, contentMode: .aspectFit, options: options, resultHandler: {(result, info)->Void in
      thumbnail = result!
    })
    return thumbnail
  }
  
}
