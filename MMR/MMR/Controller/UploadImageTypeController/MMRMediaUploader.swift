//
//  MMRMediaUploader.swift
//  MMR
//
//  Created by Mindbowser on 4/12/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit
import AWSCore
import AWSS3

protocol MMRMediaUploaderDelegate: class {
  func didCompleteImageUpload(_ status:Bool)
}

class MMRMediaUploader: NSObject, AWSS3UploadManagerDelegate{
  
  class var sharedInstance :MMRMediaUploader {
    struct Singleton {
      static let instance = MMRMediaUploader()
    }
    return Singleton.instance
  }
  
  weak var delegate:MMRMediaUploaderDelegate?

  
  let aws = AWSUploadManager()
  func startUploadProcess(_ mediaType : String) -> Bool {
    aws.delegate = self
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let resultArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID! as AnyObject, type: mediaType)
    
    for resultDict in resultArray {
      let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
      let imgName = "\(userID!)/\(timestamp)"
      let type = resultDict[quest_type] as! String
      if type == "videoChoice" {
        aws.ext = "mov"
        aws.contentType = "video"
        aws.relatedSurveyId = resultDict.value(forKey: surveyId) as AnyObject
      }
      else {
        aws.ext = "png"
        aws.contentType = "image"
        aws.relatedSurveyId = resultDict.value(forKey: surveyId) as AnyObject
      }
        print(resultDict)
      aws.uploadMedia(resultDict, imageName: imgName,resumeFlag: false,uploadRequest1: "" as AnyObject)
    }
    if resultArray.count > 0 {
      return true
    }
    else {
      return false
    }
  }
  
    
  func didCompleteImageUpload(_ imageName: String, serialID: String, doneFlag: Bool, contentType: String,relatedSurveyId: AnyObject) {
    //Check whether anything remain or not
    //If remain then wait else create json and hit api
    if imageName == "" {
        let appDel = UIApplication.shared.delegate as? AppDelegate
        if appDel?.netWorkStatus == true {
        self.startUploadProcess("")
        }
    } else {
    let userID1 = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let resultArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID1! as AnyObject, type: "")
    if resultArray.count > 0 {
      //wait do nothing
//        aws.pauseAllUploads()
//        aws.resumeAllUploads()
    }
    else {
        let appDel = UIApplication.shared.delegate as? AppDelegate
        let rootControl = appDel?.window?.rootViewController
        if rootControl!.isKind(of: UINavigationController.self){
            if (rootControl as? UINavigationController)?.viewControllers.last?.isKind(of: MMRSubmitSurvey.self) == true {
                appDel?.uploader.delegate = nil
                ((rootControl as? UINavigationController)?.viewControllers.last as? MMRSubmitSurvey)?.uploader.delegate = (rootControl as? UINavigationController)?.viewControllers.last as? MMRSubmitSurvey
            } else {
                appDel?.s3UploadRequestNameArray = [String]()
                appDel?.s3UploadRequestProgressDict = [String : Double]()
                appDel?.s3UploadProgress = 0
            }
        } else {
            appDel?.s3UploadRequestNameArray = [String]()
            appDel?.s3UploadRequestProgressDict = [String : Double]()
            appDel?.s3UploadProgress = 0
        }
        delegate?.didCompleteImageUpload(true)
    }
    }
  }
  
  func cancelAll() {
    let CognitoRegionType = AWSRegionType.usWest2
    let DefaultServiceRegionType = AWSRegionType.usWest2
    let CognitoIdentityPoolId = COGNITO_IDENTITY_POOL_ID
    
    let credentialsProvider = AWSCognitoCredentialsProvider(regionType: CognitoRegionType, identityPoolId: CognitoIdentityPoolId)
    let configuration = AWSServiceConfiguration(region: DefaultServiceRegionType, credentialsProvider: credentialsProvider)
    AWSServiceManager.default().defaultServiceConfiguration = configuration
    AWSS3TransferManager.default().pauseAll()
    UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
    UserDefaults.standard.synchronize()
  }
    
    func trackOfProgress() {
        let appDel = UIApplication.shared.delegate as? AppDelegate
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        appDel?.totalNotUploadedRecords = UploadRecordSharedClass.sharedInstance.getCountOfNotUploadedRecords(userID! as AnyObject, type: "")
        var totalPercent = 0.0
        for obj in (appDel?.s3UploadRequestNameArray)! {
            let singleReqProgress = appDel?.s3UploadRequestProgressDict[obj]
            totalPercent = totalPercent + singleReqProgress!
        }
        let pro = totalPercent / Double((appDel?.totalNotUploadedRecords)!)
        print("S3 progress : \(pro)")
        appDel?.s3UploadProgress = pro
    }
}
