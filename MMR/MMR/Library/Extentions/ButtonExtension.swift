//
//  ButtonExtension.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
extension UIButton {
    
    func roundbutton (_ button : UIButton, cornerradius : CGFloat){
       
        button.layer.cornerRadius = cornerradius
        button.clipsToBounds      = true
    }
    
}
