//
//  NSDateExtension.swift
//  AWSS3Demo
//
//  Created by Mindbowser on 11/18/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

extension Date {
  struct Formatter {
    static let iso8601: DateFormatter = {
      let formatter = DateFormatter()
      formatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
//      formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
//      formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
      formatter.timeZone = TimeZone.autoupdatingCurrent
      formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
      return formatter
    }()
  }
  var iso8601: String { return Formatter.iso8601.string(from: self) }
  
  func seconds(from date: Date, to date2: Date) -> Int {
    return (Calendar.current as NSCalendar).components([.second], from: date2, to: date, options: NSCalendar.Options.matchStrictly).second ?? 0
//    return NSCalendar.current.dateComponents([.second], from: date, to: self).second ?? 0
  }
  
}

extension String {
  var dateFromISO8601: Date? {
    return Date.Formatter.iso8601.date(from: self)
  }
}
