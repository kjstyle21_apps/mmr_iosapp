//
//  String Entension.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import Foundation
import UIKit
extension String {

func isValidEmail() -> Bool {
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPredicate.evaluate(with: self)
}
 
func isValidEmailFormat() -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
  }
//MARK:- password encryption
func md5(string: String) -> String {
    var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
    if let data = string.data(using: String.Encoding.utf8) {
        CC_MD5((data as NSData).bytes, CC_LONG(data.count), &digest)
    }
    
    var digestHex = ""
    for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
        digestHex += String(format: "%02x", digest[index])
    }
    
    return digestHex
}

func validateStringForEmpty( _ string : String) -> Bool
{
    if string.isEmpty == false
    {
        let trimmedString = string.trimmingCharacters(in: CharacterSet.whitespaces)
        if trimmedString.isEmpty == false
        {
            return true
        }
    }
    return false
}
    
        
    func getDataBasePath() -> String {
        let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as AnyObject
//        let destinationPath:NSString = documentsPath.appending("/MMR.sqlite")
        let destinationPath = (documentsPath as! String) + "/MMR.sqlite"

        return destinationPath as String
    }
  
    func attributedStringFromHTML(_ completionBlock:@escaping (NSAttributedString?) ->()) {
//        guard let data = data(using: String.Encoding.utf8, allowLossyConversion: true) else {
//            return completionBlock(nil)
//
//        }
        
//      guard let data = data(using: String.Encoding.utf8, allowLossyConversion: true) else {
//        print("Unable to decode data from html string: \(self)")
//        return completionBlock(nil)
//      }
      
//      let options = [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType]

      
//      DispatchQueue.main.async {
//        
////        if let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) {
////          print("Attributed String : \(attributedString)")
////          completionBlock(attributedString)
////        } else {
////          print("Unable to create attributed string from html string: \(self)")
////          completionBlock(nil)
////        }
//        
//        var htmlToAttributedString: NSAttributedString? {
//            let options = [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:String.Encoding.utf8] as [String : Any]
//            
//            guard let data = self.data(using: .utf8, allowLossyConversion: true) else { return NSAttributedString() }
//            do {
//                return try NSAttributedString(data: data, options: options, documentAttributes: nil)
//            } catch {
//                return NSAttributedString()
//            }
//        }
//        completionBlock(htmlToAttributedString)
//
//      }
        
        
        guard let data = data(using: String.Encoding.utf8, allowLossyConversion: false) else {
            print("Unable to decode data from html string: \(self)")
            return completionBlock(nil)
        }
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html]
        DispatchQueue.main.async {
            if let attributedString = try? NSAttributedString(data: data, options:
                options, documentAttributes: nil) {
                completionBlock(attributedString)
            } else {
                print("Unable to create attributed string from html string: \(self)")
                completionBlock(nil)
            }
        }
    }
  
}
