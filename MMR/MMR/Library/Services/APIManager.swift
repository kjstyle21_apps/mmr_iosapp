//
//  APIManager.swift
//  Coursic
//
//  Created by Mindbowser on 6/23/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class APIManager: NSObject {
  
  
    
    func serverLoginUser(_ userInfo: [String:AnyObject]) -> AnyObject? {
        let responseInfo: AnyObject? = CRServiceFetcher().loginUser(userInfo)
        let isSuccess = verifyAndSaveSuccessResponse(responseInfo)
        if isSuccess == true
        {
            return responseInfo!
        }
        return nil
       // return verifyAndSaveSuccessResponse(responseInfo)
    }
  
    func contactUs(_ userInfo: [String:AnyObject]) -> Bool {
        let responseInfo: AnyObject? = CRServiceFetcher().contactUs(userInfo)
        return verifyAndSaveSuccessResponse(responseInfo)
    }

    
    func changePassword(_ userInfo: [String:AnyObject]) -> Bool {
        let responseInfo: AnyObject? = CRServiceFetcher().changePasswordOfUser(userInfo)
         return verifyAndSaveSuccessResponse(responseInfo)
    }
  
  func submitSurvey(_ userInfo: [NSMutableDictionary], surveyStatus : Bool) -> Bool {
    let responseInfo: AnyObject? = CRServiceFetcher().submitSurvey((userInfo as? [NSMutableDictionary])!, surveyStatus : surveyStatus)
      return verifyAndSaveSuccessResponse(responseInfo)
  }
  
  func submitBLEData(_ userInfo: [NSMutableDictionary]) -> Bool {
    let responseInfo: AnyObject? = CRServiceFetcher().submitBLEData(userInfo)
    return verifyAndSaveSuccessResponse(responseInfo)
  }

  
    func logoutUser() -> Bool {
        if let responseInfo: AnyObject? = CRServiceFetcher().logoutFromTheApp() {
            if responseInfo == nil {
                return false
            }
            return verifyAndSaveSuccessResponse(responseInfo)

        }
        return false
    }
  
  func getUserSurvey() -> AnyObject? {
    var deviceToken = UserDefaults.standard.object(forKey: MMRServiceRegisterRequestParameterDeviceToken)
    if deviceToken == nil{
      deviceToken = ""
    }
    
    let dict: NSMutableDictionary = [MMRDeviceType : MMRDeviceTypeiOS as AnyObject,
                                                MMRDeviceToken : deviceToken as! String as AnyObject]
    let responseInfo: AnyObject? = CRServiceFetcher().getUserSurvey((dict as? NSMutableDictionary)!)
    
    let isSuccess = verifySuccessResponse(responseInfo)
    if isSuccess == true
    {
      return responseInfo!
    }
    return nil
  }
  
  func validateUser() -> Bool {
    if let responseInfo : AnyObject? = CRServiceFetcher().validateUserOfApp() {
      if responseInfo == nil {
          return false
        }
      return verifyAndSaveSuccessResponse(responseInfo)
    }
    return false
  }
  
  func getFaqs() -> AnyObject {
    let responseInfo : AnyObject? = CRServiceFetcher().getfaqs()
    let result = verifyAndSaveSuccessResponse(responseInfo)
    
    if result == true{
        if let resInfo = responseInfo{
            return resInfo
        }else{
            return "FAILED" as AnyObject
        }
    }
    else
    {
      return "FAILED" as AnyObject
    }
  }
  
       func serverForgotPassword(_ userInfo: [String:AnyObject]) -> Bool {
        let responseInfo: AnyObject? = CRServiceFetcher().forgotPassword(userInfo)
        let success = verifySuccessResponse(responseInfo)
        if success {
            if let info = responseInfo as? [String:AnyObject] {
                let message = JSONString(info[MMRServiceResponseParameterMessage])
                if let message = message {
                    AlertModel.sharedInstance.showMessage("", message: message)
                }
            }
        }
        return success
    }

    func verifyAndSaveSuccessResponse(_ responseInfo:AnyObject?) -> Bool {
        if let info = responseInfo as? [String:AnyObject] {
            
            let status = info ["status"] as! String
            if status == "FAIL" {

            if let errorInfo:[String:AnyObject] = info[MMRServiceResponseObjectError] as? [String:AnyObject] {
                if let message = JSONString(errorInfo[MMRServiceResponseParameterMessage]) {
                if message == invalidUserTokenMessageKeyword {
                    showLoginScreen()
                } else {
                    AlertModel.sharedInstance.showErrorMessage(message)
                }
                }
                return false
            }
            }else {
                // Save user's object
                if let userInfo = JSONDictionary(info[MMRServiceResponseObjectUserInfo]) {
//                    let sharedAppUser = SMAppUser.sharedInstance
//                    sharedAppUser.name = JSONString(userInfo[kCRServiceRegisterRequestParameterName])!
//                    sharedAppUser.emailAddress = JSONString(userInfo[kCRServiceRegisterRequestParameterEmailId])!
//                    sharedAppUser.gender = JSONString(userInfo[kCRServiceRegisterRequestParameterGender])!
//                    sharedAppUser.save()
                    
                    // Save auth token in NSUserDefaults
                    let authToken = JSONString(userInfo[MMRServiceUserInfoAuthToken])
                    UserDefaults.standard.set(authToken, forKey: MMRServiceUserInfoAuthToken)
                    UserDefaults.standard.synchronize()
                }
                
                return true
            }
            
        }
//        else {
//            if responseInfo?.statusCode == 304 {
//                AlertModel.sharedInstance.showErrorMessage("There are no changes done while editing")
//            }else{
//                AlertModel.sharedInstance.showErrorMessage("Something went wrong. Please try again.")}        }
        return false
    }
    
    func verifyAndSaveSuccessResponseAfterSignUp(_ responseInfo:AnyObject?) -> Bool {
        if let info = responseInfo as? [String:AnyObject] {
            if let errorInfo:[String:AnyObject] = info[MMRServiceResponseObjectError] as? [String:AnyObject] {
                let message = JSONString(errorInfo[MMRServiceResponseParameterMessage])
                if message != nil {
                   // AlertModel.sharedInstance.showErrorMessage(message!)
                    if message == invalidUserTokenMessageKeyword {
                        showLoginScreen()
                    }
                }
//                AlertModel.sharedInstance.showErrorMessage("Select opening and closing time at least for one day.")
                return false
            } else {
                // Save user's object
                if let userInfo = JSONDictionary(info[MMRServiceResponseObjectUserInfo]) {
                    //                    let sharedAppUser = SMAppUser.sharedInstance
                    //                    sharedAppUser.name = JSONString(userInfo[kCRServiceRegisterRequestParameterName])!
                    //                    sharedAppUser.emailAddress = JSONString(userInfo[kCRServiceRegisterRequestParameterEmailId])!
                    //                    sharedAppUser.gender = JSONString(userInfo[kCRServiceRegisterRequestParameterGender])!
                    //                    sharedAppUser.save()
                    
                    // Save auth token in NSUserDefaults
//                    let authToken = JSONString(userInfo[kCRUserSessionModel]![MMRServiceUserInfoAuthToken])
//                    NSUserDefaults.standardUserDefaults().setObject(authToken, forKey: MMRServiceUserInfoAuthToken)
                    UserDefaults.standard.synchronize()
                }
                
                return true
            }
        } else {
            if responseInfo?.statusCode == 304 {
//                AlertModel.sharedInstance.showErrorMessage("There are no changes done while editing")
            }else{
                AlertModel.sharedInstance.showErrorMessage(somethingWentWrongMessageKeyword)}
        }
        return false
    }

    func showLoginScreen() {
        
        DispatchQueue.main.async {
            
            // Clear authToken
            UserDefaults.standard.set(nil, forKey: MMRServiceUserInfoAuthToken)
            UserDefaults.standard.synchronize()
            
          //clear options from answer table
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//          AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID!,surveyID: "")
          if let tempNoSurveyFlag = UserDefaults.standard.value(forKey: NoSurveyForUser) as? Bool {
            if !tempNoSurveyFlag {
              UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_UUID_UD)
              UserDefaults.standard.removeObject(forKey: CURRENT_SENSOR_NAME_UD)
              MMRBLEModule.sharedInstance.disconnectMMRDevice()
              (UIApplication.shared.delegate as! AppDelegate).stopBeacon()
            }
          }
            // Pop to login screen
            var loginViewController: UIViewController?
            
            let sharedApplication = UIApplication.shared.delegate as! AppDelegate
            let rootViewController = sharedApplication.window?.rootViewController as! UINavigationController
            let viewControllers = rootViewController.viewControllers
            
            for viewController in viewControllers {
                if viewController.isKind(of: MMRLoginViewController.self) {
                    loginViewController = viewController
                    break
                }
            }
          
//            if let loginViewController = loginViewController {
//                rootViewController.popToViewController(loginViewController, animated: true)
//            } else {
//                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//                let signUpSignInViewController: UIViewController! = mainStoryboard.instantiateViewControllerWithIdentifier("MMRLoginViewController")
//                rootViewController.setViewControllers([signUpSignInViewController], animated: true)
//            }
//            
//            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: MMRServiceUserInfoAuthToken)
            
//            var loginViewController: UIViewController?
//            
//            if let viewControllers = self.navigationController?.viewControllers {
//                for viewController in viewControllers {
//                    if viewController.isKindOfClass(StartOfTourViewController) {
//                        
//                        (viewController as! StartOfTourViewController).isFromHome = true
//                        loginViewController = viewController
//                        
//                        break
//                    }
//                }
//            }
//            
            if let loginViewController = loginViewController {
                
                (loginViewController as! StartOfTourViewController).isFromHome = true
                
                rootViewController.popToViewController(loginViewController, animated: true)
            } else {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let mmrloginViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "StartOfTourViewController")
                (mmrloginViewController as! StartOfTourViewController).isFromHome = true
                rootViewController.setViewControllers([mmrloginViewController], animated: true)
            }
        }
    }
  func verifySuccessResponse(_ responseInfo:AnyObject?) -> Bool {
    if let info = responseInfo as? [String:AnyObject] {
      if let errorInfo:[String:AnyObject] = info[MMRServiceResponseObjectError] as? [String:AnyObject] {
        let message = JSONString(errorInfo[MMRServiceResponseParameterMessage])!
        if message == invalidUserTokenMessageKeyword {
           showLoginScreen()
        } else if message != userNotRegisterToMMRMessageKeyword {
          AlertModel.sharedInstance.showErrorMessage(message)
        }
        return false
      }  else {
        return true
      }
    } else {
      if responseInfo?.statusCode == 304 {
//        AlertModel.sharedInstance.showErrorMessage("There are no changes done while editing")
      }else{
//        AlertModel.sharedInstance.showErrorMessage(somethingWentWrongMessageKeyword)
      }
      }
    return false
  }
  
  
    // Eliminate JSON Type Checking using these functions
    func JSONString(_ object: AnyObject?) -> String? {
        return object as? String
    }
    
    func JSONInt(_ object: AnyObject?) -> Int? {
        return object as? Int
    }
    
    func JSONDictionary(_ object: AnyObject?) -> [String:AnyObject]? {
        return object as? [String:AnyObject]
    }
    
    func JSONArray(_ object: AnyObject?) -> NSArray? {
        return object as? NSArray
    }
    
    func JSONBool(_ object: AnyObject?) -> Bool? {
        return object as? Bool
    }
    
    }
