//
//  CRServiceFetcher.swift
//  Coursic
//
//  Created by Mindbowser on 6/23/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class CRServiceFetcher: NSObject {
    let kHTTPMethodGet      = "GET"
    let kHTTPMethodPost     = "POST"
    let kHTTPMethodPut      = "PUT"
    let kHTTPMethodDelete   = "DELETE"

    
       
       func loginUser(_ userInfo: [String:AnyObject]) -> AnyObject? {
        return executeService(MMRServiceLogin, requestInfo: userInfo as AnyObject, httpMethod: kHTTPMethodPost)
    }
    
  
    
    func forgotPassword(_ userInfo: [String:AnyObject]) -> AnyObject? {
        return executeService(MMRServiceForgotPassword, requestInfo: userInfo as AnyObject, httpMethod: kHTTPMethodGet)
    }
    
    func changePasswordOfUser(_ userInfo : [String:AnyObject]) -> AnyObject? {
        return executeService(MMRServiceChangeUserPasswd, requestInfo: userInfo as AnyObject, httpMethod: kHTTPMethodPut)
    }
  
    func contactUs(_ userInfo : [String:AnyObject]) -> AnyObject? {
        return executeService(MMRServiceContactUs, requestInfo: userInfo as AnyObject, httpMethod: kHTTPMethodPost)
    }
  
  func submitSurvey(_ userInfo : [NSMutableDictionary], surveyStatus : Bool) -> AnyObject? {
    let requestData = ["User_Info" : userInfo, "Survey_Status" : surveyStatus] as [String : Any]
    return executeService(MMRServiceSubmitSurvey, requestInfo: requestData as AnyObject, httpMethod: kHTTPMethodPost)
  }
  
  func submitBLEData(_ userInfo : [NSMutableDictionary]) -> AnyObject? {
    //    let requestData = ["User_Info" : userInfo, "Survey_Status" : surveyStatus]
    return executeService(MMRServiceSubmitBLEData, requestInfo: userInfo as AnyObject, httpMethod: kHTTPMethodPost)
  }

  
    func logoutFromTheApp () ->AnyObject? {
//      var authToken = NSUserDefaults.standardUserDefaults().objectForKey(MMRServiceUserInfoAuthToken)
//      if authToken == nil{
//        authToken = ""
//      }
//      let dict: [String:AnyObject] = [MMRServiceUserInfoAuthToken : authToken!]
        return executeService(MMRServiceLogout, requestInfo:nil, httpMethod: kHTTPMethodGet)
    }
  
  func getUserSurvey(_ userInfo : NSMutableDictionary) -> AnyObject? {
    return executeService(MMRServiceGetSurvey, requestInfo: userInfo, httpMethod: kHTTPMethodPost)
  }
  
  func validateUserOfApp () ->AnyObject? {
    return executeService(MMRServiceValidate, requestInfo:nil, httpMethod: kHTTPMethodGet)
  }

  func getfaqs () ->AnyObject {
    return executeService(MMRServiceFAQs, requestInfo:nil, httpMethod: kHTTPMethodGet)!
  }


        fileprivate func executeService(_ servicePath: String, requestInfo: AnyObject?, httpMethod: String) -> AnyObject? {
            var finalRequestInfo : AnyObject?
            var surveyStatus = Bool()
          if servicePath == MMRServiceSubmitSurvey {
            finalRequestInfo = (requestInfo as! NSDictionary).object(forKey: "User_Info") as AnyObject
            surveyStatus = (requestInfo as! NSDictionary).object(forKey: "Survey_Status") as! Bool
          }
          else {
            finalRequestInfo = requestInfo
          }
          var urlAsString : String?
          
            if httpMethod == kHTTPMethodDelete {
                

            } else if httpMethod == kHTTPMethodGet{
              if servicePath == MMRServiceForgotPassword{
              let dict = requestInfo as! NSDictionary
              let email = dict[MMRUserEmail] as! String
              urlAsString  = MMRServiceStagingURL + servicePath + "/" + email
              }
              else{
                urlAsString  = MMRServiceStagingURL + servicePath
              }
            }
            else {
              if servicePath == MMRServiceSubmitSurvey {
                if surveyStatus {
//                  urlAsString  = MMRServiceStagingURL + servicePath + "?surveyComplete=true"
                  urlAsString  = MMRServiceStagingURL + servicePath
                }
                else {
//                  urlAsString  = MMRServiceStagingURL + servicePath + "?surveyComplete=false"
                  urlAsString  = MMRServiceStagingURL + servicePath
                }
              }
              else {
                urlAsString  = MMRServiceStagingURL + servicePath
              }
            }
        print("Service Path : \(urlAsString)")
        let url: URL = URL(string: urlAsString!)!
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest()
        urlRequest.url = url
        urlRequest.httpMethod = httpMethod
        //urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("en", forHTTPHeaderField: "Accept-Language")
        urlRequest.timeoutInterval = 30
        
        if servicePath != MMRServiceLogin || servicePath != MMRServiceSaveUserDetails || servicePath != MMRServiceForgotPassword {
            let authToken: String? = UserDefaults.standard.object(forKey: MMRServiceUserInfoAuthToken) as? String
            if let authToken = authToken {
                print("authToken \(authToken)")
                urlRequest.setValue(authToken, forHTTPHeaderField: "authToken")
            } else {
                print("No authToken found")
            }
        }
        
       
        
        var error: NSError?
        if urlRequest.httpMethod == kHTTPMethodPost || urlRequest.httpMethod == kHTTPMethodPut || urlRequest.httpMethod == kHTTPMethodDelete  {

         
                print(urlRequest.httpMethod)
                var requestData: Data!
            
                do {

                    if finalRequestInfo != nil {
                        
                        requestData = try JSONSerialization.data(withJSONObject: finalRequestInfo!, options: JSONSerialization.WritingOptions.prettyPrinted) as Data!
                        let requestDataAsJSON = NSString(data: requestData!,
                                                         encoding: String.Encoding.ascii.rawValue)
                        print("requestBody \(requestDataAsJSON)")
                        urlRequest.httpBody = requestData
                    }
                    
                    
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
//            }
       
      
        }
        
        var response: URLResponse?
        var responseInfo: AnyObject?
        var httpResponse: HTTPURLResponse?
        var reqProceed : Bool = false
           

     let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return nil
        }
        
        if reachability.isReachable() {
            var rd: Data?
//            do {
//                rd = try NSURLConnection.sendSynchronousRequest(urlRequest, returningResponse: &response)
//                print("response: ",response);
//                httpResponse = response as? NSHTTPURLResponse
//                if  (httpResponse != nil) {
//                    print("Error",httpResponse!.statusCode)
//                }
//            } catch let error1 as NSError {
//                error = error1
//     
//                rd = nil
//            }
            //for asyncronous request …
            NSURLConnection.sendAsynchronousRequest(urlRequest as URLRequest, queue: OperationQueue.main, completionHandler: { (response1, data, error1) in
                print("response",response1)
                print("error1",error1)
                print(error?.code)
                response = response1
                rd = data
                if error1 != nil{
                    error = error1 as! NSError
                    print("error",error)
                }else{
                }
                httpResponse = response as? HTTPURLResponse
                print(httpResponse?.statusCode)
                reqProceed = true
            })
//            NSURLConnection.sendAsynchronousRequest(urlRequest as URLRequest, queue: OperationQueue.main) { (response1, data: Data?, error1: NSError?) in
//                
//            }
            while reqProceed == false {
                Thread.sleep(forTimeInterval: 0)
            }
          
            if let responseData = rd {
    
                let datastring = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue)
                print("Response From Server : \(datastring)")
                
                
                responseInfo = try? JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject

                if let responseInfo: AnyObject = responseInfo {
                    print("jsonObject: \(responseInfo)")
                    
                    if responseInfo["status"] as? String == "FAIL"
                    {
                        let massageInfo : AnyObject? = (responseInfo as? NSMutableDictionary)?.value(forKey: "error") as AnyObject
                        if let errorMessage = massageInfo?["message"] as? String
                        {
                            //                            if errorMessage == kInvalidUser {
                            //                                NSUserDefaults.standardUserDefaults().setBool(true, forKey: kSMServiceUserInvalid)
                            //                                NSUserDefaults.standardUserDefaults().synchronize()
                            //                                self.clearData()
                            //                            }
                            print(errorMessage)
                            return responseInfo
                        }
                        else
                        {
                            if httpResponse?.statusCode == 304
                            {
                                return httpResponse
                            }
                            else if httpResponse?.statusCode == 417
                            {
                                return httpResponse
                            }
                            return responseInfo
                        }
                        
                    }
                    else
                    {
                        if httpResponse?.statusCode == 304
                        {
                            return httpResponse
                        }
                        if error != nil{
                            print(error)
                            
                            if error?.code == -1012{
                              if servicePath == MMRServiceLogout {
                                UserDefaults.standard.set(true, forKey: MMRServiceLogout_UD)
                                UserDefaults.standard.synchronize()
                              }
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.isanAuthUser = true
                              
                                self.showLoginScreen()
                                return nil
                                
                            }
                        }
                        return responseInfo
                    }
                }
            } else {
                if  (httpResponse != nil) {
                    print(httpResponse!.statusCode)
                    if httpResponse?.statusCode == 304 {
                        return httpResponse
                    }
                    return responseInfo
                }else{
                    
                    if error != nil{
                        print(error)
                        
                        if error?.code == -1012{
                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.isanAuthUser = true
                          
                            self.showLoginScreen()
                            return nil
                            
                        }
                    } else {
                print("Something went wrong. Request could not be sent to server.")
                print(error)
                let errorMessageInfo = [MMRServiceResponseParameterMessage:"Something went wrong. Request could not be sent to server."]
                
                let responseInfo:[String:AnyObject] = [MMRServiceResponseObjectError:errorMessageInfo as AnyObject,MMRServiceResponseParameterStatus:MMRServiceResponseParameterStatusFail as AnyObject]
                return responseInfo as AnyObject
                    }
                }
            }
        } else  {
            let errorMessageInfo = [MMRServiceResponseParameterMessage:kCRMessageNoInternetConnection.localized()]
            let responseInfo:[String:AnyObject] = [MMRServiceResponseObjectError:errorMessageInfo as AnyObject,MMRServiceResponseParameterStatus:MMRServiceResponseParameterStatusFail as AnyObject]
            return responseInfo as AnyObject
        }
            if httpResponse?.statusCode == 304 {
                return httpResponse
            }else{
                return nil
            }
    }
    
    func showLoginScreen() {
        
        DispatchQueue.main.async {
            
            // Clear authToken
            UserDefaults.standard.set(nil, forKey: MMRServiceUserInfoAuthToken)
            UserDefaults.standard.synchronize()
          
           //clear options from answer table
          MMRMediaUploader.sharedInstance.cancelAll()
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
          
          if let flagScreen = UserDefaults.standard.value(forKey: IS_ON_SUBMIT_SCREEN_UD) as? Bool  {
            UserDefaults.standard.removeObject(forKey: IS_ON_SUBMIT_SCREEN_UD)
            UserDefaults.standard.set(true, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID!)")
            UserDefaults.standard.synchronize()
          }

//          AnswerSharedClass.sharedInstance.deleteAnswerDataOfUploadedSurvey(userID!)
//          (UIApplication.sharedApplication().delegate as! AppDelegate).stopBeacon()
//          UIApplication.sharedApplication().cancelAllLocalNotifications()

            // Pop to login screen
            var loginViewController: UIViewController?
            
            let sharedApplication = UIApplication.shared.delegate as! AppDelegate
            let rootViewController = sharedApplication.window?.rootViewController as! UINavigationController
            let viewControllers = rootViewController.viewControllers
            
            for viewController in viewControllers {
                if viewController.isKind(of: MMRLoginViewController.self) {
                    loginViewController = viewController
                    break
                }
            }
           
            
            if let loginViewController = loginViewController {
                
                (loginViewController as! StartOfTourViewController).isFromHome = true
                
                rootViewController.popToViewController(loginViewController, animated: false)
            } else {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let mmrloginViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "StartOfTourViewController")
                (mmrloginViewController as! StartOfTourViewController).isFromHome = true
                rootViewController.setViewControllers([mmrloginViewController], animated: false)
            }
           
        }
    }


}
