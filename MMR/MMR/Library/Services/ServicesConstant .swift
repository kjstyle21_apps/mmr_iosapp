 //
//  ServicesConstant .swift
//  Coursic
//
//  Created by Mindbowser on 6/23/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import Foundation
import UIKit


     // Use this while uploading build for client
//let MMRServiceStagingURL        = "http://52.43.72.183:8080/MMR/api"
 
    // Use this while developing
// let MMRServiceStagingURL        = "http://35.160.255.165:8080/MMR/api"
 
 let MMRServiceStagingURL        = "http://52.26.200.249/MMR/api" //Client-TestFlight
// let MMRServiceStagingURL       = "http://52.43.72.183:8080/MMR/api" //Development
 
// Userr
let MMRServiceSignUp            = "/user/signUp"
let MMRServiceLogin             = "/user/logIn"
let MMRServiceEditProfile       = "/user/editProfile"
let MMRServiceForgotPassword    = "/user/forgot-password"
let MMRServiceLogout            = "/user/log-out"
let MMRServiceGetSurvey         = "/user/survey"
let MMRServiceValidate          = "/user/validate"
let MMRServiceSaveUserDetails   = "/user"
let MMRServiceChangeUserPasswd  = "/user/changePassword"
let MMRServiceContactUs         = "/user/query"
let MMRServiceFAQs              = "/faq/getFAQ"
let MMRServiceSubmitSurvey      = "/user/submitSurvey"
 let MMRServiceSubmitBLEData    = "/user/ble-data"

 //MARK: CR Service Response Parameters

let MMRServiceUserInfoAuthToken         = "authToken"
let MMRServiceUserMacID                 = "macId"
let MMRServiceResponseObjectError       = "error"
let MMRServiceResponseParameterMessage  = "message"
let MMRServiceResponseParameterStatus   = "status"
let MMRServiceResponseObjectUserInfo    = "object"
let MMRServiceResponseDeviceList        = "deviceList"


let MMRServiceResponseParameterStatusFail    = "FAIL"
let MMRServiceResponseParameterStatusSuccess = "SUCCESS"



//Request parameters
let MMRServiceRegisterRequestParameterEmailId   = "email"
let MMRServiceRegisterRequestParameterPassword  = "password"
let MMRServiceRegisterRequestParameterConfirmPassword = "confirmPassword"
let MMRServiceRegisterRequestParameterName      = "name"
let MMRServiceRegisterRequestParameterGender    = "gender"
let MMRServiceUser                              = "user"
//Response parameters
let MMRServiceRegisterResponseParameterUserInfo      = "object"
let MMRServiceRegisterResponseParameterUserInfoToken = "token"
let MMRRServerObject                             = "object"
let MMRFaqsQuestion = "question"
let MMRFaqsAnswer = "answer"
 
 // User object
let MMRUserFirstname            = "firstName"
let MMRUserLastname             = "lastName"
let MMRUserGender               = "gender"
let MMRUserEmail                = "email"
let MMRUserTestEmail            = "testemail"
let MMRUserRole                 = "role"
let MMRUserPassword             = "password"
let MMRDeviceType               = "deviceType"
let MMRDeviceToken              = "deviceToken"
let MMRDeviceTypeiOS            = "iOS"
let MMRQRCode                   = "qrCode"
let MMROldPassword              = "oldPassword"
let MMRNewPassword              = "newPassword"
let MMRUserId                   = "userId"
let MMRRegardingType            = "regardingType"
let MMRPhoneType                = "phoneType"
let MMRUserQuery                = "userQuery"

 
 // Constants used for keys
// let userId                  = "userId"
 let surveyId                = "surveyId"
 let isSurveySubmittedLocally = "isSurveySubmittedLocally"
 let surveySerialNo          = "surveySerialNo"
 let emailId                 = "emailId"
 let surveyName              = "surveyName"
 let surveyEndTime           = "endTime"
 let surveyStartTime         = "startTime"
 let surveyIntroductoryQuestion = "introductoryQuestion"
 let assignedSurvey          = "assignedSurvey"
 let surveyIntervalData      = "surveyIntervalData"
 let userSurveyIntervalTime  = "userSurveyIntervalTime"
 let noSurveyMessage         = "noSurveyMessage"
 let NoSurveyForUser         = "NoSurveyForUser"
 let questions               = "questions"
 let serialNoOfQuestion      = "serialNo"
 let userId = "userId"
 let serial_id               = "serial_id"
 let questionId              = "questionId"
 let ques_id                 = "ques_id"
 let questionTypeId          = "questionTypeId"
 let questionType            = "questionType"
 let quest_type              = "quest_type"
 let questionTexts           = "questionTexts"
 let questionText            = "questionText"
 let quest_text              = "quest_text"
 let answerRequired          = "answerRequired"
 let isHintAvailabel         = "isHintAvailabel"
 let hintText                = "hintText"
 let questionInstructions    = "questionInstruction"
 let label                   = "label"
 let isLabelAvailable        = "isLabelAvailable"
 let labelText               = "labelText"
 let answerStartTime         = "start_time"
 let answerEndTime           = "end_time"
 let dropEndTime           = "drop_time"
 let isAnswered              = "isAnswered"
 let answer_text             = "answer_text"
 let optionText              = "optionText"
 let optionId              = "optionID"
 let settings                = "settings"
 let frequency               = "frequency"
 let minRange                = "minRange"
 let maxRange                = "maxRange"
 let maxCharacters           = "maxCharacters"
 let serialNoID              = "serialNo_ID"
 let surveyBriefDescription  = "surveyBriefDescription"
 let surveyInstruction       = "surveyInstruction"
 let routingID  = "routingId"
 let selectedOptions = "selectedOptions"
 let actionName = "actionName"
 let nextQuestionID = "nextSerialNo"
 let sensorName = "sensorName"
 let multipleTimeSubmit = "multipleTimeSubmit"
 let localPushTime = "localPushTime"
 let maxPushNo = "maxPushNo"
 let pushInterval = "pushInterval"
 let pushNotificationText = "pushNotificationText"
 let uuid = "uuid"
 let questionIntervalTime = "questionIntervalTime"
 let orginalSerialNo = "orginalSerialNo"
 let textOptionId = "textOptionId"
 
 let bleWeight = "weight"
 let bleUpdatedDate = "updatedDate"
 let bleCreatedDate = "createdDate"
 let bleBatteryLevel = "batteryLevel"
 let surveyLocalPush = "surveyLocalPush"
 let pushTime = "pushTime"
 let IsSubmittedFlag = "IsSubmittedFlag"

 let currentUserId = "CURRENT_USER_ID"
 
 //Local Push Code
 
 let INTERVAL = "INTERVAL"
 let PUSH_CODE = "PUSH_CODE"
