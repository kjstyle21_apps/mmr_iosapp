//
//  AWSUploadManager.swift
//  AWSS3Demo
//
//  Created by Mindbowser on 11/17/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
import AWSCore
import AWSS3
import AssetsLibrary

protocol AWSS3UploadManagerDelegate: class {
    func didCompleteImageUpload(_ imageName:String, serialID : String, doneFlag : Bool, contentType : String, relatedSurveyId: AnyObject)
    func trackOfProgress()
}

class AWSUploadManager: NSObject {
  
  weak var delegate:AWSS3UploadManagerDelegate?

  let CognitoRegionType = AWSRegionType.usWest2
  let DefaultServiceRegionType = AWSRegionType.usWest2
  let CognitoIdentityPoolId = COGNITO_IDENTITY_POOL_ID
  let S3BucketName = "mmrstaging"
//  let S3BucketName = "vesica-development"
//  let S3BucketName = "mmrproduction"
  let s3FolderName = "mmrappImages"
  var ext = "png"
  var contentType = String()
    var relatedSurveyId : AnyObject?
  var uploadReqArray = [AWSS3TransferManagerUploadRequest]()
    var uploadDictArray = [NSMutableDictionary]()
  var serialArray = [String]()
    let appDel = UIApplication.shared.delegate as? AppDelegate

  func uploader(_ imageURL : URL, imageName : String, index : String, uploadRequest1 : AWSS3TransferManagerUploadRequest, resumeFlag : Bool)
  {
    
    // Configure AWS Cognito Credentials
    let credentialsProvider = AWSCognitoCredentialsProvider(regionType: CognitoRegionType, identityPoolId: CognitoIdentityPoolId)
    let configuration = AWSServiceConfiguration(region: DefaultServiceRegionType, credentialsProvider: credentialsProvider)
    AWSServiceManager.default().defaultServiceConfiguration = configuration
//    AWSServiceManager.default().defaultServiceConfiguration = configuration
    
    
    // Set up AWS Transfer Manager Request
    let localFileName = imageName // local file name here
    
    let remoteName = localFileName + "." + ext
    
    let fileName = String(format: "\(s3FolderName)/%@", remoteName)
    var uploadRequest : AWSS3TransferManagerUploadRequest!
    if resumeFlag {
      uploadRequest = uploadRequest1
    }
    else {
      uploadRequest = AWSS3TransferManagerUploadRequest()
      uploadRequest?.body = imageURL
      uploadRequest?.key = fileName
      uploadRequest?.bucket = S3BucketName
      uploadRequest?.contentType = "\(self.contentType)/" + ext
      uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
      self.uploadReqArray.append(uploadRequest)
      self.serialArray.append(index)

      let encodedData1 = NSKeyedArchiver.archivedData(withRootObject: self.uploadReqArray)
      let encodedData2 = NSKeyedArchiver.archivedData(withRootObject: self.serialArray)
      UserDefaults.standard.set(encodedData1, forKey: "UPLOAD_REQ_ARRAY")
      UserDefaults.standard.set(encodedData2, forKey: "UPLOAD_SERIAL_ARRAY")
      UserDefaults.standard.synchronize()
    }
       let transferManager = AWSS3TransferManager.default()
    uploadRequest.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
      DispatchQueue.main.async(execute: {
        let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        print("Progress is: \(progress)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOAD PROGRESS"), object: progress * 100)
      })
    }
    
    
    // Perform file upload
    
    
    transferManager?.upload(uploadRequest).continue ({ (task) -> AnyObject! in

//    transferManager?.upload(uploadRequest).continue({ (task:AWSTask) -> Any? in
      
      
      if let error = task.error {
        print("Upload failed with error: (\(error.localizedDescription))")
      }
      
      if let exception = task.exception {
        print("Upload failed with exception (\(exception))")
      }
      
      if task.result != nil {
      
        let s3URL = URL(string: "https://\(self.S3BucketName).s3.amazonaws.com/\(uploadRequest!.key!)")!
        
//        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        do {
        let success = try FileManager.default.removeItem(at: imageURL)
//          if (success as! Bool) {
            print("Item deleted Succesfully")
//          }
        print("Uploaded to:\n\(s3URL)")
        self.delegate?.didCompleteImageUpload("\(s3URL)", serialID: index, doneFlag: true,contentType: self.contentType,relatedSurveyId: self.relatedSurveyId!)
        } catch {
          print("Deletion error : \(error)")
          self.delegate?.didCompleteImageUpload("\(s3URL)", serialID: index, doneFlag: true,contentType: self.contentType,relatedSurveyId: self.relatedSurveyId!)

        }
        // Read uploaded image and display in a view
       }
      else {
        print("Unexpected empty result.")
        self.delegate?.didCompleteImageUpload("", serialID: index, doneFlag: false,contentType: self.contentType,relatedSurveyId: self.relatedSurveyId!)
      }
      return nil
    })
    
  }

    func uploadMedia(_ mediaDict : NSMutableDictionary, imageName : String,resumeFlag: Bool, uploadRequest1 : AnyObject)
  {
    
    // Configure AWS Cognito Credentials
    let SURVEY_ID = self.relatedSurveyId
    let credentialsProvider = AWSCognitoCredentialsProvider(regionType: CognitoRegionType, identityPoolId: CognitoIdentityPoolId)
    let configuration = AWSServiceConfiguration(region: DefaultServiceRegionType, credentialsProvider: credentialsProvider)
    AWSServiceManager.default().defaultServiceConfiguration = configuration
    //    AWSServiceManager.default().defaultServiceConfiguration = configuration
    var uploadRequest : AWSS3TransferManagerUploadRequest!
    let localFileName = imageName // local file name here
    let remoteName = localFileName + "." + ext
    
    let fileName = String(format: "\(s3FolderName)/%@", remoteName)
    
    let ans = mediaDict[answer_text]
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let imageURL = URL(fileURLWithPath: "\(documentsPath)/\(ans!)")

    if resumeFlag {
        uploadRequest = uploadRequest1 as? AWSS3TransferManagerUploadRequest
    } else {
        
    // Set up AWS Transfer Manager Request
    
      uploadRequest = AWSS3TransferManagerUploadRequest()
      uploadRequest?.body = imageURL
      uploadRequest?.key = fileName
      uploadRequest?.bucket = S3BucketName
      uploadRequest?.contentType = "\(self.contentType)/" + ext
      uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
    self.uploadReqArray.append(uploadRequest)
    self.uploadDictArray.append(mediaDict)
    }
    let transferManager = AWSS3TransferManager.default()
   
    uploadRequest.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
      DispatchQueue.main.async(execute: {
        let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        print("Progress is: \(progress)")
        let progressInt = Double(progress * 100)
        let appDel = UIApplication.shared.delegate as? AppDelegate
        if ((appDel?.s3UploadRequestNameArray.contains(remoteName)) == true) {
            appDel?.s3UploadRequestProgressDict[remoteName] = progressInt
            self.delegate?.trackOfProgress()
        } else {
            appDel?.s3UploadRequestNameArray.append(remoteName)
            appDel?.s3UploadRequestProgressDict[remoteName] = progressInt
            self.delegate?.trackOfProgress()
        }
//        NSNotificationCenter.defaultCenter().postNotificationName("UPLOAD PROGRESS", object: progress * 100)
        
      })
    }

    // Perform file upload
  
    
    transferManager?.upload(uploadRequest).continue ({ (task) -> AnyObject! in
      
      if let error = task.error {
        print("Upload failed with error: (\(error.localizedDescription))")
      }
      
      if let exception = task.exception {
        print("Upload failed with exception (\(exception))")
      }
      
      if task.isCompleted {
        print("Upload done")
      }
      
      if task.result != nil && task.isCompleted {
        
        let s3URL = URL(string: "https://\(self.S3BucketName).s3.amazonaws.com/\(uploadRequest!.key!)")!
        
        do {
          let success = try FileManager.default.removeItem(at: imageURL)
          print("Item deleted Succesfully")
          print("Uploaded to:\n\(s3URL)")
            let userID = UserDefaults.standard.value(forKey: currentUserId)
            let val1 = mediaDict.value(forKey: serial_id) as! String
            let val2 = mediaDict.value(forKey: "SeqNum") as! String
            print(val1)
            print(val2)
            
          UploadRecordSharedClass.sharedInstance.updateUploadStatusForRecord("\(String(describing: val1))", userID: userID! as AnyObject, sequenceNum: "\(String(describing: val2))", answerText: "\(s3URL)", uploadFlag: "TRUE",fetchedSurveyID: SURVEY_ID!)
            UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecordsToVerify(userID! as AnyObject)
          
          self.delegate?.didCompleteImageUpload("\(s3URL)", serialID: "index", doneFlag: true,contentType: self.contentType,relatedSurveyId: SURVEY_ID!)
        } catch {
          print("Deletion error : \(error)")
          self.delegate?.didCompleteImageUpload("\(s3URL)", serialID: "index", doneFlag: true,contentType: self.contentType,relatedSurveyId: SURVEY_ID!)
          
        }
        // Read uploaded image and display in a view
      }
      else {
        print("Unexpected empty result.")
//        if self.appDel?.netWorkStatus == true {
//        self.uploadMedia(mediaDict, imageName: imageName,resumeFlag: false,uploadRequest1: "")
        self.delegate?.didCompleteImageUpload("", serialID: "index", doneFlag: false,contentType: self.contentType,relatedSurveyId: SURVEY_ID!)
//      }
    }
      return nil
    })
    
  }
  
  func cancelAllUploads() {
    for (_, uploadRequest) in self.uploadReqArray.enumerated() {
        uploadRequest.cancel().continue({ (task) -> AnyObject! in
          if let error = task.error {
            print("cancel() failed: [\(error)]")
          }
          if let exception = task.exception {
            print("cancel() failed: [\(exception)]")
          }
          return nil
        })
    }
  }

  
  func pauseAllUploads() {
    for (_, uploadRequest) in self.uploadReqArray.enumerated() {
      if uploadRequest.state != .completed {
      uploadRequest.pause()
      }
    }
  }
  
  func resumeAllUploads() {
    var cnt = 0
    for (_, uploadRequest) in self.uploadReqArray.enumerated() {
      if uploadRequest.state == .paused {
//      self.uploader(NSURL(), imageName: "", index: self.serialArray[cnt], uploadRequest1: uploadRequest, resumeFlag: true)
        self.uploadMedia(self.uploadDictArray[cnt], imageName: "", resumeFlag: true, uploadRequest1: uploadRequest)
      }
      cnt = cnt + 1
    }
  }
  
  
}
