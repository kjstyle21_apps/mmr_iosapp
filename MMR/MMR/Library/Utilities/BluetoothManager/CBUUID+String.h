//
//  CBUUID+String.h
//  BluetoothManagerExample
//
//  Created by Amit on 30/07/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>

@interface CBUUID (String)
- (NSString *)representativeString;

@end
