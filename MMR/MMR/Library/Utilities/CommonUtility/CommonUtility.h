//
//  CommonUtility.h
//  UEI_Scale
//
//  Created by Mindbowser on 19/10/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonUtility : NSObject

+ (CommonUtility *)sharedInstance;
-(void)keyboardShownViewToAnimate:(UIView *)view heightToMove:(CGFloat)height;
-(void)keyboardHideViewToAnimateBack:(UIView *)view prevFrame:(CGRect)previousframe;
- (Byte)calculateCheckSum:(Byte)i data:(NSMutableData *)cmd;
- (unsigned int)intFromHexString:(NSString *) hexStr;

@end
