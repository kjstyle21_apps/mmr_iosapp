//
//  CommonUtility.m
//  UEI_Scale
//
//  Created by Mindbowser on 19/10/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

#import "CommonUtility.h"
@implementation CommonUtility

static CommonUtility *sharedInstance = nil;

+ (CommonUtility *)sharedInstance
{
    @synchronized(self) {
        if (!sharedInstance) {
            sharedInstance = [CommonUtility new];
        }
    }
    return sharedInstance;
}

// animate view if keyboard shown
-(void)keyboardShownViewToAnimate:(UIView *)view heightToMove:(CGFloat)height {
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = height;
        view.frame = frame;
    }];
}

// hide view on keyboard resign
-(void)keyboardHideViewToAnimateBack:(UIView *)view prevFrame:(CGRect)previousframe {
    [UIView animateWithDuration:0.25 animations:^{
        view.frame = previousframe;
    }];
}

#pragma mark -calculate check sum
- (Byte)calculateCheckSum:(Byte)i data:(NSMutableData *)cmd
{   Byte * cmdByte = (Byte *)malloc(i);
    memcpy(cmdByte, [cmd bytes], i);
    Byte local_cs = 0;
    int j = 0;
    while (i>0) {
        local_cs += cmdByte[j];
        i--;
        j++;
    };
    local_cs = local_cs&0xff;
    return local_cs;
}

#pragma mark -wrape hex string into int value
- (unsigned int)intFromHexString:(NSString *) hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    return hexInt;
}
@end
