//
//  Constant.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

// Password
let kMaximumPasswordCharacters = 15
let kMinimumPasswordCharacters = 6


// fonts
let TAHOMA_REGULAR      = "tahoma"
let TAHOMA_BOLD         = "tahomabd"
let VAGTRUNDSCHRIFT     = "VAGRundschriftD"

let SCREEN_WIDTH                = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT               = UIScreen.main.bounds.size.height
let NAVIGATION_BAR_COLOR        = UIColor(red: 74/255.0, green: 26/255, blue: 102/255, alpha: 1.0)
let kCRMessageNoInternetConnection = "Please check your internet connection and your MMR preferences for cellular network usage.".localized()

let MMRServiceRegisterRequestParameterDeviceToken = "MMRServiceRegisterRequestParameterDeviceToken"

let MMRProjectName         = "MMR"
let MMRInstructionsTips   = "Tips and Information".localized()
let kMMRDescriptionPlaceholder = "Please enter your answer here".localized()

//aws pool id

let COGNITO_IDENTITY_POOL_ID = "us-west-2:faae55a9-73eb-4061-909b-5f8749b22ae6"

//let COGNITO_IDENTITY_POOL_ID = "us-west-2:6f025034-cd19-4d0b-a7aa-56f29ac4a5e2"

//common for all screens
let NextButtonTitleKeyword = "NEXT"

//StartOfTourViewController

let startTourVCSkipButtonTitleKeyword = "SKIP"

//GetStartedContentViewController

let helperScreenOneDescriptionKeyword = "This survey will only take couple of minutes."

//HelperScreenTwo

let helperScreenTwoDescriptionKeyword = "Once you have logged on, answer each questions before moving on to the next one by pressing NEXT."

//HelperScreenThree

let helperScreenThreeDescriptionKeyword = "At the end, press SUBMIT button, and that’s it!"

//MMRLoginViewController

let loginVCemailLabelKeyword = "EMAIL"
let loginVCpasswordLabelKeyword = "PASSWORD"
let loginVCemailTextfieldPlaceholderKeyword = "abc@xyz.com"
let loginVCpasswordTextfieldPlaceholderKeyword = "password"
let loginVCshowPasswordButtonTitleKeyword = "Show Password"
let loginVChidePasswordButtonTitleKeyword = "Hide Password"
let loginVCforgotPasswordButtonTitleKeyword = "Forgot Password"
let loginVCloginButtonTitleKeyword = "LOGIN"
let loginVCscanQRCodeTitleKeyword = "SCAN QR CODE"
let loginVCorButtonTitleKeyword = "Or"

//MMRForgotPasswordViewController

let forgotPasswordVCScreenTitleLabelKeyword = "Forgot Password?"
let forgotPasswordVCDescriptionLabelKeyword = "Enter your email address below and we’ll help you to recover your password."
let forgotPasswordVCEmailTextfieldPlaceholderKeyword = "Email"
let forgotPasswordVCSendButtonTitleKeyword = "SEND"


//MMRQRCodeScanViewController

let scanQRCodeVCScreenTitleLabelKeyword = "Scan QR Code"
let scanQRCodeVCDescriptionLabelKeyword = "Focus the camera on QR code"
let noBarcodeQRcodeDetectedKeyword = "No barcode/QR code is detected"

//HomeViewController

//let homeVCProductQuestionLabelKeyword = "Are you using product X?"
var homeVCYesButtonTitleKeyword = "YES"
var homeVCNOButtonTitleKeyword = "NO"
let homeVCDescriptionLabelKeyword  = "Description:"

//MMRInstructionsAndTips

let instructionsVCScreenTitleLabelKeyword = "Instructions & Tips"
let instructionsVCStartButtonTitleKeyword = "START"

//MMRSliderTypeQuestion
let sliderTypeVCScreenTitleLabelKeyword = "Slider Question"

//MMRDiscriptiveTypeViewController
let descriptiveTypeVCScreenTitleLabelKeyword = "Discriptive Question"

//MMRRadioTypeQuestions

let radioTypeVCScreenTitleLabelKeyword = "Radio Question"

//SideMenuViewController

let homeMenuKeyword = "Home"
let changeLanguageMenuKeyword = "Change Language"
let changePasswordMenuKeyword = "Change Password"
let helpMenuKeyword = "Help"
let contactUsMenuKeyword = "Contact Us"
let logoutMenuKeyword = "Logout"
let ScanQRCode      = "SCAN QR CODE"
let Tour = "Tour"

//Alert Titles

let invalidEmailMessageKeyword = "Please enter valid Email address"
let emptyEmailMessageKeyword = "Please enter email"
let allFieldsMandatoryMessageKeyword = "All the fields are mandatory"
let emptyPasswordMessageKeyword = "Please enter your password"
let invalidUserTokenMessageKeyword = "Invalid User Token"
let somethingWentWrongMessageKeyword = "Something went wrong. Please try again."
let userNotRegisterToMMRMessageKeyword = "User not register to MMR"

//Contact Us Controller
let explainProblemInDetailMessageKeyword = "Explain your problem in detail"

//Change Password Controller
let passwordTooShortMessageKeyword = "The Password you entered is too short. Please try again."
let passwordTooLongMessageKeyword = "The Password you entered is too long. Please try again."
let passwordDoesNotMatchMessageKeyword = "Password does not match"
let samePasswordMessageKeyword = "Old Password matches with New Password. Please try another password"
let passwordChangeSuccessMessageKeyword = "Password changed successfully"

//Submit Survey Controller
let submitSurveyTitleKeyword = "Submit Survey"
let submitButtonTitleKeyword = "SUBMIT"

//Help Controller
let tutorialsButtonTitleKeyword = "Tutorials"
let FAQsButtonTitleKeyword = "FAQs"

//Change Password Controller
let oldPasswordTextfieldPlaceholderKeyword = "Old Password"
let newPasswordTextfieldPlaceholderKeyword = "New Password"
let confirmPasswordTextfieldPlaceholderKeyword = "Confirm Password"
let updateButtonTitleKeyword = "UPDATE"

//ContactUs Controller
let regardingQLabelKeyword = "What is this regarding?"
let makeSelectionPlaceholderKeyword = "Make Selection"
let phoneTypeQLabelKeyword = "What type of phone do you have?"
let emailQLabelKeyword = "What is you email address?"
let problemQLabelKeyword = "What seems to be the problem?"
let contactUsMessageLabelKeyword = "Thank You for contacting us.                                    We have received your query and will get back to you shortly."

//Change Language Controller
let selectLanguageKeyword  = "Select Language"

// Device name
let DEVICE_NAME = "MMR-USAGE MONITOR";

let CURRENT_SENSOR_NAME_UD = "CURRENT_SENSOR_NAME";
let CURRENT_SENSOR_UUID_UD = "CURRENT_SENSOR_UUID";
// Real time services UUID's
//let  REAL_TIME_DATA_SERVICE_UUID = CBUUID(string: "00005100-0000-1000-8000-008055aa55aa");
//let   BATTERY_CHARACTERISTICS_UUID = CBUUID(string: "00005101-0000-1000-8000-008055aa55aa");
//let  REAL_TIME_MOTION_CHARACTERISTICS_UUID = CBUUID(string: "00005102-0000-1000-8000-008055aa55aa");
//let  REAL_TIME_WEIGHT_CHARACTERISTICS_UUID = CBUUID(string: "00005103-0000-1000-8000-008055aa55aa");
//let  WEIGHT_CALIBRATION_CHARACTERISTICS_UUID = CBUUID(string: "00005104-0000-1000-8000-008055aa55aa");
//let  ACCELEROMETER_CHARACTERISTICS_UUID = CBUUID(string: "00005105-0000-1000-8000-008055aa55aa");
//let  UUID_CHARACTERISTICS_UUID = CBUUID(string: "00005106-0000-1000-8000-008055aa55aa");

let  REAL_TIME_DATA_SERVICE_UUID = CBUUID(string: "00005100-0000-1000-8000-008055aa55aa");
let   BATTERY_CHARACTERISTICS_UUID = CBUUID(string: "00005101-0000-1000-8000-008055aa55aa");
let  REAL_TIME_MOTION_CHARACTERISTICS_UUID = CBUUID(string: "00005102-0000-1000-8000-008055aa55aa");
let  REAL_TIME_WEIGHT_CHARACTERISTICS_UUID = CBUUID(string: "00005103-0000-1000-8000-008055aa55aa");
let  WEIGHT_CALIBRATION_CHARACTERISTICS_UUID = CBUUID(string: "00005104-0000-1000-8000-008055aa55aa");
let  ACCELEROMETER_CHARACTERISTICS_UUID = CBUUID(string: "00005105-0000-1000-8000-008055aa55aa");
let  UUID_CHARACTERISTICS_UUID = CBUUID(string: "00005107-0000-1000-8000-008055aa55aa");



// History data services UUID's
//let  HISTORY_DATA_SERVICE_UUID = CBUUID(string:"00006100-0000-1000-8000-008055aa55aa");
//let  CLEAR_HISTORY_CHARACTERISTICS_UUID = CBUUID(string:"00006101-0000-1000-8000-008055aa55aa");
//let  HISTORY_MOTION_DATA_CHARACTERISTICS_UUID = CBUUID(string:"00006102-0000-1000-8000-008055aa55aa");
//let  HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID = CBUUID(string:"00006103-0000-1000-8000-008055aa55aa");
let  HISTORY_DATA_SERVICE_UUID = CBUUID(string:"00006100-0000-1000-8000-008055aa55aa");
let  CLEAR_HISTORY_CHARACTERISTICS_UUID = CBUUID(string:"00006101-0000-1000-8000-008055aa55aa");
let  HISTORY_MOTION_DATA_CHARACTERISTICS_UUID = CBUUID(string:"00006102-0000-1000-8000-008055aa55aa");
let  HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID = CBUUID(string:"00006103-0000-1000-8000-008055aa55aa");
let  HISTORY_RANGE_WEIGHT_DATA_CHARACTERISTICS_UUID = CBUUID(string:"00006104-0000-1000-8000-008055aa55aa");

// Time service
//let  TIME_SERVICE_UUID = CBUUID(string:"00001805-0000-1000-8000-00805f9b34fb");
//let  CURRENT_TIME_CHARACTERISTIC_UUID = CBUUID(string:"00002a2b-0000-1000-8000-00805f9b34fb");
let  TIME_SERVICE_UUID = CBUUID(string:"00001805-0000-1000-8000-00805f9b34fb");
let  CURRENT_TIME_CHARACTERISTIC_UUID = CBUUID(string:"00002a2b-0000-1000-8000-00805f9b34fb");


// Notification characteristics
//let  CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID_1 = CBUUID(string:"00002901-0000-1000-8000-00805f9b34fb");
//let  CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = CBUUID(string:"00002902-0000-1000-8000-00805f9b34fb");
let  CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID_1 = CBUUID(string:"00002901-0000-1000-8000-00805f9b34fb");
let  CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = CBUUID(string:"00002902-0000-1000-8000-00805f9b34fb");

/*
 //Unit
 let UNIT_GRAM = " gm";
 let UNIT_PER = "%";
 let SCALING_FACTOR : uint = 10000;
 let UUID_LENGHT : Int = 32;
 
 // Parameters
 //let WEIGHT_PARAMETER_ID : uint = 1;
 //let HISTORY_COUNT_PARAMETER_ID : uint = 2;
 //let BATTERY_LEVEL_PARAMETER_ID : uint = 3;
 
 let WEIGHT_PARAMETER_ID : uint = 3;
 //let HISTORY_COUNT_PARAMETER_ID : uint = 2;
 let BATTERY_LEVEL_PARAMETER_ID : uint = 2;
 //let TIMESTAMP_PARAMETER_ID : uint = 5;
 */


//Unit
let UNIT_GRAM = " gm";
let UNIT_PER = "%";

// Parameters
let SCALING_FACTOR : uint = 100;
let UUID_LENGHT : Int = 32;
let DEFUALT_VALUE  = "0";
let WEIGHT_PARAMETER_ID : uint = 1;
let HISTORY_COUNT_PARAMETER_ID : uint = 2;
let BATTERY_LEVEL_PARAMETER_ID : uint = 3;
let COMPLETE_HISTORY : uint = 1;
let SPECIFIC_DATERANGE_HISTORY : uint = 2;
let DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"  //"dd-MM-yyTHH:mm:ss";
let CLEAR_MOTION_HISTORY : UInt8 =  1;
let CLEAR_WEIGHT_HISTORY : UInt8 = 2;
let CLEAR_ALL_HISTORY : UInt8 = 3;
let BEACON_NO_OF_PACKETS = 5;
let BEACON_COMPLETE_DATA_LENGTH = BEACON_NO_OF_PACKETS * 3;


let STATE_1 : uint = 1;
let STATE_2 : uint = 2;
let STATE_3 : uint = 3;
let STATE_4 : uint = 4;
let STATE_5 : uint = 5;
let BEACON_PACKET_VERSION : UInt8 = 2; //TODO
let BEACON_PACKET_LENGHT : UInt8 = 12;

let STATE_1_STR = "1";
let STATE_2_STR = "2";
let STATE_3_STR = "3";
let STATE_4_STR = "4";
let STATE_5_STR = "5";

//Device types
let MOTION_DEVICE : UInt8 = 1;
let WEIGHT_200_DEVICE : UInt8 = 2;
let WEIGHT_1000_DEVICE : UInt8 = 3;





//BLUETOOTH STATUS NOTIFICATIONS

let BLUTOOTH_POWER_STATUS_OFF = "BLUTOOTH_POWER_STATUS_OFF"
let BLUTOOTH_POWER_STATUS_ON = "BLUTOOTH_POWER_STATUS_ON"
let BLE_HISTORY_AVAILABLE_NOTIFY = "BLE_HISTORY_AVAILABILITY_NOTIFY"

//BLUETOOTH ALERT MESSAGES
let bluetoothePowerOffMessage = "Bluetooth is off on it and reconnect the device"

//Userdefaults keys
let DATABASE_VERSION_UD = "DATABASE_VERSION"
let authToken_UD = "authToken"
let DONE_WITH_INTERVAL_UD = "DONE_WITH_INTERVAL"
let SURVEY_TIMER_STOP_DATE_UD = "SURVEY_TIMER_STOP_DATE"
let SURVEY_TIMER_START_DATE_UD = "SURVEY_TIMER_START_DATE"
let SURVEY_TIMER_COMPLETION_SECs_UD = "SURVEY_TIMER_COMPLETION_SECs"
let CURRENT_USER_ID_UD = "CURRENT_USER_ID"
let UPLOADING_IN_PROCESS_UD = "UPLOADING_IN_PROCESS"
let TIMER_STOP_DATE_UD = "TIMER_STOP_DATE"
let TIMER_START_DATE_UD = "TIMER_START_DATE"
let TIMER_COMPLETION_SECs_UD = "TIMER_COMPLETION_SECs"
let Start_Tour_UD = "Start_Tour"
let BLE_HISTORY_NOTIFY_FLAG_UD = "BLE_HISTORY_NOTIFY_FLAG"
let INTERVAL_QUESTION_SERIAL_NO_UD = "INTERVAL_QUESTION_SERIAL_NO"
let SURVEY_COMPLETE_STATUS_UD = "SURVEY_COMPLETE_STATUS"
let IS_HISTORY_SAVED_UD = "IS_HISTORY_SAVED"
let isEntryDoneFlag_UD = "isEntryDoneFlag"
let UPLOAD_COUNTER_UD = "UPLOAD_COUNTER"
let IS_ON_SUBMIT_SCREEN_UD = "IS_ON_SUBMIT_SCREEN"
let MULTIPLE_FLAG_UD = "MULTIPLE_FLAG"
let CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE_UD = "CURRENT_MULTIPLE_SURVEY_IS_LIVE_BUT_SUBMITTED_ONLINE"
let SelectedRow_UD = "SelectedRow"
let VIDEO_COMPRESSION_COUNT_UD = "VIDEO_COMPRESSION_COUNT"
let CURRENT_USER_ROLE_UD = "CURRENT_USER_ROLE"
let API_EXECUTION_ON_UD = "API_EXECUTION_ON"
let SAME_USER_FLAG_STATUS_UD = "SAME_USER_FLAG_STATUS"
let MMRServiceLogout_UD = "MMRServiceLogout"
let Welcome_Screen_Again_UD = "Welcome_Screen_Again"
//
let FIRST_QUESTION_SEEN_TIME = "FIRST_QUESTION_SEEN_TIME"
let LAST_QUESTION_ANSWER_TIME = "LAST_QUESTION_ANSWER_TIME"
//
let IS_NIL_SURVEY = "Is_Nil_survey"

//Tour Texts
let Tour_Text1 = "Welcome to the MMR app! To help you find your way around, here is a quick tour of the app features. Just press this text when you’re ready to start."
let Tour_Survey_Title = "Welcome to the MMR App"
let Tour_Desc_Text = "Thank you for agreeing to take part in our survey today, we just have a few short questions and we’d love to hear your thoughts and opinions."

//New text added
let Tour_Desc_New_Text = "Are you ready to begin?"

let Tour_Welcome_Question_Text = "Thank you for taking part in this research – we’d love to understand how your deodorant is working for you throughout the day. Are you ready to begin the survey?"
let Tour_Menu_Button_Text = "Here is the menu button. Here you can find FAQs, this welcome tour, and MMR contact details if you get stuck.\nPress the screen when you’re ready to move on."
let Tour_Update_Button_Text = "This is the refresh button. Pressing here updated the survey you’re completing so you can get the latest version."
let Tour_Yes_Button_Text = "This is the main welcome screen to your survey. This lets you choose when you’re ready to start your survey."
let Tour_Slider_Text = "Here is a typical question we might ask you. Here we would like to know how much you liked something, giving a score out of ten.  To answer, simply move the slider until you reach the score you want to give. You’ll have to move the slider from the middle start position to provide a score."
let Tour_Slider_Question_Text = "How much are you enjoying the tour so far? Where 1 means not at all enjoyable and 10 means very enjoyable"
let Tour_i_Button_Text = "If you’re not sure what to do to answer a question, here is a handy hints button to help!"
let Tour_ProgressBar_Text = "As you go through the survey you can check your progress with this bar along the top. Our survey are usually short & sweet and this can help you keep track."
let Tour_Next_Button_Text = "When you’ve answered each question press the next button will change from grey to purple. You’ll just need to press it to move on."
let Tour_Submit_Screen_Text = "When you’ve answered all the questions in the survey you should see a screen like this.\nSo we don’t lose your feedback and scores you need to press the submit button and a notification will appear when the survey has been successfully submitted like this…"
let Tour_Welcome_Screen_Text_again = "Now you’re ready to start your survey. When you’re ready to start just press the screen and you’ll see your survey welcome screen. If you need to see any of these instructions again you can take the tour again by visiting the tour section in the menu."
