//
//  AnswerSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 11/9/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class AnswerSharedClass: NSObject {
    var none_label = String()

    class var sharedInstance :AnswerSharedClass {
        struct Singleton {
            static let instance = AnswerSharedClass()
        }
        return Singleton.instance
    }
    
    func parseDictionary(_ answerDetailsArrary : [AnyObject], questionID : Int, fetchedSurveyId: AnyObject)
    {
       for answerDetailsDict in answerDetailsArrary
      {
        let dict = answerDetailsDict as? NSDictionary
        var parameterDictOfAns = [String : AnyObject]()
      
        parameterDictOfAns.updateValue(fetchedSurveyId, forKey: surveyId)
        
        if let optnText = answerDetailsDict[optionText] as? String {
            parameterDictOfAns.updateValue(optnText as AnyObject, forKey: optionText)
        } else {
            parameterDictOfAns.updateValue("" as AnyObject, forKey: optionText)
        }
        
        if let optnId = answerDetailsDict["optionId"] as? NSNumber{
            parameterDictOfAns.updateValue(optnId, forKey: optionId)
        } else {
            parameterDictOfAns.updateValue("" as AnyObject, forKey: optionId)
        }
        
        if let noneLabel = answerDetailsDict[label] as? String {
            parameterDictOfAns.updateValue(noneLabel as AnyObject, forKey: label)
        } else {
            parameterDictOfAns.updateValue("" as AnyObject, forKey: label)
        }

        
        if  questionID != 0 {
            parameterDictOfAns.updateValue(questionID as AnyObject, forKey: questionId)
        }
        
        if  let settings1  = answerDetailsDict[settings] as? NSArray  {
        if settings1.count > 0 {
            if let freq    = ((answerDetailsDict[settings] as? NSArray)?[0] as AnyObject)[frequency] {
                parameterDictOfAns.updateValue(freq as AnyObject, forKey: frequency)
            }
            if let MinRange = ((answerDetailsDict[settings] as? NSArray)?[0] as AnyObject)[minRange] {
                parameterDictOfAns.updateValue(MinRange as AnyObject, forKey: minRange)
            }
        if let MaxRange    = ((answerDetailsDict[settings] as? NSArray)?[0] as AnyObject)[maxRange] {
            parameterDictOfAns.updateValue(MaxRange as AnyObject, forKey: maxRange)
        }
        if let maxChara    = ((answerDetailsDict[settings] as? NSArray)?[0] as AnyObject)[maxCharacters] {
            parameterDictOfAns.updateValue(maxChara as AnyObject, forKey: maxCharacters)
            }
        }
        else {
          parameterDictOfAns.updateValue("" as AnyObject, forKey: minRange)
          parameterDictOfAns.updateValue("" as AnyObject, forKey: maxRange)
          parameterDictOfAns.updateValue("" as AnyObject, forKey: maxCharacters)
          parameterDictOfAns.updateValue("" as AnyObject, forKey: frequency)

          }
        }
        else {
            parameterDictOfAns.updateValue("" as AnyObject, forKey: minRange)
            parameterDictOfAns.updateValue("" as AnyObject, forKey: maxRange)
            parameterDictOfAns.updateValue("" as AnyObject, forKey: maxCharacters)
            parameterDictOfAns.updateValue("" as AnyObject, forKey: frequency)

        }
        if let seralNo = answerDetailsDict[serialNoOfQuestion] {
            parameterDictOfAns.updateValue(seralNo as AnyObject, forKey: serialNoOfQuestion)
            }
        else {
          parameterDictOfAns.updateValue("" as AnyObject, forKey: serialNoOfQuestion)
        }
        
        if let orgSerialNo = answerDetailsDict[orginalSerialNo] {
            parameterDictOfAns.updateValue(orgSerialNo as AnyObject, forKey: orginalSerialNo)
        }
        else {
          parameterDictOfAns.updateValue("" as AnyObject, forKey: orginalSerialNo)
        }
        
        if let temptextOptionId = answerDetailsDict[textOptionId] as? String {
            parameterDictOfAns.updateValue(temptextOptionId as AnyObject, forKey: textOptionId)
        } else {
            parameterDictOfAns.updateValue("" as AnyObject, forKey: textOptionId)
        }
        
        print ("Dict :\(parameterDictOfAns)")
//        let userID = NSUserDefaults.standardUserDefaults().valueForKey("CURRENT_USER_ID")
//        let status = self.checkForAnswerPresentforUserID(userID!)
//        if status {
//          print("Already Answer")
//        }
//        else {
          if let result: Bool = self.insertForAnswers(parameterDictOfAns) {
            print("Success")
          }
          else {
            print("Fail")
            
          }
//        }
   }
}
  
    func insertForAnswers(_ details : [String : AnyObject]) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
          
          let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)

            let sql_stmt = "INSERT INTO ANSWER_RECORDS (User_ID, Survey_ID, Question_ID , Option_Text, Option_ID, None_label , Sequence_Number, Min_Value , Max_Value , Char_Limit ,Frequency ,Original_Sequence, Option_Text_ID) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            do {
//                var optiontext :  details[optionText] as? String
//                if optiontext.characters.count == 0 {
//                    optiontext = ""
//                }
                try database?.executeUpdate(sql_stmt, values: [userID!,details[surveyId]!, details[questionId]!,details[optionText]!,details[optionId]!,details[label]!,details[serialNoOfQuestion]!,details[minRange]!,details[maxRange]!,details[maxCharacters]!,details[frequency]!, details[orginalSerialNo]!, details[textOptionId]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }


  func checkForAnswerPresentforUserID(_ userID : AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * from ANSWER_RECORDS WHERE User_ID =?", values: [userID])
      var count = 0
      while (rs?.next())! {
        count = count + 1
      }
      
      if count > 0 {
        print("Exists")
        return true
      } else {
        print("No exists")
        return false
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return false
  }
  
    func fetchDataForOptions( _ questionID : Int, userID : AnyObject,fetchedSurveyID: AnyObject) -> (optionText : NSMutableArray, optionId: NSMutableArray, originalSeqNo : NSMutableArray, textOptionId : NSMutableArray) {
        //let question_id : Int = BaseClassForQuestions.serialNo_ID

        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let arrayInfo =  NSMutableArray ()
        let optionIdArray = NSMutableArray()
        let optionOrgSeqArray = NSMutableArray()
        let textOptionIdArray = NSMutableArray()
        var noneFlag = false
        
        do {
            var noneParaValue = ""
            var noneOptionIDValue = ""
            var noneOptionOrgSeqValue = ""
            var noneTextOptionID = ""
            let rs = try database?.executeQuery("SELECT * FROM ANSWER_RECORDS_COPY WHERE Question_ID = ? AND User_ID =? AND Survey_ID =? ORDER BY Sequence_Number", values: [questionID, userID,fetchedSurveyID])
            while (rs?.next())! {
                
                none_label =  (rs?.string(forColumn: "None_Label"))!
                
                if none_label == "none"
                {
                    noneFlag = true
                    noneParaValue = (rs?.string(forColumn: "Option_Text"))!
                }
                else
                {
                    arrayInfo.add(rs?.string(forColumn: "Option_Text"))
                }
              
              if none_label == "none"
              {
                noneOptionIDValue = (rs?.string(forColumn: "Option_ID"))!
                noneOptionOrgSeqValue = (rs?.string(forColumn: "Original_Sequence"))!
//                noneOptionOrgSeqValue = rs.stringForColumn("Sequence_Number")
                if let temp = rs?.string(forColumn: "Option_Text_ID") as? String {
                    if let temp = rs?.string(forColumn: "Option_Text_ID") as? String {
                        if temp != "" {
                            noneTextOptionID = temp
                        }
                    }
                }
              }
              else
              {
//                arrayInfo.addObject(rs.stringForColumn("Option_Text"))
                optionIdArray.add(rs?.string(forColumn: "Option_ID"))
                optionOrgSeqArray.add(rs?.string(forColumn: "Original_Sequence"))
//                optionOrgSeqArray.addObject(rs.stringForColumn("Sequence_Number"))
                if let temp = rs?.string(forColumn: "Option_Text_ID") as? String {
                    if temp != "" {
                        textOptionIdArray.add(temp)
                    }
                }
              }
              
            }
          
          if noneFlag {
            none_label = "none"
          }
            if noneParaValue != ""
            {
                arrayInfo.add(noneParaValue)
                optionIdArray.add(noneOptionIDValue)
                optionOrgSeqArray.add(noneOptionOrgSeqValue)
                textOptionIdArray.add(noneTextOptionID)
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        
        if arrayInfo.count > 0 {
            print(arrayInfo)
            return (arrayInfo, optionIdArray, optionOrgSeqArray, textOptionIdArray)
        }
        else {
            return ([""], [""], [""], [""])
        }
    }
    
  func fetchMinMaxAndFreqForSlider( _ questionID : Int, userID : AnyObject, fetchedSurveyID: AnyObject) -> NSMutableDictionary {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let dictInfo = NSMutableDictionary ()
        do {
//            let rs = try database.executeQuery("SELECT Min_Value, Max_Value, Frequency FROM ANSWER_RECORDS WHERE Question_ID =? AND User_ID =? AND Survey_ID =?", values: [questionID,userID,fetchedSurveyID])
            let rs = try database?.executeQuery("SELECT Min_Value, Max_Value, Frequency FROM ANSWER_RECORDS_COPY WHERE Question_ID =? AND User_ID =? AND Survey_ID =?", values: [questionID,userID,fetchedSurveyID])
            while (rs?.next())! {
                dictInfo.setValue(rs?.string(forColumn: "Min_Value"), forKey:"Min_Value")
                dictInfo.setValue(rs?.string(forColumn: "Max_Value"), forKey:"Max_Value")
                dictInfo.setValue(rs?.string(forColumn: "Frequency"), forKey:"Frequency")
                
                break
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if dictInfo.count > 0 {
            return dictInfo
        }
        else {
            return ["":""]
        }
    }
    
  func fetchMaxCharForDiscriptiveType( _ questionID : Int, userID : AnyObject,fetchedSurveyID: AnyObject) -> Int {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let dictInfo = NSMutableDictionary ()
        do {
//            let rs = try database.executeQuery("SELECT Char_Limit FROM ANSWER_RECORDS WHERE Question_ID =? AND User_ID =? AND Survey_ID =?", values: [questionID, userID, fetchedSurveyID])
            let rs = try database?.executeQuery("SELECT Char_Limit FROM ANSWER_RECORDS_COPY WHERE Question_ID =? AND User_ID =? AND Survey_ID =?", values: [questionID, userID, fetchedSurveyID])
            while (rs?.next())! {
                dictInfo.setValue(rs?.string(forColumn: "Char_Limit"), forKey:"Char_Limit")
                break
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if dictInfo.count > 0 {
            
             let characterLimit = (dictInfo["Char_Limit"] as! NSString).integerValue
            if characterLimit > 0 {
                return characterLimit
            }
        }
            return 0

    }

  func fetchMaxCharacterLimitForMultipleTextBox( _ questionID : Int, userID : AnyObject,fetchedSurveyID: AnyObject) -> NSMutableArray {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let arrayInfo = NSMutableArray ()
        do {
            let rs = try database?.executeQuery("SELECT Char_Limit FROM ANSWER_RECORDS WHERE Question_ID =? AND User_ID =? AND Survey_ID", values: [questionID, userID, fetchedSurveyID])
            while (rs?.next())! {
                arrayInfo.add(rs?.string(forColumn: "Char_Limit"))
                
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if arrayInfo.count > 0 {
            return arrayInfo
        }
        else {
            return [""]
        }
    }

    func deleteAnswerDataOfUploadedSurvey(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
        var sql_stmt = "DELETE FROM ANSWER_RECORDS WHERE User_ID =? AND Survey_ID =?"

        if surveyID as? String == "" {
            sql_stmt = "DELETE FROM ANSWER_RECORDS WHERE User_ID =?"
        }
      do {
        if surveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func deleteAnswerDataOfUploadedSurveyExcludingCurrentSurvey(_ userID : AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        if (database?.open())! {
            let sql_stmt = "DELETE FROM ANSWER_RECORDS WHERE User_ID =? AND Survey_ID !=?"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID,surveyID!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func copyAllOptionsIntoAnswerRecordsCopy(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> [NSMutableDictionary] {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        var optionsInfoArray = [NSMutableDictionary]()
        database?.open()
        do {
            //uncomment while next changes
            let rs = try database?.executeQuery("SELECT * FROM ANSWER_RECORDS WHERE User_ID =? AND Survey_ID =?", values: [userID,fetchedSurveyID])
            
            while (rs?.next())! {
                let optionsInfo = NSMutableDictionary ()
                    let val1 = String(format: "%d", arguments: [(rs?.int(forColumn: "Survey_ID"))!])
                    optionsInfo.setValue("\(val1)", forKey: surveyId)
                    let val2 = String(format: "%d", arguments: [(rs?.int(forColumn: "Question_ID"))!])
                    optionsInfo.setValue("\(val2)", forKey: questionId)
                    optionsInfo.setValue(rs?.string(forColumn: "Option_Text"), forKey:optionText)
                    let val3 = String(format: "%d", arguments: [(rs?.int(forColumn: "Option_ID"))!])
                    optionsInfo.setValue("\(val3)", forKey: optionId)
                    optionsInfo.setValue(rs?.string(forColumn: "None_Label"), forKey: label)
                    let val4 = String(format: "%d", arguments: [(rs?.int(forColumn: "Sequence_Number"))!])
                    optionsInfo.setValue("\(val4)", forKey: serialNoOfQuestion)
                    let val5 = String(format: "%d", arguments: [(rs?.int(forColumn: "Min_Value"))!])
                    optionsInfo.setValue("\(val5)", forKey: minRange)
                    let val6 = String(format: "%d", arguments: [(rs?.int(forColumn: "Max_Value"))!])
                    optionsInfo.setValue("\(val6)", forKey: maxRange)
                    let val7 = String(format: "%d", arguments: [(rs?.int(forColumn: "Char_Limit"))!])
                    optionsInfo.setValue("\(val7)", forKey: maxCharacters)
                    let val8 = String(format: "%d", arguments: [(rs?.int(forColumn: "Frequency"))!])
                    optionsInfo.setValue("\(val8)", forKey: frequency)
                    let val9 = String(format: "%d", arguments: [(rs?.int(forColumn: "Original_Sequence"))!])
                    optionsInfo.setValue("\(val9)", forKey: orginalSerialNo)
                    optionsInfo.setValue(rs?.string(forColumn: "Option_Text_ID"), forKey: textOptionId)
                    optionsInfoArray.append(optionsInfo)
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        return optionsInfoArray
        database?.close()
    }
    
    func checkIsOptionAvailableAndThenInsert(_ arrayOfQuestion: [NSMutableDictionary]) {
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
        for tempQues in arrayOfQuestion {
            let serialNo = tempQues.value(forKey: serial_id)
            let optionID = tempQues.value(forKey: optionId)
            let quesID = tempQues.value(forKey: questionId)
            let result = self.isOPtionAvailableInAnswerRecordsCopy(userID! as AnyObject, fetchedSurveyID: curSurveyID! as AnyObject, optionID: optionID! as AnyObject, questionid: quesID! as AnyObject)
            if result == 0 {
                self.insertForAnswersCopy((tempQues as? [String : AnyObject])!)
            }
        }
    }
    
    func isOPtionAvailableInAnswerRecordsCopy(_ userID : AnyObject,fetchedSurveyID: AnyObject, optionID: AnyObject, questionid: AnyObject) -> Int {
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)")
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        var count = 0
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM ANSWER_RECORDS_COPY WHERE Survey_ID =? AND User_ID =? AND Question_ID = ? AND Option_ID =?", values: [fetchedSurveyID,userID,questionid,optionID])
            while (rs?.next())! {
                count = count + 1
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        return count
    }
    
    func insertForAnswersCopy(_ details : [String : AnyObject]) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            
            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            
            let sql_stmt = "INSERT INTO ANSWER_RECORDS_COPY (User_ID, Survey_ID, Question_ID , Option_Text, Option_ID, None_label , Sequence_Number, Min_Value , Max_Value , Char_Limit ,Frequency ,Original_Sequence, Option_Text_ID) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID!,details[surveyId]!, details[questionId]!,details[optionText]!,details[optionId]!,details[label]!,details[serialNoOfQuestion]!,details[minRange]!,details[maxRange]!,details[maxCharacters]!,details[frequency]!, details[orginalSerialNo]!, details[textOptionId]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func deleteOptionsFromAnswerCopy(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            var sql_stmt = "DELETE FROM ANSWER_RECORDS_COPY WHERE User_ID =? AND Survey_ID =?"
            
            if surveyID as? String == "" {
                sql_stmt = "DELETE FROM ANSWER_RECORDS_COPY WHERE User_ID =?"
            }
            do {
                if surveyID as? String == "" {
                    try database?.executeUpdate(sql_stmt, values: [userID])
                } else {
                    try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
                }
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
}
