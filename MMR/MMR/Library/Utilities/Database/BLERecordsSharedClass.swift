//
//  BLERecordsSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 1/19/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class BLERecordsSharedClass: NSObject {

  class var sharedInstance :BLERecordsSharedClass {
    struct Singleton {
      static let instance = BLERecordsSharedClass()
    }
    return Singleton.instance
  }
  
  func insertbleData(_ details : [String : AnyObject]) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      let surveyID = UserDefaults.standard.value(forKey: surveyId)
      
      if userID != nil && surveyID != nil {
        let seq = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        let sql_stmt = "INSERT INTO BLE_RECORDS (User_ID, Survey_ID , Weight, Updated_Time, Created_Time, Battery_Level, SequenceNum) values (?,?,?,?,?,?,?)"
        do {
          try database?.executeUpdate(sql_stmt, values: [userID!,surveyID!,details[bleWeight]!, details[bleUpdatedDate]!, details[bleCreatedDate]!, details[bleBatteryLevel]!, seq!])
          return true
        } catch let error as NSError {
          print("failed: \(error.localizedDescription)")
        }
      }
    }
    return false
  }
  
  //check record exists or not
  func isRecordAvailable(_ updatedTime : String) -> String {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * FROM BLE_RECORDS WHERE Updated_Time =? AND User_ID =? AND Survey_ID =?", values: [updatedTime,userID!,currentSurveyID!])
      var count = 0
      while (rs?.next())! {
        count = count + 1
      }
      
      if count > 0 {
        print("Exists")
        return "Exists"
      } else {
        print("Empty Table")
        return "No_Exists"
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return "Error"
  }

  func fetchBLEData(_ surveyID : AnyObject, userID : AnyObject) -> [NSMutableDictionary] {
    
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var dictInfoArray = [NSMutableDictionary] ()
    do {
      let rs = try database?.executeQuery("SELECT * FROM BLE_RECORDS WHERE User_ID =? AND Survey_ID =?", values: [userID,surveyID])
      while (rs?.next())! {
        let dictInfo = NSMutableDictionary ()
        dictInfo.setValue(rs?.string(forColumn: "User_ID"), forKey:"userId")
        dictInfo.setValue(rs?.string(forColumn: "Survey_ID"), forKey:"surveyId")
        dictInfo.setValue(rs?.string(forColumn: "Weight"), forKey:"weight")
        dictInfo.setValue(rs?.string(forColumn: "Updated_Time"), forKey:"updatedTime")
        dictInfo.setValue(rs?.string(forColumn: "Created_Time"), forKey:"createdTime")
        dictInfo.setValue(rs?.string(forColumn: "Battery_Level"), forKey:"batteryLevel")
        dictInfo.setValue(rs?.string(forColumn: "SequenceNum"), forKey:"submissionCount")
        dictInfoArray.append(dictInfo)
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return dictInfoArray
  }
  
  func deleteBLERecord(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    if (database?.open())! {
        var sql_stmt = "DELETE FROM BLE_RECORDS WHERE User_ID =? AND SurveyID =?"
        if fetchedSurveyID as? String == "" {
            sql_stmt = "DELETE FROM BLE_RECORDS WHERE User_ID =?"
        }
      do {
        if fetchedSurveyID as? String == "" {
        try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,fetchedSurveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
}
