//
//  CompressionRecordsSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 14/04/18.
//  Copyright © 2018 Mindbowser. All rights reserved.
//

import Foundation
import AVFoundation

class CompressionRecordsSharedClass: NSObject {

    class var sharedInstance :CompressionRecordsSharedClass {
        struct Singleton {
            static let instance = CompressionRecordsSharedClass()
        }
        return Singleton.instance
    }
    
    func insertMediaDetails(_ surveyId: AnyObject, questionId: AnyObject, serialId: AnyObject, SurveySeqNum: AnyObject, videoLocalPath: String, assetType: String) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            
            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//            let sql_stmt_Compression = "CREATE TABLE IF NOT EXISTS COMPRESSION_RECORDS ( User_ID Integer, Survey_ID Integer, Question_ID INTEGER, SerialNo INTEGER, SequenceNum INTEGER, video_localPath TEXT, asset_Type TEXT)"

            let sql_stmt = "INSERT INTO COMPRESSION_RECORDS (User_ID, Survey_ID, Question_ID, SerialNo, SequenceNum, video_localPath, asset_Type) values (?,?,?,?,?,?,?)"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID!,surveyId, questionId, serialId, SurveySeqNum, videoLocalPath, assetType])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func fetchDataForCompression(_ userID : AnyObject) -> [[String : AnyObject]] {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        var arrayInfo =  [[String : AnyObject]] ()
        var mediaDict = [String : AnyObject]()
        do {
            let rs = try database?.executeQuery("SELECT * FROM COMPRESSION_RECORDS WHERE User_ID = ?", values: [userID])
            while (rs?.next())! {
                mediaDict["userId"] = rs?.string(forColumn: "User_ID") as AnyObject
                mediaDict["surveyID"] = rs?.string(forColumn: "Survey_ID") as AnyObject
                mediaDict["surveySeqNum"] = rs?.string(forColumn: "SequenceNum") as AnyObject
                mediaDict["questionId"] = rs?.string(forColumn: "Question_ID") as AnyObject
                mediaDict["serialNo"] = rs?.string(forColumn: "SerialNo") as AnyObject
                mediaDict["video_localPath"] = rs?.string(forColumn: "video_localPath") as AnyObject
                mediaDict["asset_Type"] = rs?.string(forColumn: "asset_Type") as AnyObject
                arrayInfo.append(mediaDict)
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        return (arrayInfo)
    }
    
    func deleteData(_ userID : AnyObject, surveyID: AnyObject, seqNum: AnyObject, questionID: AnyObject, serialNo: AnyObject) {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            let sql_stmt = "DELETE FROM COMPRESSION_RECORDS WHERE User_ID =? AND Survey_ID =? AND SequenceNum =? AND Question_ID =? AND SerialNo =?"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID,surveyID,seqNum,questionID,serialNo])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func convertVideoToLowQuality(_ asset : AVAsset, assetype : String, outputURL : URL, outputFileName : NSString, userID: AnyObject, surveyId: AnyObject, surveySeqNum: AnyObject, questionId: AnyObject, serialNo: AnyObject, olderPath: URL, handler:(_ session: AVAssetExportSession)-> Void) {
        
            do {
                try FileManager.default.removeItem(at: outputURL)
            }
            catch let error {
                print("Compression Error : \(error)")
            }
            var exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)
            exportSession!.outputURL = outputURL
            
            let dur = CMTimeGetSeconds(asset.duration)
            if dur > 60.0 {
                let start = CMTimeMakeWithSeconds(1.0, 600)
                let duration = CMTimeMakeWithSeconds(60.0, 600)
                let range = CMTimeRangeMake(start, duration)
                exportSession?.timeRange = range
            }
            exportSession!.outputFileType = AVFileType.mov
            
            exportSession?.exportAsynchronously(completionHandler: {
                switch exportSession!.status {
                case  AVAssetExportSessionStatus.failed:
                    print("ERROR AKAudioFile: Export Failed!...")
                    print("Error: \(exportSession!.error)")
                case AVAssetExportSessionStatus.cancelled:
                    print("ERROR AKAudioFile: Export Cancelled!...")
                    print("Error: \(exportSession!.error)")
                default:
                    // Export succeeded !
                    let url = URL(string: exportSession!.outputURL!.path)
                    print("success")
                    if assetype == "PH" {
                        let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                        let dateStr1 = "\(timestamp).mov"
                        
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                        let dataPath2 = URL(fileURLWithPath: "\(documentsPath)/\(dateStr1)")
                        
//                        let asset2 = AVAsset(URL: NSURL(string: exportSession!.outputURL!.path!)!)
                        let asset2 = AVURLAsset(url: URL(fileURLWithPath: exportSession!.outputURL!.path))

                        self.convertVideoToLowQuality(asset2, assetype: "AV", outputURL: dataPath2, outputFileName: dateStr1 as NSString, userID: userID, surveyId: surveyId, surveySeqNum: surveySeqNum, questionId: questionId, serialNo: serialNo, olderPath: olderPath, handler: { (session) in
                        })
                    } else {
                        let details : NSMutableDictionary = [
                            "Question_ID" : questionId,
                            "Question_Type" : "videoChoice",
                            answer_text : outputFileName,
                            "SeqNum" : surveySeqNum,
                            "Upload_Flag" : "FALSE",
                            serial_id : serialNo]
                        self.deleteData(userID, surveyID: surveyId, seqNum: surveySeqNum, questionID: questionId, serialNo: serialNo)
                        UploadRecordSharedClass.sharedInstance.insertuploadDataUpdated(details, fetchedSurveyID: surveyId)
//                        _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.startUploading), userInfo: nil, repeats: false)
                        self.startUploading()
                    }
                }
            })
    }
    
    func startCompression() {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        let arrayOfData = self.fetchDataForCompression(userID! as AnyObject)
        
        for tempData in arrayOfData {
            let tempUserID = tempData["userId"]
            let tempSurveyID = tempData["surveyID"]
            let tempSeqNum = tempData["surveySeqNum"]
            let tempquesid = tempData["questionId"]
            let tempSerialNo = tempData["serialNo"]
            let temppath = tempData["video_localPath"]
            let tempAssetType = tempData["asset_Type"]
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            if (tempAssetType as? String == "AV") {
//                let tempAsset = AVAsset(URL: NSURL(string: temppath as! String)!)
                
//                let tempAsset = AVURLAsset(URL: NSURL(fileURLWithPath: temppath as! String))
//                let url = NSURL(string: temppath as! String)
//                  let tempAsset =  AVURLAsset(URL: NSURL(fileURLWithPath: url!.absoluteString!))
                let tempAsset = AVURLAsset(url: URL(fileURLWithPath: "\(documentsPath)/\(temppath as! String)"))

                let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                let dateStr1 = "\(timestamp).mov"
                let dataPath2 = URL(fileURLWithPath: "\(documentsPath)/\(dateStr1)")
                self.convertVideoToLowQuality(tempAsset, assetype: tempAssetType as! String, outputURL: dataPath2, outputFileName: dateStr1 as NSString, userID: tempUserID!, surveyId: tempSurveyID!, surveySeqNum: tempSeqNum!, questionId: tempquesid!, serialNo: tempSerialNo!, olderPath: URL(string: "\(documentsPath)/\(temppath as! String)")!, handler: { (session) in
                })
            } else {
//                let tempAsset = AVAsset(URL: NSURL(string: "\(documentsPath)/\(temppath as! String)")!)
                let tempAsset = AVURLAsset(url: URL(fileURLWithPath: "\(temppath as! String)"))
                let timestamp : Double = ((Date().timeIntervalSince1970 * 1000) )
                let dateStr1 = "\(timestamp).mov"
                let dataPath2 = URL(fileURLWithPath: "\(documentsPath)/\(dateStr1)")
//                let dataPath2 = NSURL(string: "\(documentsPath)/\(dateStr1)")
                self.convertVideoToLowQuality(tempAsset, assetype: tempAssetType as! String, outputURL: dataPath2, outputFileName: dateStr1 as NSString, userID: tempUserID!, surveyId: tempSurveyID!, surveySeqNum: tempSeqNum!, questionId: tempquesid!, serialNo: tempSerialNo!, olderPath: URL(string: "\(documentsPath)/\(temppath as! String)")!,handler: { (session) in
                })
            }
            
        }
        
        
    }
    
    func startUploading() {
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable() {
        print("Online")
        let appDel = UIApplication.shared.delegate as? AppDelegate
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        appDel!.totalNotUploadedRecords = UploadRecordSharedClass.sharedInstance.getCountOfNotUploadedRecords(userID! as AnyObject, type: "")
        appDel!.netWorkStatus = true
//        if UserDefaults.standard.bool(forKey: "UPLOADING_IN_PROCESS") == false {
            appDel!.uploader.delegate = appDel
            let status = appDel!.uploader.startUploadProcess("")
            if status {
                UserDefaults.standard.set(true, forKey: UPLOADING_IN_PROCESS_UD)
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOADING_IN_PROCESS_NOTIFY"), object: ["Status" : "true"])
            }
            else {
                UserDefaults.standard.set(false, forKey: UPLOADING_IN_PROCESS_UD)
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOADING_IN_PROCESS_NOTIFY"), object: ["Status" : "false"])
            }
//        }
    }
    }
    
    func countOfCompressionData(_ userID : AnyObject) -> Int {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        //var arrayInfo =  [[String : AnyObject]] ()
        //var mediaDict = [String : AnyObject]()
        var cnt = 0
        do {
            let rs = try database?.executeQuery("SELECT * FROM COMPRESSION_RECORDS WHERE User_ID = ?", values: [userID])
            while (rs?.next())! {
               /* mediaDict["userId"] = rs.stringForColumn("User_ID")
                mediaDict["surveyID"] = rs.stringForColumn("Survey_ID")
                mediaDict["surveySeqNum"] = rs.stringForColumn("SequenceNum")
                mediaDict["questionId"] = rs.stringForColumn("Question_ID")
                mediaDict["serialNo"] = rs.stringForColumn("SerialNo")
                mediaDict["video_localPath"] = rs.stringForColumn("video_localPath")
                mediaDict["asset_Type"] = rs.stringForColumn("asset_Type")
                arrayInfo.append(mediaDict)*/
                cnt = cnt + 1
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        return cnt
    }
}
