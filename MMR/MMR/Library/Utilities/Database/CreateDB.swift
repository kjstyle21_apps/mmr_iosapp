//
//  CreateDB.swift
//  MMR
//
//  Created by Mindbowser on 11/9/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class CreateDB: NSObject {
    
    class func getDataBasePath() -> String {
        let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as AnyObject
        let destinationPath = (documentsPath as! String) + "/MMR.sqlite"
        print (destinationPath)
        

        
        return destinationPath as String
    }
    
    class func createAndCheckUpdateDatabaseOfQuestionTable(){
        
        let databaseExists = FileManager.default.fileExists(atPath: CreateDB.getDataBasePath())
        
        if !databaseExists {
            let database = FMDatabase(path : CreateDB.getDataBasePath())
            
            if (database?.open())! {
              
               let sql_stmt = "CREATE TABLE IF NOT EXISTS QUESTION_RECORDS ( Question_ID INTEGER, Question_Text TEXT, Question_Type TEXT, Start_Time TEXT, End_Time TEXT, answerRequired BOOL, isAnswered BOOL, Answer_Text TEXT, isHintAvailable BOOL, HintText TEXT, Label TEXT, SerialNo INTEGER, AnswerId TEXT, SequenceNum INTEGER, Scale TEXT, QuestionIntervalTime INTEGER, IsSubmittedFlag TEXT, TextOptionIDSeq TEXT)"
              
                if !(database?.executeStatements(sql_stmt))! {
                    print("Error: \(database?.lastErrorMessage())")
                } else {
                    print("Success: Table Created question_records.")
                    
                }
              
              let sql_stmt_copy = "CREATE TABLE IF NOT EXISTS QUESTION_RECORDS_COPY ( Question_ID INTEGER, User_ID Integer, Question_Text TEXT, Question_Type TEXT, Start_Time TEXT, End_Time TEXT, answerRequired BOOL, isAnswered BOOL, Answer_Text TEXT, isHintAvailable BOOL, HintText TEXT, Label TEXT, SerialNo INTEGER, AnswerId TEXT, SequenceNum INTEGER, Scale TEXT, QuestionIntervalTime INTEGER, IsSubmittedFlag TEXT, TextOptionIDSeq TEXT)"
              
              if !(database?.executeStatements(sql_stmt_copy))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created question_records_copy")
              }
             // userId,surveyId,uuid,sensorName,surveyName,surveyInstruction,surveyBriefDescription,surveyEndTime,localPushTime,maxPushNo,pushInterval,pushNotificationText,surveyIntroductoryQuestion
              
              //userId,surveyId,pushTime,pushNotificationText
              
              let sql_stmt_survey_details = "CREATE TABLE IF NOT EXISTS SURVEY_RECORDS (User_ID Integer,Survey_Seq_Num Integer, IS_SUBMITTED_LOCALLY BOOL,Multiple_Time_Submit BOOL, Survey_ID Integer, Survey_SerialNo Integer, Sensor_UUID TEXT, Sensor_Name TEXT, Survey_Name TEXT, Survey_Instruction TEXT, Survey_Brief_Description TEXT, Survey_End_Time TEXT, Survey_Start_Time TEXT, LocalPush_Time TEXT, Max_No_Push Integer, Push_Interval TEXT, Push_Notification_Text TEXT, Survey_Introductory_Question TEXT)"
              
              if !(database?.executeStatements(sql_stmt_survey_details))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created survey_records")
              }
              
              let sql_stmt_survey_basic_push = "CREATE TABLE IF NOT EXISTS SURVEY_BASIC_PUSH_RECORDS (User_ID Integer, Survey_ID Integer,Push_Time TEXT, Push_Notification_Text TEXT)"
              
              if !(database?.executeStatements(sql_stmt_survey_basic_push))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created survey_basic_push_records")
              }
              
              let sql_stmt5 = "CREATE TABLE IF NOT EXISTS UPLOAD_RECORDS ( Question_ID INTEGER, User_ID Integer , Question_Type TEXT, Answer_Text TEXT, SerialNo TEXT, SequenceNum INTEGER, Upload_Flag TEXT)"
              
              if !(database?.executeStatements(sql_stmt5))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created upload_records.")
                
              }
              
              let sql_stmt2 = "CREATE TABLE IF NOT EXISTS QUESTION_DROP_RECORDS ( SrNo INTEGER PRIMARY KEY AUTOINCREMENT, SerialNo INTEGER, Drop_Time TEXT, New_Seen_Time TEXT, SequenceNum INTEGER)"
              if !(database?.executeStatements(sql_stmt2))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created question_records.")
              }
              
                let sql_stmt1 = "CREATE TABLE IF NOT EXISTS ANSWER_RECORDS (ID INTEGER PRIMARY KEY AUTOINCREMENT,Question_ID INTEGER,Option_Text TEXT, Option_ID INTEGER, None_Label, Sequence_Number INTEGER, Min_Value INTEGER,Max_Value INTEGER,Char_Limit INTEGER, Frequency INTEGER, Original_Sequence INTEGER, Option_Text_ID TEXT, FOREIGN KEY (Question_ID) REFERENCES QUESTION_RECORDS (Question_ID))"
                if !(database?.executeStatements(sql_stmt1))! {
                    print("Error: \(database?.lastErrorMessage())")
                } else {
                    print("Success: Table Created answer_records.")
                    
                }
                
                let sql_stmtAns = "CREATE TABLE IF NOT EXISTS ANSWER_RECORDS_COPY (ID INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Question_ID INTEGER,Option_Text TEXT, Option_ID INTEGER, None_Label, Sequence_Number INTEGER, Min_Value INTEGER,Max_Value INTEGER,Char_Limit INTEGER, Frequency INTEGER,Original_Sequence INTEGER, Option_Text_ID TEXT, FOREIGN KEY (Question_ID) REFERENCES QUESTION_RECORDS (Question_ID))"
                if !(database?.executeStatements(sql_stmtAns))! {
                    print("Error: \(database?.lastErrorMessage())")
                } else {
                    print("Success: Table Created answer_records_copy.")
                    
                }
              
              let sql_stmtUpdate = "ALTER TABLE QUESTION_RECORDS ADD User_ID Integer"
              if !(database?.executeStatements(sql_stmtUpdate))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Altered question_records.")
              }
              
              let sql_stmtUpdate2 = "ALTER TABLE QUESTION_DROP_RECORDS ADD User_ID Integer"
              if !(database?.executeStatements(sql_stmtUpdate2))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Altered question_drop_records.")
              }
              
              let sql_stmtUpdate3 = "ALTER TABLE ANSWER_RECORDS ADD User_ID Integer"
              if !(database?.executeStatements(sql_stmtUpdate3))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Altered answer_records.")
              }
              
              let sql_stmt3 = "CREATE TABLE IF NOT EXISTS BLE_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Weight INTEGER,Updated_Time TEXT, Created_Time TEXT, Battery_Level TEXT, SequenceNum TEXT)"
              if !(database?.executeStatements(sql_stmt3))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created ble_records.")
              }
              
              let sql_stmt4 = "CREATE TABLE IF NOT EXISTS ROUTING_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Routing_ID INTEGER, Selected_Options TEXT, Action_Name TEXT, Next_Question_ID INTEGER, Question_ID INTEGER)"
              if !(database?.executeStatements(sql_stmt4))! {
                print("Error: \(database?.lastErrorMessage())")
              } else {
                print("Success: Table Created routing_records.")
              }
                
                let sql_stmtRouteCopy = "CREATE TABLE IF NOT EXISTS ROUTING_RECORDS_COPY (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Routing_ID INTEGER, Selected_Options TEXT, Action_Name TEXT, Next_Question_ID INTEGER, Question_ID INTEGER)"
                if !(database?.executeStatements(sql_stmtRouteCopy))! {
                    print("Error: \(database?.lastErrorMessage())")
                } else {
                    print("Success: Table Created routing_records_copy.")
                }
              
                let sql_stmt_Compression = "CREATE TABLE IF NOT EXISTS COMPRESSION_RECORDS ( User_ID Integer, Survey_ID Integer, Question_ID INTEGER, SerialNo INTEGER, SequenceNum INTEGER, video_localPath TEXT, asset_Type TEXT)"
                
                if !(database?.executeStatements(sql_stmt_Compression))! {
                    print("Error: \(database?.lastErrorMessage())")
                } else {
                    print("Success: Table Created compression_records.")
                }
                
                database?.close()
            } else {
              
                print("Error: \(database?.lastErrorMessage())")
            }
        }
      
      let database = FMDatabase(path : CreateDB.getDataBasePath())
      if (database?.open())!{
        let sql_stmtUpdate = "ALTER TABLE QUESTION_RECORDS ADD User_ID Integer"
        if !(database?.executeStatements(sql_stmtUpdate))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Altered question_records.")
        }
        
        let sql_stmtUpdate2 = "ALTER TABLE QUESTION_DROP_RECORDS ADD User_ID Integer"
        if !(database?.executeStatements(sql_stmtUpdate2))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Altered question_drop_records.")
        }
        
        let sql_stmtUpdate3 = "ALTER TABLE ANSWER_RECORDS ADD User_ID Integer"
        if !(database?.executeStatements(sql_stmtUpdate3))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Altered answer_records.")
        }
        
//        let sql_stmt3 = "CREATE TABLE IF NOT EXISTS BLE_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Weight INTEGER,Updated_Time TEXT, Created_Time TEXT, Battery_Level TEXT)"
        let sql_stmt3 = "CREATE TABLE IF NOT EXISTS BLE_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Survey_ID Integer,Weight INTEGER,Updated_Time TEXT, Created_Time TEXT, Battery_Level TEXT, SequenceNum TEXT)"
        if !(database?.executeStatements(sql_stmt3))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created ble_records.")
        }
        
        let sql_stmt4 = "CREATE TABLE IF NOT EXISTS ROUTING_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Routing_ID INTEGER, Selected_Options TEXT, Action_Name TEXT, Next_Question_ID INTEGER, Question_ID INTEGER)"
        if !(database?.executeStatements(sql_stmt4))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created routing_records.")
        }

        database?.close()
        UserDefaults.standard.setValue(3, forKey: DATABASE_VERSION_UD)
        UserDefaults.standard.synchronize()
      }
      else {
        
        print("Error: \(database?.lastErrorMessage())")
      }


    }
  
  
  class func createAndCheckDatabaseOfQuestionTable(){
    
    let databaseExists = FileManager.default.fileExists(atPath: CreateDB.getDataBasePath())
    
    if !databaseExists {
      let database = FMDatabase(path : CreateDB.getDataBasePath())
      
      if (database?.open())! {
//        let sql_stmt = "CREATE TABLE IF NOT EXISTS QUESTION_RECORDS ( Question_ID INTEGER PRIMARY KEY, User_ID Integer, Question_Text TEXT, Question_Type TEXT, Start_Time TEXT, End_Time TEXT, answerRequired BOOL, isAnswered BOOL, Answer_Text TEXT, isHintAvailable BOOL, HintText TEXT, Label TEXT, SerialNo INTEGER, AnswerId TEXT)"
        let sql_stmt = "CREATE TABLE IF NOT EXISTS QUESTION_RECORDS ( Question_ID INTEGER, User_ID Integer, Survey_ID Integer, Question_Text TEXT, Question_Type TEXT, Start_Time TEXT, End_Time TEXT, answerRequired BOOL, isAnswered BOOL, Answer_Text TEXT, isHintAvailable BOOL, HintText TEXT, Label TEXT, SerialNo INTEGER, AnswerId TEXT, SequenceNum INTEGER, Scale TEXT, QuestionIntervalTime INTEGER, IsSubmittedFlag TEXT, TextOptionIDSeq TEXT)"

        if !(database?.executeStatements(sql_stmt))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created question_records.")
          
        }
        
        let sql_stmt_copy = "CREATE TABLE IF NOT EXISTS QUESTION_RECORDS_COPY ( Question_ID INTEGER, User_ID Integer,Survey_ID Integer, Question_Text TEXT, Question_Type TEXT, Start_Time TEXT, End_Time TEXT, answerRequired BOOL, isAnswered BOOL, Answer_Text TEXT, isHintAvailable BOOL, HintText TEXT, Label TEXT, SerialNo INTEGER, AnswerId TEXT, SequenceNum INTEGER, Scale TEXT, QuestionIntervalTime INTEGER, IsSubmittedFlag TEXT, TextOptionIDSeq TEXT)"
        
        if !(database?.executeStatements(sql_stmt_copy))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created question_records_copy")
        }
        
        let sql_stmt_survey_details = "CREATE TABLE IF NOT EXISTS SURVEY_RECORDS (User_ID Integer, Survey_Seq_Num Integer, IS_SUBMITTED_LOCALLY BOOL,Multiple_Time_Submit BOOL, Survey_ID Integer, Survey_SerialNo Integer, Sensor_UUID TEXT, Sensor_Name TEXT, Survey_Name TEXT, Survey_Instruction TEXT, Survey_Brief_Description TEXT, Survey_End_Time TEXT, Survey_Start_Time TEXT, LocalPush_Time TEXT, Max_No_Push Integer, Push_Interval TEXT, Push_Notification_Text TEXT, Survey_Introductory_Question TEXT)"
        
        if !(database?.executeStatements(sql_stmt_survey_details))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created survey_records")
        }
        
        let sql_stmt_survey_basic_push = "CREATE TABLE IF NOT EXISTS SURVEY_BASIC_PUSH_RECORDS (User_ID Integer, Survey_ID Integer,Push_Time TEXT, Push_Notification_Text TEXT)"
        
        if !(database?.executeStatements(sql_stmt_survey_basic_push))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created survey_basic_push_records")
        }

        
        let sql_stmt5 = "CREATE TABLE IF NOT EXISTS UPLOAD_RECORDS ( Question_ID INTEGER, User_ID Integer ,Survey_ID Integer, Question_Type TEXT, Answer_Text TEXT, SerialNo TEXT, SequenceNum INTEGER, Upload_Flag TEXT)"
        
        if !(database?.executeStatements(sql_stmt5))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created upload_records.")
          
        }

        
        let sql_stmt2 = "CREATE TABLE IF NOT EXISTS QUESTION_DROP_RECORDS ( SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, SerialNo INTEGER, Drop_Time TEXT, New_Seen_Time TEXT, SequenceNum INTEGER)"
        if !(database?.executeStatements(sql_stmt2))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created question_records.")
        }
        
          let sql_stmt1 = "CREATE TABLE IF NOT EXISTS ANSWER_RECORDS (ID INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Question_ID INTEGER,Option_Text TEXT, Option_ID INTEGER, None_Label, Sequence_Number INTEGER, Min_Value INTEGER,Max_Value INTEGER,Char_Limit INTEGER, Frequency INTEGER,Original_Sequence INTEGER, Option_Text_ID TEXT, FOREIGN KEY (Question_ID) REFERENCES QUESTION_RECORDS (Question_ID))"
        if !(database?.executeStatements(sql_stmt1))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created answer_records.")
          
        }
        
        let sql_stmtAns = "CREATE TABLE IF NOT EXISTS ANSWER_RECORDS_COPY (ID INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Question_ID INTEGER,Option_Text TEXT, Option_ID INTEGER, None_Label, Sequence_Number INTEGER, Min_Value INTEGER,Max_Value INTEGER,Char_Limit INTEGER, Frequency INTEGER,Original_Sequence INTEGER, Option_Text_ID TEXT, FOREIGN KEY (Question_ID) REFERENCES QUESTION_RECORDS (Question_ID))"
        if !(database?.executeStatements(sql_stmtAns))! {
            print("Error: \(database?.lastErrorMessage())")
        } else {
            print("Success: Table Created answer_records_copy.")
            
        }
        
//        let sql_stmt3 = "CREATE TABLE IF NOT EXISTS BLE_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Weight INTEGER,Updated_Time TEXT, Created_Time TEXT, Battery_Level TEXT)"
        let sql_stmt3 = "CREATE TABLE IF NOT EXISTS BLE_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer, Survey_ID Integer, Weight INTEGER,Updated_Time TEXT, Created_Time TEXT, Battery_Level TEXT, SequenceNum TEXT)"

        if !(database?.executeStatements(sql_stmt3))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created ble_records.")
        }
        
        let sql_stmt4 = "CREATE TABLE IF NOT EXISTS ROUTING_RECORDS (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Routing_ID INTEGER, Selected_Options TEXT, Action_Name TEXT, Next_Question_ID INTEGER, Question_ID INTEGER)"
        if !(database?.executeStatements(sql_stmt4))! {
          print("Error: \(database?.lastErrorMessage())")
        } else {
          print("Success: Table Created routing_records.")
        }

        let sql_stmtRouteCopy = "CREATE TABLE IF NOT EXISTS ROUTING_RECORDS_COPY (SrNo INTEGER PRIMARY KEY AUTOINCREMENT, User_ID Integer,Survey_ID Integer, Routing_ID INTEGER, Selected_Options TEXT, Action_Name TEXT, Next_Question_ID INTEGER, Question_ID INTEGER)"
        if !(database?.executeStatements(sql_stmtRouteCopy))! {
            print("Error: \(database?.lastErrorMessage())")
        } else {
            print("Success: Table Created routing_records_copy.")
        }
        
        let sql_stmt_Compression = "CREATE TABLE IF NOT EXISTS COMPRESSION_RECORDS ( User_ID Integer, Survey_ID Integer, Question_ID INTEGER, SerialNo INTEGER, SequenceNum INTEGER, video_localPath TEXT, asset_Type TEXT)"
        
        if !(database?.executeStatements(sql_stmt_Compression))! {
            print("Error: \(database?.lastErrorMessage())")
        } else {
            print("Success: Table Created compression_records.")
        }

        
        database?.close()
        UserDefaults.standard.setValue(3, forKey: DATABASE_VERSION_UD)
        UserDefaults.standard.synchronize()
      } else {
        print("Error: \(database?.lastErrorMessage())")
      }
    }
  }
  
    class func insertForQuestions(_ details : [String : AnyObject]) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            let sql_stmt = "INSERT INTO QUESTION_RECORDS (Question_ID , User_ID, Question_Text , Question_Type , Start_Time , End_Time , answerRequired , isAnswered , Answer_Text , isHintAvailable , HintText, Label ,SerialNo, Scale) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            do {
              try database?.executeUpdate(sql_stmt, values: [details["ques_id"]!,details["user_id"]!,details["quest_text"]!,details["quest_type"]!, details["start_time"]!,details["end_time"]!,details["answerRequired"]!,details["isAnswered"]!,details["answer_text"]!,details["isHintAvailabel"]!,details["hintText"]!,details["labelText"]!,details["SerialNo"]!,details["Scale"]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    
    class func createAndCheckDatabaseOfAnswerTable(){
        
        var myNewDictArray = [[String:String]]()
        
        let dict1 = ["id"               : "1",
                     "ques_id"          : "1",
                     "optn_text"        : "Lorem ipsum.",
                     "sequ_no"          : "1" ]
        
        let dict2 = ["id"               : "1",
                     "ques_id"          : "1",
                     "optn_text"        : "Lorem.",
                     "sequ_no"          : "2" ]
        
        let dict3 = ["id"               : "1",
                     "ques_id"          : "1",
                     "optn_text"        : "Lorem ipsum Lorem ipsum.",
                     "sequ_no"          : "3" ]
        
        let dict4 = ["id"               : "1",
                     "ques_id"          : "1",
                     "optn_text"        : "Ipsum.",
                     "sequ_no"          : "4" ]
        
        
        let dict5 = ["id"               : "1",
                     "ques_id"          : "2",
                     "optn_text"        : "Ipsum MB.",
                     "sequ_no"          : "1" ]
        
        let dict6 = ["id"               : "1",
                     "ques_id"          : "2",
                     "optn_text"        : "Ipsum Ipsum HEHE.",
                     "sequ_no"          : "2" ]
        
        let dict7 = ["id"               : "1",
                     "ques_id"          : "2",
                     "optn_text"        : "Ipsum. Hu- La - La",
                     "sequ_no"          : "3" ]
        
        let dict8 = ["id"               : "1",
                     "ques_id"          : "2",
                     "optn_text"        : "Ipsum. HHHHEEEEHHAAA",
                     "sequ_no"          : "4" ]
        
        let dict9 = ["id"               : "1",
                     "ques_id"          : "2",
                     "optn_text"        : "Ipsum No laughing",
                     "sequ_no"          : "5" ]
        
        
        
        let dict10 = ["id"               : "1",
                      "ques_id"          : "3",
                      "optn_text"        : "18",
                      "sequ_no"          : "1" ]
        
        let dict11 = ["id"               : "1",
                      "ques_id"          : "3",
                      "optn_text"        : "30",
                      "sequ_no"          : "2" ]
        
        let dict12 = ["id"               : "1",
                      "ques_id"          : "4",
                      "optn_text"        : "",
                      "sequ_no"          : "0" ]
        
        
        
        let dict13 = ["id"               : "1",
                      "ques_id"          : "5",
                      "optn_text"        : "True",
                      "sequ_no"          : "1" ]
        
        let dict14 = ["id"               : "1",
                      "ques_id"          : "5",
                      "optn_text"        : "False",
                      "sequ_no"          : "2" ]
        
        let dict15 = ["id"               : "1",
                      "ques_id"          : "6",
                      "optn_text"        : "Ipsum",
                      "sequ_no"          : "1" ]
        
        let dict16 = ["id"               : "1",
                      "ques_id"          : "6",
                      "optn_text"        : "Lorem",
                      "sequ_no"          : "2" ]
        
        let dict17 = ["id"               : "1",
                      "ques_id"          : "6",
                      "optn_text"        : "Aldus",
                      "sequ_no"          : "3" ]
        
        let dict18 = ["id"               : "1",
                      "ques_id"          : "7",
                      "optn_text"        : "Lorem Ipsum",
                      "sequ_no"          : "1" ]
        
        let dict19 = ["id"               : "1",
                      "ques_id"          : "7",
                      "optn_text"        : "Ipsum Ipsum",
                      "sequ_no"          : "2" ]
        
        let dict20 = ["id"               : "1",
                      "ques_id"          : "7",
                      "optn_text"        : "Lorem Lorem",
                      "sequ_no"          : "3" ]
        
        let dict21 = ["id"               : "1",
                      "ques_id"          : "7",
                      "optn_text"        : "LP Lorem",
                      "sequ_no"          : "4" ]
        
        let dict22 = ["id"               : "1",
                      "ques_id"          : "8",
                      "optn_text"        : "100",
                      "sequ_no"          : "1" ]
        
        let dict23 = ["id"               : "1",
                      "ques_id"          : "8",
                      "optn_text"        : "200",
                      "sequ_no"          : "2" ]
        
        myNewDictArray.append(dict1)
        myNewDictArray.append(dict2)
        myNewDictArray.append(dict3)
        myNewDictArray.append(dict4)
        myNewDictArray.append(dict5)
        myNewDictArray.append(dict6)
        myNewDictArray.append(dict7)
        myNewDictArray.append(dict8)
        myNewDictArray.append(dict9)
        myNewDictArray.append(dict10)
        myNewDictArray.append(dict11)
        myNewDictArray.append(dict12)
        myNewDictArray.append(dict13)
        myNewDictArray.append(dict14)
        myNewDictArray.append(dict15)
        myNewDictArray.append(dict16)
        myNewDictArray.append(dict17)
        myNewDictArray.append(dict18)
        myNewDictArray.append(dict19)
        myNewDictArray.append(dict20)
        myNewDictArray.append(dict21)
        myNewDictArray.append(dict22)
        myNewDictArray.append(dict23)
        
        for dict in myNewDictArray
        {
            let result = CreateDB.insertForAnswers(dict as [String : AnyObject])
            print(result)
        }
    }
    
    class func insertForAnswers(_ details : [String : AnyObject]) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            let sql_stmt = "INSERT INTO ANSWER_RECORDS (User_ID, Question_ID , Option_Text, Option_ID , None_Label , Sequence_Number) values (?,?,?,?,?,?)"
            do {
                try database?.executeUpdate(sql_stmt, values: [details["user_id"]!,details["ques_id"]!,details["optn_text"]!,details["optn_id"]!,details["none_label"]!,details["sequ_no"]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
}
