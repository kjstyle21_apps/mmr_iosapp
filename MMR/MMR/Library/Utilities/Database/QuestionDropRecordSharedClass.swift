//
//  QuestionDropRecordSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 12/28/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class QuestionDropRecordSharedClass: NSObject {
  
  class var sharedInstance :QuestionDropRecordSharedClass {
    struct Singleton {
      static let instance = QuestionDropRecordSharedClass()
    }
    return Singleton.instance
  }

  func insertForQuestions(_ details : [String : AnyObject], type : Int) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//      let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
      let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
      if type == 0{
        let sql_stmt = "INSERT INTO QUESTION_DROP_RECORDS (SerialNo , Drop_Time, User_ID, SequenceNum, Survey_ID) values (?,?,?,?,?)"
        do {
          try database?.executeUpdate(sql_stmt, values: [details[serial_id]!, details[dropEndTime]!, userID!, seqNum, currentSurveyID!])
          return true
        } catch let error as NSError {
          print("failed: \(error.localizedDescription)")
        }
      }
      else if type == 1 {
        let sql_stmt = "INSERT INTO QUESTION_DROP_RECORDS (SerialNo , New_Seen_Time , Drop_Time, User_ID, SequenceNum, Survey_ID) values (?,?,?,?,?,?)"
        do {
          try database?.executeUpdate(sql_stmt, values: [details[serial_id]!, details[answerStartTime]!, details[dropEndTime]!,userID!,seqNum,currentSurveyID!])
          return true
        } catch let error as NSError {
          print("failed: \(error.localizedDescription)")
        }
      }
      else if type == 2 {
        let sql_stmt = "INSERT INTO QUESTION_DROP_RECORDS (SerialNo , New_Seen_Time, User_ID, SequenceNum, Survey_ID) values (?,?,?,?,?)"
        do {
          try database?.executeUpdate(sql_stmt, values: [details[serial_id]!, details[answerStartTime]!, userID!, seqNum, currentSurveyID!])
          return true
        } catch let error as NSError {
          print("failed: \(error.localizedDescription)")
        }
      }
    }
    return false
  }
  
  //check for dropOut time exists or not
  func isDropOutTimeAvailable(_ serialID : String, userID : AnyObject) -> String {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
//    let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
    let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID)")
    let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT Drop_Time FROM QUESTION_DROP_RECORDS WHERE SerialNo =? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?", values: [serialID,userID,seqNum,currentSurveyID!])
      var count = 0
      while (rs?.next())! {
        count = count + 1
      }
      
      if count > 0 {
        print("Exists")
        return "Exists"
      } else {
        print("Empty Table")
        return "No_Exists"
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return "Error"
  }

  func fetchDropOutData(_ serialId : AnyObject, userID : AnyObject, seqNum : AnyObject,fetchedSurveyID: AnyObject) -> [NSMutableDictionary] {
    
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var dictInfoArray = [NSMutableDictionary] ()
    
    do {
      let rs = try database?.executeQuery("SELECT * FROM QUESTION_DROP_RECORDS WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?", values: [serialId,userID,seqNum,fetchedSurveyID])
      while (rs?.next())! {
        let dictInfo = NSMutableDictionary ()
        dictInfo.setValue(rs?.string(forColumn: "Drop_Time"), forKey:"dropoutTime")
        dictInfo.setValue(rs?.string(forColumn: "New_Seen_Time"), forKey:"newSeenTime")
        dictInfoArray.append(dictInfo)
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
      return dictInfoArray
  }
  
  func deleteQuestionDropDataOfUploadedSurvey(_ userID : AnyObject, fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
        var sql_stmt = "DELETE FROM QUESTION_DROP_RECORDS WHERE User_ID =? AND Survey_ID =?"
        if fetchedSurveyID as? String == "" {
            sql_stmt = "DELETE FROM QUESTION_DROP_RECORDS WHERE User_ID =?"

        }
      do {
        if fetchedSurveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,fetchedSurveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
}
