//
//  QuestionSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 11/9/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit


class QuestionSharedClass: NSObject {
    class var sharedInstance :QuestionSharedClass {
        struct Singleton {
            static let instance = QuestionSharedClass()
        }
        return Singleton.instance
    }
    
    func parseDictionary(_ questionDetails : [String : AnyObject], surveyIDFetched: AnyObject)
    {
        
        var parameterInfoOfQuestions = [String:AnyObject]()
        
        parameterInfoOfQuestions.updateValue(surveyIDFetched, forKey: surveyId)
        
        if let serialNo = questionDetails[serialNoOfQuestion] {
            
            parameterInfoOfQuestions.updateValue(serialNo, forKey: serial_id)
        }
        
        if let questID = questionDetails[questionId] {
            parameterInfoOfQuestions.updateValue(questID, forKey: ques_id)
            
        }
        
        if let questTypeID = questionDetails[questionTypeId]![questionType] {
            parameterInfoOfQuestions.updateValue(questTypeID as AnyObject, forKey: quest_type)
            
        }
        
        if let questTextArray = questionDetails[questionTexts] as? NSArray {
            if let questText = (questTextArray[0] as? AnyObject)?[questionText] {
                parameterInfoOfQuestions.updateValue(questText as AnyObject, forKey: quest_text)
            }
        }
        
//        if let questText = (questionDetails[questionTexts]![0] as AnyObject)[questionText] {
//            
//        }
        
        if let isRequired = (questionDetails[answerRequired])?.boolValue
        {
            parameterInfoOfQuestions.updateValue(isRequired as AnyObject, forKey: answerRequired)
            
        }
        else
        {
            parameterInfoOfQuestions.updateValue(false as AnyObject, forKey: answerRequired)
        }
      
      if let scale = questionDetails["scale"] as? String {
        parameterInfoOfQuestions.updateValue(scale as AnyObject, forKey: "Scale")
      }
      else {
        parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: "Scale")
      }
      
      if let intervalTime = questionDetails[questionIntervalTime] as? Int {
        parameterInfoOfQuestions.updateValue(intervalTime as AnyObject, forKey: "questionIntervalTime")
      }
      else {
        parameterInfoOfQuestions.updateValue(0 as AnyObject, forKey: "questionIntervalTime")
      }
      
      
        parameterInfoOfQuestions.updateValue(false as AnyObject, forKey: isHintAvailabel)
        parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: hintText)
        
        parameterInfoOfQuestions.updateValue(false as AnyObject, forKey: isAnswered)
        parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: answer_text)
        parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: answerStartTime)
        parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: answerEndTime)
        parameterInfoOfQuestions.updateValue("FALSE" as AnyObject, forKey: IsSubmittedFlag)


        if let questArray = questionDetails[questionTexts] as? NSArray {
            if let hintAvailable = (questArray[0] as AnyObject)[questionInstructions] {
                if hintAvailable != nil && (hintAvailable as! String) != ""
                {
                    
                    parameterInfoOfQuestions.updateValue(true as AnyObject, forKey: isHintAvailabel)
                    parameterInfoOfQuestions.updateValue(hintAvailable as AnyObject, forKey: hintText)
                    
                }
            }
        }
//        if let hintAvailable = (questionDetails[questionTexts]![0] as AnyObject)[questionInstructions]
//        {
//            if hintAvailable != nil && (hintAvailable as! String) != ""
//            {
//                
//                parameterInfoOfQuestions.updateValue(true as AnyObject, forKey: isHintAvailabel)
//                parameterInfoOfQuestions.updateValue(hintAvailable, forKey: hintText)
//
//            }
//        }
        
        if let questArray = questionDetails[questionTexts] as? NSArray {
            if let label = (questArray[0] as AnyObject)[label]
            {
                if label != nil && (label as AnyObject).string != ""
                {
                    //                parameterInfoOfQuestions.updateValue(true, forKey: isLabelAvailable)
                    parameterInfoOfQuestions.updateValue(label as AnyObject, forKey: labelText)
                }
                else
                {
                    parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: labelText)
                    
                }
            }
        }
//        if let label = (questionDetails[questionTexts]![0] as AnyObject)[label]
//        {
//            if label != nil && label?.string != ""
//            {
////                parameterInfoOfQuestions.updateValue(true, forKey: isLabelAvailable)
//                parameterInfoOfQuestions.updateValue(label, forKey: labelText)
//            }
//            else
//            {
//                parameterInfoOfQuestions.updateValue("" as AnyObject, forKey: labelText)
//
//            }
//        }
      
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      let tempQuestionID = parameterInfoOfQuestions[ques_id]
      let status = self.checkForQuestionPresentforUserID(userID! as AnyObject, questionID: tempQuestionID!,fetchedSurveyId: surveyIDFetched)
      
      if status {
        print("Already Present")
        var optionsArray : AnyObject?   = ((questionDetails["questionTexts"] as? NSArray)?.object(at: 0) as? NSDictionary)?.value(forKey: "options") as? [AnyObject] as AnyObject
        if optionsArray != nil{
        } else{
          optionsArray = [""] as? [AnyObject] as AnyObject
        }
        
        let questionId = questionDetails["questionId"] as! Int
        AnswerSharedClass.sharedInstance.parseDictionary(optionsArray as! [AnyObject], questionID: questionId, fetchedSurveyId: surveyIDFetched)
      }
      else {
        print("New entry")
//        let result = self.insertForQuestions(parameterInfoOfQuestions)
        //uncomment while next changes
        let result2 = self.insertForQuestionsCopy(parameterInfoOfQuestions)
        print(result2)
        if result2 == true
        {
          var optionsArray  = ((questionDetails["questionTexts"] as! NSArray).object(at: 0) as? NSDictionary)?.value(forKey: "options") as? [AnyObject]
            if let arr1 = questionDetails["questionTexts"] as? NSArray {
                if let obj1 = arr1.object(at: 0) as? NSDictionary {
                    if let obj2 = obj1.value(forKey: "options") as? [AnyObject] {
                        optionsArray = obj2
                    }
                }
            }
            
          if optionsArray != nil{
          } else{
            optionsArray = ["" as AnyObject]
          }

          let questionId = questionDetails["questionId"] as! Int
          AnswerSharedClass.sharedInstance.parseDictionary((optionsArray as? [AnyObject])!, questionID: questionId, fetchedSurveyId: surveyIDFetched)
        }
      }
      
      if let routingArray = questionDetails["routing"] as? [[String : AnyObject]] {
        for tempRoute in routingArray {
          self.insertForQuestionsRouting(tempRoute,fetchedSurveyId: surveyIDFetched)
        }
      }
      
    }
 
  func insertForQuestions(_ details : [String : AnyObject]) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
//        let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
      let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID!)")
        if (database?.open())! {
            let sql_stmt = "INSERT INTO QUESTION_RECORDS (Question_ID , User_ID,Survey_ID, Question_Text , Question_Type , Start_Time , End_Time , answerRequired , isAnswered , Answer_Text , isHintAvailable , HintText, Label , SerialNo, SequenceNum, Scale, QuestionIntervalTime, IsSubmittedFlag) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            do {
              try database?.executeUpdate(sql_stmt, values: [details[ques_id]!,userID!,details[surveyId]!,details[quest_text]!,details[quest_type]!, details[answerStartTime]!,details[answerEndTime]!,details[answerRequired]!,details[isAnswered]!,details[answer_text]!,details[isHintAvailabel]!,details[hintText]!,details[labelText]!,details[serial_id]!,seqNum, details["Scale"]!, details["questionIntervalTime"]!, details[IsSubmittedFlag]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
  
  func insertForQuestionsCopy(_ details : [String : AnyObject]) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    //        let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
    let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID!)")
    if (database?.open())! {
      let sql_stmt = "INSERT INTO QUESTION_RECORDS_COPY (Question_ID , User_ID, Survey_ID, Question_Text , Question_Type , Start_Time , End_Time , answerRequired , isAnswered , Answer_Text , isHintAvailable , HintText, Label , SerialNo, SequenceNum, Scale, QuestionIntervalTime, IsSubmittedFlag) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
      do {
        try database?.executeUpdate(sql_stmt, values: [details[ques_id]!,userID!,details[surveyId]!,details[quest_text]!,details[quest_type]!, details[answerStartTime]!,details[answerEndTime]!,details[answerRequired]!,details[isAnswered]!,details[answer_text]!,details[isHintAvailabel]!,details[hintText]!,details[labelText]!,details[serial_id]!,seqNum, details["Scale"]!, details["questionIntervalTime"]!, details[IsSubmittedFlag]!])
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
    func insertForQuestionsRouting(_ details : [String : AnyObject], fetchedSurveyId: AnyObject) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    if (database?.open())! {
      let sql_stmt = "INSERT INTO ROUTING_RECORDS (User_ID, Survey_ID, Routing_ID , Selected_Options , Action_Name , Next_Question_ID , Question_ID) values (?,?,?,?,?,?,?)"
      do {
        let selectedOptionStr = details[selectedOptions]!
        let trimmedString = selectedOptionStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        try database?.executeUpdate(sql_stmt, values: [userID!,fetchedSurveyId,details[routingID]!,trimmedString, details[actionName]!,details[nextQuestionID]!,details[questionId]!])
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }

  
    func checkForQuestionPresentforUserID(_ userID : AnyObject, questionID : AnyObject, fetchedSurveyId: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
//      let rs = try database.executeQuery("SELECT COUNT(*) from QUESTION_RECORDS WHERE User_ID =? AND Question_ID =?", values: [userID,questionID])
//      let count = rs.intForColumnIndex(0)
      let rs = try database?.executeQuery("SELECT * from QUESTION_RECORDS_COPY WHERE User_ID =? AND Question_ID =? AND Survey_ID =?", values: [userID,questionID,fetchedSurveyId])
      var count = 0
      while (rs?.next())! {
        count = count + 1
      }

      if count > 0 {
        print("Exists")
        return true
      } else {
        print("No exists")
        return false
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return false
  }
  
  func checkForQuestionPresentforUserIDCopy(_ userID : AnyObject, questionID : AnyObject, fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      //      let rs = try database.executeQuery("SELECT COUNT(*) from QUESTION_RECORDS WHERE User_ID =? AND Question_ID =?", values: [userID,questionID])
      //      let count = rs.intForColumnIndex(0)
      let rs = try database?.executeQuery("SELECT * from QUESTION_RECORDS_COPY WHERE User_ID =? AND Question_ID =? AND Survey_ID =?", values: [userID,questionID,fetchedSurveyID])
      var count = 0
      while (rs?.next())! {
        count = count + 1
      }
      
      if count > 0 {
        print("Exists")
        return true
      } else {
        print("No exists")
        return false
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return false
  }
  
  //Update AnswerText for a Question
    func updateAnswerTextForQuestion(_ answerText : String, serialNo : String, userID : AnyObject, uploadSeqNum : AnyObject, surveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    if (database?.open())! {
      let sql_stmt = "UPDATE QUESTION_RECORDS SET Answer_Text = ? WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?"
      do {
        try database?.executeUpdate(sql_stmt, values: [answerText,serialNo,userID,uploadSeqNum,surveyID])
        return true
      } catch let error as NSError {
        print("failed : \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func updateOptionTextIDSeqForQuestion(_ answerText : String, serialNo : String, userID : AnyObject, uploadSeqNum : AnyObject, surveyID: AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            let sql_stmt = "UPDATE QUESTION_RECORDS SET TextOptionIDSeq = ? WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?"
            do {
                try database?.executeUpdate(sql_stmt, values: [answerText,serialNo,userID,uploadSeqNum,surveyID])
                return true
            } catch let error as NSError {
                print("failed : \(error.localizedDescription)")
            }
        }
        return false
    }
  
  func getAnswerRowForCopy(_ userID : AnyObject, fetchedSurveyID: AnyObject) -> NSMutableDictionary {
    let returnDict = NSMutableDictionary()
    let serial_id : Int = BaseClassForQuestions.serialNo_ID
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * from QUESTION_RECORDS WHERE User_ID =? AND SerialNo =? AND Survey_ID =?", values: [userID,serial_id,fetchedSurveyID])
      while (rs?.next())! {
        let val1 = String(format: "%d", (rs?.int(forColumn: "Question_ID"))!)
        returnDict[ques_id] = "\(val1)"
        returnDict[quest_text] = rs?.string(forColumn: "Question_Text")
        returnDict[quest_type] = rs?.string(forColumn: "Question_Type")
        returnDict[answerRequired] = rs?.bool(forColumn: "answerRequired")
        returnDict[isAnswered] = rs?.bool(forColumn: "isAnswered")
        returnDict[isHintAvailabel] = rs?.bool(forColumn: "isHintAvailable")
        returnDict[hintText] = rs?.string(forColumn: "HintText")
        returnDict[label] = rs?.string(forColumn: "Label")
        let val2 = String(format: "%d", (rs?.int(forColumn: "SerialNo"))!)
        returnDict["serial_id"] = "\(val2)"
      }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return returnDict
  }
  
  //Update Out time for a Question
  func updateOutTimeForQuestion(_ outTime : String, serialNo : String, userID : AnyObject,fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
//    let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
    let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID)")
    if (database?.open())! {
      let sql_stmt = "UPDATE QUESTION_RECORDS SET End_Time = ? WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?"
      do {
          try database?.executeUpdate(sql_stmt, values: [outTime,serialNo,userID,seqNum,fetchedSurveyID])
          return true
          } catch let error as NSError {
        print("failed : \(error.localizedDescription)")
      }
    }
      return false
  }
  
  //Update In time for a Question
  func updateInTimeForQuestion(_ inTime : String, serialNo : String, userID : AnyObject,fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
//    let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
//    let seqNum = NSUserDefaults.standardUserDefaults().integerForKey("SURVEY_SEQ_NUM_\(userID)")
    let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)") as! Int

    if (database?.open())! {
      let sql_stmt = "UPDATE QUESTION_RECORDS SET Start_Time = ? WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?"
      do {
        try database?.executeUpdate(sql_stmt, values: [inTime,serialNo,userID,seqNum,fetchedSurveyID])
        return true
      } catch let error as NSError {
        print("failed : \(error.localizedDescription)")
      }
    }
    return false
  }
  
  //check for endTime exists or not
  func isStartTimeAvailable(_ serialID : String, userID : AnyObject,fetchedSurveyID: AnyObject) -> String {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
//    let seqNum = NSUserDefaults.standardUserDefaults().valueForKey("SURVEY_SEQ_NUM_\(userID)")
    let seqNum = UserDefaults.standard.integer(forKey: "SURVEY_SEQ_NUM_\(userID)")
    var count = 0
    database?.open()
      do {
         let rs = try database?.executeQuery("SELECT Start_Time from QUESTION_RECORDS WHERE SerialNo = ? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?", values: [serialID,userID,seqNum,fetchedSurveyID])
//          let count = rs.intForColumnIndex(0)
        while (rs?.next())! {
            if rs?.string(forColumn: "Start_Time") != "" {
                count = count + 1
            }
        }
          if count > 0 {
            print("Exists")
            return "Exists"
          } else {
            print("Empty Table")
            return "No_Exists"
        }
    }
    catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return "Error"
  }
  
    // Fetching question type as per Serial No.
  func fetchQuestionType(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> String
    {
        let serial_id : Int = BaseClassForQuestions.serialNo_ID
        var questiontype = ""
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)") as? Int
        database?.open()
        do {
//            let rs = try database.executeQuery("SELECT Question_Type FROM QUESTION_RECORDS_COPY WHERE SerialNo =? AND User_ID =? AND Survey_ID =?", values: [serial_id,userID,fetchedSurveyID])
            
            //@kj
            if (seqNum != nil){
                let rs = try database?.executeQuery("SELECT Question_Type FROM QUESTION_RECORDS WHERE SerialNo =? AND User_ID =? AND Survey_ID =? AND SequenceNum =?", values: [serial_id,userID,fetchedSurveyID, seqNum!])
                while (rs?.next())! {
                    
                    questiontype = (rs?.string(forColumn: "Question_Type"))!
                    print("questiontype\(questiontype)")
                    break
                }
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        return questiontype
    }
  
  
  func fetchMediaTypeDataForMultipleSubmit(_ userID : AnyObject, type : String) -> [NSMutableDictionary]
  {
//    let serial_id : Int = BaseClassForQuestions.serialNo_ID
//    var questiontype = ""
    var resultArray = [NSMutableDictionary]()
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE Question_Type =? AND Answer_Text !=? AND User_ID =?", values: [type,"NOT ANSWERED",userID])
      while (rs?.next())! {
          let resultDict = NSMutableDictionary()
        let val1 = String(format: "%d", (rs?.int(forColumn: "SerialNo"))!)
          resultDict.setValue("\(val1)", forKey: serial_id)
        let val2 = String(format: "%d", (rs?.int(forColumn: "SequenceNum"))!)
          resultDict.setValue("\(val2)", forKey: "SeqNum")
          resultDict.setValue(rs?.string(forColumn: "Question_Type"), forKey: quest_type)
        let val3 = String(format: "%d", (rs?.int(forColumn: "Survey_ID"))!)
            resultDict.setValue("\(val3)", forKey: surveyId)
          resultArray.append(resultDict)
//        break
      }
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return resultArray
  }

  func fetchRoutingForQuestionID(_ userId : String, questionID : NSNumber, fetchedSurveyID: AnyObject) -> [[String : String]] {
    var routingDetails = [[String : String]]()
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      
      let rs = try database?.executeQuery("SELECT Selected_Options, Action_Name, Next_Question_ID FROM ROUTING_RECORDS WHERE User_ID =? AND Question_ID =? AND Survey_ID =?", values: [userId, questionID,fetchedSurveyID])
      
      while (rs?.next())! {
        var tempRoute = [String : String]()
        tempRoute[selectedOptions] = rs?.string(forColumn: "Selected_Options")
        tempRoute[actionName] = rs?.string(forColumn: "Action_Name")
        let val1 = String(format: "%d", (rs?.int(forColumn: "Next_Question_ID"))!)
        tempRoute[nextQuestionID] = "\(val1)"
        routingDetails.append(tempRoute)
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return routingDetails
  }
  
    func deleteRoutingForQuestionID(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
        var sql_stmt = "DELETE FROM ROUTING_RECORDS WHERE User_ID =? AND Survey_ID =?"
        if surveyID as? String == "" {
            sql_stmt = "DELETE FROM ROUTING_RECORDS WHERE User_ID =?"
        }
      do {
        if surveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,surveyID])

        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func deleteRoutingExcludingCurrentSurvey(_ userID : AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        if (database?.open())! {
            let sql_stmt = "DELETE FROM ROUTING_RECORDS WHERE User_ID =? AND Survey_ID !=?"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID,surveyID!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
  
  func fetchDataForQuestions(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> NSMutableDictionary {
        
        let serial_id : Int = BaseClassForQuestions.serialNo_ID
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let dictInfo =  NSMutableDictionary ()
        do {
          //uncomment while next changes
            let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)")
//            let rs = try database.executeQuery("SELECT * FROM QUESTION_RECORDS_COPY WHERE SerialNo =? AND User_ID =? AND Survey_ID =?", values: [serial_id,userID,fetchedSurveyID])
            //@kj
            if (seqNum != nil){
                let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE SerialNo =? AND User_ID =? AND Survey_ID =? AND SequenceNum =?", values: [serial_id,userID,fetchedSurveyID,seqNum!])
                
                while (rs?.next())! {
                    var questionInfo = NSMutableArray ()
                    
                    dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"Question_ID")
                    dictInfo.setValue(rs?.string(forColumn: "Question_Text"), forKey:questionText)
                    dictInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey:"Question_Type")
                    dictInfo.setValue(rs?.string(forColumn: "answerRequired"), forKey:"answerRequired")
                    dictInfo.setValue(rs?.string(forColumn: "isHintAvailable"), forKey:"isHintAvailable")
                    dictInfo.setValue(rs?.string(forColumn: "HintText"), forKey:"hintText")
                    dictInfo.setValue(rs?.string(forColumn: "Label"), forKey:"labelText")
                    dictInfo.setValue(rs?.string(forColumn: "Scale"), forKey: "Scale")
                    let val1 = String(format: "%d", arguments: [(rs?.int(forColumn: "QuestionIntervalTime"))!])
                    dictInfo.setValue("\(val1)", forKey: "questionIntervalTime")
                    let val2 = String(format: "%d", arguments: [(rs?.int(forColumn: "Survey_ID"))!])
                    dictInfo.setValue("\(val2)", forKey: surveyId)
                    print("Dict\(dictInfo)")
                    break
                }
            }

//          let rs = try database.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE SerialNo =? AND User_ID =?", values: [serial_id,userID])

        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if dictInfo.count > 0 {
            return dictInfo
        }
        else {
            return ["" : ""]
        }
    }
  
  func isQuestionAvailable(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> [NSMutableDictionary] {
    
    let serial_id : Int = BaseClassForQuestions.serialNo_ID
    let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)")
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    let dictInfo =  NSMutableDictionary ()
    var questionInfo = [NSMutableDictionary] ()
    do {
      let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE SerialNo =? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?", values: [serial_id,userID,seqNum!,fetchedSurveyID])
      while (rs?.next())! {
        
        
        dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"Question_ID")
        dictInfo.setValue(rs?.string(forColumn: "Question_Text"), forKey:questionText)
        dictInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey:"Question_Type")
        dictInfo.setValue(rs?.string(forColumn: "answerRequired"), forKey:"answerRequired")
        dictInfo.setValue(rs?.string(forColumn: "isHintAvailable"), forKey:"isHintAvailable")
        dictInfo.setValue(rs?.string(forColumn: "HintText"), forKey:"hintText")
        dictInfo.setValue(rs?.string(forColumn: "Label"), forKey:"labelText")
        questionInfo.append(dictInfo)
        
        print("Dict\(dictInfo)")
        break
      }
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
//    if questionInfo.count > 0 {
//      return questionInfo
//    }
//    else {
      return questionInfo
//    }
  }
  
    func compareTwoDates() -> Bool {
        let date0 = UserDefaults.standard.value(forKey: surveyEndTime) as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date1 = dateFormatter.date(from: date0)
        let date2 : Date = Date()
        let interval = date1!.timeIntervalSince(date2)
        
        print ("Interval : \(interval)")
        if interval > 0 {
            return true
        }
        return false
    }
  
  func compareTwoDatesforPush(_ pushdate : String) -> Bool {
    let date0 = UserDefaults.standard.value(forKey: surveyEndTime) as! String
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    let date1 = dateFormatter.date(from: date0)
    
    let dateFormatter2 = DateFormatter()
    dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
    dateFormatter2.timeZone = TimeZone(identifier: "UTC")
    let date2 = dateFormatter2.date(from: pushdate)
    
    let interval = date1!.timeIntervalSince(date2!)
    print ("Interval : \(interval)")
    
    if interval > 0 {
      return true
    }
    return false
  }
  
  func compareBeforeDatesforPush(_ pushdate : String) -> Bool {
//    let date0 = NSUserDefaults.standardUserDefaults().valueForKey(surveyEndTime) as! String
//    let dateFormatter = NSDateFormatter()
//    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
//    dateFormatter.timeZone = NSTimeZone(name: "UTC")
//    let date1 = dateFormatter.dateFromString(date0)
    
    let date1 = Date()
    
    let dateFormatter2 = DateFormatter()
    dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
    dateFormatter2.timeZone = TimeZone(identifier: "UTC")
    let date2 = dateFormatter2.date(from: pushdate)
    
    let interval = date2!.timeIntervalSince(date1)
    print ("Interval : \(interval)")
    
    if interval > 0 {
      return true
    }
    return false
  }
    
  func getDateInUTC(_ date : Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//    dateFormatter.timeZone = NSTimeZone(name: "UTC")
//    let date = dateFormatter.dateFromString(dateStr)
    let tempdateStr = dateFormatter.string(from: date)
    return tempdateStr
  }
    func getSurveyDateInUTC(_ date : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        //    dateFormatter.timeZone = NSTimeZone(name: "UTC")
        //    let date = dateFormatter.dateFromString(dateStr)
        let tempdateStr = dateFormatter.string(from: date)
        return tempdateStr
    }
    

  
    func getHTMLTextAndRetriveNormalText (_ string : String) -> NSAttributedString {
        let myAttribute = [ NSAttributedStringKey.font: UIFont(name: TAHOMA_REGULAR, size: 14.0)! ]

        let options:[NSAttributedString.DocumentReadingOptionKey : Any] = [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html]

        let strStr = try? NSMutableAttributedString(data: string.data(using: .utf8)!, options: options, documentAttributes: nil)
        
//        let theAttributedString = try! NSMutableAttributedString(data: string.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                          options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:String.Encoding.utf8],
//                                                          documentAttributes: nil)
        return strStr!
        
        
        
//        guard let data = data(using: String.Encoding.utf8, allowLossyConversion: false) else {
//            print("Unable to decode data from html string: \(self)")
//            return completionBlock(nil)
//        }
//        let options = [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType]
//        DispatchQueue.main.async {
//            if let attributedString = try? NSAttributedString(data: data, options:
//                options, documentAttributes: nil) {
//                completionBlock(attributedString)
//            } else {
//                print("Unable to create attributed string from html string: \(self)")
//                completionBlock(nil)
//            }
//        }

        
        
    }
  
  func fetchQuestionData(_ userID : AnyObject, seqNum : AnyObject, surveySubmitStatus : Bool) -> [NSMutableDictionary] {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var dictInfoArray = [NSMutableDictionary] ()

    do {
      let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE USER_ID = ?", values: [userID])
      while (rs?.next())! {
        var dictInfo = NSMutableDictionary ()
        dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"questionId")
        dictInfo.setValue(rs?.string(forColumn: "User_ID"), forKey:"userId")
        var ans = rs?.string(forColumn: "Answer_Text")
        if ans == ""{
          ans = "-"
        }
//        dictInfo.setValue(rs.stringForColumn("Answer_Text"), forKey:"answerText")
        dictInfo.setValue(ans, forKey:"answerText")
        //@kj
        var seenTime = rs?.string(forColumn: "Start_Time")
        if seenTime != "" {
        dictInfo.setValue(seenTime, forKey:"questionSeenTime")
        }else{
            if let newSeenTime = UserDefaults.standard.value(forKey: FIRST_QUESTION_SEEN_TIME) as? String {
                    dictInfo.setValue(newSeenTime, forKey:"questionSeenTime")
            }
        }
        
        var endTime = rs?.string(forColumn: "End_Time")
        if endTime != "" {
        dictInfo.setValue(rs?.string(forColumn: "End_Time"), forKey:"questionAnsweredTime")
        }else{
            if let newEndTime = UserDefaults.standard.value(forKey: LAST_QUESTION_ANSWER_TIME) as? String {
                dictInfo.setValue(newEndTime, forKey:"questionAnsweredTime")
            }
        }
        
        let serailID = rs?.string(forColumn: "SerialNo")
        let seqNum2 = rs?.int(forColumn: "SequenceNum")
        let val1 = String(format: "%d", seqNum2!)
        dictInfo.setValue("\(val1)", forKey: "submissionCount")
        
        if let quesType = rs?.string(forColumn: "Question_Type") {
            if quesType == "bullseye" {
                if let tempoptionTextIdSeq = rs?.string(forColumn: "TextOptionIDSeq") {
                    dictInfo.setValue(tempoptionTextIdSeq, forKey: "textOptionIdSequence")
                }
            }
        }
        if let tempFlag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
          if tempFlag {
            let seqNumUD = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)") as! Int
            if Int(seqNum2!) == seqNumUD {
              if surveySubmitStatus {
                dictInfo.setValue("true", forKey: "surveyComplete")
              }
              else {
                dictInfo.setValue("false", forKey: "surveyComplete")
              }
            }
            else {
              dictInfo.setValue("true", forKey: "surveyComplete")
            }
          }
          else {
            if surveySubmitStatus {
              dictInfo.setValue("true", forKey: "surveyComplete")
            }
            else {
              dictInfo.setValue("false", forKey: "surveyComplete")
            }
          }
        }
        else {
          if surveySubmitStatus {
            dictInfo.setValue("true", forKey: "surveyComplete")
          }
          else {
            dictInfo.setValue("false", forKey: "surveyComplete")
          }
        }
        dictInfo.setValue(MMRDeviceTypeiOS, forKey: MMRDeviceType)
        //fetch the dropout for serialID
        let val2 = String(format: "%d", (rs?.int(forColumn: "Survey_ID"))!)
        let surveyID = "\(val2)"
        let val3 = String(format: "%d", seqNum2!)
        let dropoutData = QuestionDropRecordSharedClass.sharedInstance.fetchDropOutData(serailID as AnyObject, userID: userID, seqNum: "\(val3)" as AnyObject,fetchedSurveyID: surveyID as AnyObject)
        if (dropoutData ).count > 0 {
          dictInfo.setValue(dropoutData, forKey: "dropouts")
        }
        dictInfoArray.append(dictInfo)
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    if dictInfoArray.count > 0 {
      return dictInfoArray
    }
    else {
      return [["":""]]
    }
  }
    
    func fetchQuestionDataForAvailable(_ userID : AnyObject, seqNum : AnyObject, surveySubmitStatus : Bool) -> [NSMutableDictionary] {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        var dictInfoArray = [NSMutableDictionary] ()
        
        do {
            let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE USER_ID = ? AND Answer_Text !=?", values: [userID,""])
            while (rs?.next())! {
                var dictInfo = NSMutableDictionary ()
                dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"questionId")
                dictInfo.setValue(rs?.string(forColumn: "User_ID"), forKey:"userId")
                var ans = rs?.string(forColumn: "Answer_Text")
                if ans == ""{
                    ans = "-"
                }
                //        dictInfo.setValue(rs.stringForColumn("Answer_Text"), forKey:"answerText")
                dictInfo.setValue(ans, forKey:"answerText")
                //@kj
                var seenTime = rs?.string(forColumn: "Start_Time")
                if seenTime != "" {
                    dictInfo.setValue(seenTime, forKey:"questionSeenTime")
                }else{
                    if let newSeenTime = UserDefaults.standard.value(forKey: FIRST_QUESTION_SEEN_TIME) as? String {
                        dictInfo.setValue(newSeenTime, forKey:"questionSeenTime")
                    }
                }
                
                var endTime = rs?.string(forColumn: "End_Time")
                if endTime != "" {
                    dictInfo.setValue(rs?.string(forColumn: "End_Time"), forKey:"questionAnsweredTime")
                }else{
                    if let newEndTime = UserDefaults.standard.value(forKey: LAST_QUESTION_ANSWER_TIME) as? String {
                        dictInfo.setValue(newEndTime, forKey:"questionAnsweredTime")
                    }
                }
                
                let serailID = rs?.string(forColumn: "SerialNo")
                let seqNum2 = rs?.int(forColumn: "SequenceNum")
                let val1 = String(format: "%d", seqNum2!)
                dictInfo.setValue("\(val1)", forKey: "submissionCount")
                
                if let quesType = rs?.string(forColumn: "Question_Type") {
                    if quesType == "bullseye" {
                        if let tempoptionTextIdSeq = rs?.string(forColumn: "TextOptionIDSeq") {
                            dictInfo.setValue(tempoptionTextIdSeq, forKey: "textOptionIdSequence")
                        }
                    }
                }
                if let tempFlag = UserDefaults.standard.bool(forKey: MULTIPLE_FLAG_UD) as? Bool {
                    if tempFlag {
                        let seqNumUD = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)") as! Int
                        if Int(seqNum2!) == seqNumUD {
                            if surveySubmitStatus {
                                dictInfo.setValue("true", forKey: "surveyComplete")
                            }
                            else {
                                dictInfo.setValue("false", forKey: "surveyComplete")
                            }
                        }
                        else {
                            dictInfo.setValue("true", forKey: "surveyComplete")
                        }
                    }
                    else {
                        if surveySubmitStatus {
                            dictInfo.setValue("true", forKey: "surveyComplete")
                        }
                        else {
                            dictInfo.setValue("false", forKey: "surveyComplete")
                        }
                    }
                }
                else {
                    if surveySubmitStatus {
                        dictInfo.setValue("true", forKey: "surveyComplete")
                    }
                    else {
                        dictInfo.setValue("false", forKey: "surveyComplete")
                    }
                }
                dictInfo.setValue(MMRDeviceTypeiOS, forKey: MMRDeviceType)
                //fetch the dropout for serialID
                let val2 = String(format: "%d", (rs?.int(forColumn: "Survey_ID"))!)
                let surveyID = "\(val2)"
                let val3 = String(format: "%d", seqNum2!)
                let dropoutData = QuestionDropRecordSharedClass.sharedInstance.fetchDropOutData(serailID as AnyObject, userID: userID, seqNum: "\(val3)" as AnyObject,fetchedSurveyID: surveyID as AnyObject)
                if (dropoutData ).count > 0 {
                    dictInfo.setValue(dropoutData, forKey: "dropouts")
                }
                dictInfoArray.append(dictInfo)
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if dictInfoArray.count > 0 {
            return dictInfoArray
        }
        else {
            return [["":""]]
        }
    }
    
    func getDummyJsonifFailsToCreate() -> [NSMutableDictionary] {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        database?.open()
        var dictInfoArray = [NSMutableDictionary] ()
        
        do {
            let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS_COPY WHERE USER_ID = ? AND Survey_ID =?", values: [userID!,surveyID!])
            while (rs?.next())! {
                let dictInfo = NSMutableDictionary ()
                dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"questionId")
                dictInfo.setValue(rs?.string(forColumn: "User_ID"), forKey:"userId")
                var ans = rs?.string(forColumn: "Answer_Text")
                if ans == ""{
                    ans = "-"
                }
                dictInfo.setValue(ans, forKey:"answerText")
                //@kj
                let seenTime = rs?.string(forColumn: "Start_Time")
                if seenTime != "" {
                    dictInfo.setValue(seenTime, forKey:"questionSeenTime")
                }else{
                    if let newSeenTime = UserDefaults.standard.value(forKey: FIRST_QUESTION_SEEN_TIME) as? String {
                        dictInfo.setValue(newSeenTime, forKey:"questionSeenTime")
                    }
                }
                
                let endTime = rs?.string(forColumn: "End_Time")
                if endTime != "" {
                    dictInfo.setValue(rs?.string(forColumn: "End_Time"), forKey:"questionAnsweredTime")
                }else{
                    if let newEndTime = UserDefaults.standard.value(forKey: LAST_QUESTION_ANSWER_TIME) as? String {
                        dictInfo.setValue(newEndTime, forKey:"questionAnsweredTime")
                    }
                }
                
                let serailID = rs?.string(forColumn: "SerialNo")
                dictInfo.setValue("1", forKey: "submissionCount")
                dictInfo.setValue("false", forKey: "surveyComplete")
                
                dictInfo.setValue(MMRDeviceTypeiOS, forKey: MMRDeviceType)
                dictInfoArray.append(dictInfo)
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if dictInfoArray.count > 0 {
            return dictInfoArray
        }
        else {
            return [["":""]]
        }
    }
  
  func fetchVideoOrImageURLs(_ userID : AnyObject, type : String) -> [NSMutableDictionary] {
    
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var dictInfoArray = [NSMutableDictionary]()
    
    do {
      let rs = try database?.executeQuery("SELECT Answer_Text, SerialNo FROM QUESTION_RECORDS WHERE USER_ID = ? AND Question_Type = ? AND Answer_Text != ?", values: [userID, type, "NOT ANSWERED"])
      while (rs?.next())! {
        let dictInfo = NSMutableDictionary()
        dictInfo.setValue(rs?.string(forColumn: "Answer_Text"), forKey:answer_text)
        dictInfo.setValue(rs?.string(forColumn: "SerialNo"), forKey:serial_id)
        dictInfoArray.append(dictInfo)
        }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return dictInfoArray
  }
  
  func deleteQuestionDataOfUploadedSurvey(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
        var sql_stmt = "DELETE FROM QUESTION_RECORDS WHERE User_ID =? AND Survey_ID =?"
        if fetchedSurveyID as? String == "" {
            sql_stmt = "DELETE FROM QUESTION_RECORDS WHERE User_ID =?"
        }
      do {
        if fetchedSurveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,fetchedSurveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
    func deleteQuestionData_CopyOfUploadedSurvey(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
        var sql_stmt = "DELETE FROM QUESTION_RECORDS_COPY WHERE User_ID =? AND Survey_ID =?"
        if surveyID as? String == "" {
            sql_stmt = "DELETE FROM QUESTION_RECORDS_COPY WHERE User_ID =?"
        }
      do {
        if surveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func deleteQuestionData_CopyExcludingCurrentSurvey(_ userID : AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        if (database?.open())! {
            let sql_stmt = "DELETE FROM QUESTION_RECORDS_COPY WHERE User_ID =? AND Survey_ID !=?"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID,surveyID!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
  
  func deleteImagesAndVideos(_ userID : String) {
    
    var urlArray = [String]()
    let videoArray = QuestionSharedClass.sharedInstance.fetchVideoOrImageURLs(userID as AnyObject, type: "videoChoice")
    print(videoArray)
    for video in videoArray {
      let ansText = video.value(forKey: "answer_text")
      let serialNo = video.value(forKey: "serial_id")
      let ansArr = (ansText as! String).components(separatedBy: "^]`,")
      for ans in ansArr {
        if ans != "" {
//          let timestamp : Double = ((NSDate().timeIntervalSince1970 * 1000) )
//          let imgName = "\(userID)/\(timestamp)"
//          let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
//          let imgURL = NSURL(fileURLWithPath: "\(documentsPath)/\(ans)")
//          print("Image URL : \(imgURL)")
          urlArray.append(ans)
        }
      }
    }
    
    let imageArray = QuestionSharedClass.sharedInstance.fetchVideoOrImageURLs(userID as AnyObject, type: "pictureChoice")
    print(imageArray)
    
    for image in imageArray {
      let ansText = image.value(forKey: "answer_text")
      let serialNo = image.value(forKey: "serial_id")
      let ansArr = (ansText as! String).components(separatedBy: "^]`,")
      for ans in ansArr {
        if ans != "" {
//          let timestamp : Double = ((NSDate().timeIntervalSince1970 * 1000) )
//          let imgName = "\(userID)/\(timestamp)"
//          let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
//          let imgURL = NSURL(fileURLWithPath: "\(documentsPath)/\(ans)")
//          print("Image URL : \(imgURL)")
          urlArray.append(ans)
        }
      }
    }
    
    for url in urlArray {
      do {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(url)")
        _ = try FileManager.default.removeItem(at: dataPath1)
        print("Item deleted Succesfully")
      } catch {
        print(error)
      }
    }
  }

    func copyAllQuestionsIntoQuestionsRecord(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> [NSMutableDictionary] {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        var questionInfoArray = [NSMutableDictionary]()
        database?.open()
        do {
            //uncomment while next changes
            let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS_COPY WHERE User_ID =? AND Survey_ID =?", values: [userID,fetchedSurveyID])
            
            while (rs?.next())! {
                let questionInfo = NSMutableDictionary ()
                questionInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:ques_id)
                let val3 = rs?.int(forColumn: "Survey_ID")
                questionInfo.setValue(String(format: "%d", arguments: [val3!]), forKey: surveyId)
                questionInfo.setValue(rs?.string(forColumn: "Question_Text"), forKey:quest_text)
                questionInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey:quest_type)
                questionInfo.setValue(rs?.string(forColumn: "Start_Time"), forKey: answerStartTime)
                questionInfo.setValue(rs?.string(forColumn: "End_Time"), forKey: answerEndTime)
                questionInfo.setValue(rs?.string(forColumn: "answerRequired"), forKey:answerRequired)
                questionInfo.setValue(rs?.string(forColumn: "isAnswered"), forKey:isAnswered)
                questionInfo.setValue(rs?.string(forColumn: "Answer_Text"), forKey:answer_text)
                questionInfo.setValue(rs?.string(forColumn: "isHintAvailable"), forKey:isHintAvailabel)
                questionInfo.setValue(rs?.string(forColumn: "HintText"), forKey:hintText)
                questionInfo.setValue(rs?.string(forColumn: "Label"), forKey:labelText)
                let val2 = rs?.int(forColumn: "SerialNo")
                questionInfo.setValue(String(format: "%d", arguments: [val2!]), forKey: serial_id)
                let serialNo = String(describing: rs?.int(forColumn: "SerialNo"))
//                let serialNo = "\(rs?.int(forColumn: "SerialNo"))"
                questionInfo.setValue(rs?.string(forColumn: "Scale"), forKey: "Scale")
                let val = rs?.int(forColumn: "QuestionIntervalTime")
                questionInfo.setValue(String(format: "%d", arguments: [val!]), forKey: "questionIntervalTime")
                questionInfo.setValue(rs?.string(forColumn: "IsSubmittedFlag"), forKey:IsSubmittedFlag)
                print("Dict\(questionInfo)")
                questionInfoArray.append(questionInfo)
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        return questionInfoArray
        database?.close()
    }
    
    
    func checkIsAvailableAndThenInsert(_ arrayOfQuestion: [NSMutableDictionary]) {
//        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
//        database.open()
//        database.beginTransaction()
        for tempQues in arrayOfQuestion {
            let serialNo = tempQues.value(forKey: serial_id)
            let result = QuestionSharedClass.sharedInstance.isQuestionAvailableInQuestionRecords(userID! as AnyObject, fetchedSurveyID: curSurveyID! as AnyObject, serialNo: serialNo! as AnyObject)
            if result.count == 0 {
                self.insertForQuestions((tempQues as? [String : AnyObject])!)
            }
        }
//        database.commit()
//        database.close()

    }
    
    func isQuestionAvailableInQuestionRecords(_ userID : AnyObject,fetchedSurveyID: AnyObject,serialNo: AnyObject) -> [NSMutableDictionary] {
        let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID)")
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        let dictInfo =  NSMutableDictionary ()
        var questionInfo = [NSMutableDictionary] ()
        do {
            let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE SerialNo =? AND User_ID =? AND SequenceNum = ? AND Survey_ID =?", values: [serialNo,userID,seqNum!,fetchedSurveyID])
            while (rs?.next())! {
                dictInfo.setValue(rs?.string(forColumn: "Question_ID"), forKey:"Question_ID")
                dictInfo.setValue(rs?.string(forColumn: "Question_Text"), forKey:questionText)
                dictInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey:"Question_Type")
                dictInfo.setValue(rs?.string(forColumn: "answerRequired"), forKey:"answerRequired")
                dictInfo.setValue(rs?.string(forColumn: "isHintAvailable"), forKey:"isHintAvailable")
                dictInfo.setValue(rs?.string(forColumn: "HintText"), forKey:"hintText")
                dictInfo.setValue(rs?.string(forColumn: "Label"), forKey:"labelText")
                questionInfo.append(dictInfo)
                print("Dict\(dictInfo)")
                break
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
//        database.close()
        return questionInfo
    }
    
    func copyAllRoutingDataIntoRoutingRecordsCopy(_ userID : AnyObject,fetchedSurveyID: AnyObject) -> [NSMutableDictionary] {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        var optionsInfoArray = [NSMutableDictionary]()
        database?.open()
        do {
            //uncomment while next changes
            let rs = try database?.executeQuery("SELECT * FROM ROUTING_RECORDS WHERE User_ID =? AND Survey_ID =?", values: [userID,fetchedSurveyID])
            while (rs?.next())! {
                let optionsInfo = NSMutableDictionary ()
                let val1 = String(format: "%d", (rs?.int(forColumn: "Routing_ID"))!)
                optionsInfo.setValue("\(val1)", forKey: routingID)
                optionsInfo.setValue(rs?.string(forColumn: "Selected_Options"), forKey:selectedOptions)
                optionsInfo.setValue(rs?.string(forColumn: "Action_Name"), forKey:actionName)
                let val2 = String(format: "%d", (rs?.int(forColumn: "Next_Question_ID"))!)
                optionsInfo.setValue("\(val2)", forKey: nextQuestionID)
                let val3 = String(format: "%d", (rs?.int(forColumn: "Question_ID"))!)
                optionsInfo.setValue("\(val3)", forKey: questionId)
                optionsInfoArray.append(optionsInfo)
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        return optionsInfoArray
        database?.close()
    }

    func checkIsRoutingAvailableAndThenInsert(_ arrayOfQuestion: [NSMutableDictionary]) {
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        let curSurveyID = UserDefaults.standard.value(forKey: surveyId)
        for tempQues in arrayOfQuestion {
            let routID = tempQues.value(forKey: routingID)
            let quesID = tempQues.value(forKey: questionId)
            let result = self.isRoutingAvailableInRoutingRecordsCopy(userID! as AnyObject, fetchedSurveyID: curSurveyID! as AnyObject, routingID: routID! as AnyObject, questionid: quesID! as AnyObject)
            if result == 0 {
                self.insertForRoutingCopy((tempQues as? [String : AnyObject])!, fetchedSurveyId: curSurveyID! as AnyObject)
            }
        }
    }

    func isRoutingAvailableInRoutingRecordsCopy(_ userID : AnyObject,fetchedSurveyID: AnyObject, routingID: AnyObject, questionid: AnyObject) -> Int {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        var count = 0
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM ROUTING_RECORDS_COPY WHERE User_ID =? AND Survey_ID =? AND Question_ID = ? AND Routing_ID =?", values: [userID,fetchedSurveyID,questionid,routingID])
            while (rs?.next())! {
                count = count + 1
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        return count
    }
    
    func insertForRoutingCopy(_ details : [String : AnyObject], fetchedSurveyId: AnyObject) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        if (database?.open())! {
            let sql_stmt = "INSERT INTO ROUTING_RECORDS_COPY (User_ID, Survey_ID, Routing_ID , Selected_Options , Action_Name , Next_Question_ID , Question_ID) values (?,?,?,?,?,?,?)"
            do {
                let selectedOptionStr = details[selectedOptions]!
                let trimmedString = selectedOptionStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                try database?.executeUpdate(sql_stmt, values: [userID!,fetchedSurveyId,details[routingID]!,trimmedString, details[actionName]!,details[nextQuestionID]!,details[questionId]!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    func deleteRoutingCopy(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            var sql_stmt = "DELETE FROM ROUTING_RECORDS_COPY WHERE User_ID =? AND Survey_ID =?"
            if surveyID as? String == "" {
                sql_stmt = "DELETE FROM ROUTING_RECORDS_COPY WHERE User_ID =?"
            }
            do {
                if surveyID as? String == "" {
                    try database?.executeUpdate(sql_stmt, values: [userID])
                } else {
                    try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
                    
                }
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func fetchQuestionCount(_ userID : AnyObject, seqNum : AnyObject, surveyId : AnyObject) -> Int {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        var returnResult = 0
        do {
            let rs = try database?.executeQuery("SELECT * FROM QUESTION_RECORDS WHERE USER_ID = ? AND Survey_ID = ?", values: [userID, surveyId])
            while (rs?.next())! {
                returnResult = returnResult + 1
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        return returnResult
    }
}
