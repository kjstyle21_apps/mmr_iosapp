//
//  SurveyRecordsSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 7/3/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class SurveyRecordsSharedClass: NSObject {
  class var sharedInstance :SurveyRecordsSharedClass {
    struct Singleton {
      static let instance = SurveyRecordsSharedClass()
    }
    return Singleton.instance
  }
  
  func parseSurveyDetails(_ userInfo : [String:AnyObject]) {
    
    
    let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
    
    if let arrayOfSurveys = userInfo[assignedSurvey] as? [[String : AnyObject]] {
    for fetchedSurvey in arrayOfSurveys {
        var paramsDict = [String : AnyObject]()
        
    if let surveyID = fetchedSurvey[surveyId]  {
      paramsDict[surveyId] = surveyID
    }
        let result = self.isTheSurveyPresentThenGetSeqNum(userID! as AnyObject, surveyID: paramsDict[surveyId]!)
        if result == "" {
            UserDefaults.standard.set(1, forKey: "SURVEY_SEQ_NUM_\(userID!)")
            paramsDict["surveySeqNum"] = 1 as AnyObject
        } else {
            let num = Int(result)
            UserDefaults.standard.set(num!, forKey: "SURVEY_SEQ_NUM_\(userID!)")
            paramsDict["surveySeqNum"] = result as AnyObject
        }
        
    if let surveySerialNum = fetchedSurvey[surveySerialNo] {
        paramsDict[surveySerialNo] = surveySerialNum
    } else {
        paramsDict[surveySerialNo] = 1 as AnyObject
        }
    
    UserDefaults.standard.set(false, forKey: NoSurveyForUser)
    
    if let surveySensorUUID = fetchedSurvey[uuid] as? String {
      paramsDict[uuid] = surveySensorUUID as AnyObject
    }
    else {
      paramsDict[uuid] = "" as AnyObject
    }
    
    if let surveySensorName = fetchedSurvey[sensorName] as? String {
      paramsDict[sensorName] = surveySensorName as AnyObject
    }
    else {
      paramsDict[sensorName] = "" as AnyObject
    }
    
    if let nameOfSurvey = fetchedSurvey[surveyName]  {
      paramsDict[surveyName] = nameOfSurvey
    }
    
    if let surveyInstructions = fetchedSurvey[surveyInstruction] {
      paramsDict[surveyInstruction] = surveyInstructions
    }
    
    if let surveyDescription = fetchedSurvey[surveyBriefDescription] {
      paramsDict[surveyBriefDescription] = surveyDescription
    }
    if let endTimeOfSurvey = fetchedSurvey[surveyEndTime]  {
      paramsDict[surveyEndTime] = endTimeOfSurvey
    }
    if let startTimeOfSurvey = fetchedSurvey[surveyStartTime]  {
        paramsDict[surveyStartTime] = startTimeOfSurvey
    }
    if (UserDefaults.standard.value(forKey: CURRENT_SENSOR_UUID_UD) != nil) && (UserDefaults.standard.value(forKey: CURRENT_SENSOR_NAME_UD) != nil) {
      
      if let localPushTimeforSurvey = fetchedSurvey[localPushTime]  {
        paramsDict[localPushTime] = localPushTimeforSurvey
      } else {
            paramsDict[localPushTime] = "" as AnyObject
        }
      
      if let maxPushNoOfSurvey = fetchedSurvey[maxPushNo]  {
        paramsDict[maxPushNo] = maxPushNoOfSurvey
      } else {
            paramsDict[maxPushNo] = 0 as AnyObject
        }
      
      if let pushIntervalOfSurvey = fetchedSurvey[pushInterval]  {
        paramsDict[pushInterval] = pushIntervalOfSurvey
      } else {
            paramsDict[pushInterval] = "" as AnyObject
        }
      
      if let pushNotificationTextOfSurvey = fetchedSurvey[pushNotificationText]  {
        paramsDict[pushNotificationText] = pushNotificationTextOfSurvey
      } else {
            paramsDict[pushNotificationText] = "" as AnyObject
        }
    }
    else {
      paramsDict[pushNotificationText] = "" as AnyObject
      paramsDict[pushInterval] = "" as AnyObject
      paramsDict[maxPushNo] = 0 as AnyObject
      paramsDict[localPushTime] = "" as AnyObject
    }
    
    if let multipleTimeSubmitFlag = fetchedSurvey[multipleTimeSubmit] as? Bool {
      paramsDict[multipleTimeSubmit] = multipleTimeSubmitFlag as AnyObject
    }
    else {
      paramsDict[multipleTimeSubmit] = false as AnyObject
    }
    
    if let surveyIntroQuestion = fetchedSurvey[surveyIntroductoryQuestion]  {
      paramsDict[surveyIntroductoryQuestion] = surveyIntroQuestion
    }
    
        //check whether the survey already present with localSubmitFLag = true
        //If yes then skip the insertion
        //else hit a delete query for the survey and insert new record
        
        if !self.isTheSurveyPresentWithLocalSubmitFlagTrue(userID! as AnyObject, surveyID: paramsDict[surveyId]!) {
            paramsDict[isSurveySubmittedLocally] = false as AnyObject
            self.deleteSurveyDetails(userID! as AnyObject, surveyID: paramsDict[surveyId]!)
            self.insertForSurveyDetails(paramsDict)
        }

    if let tempPushArray = fetchedSurvey[surveyLocalPush] as? [[String : AnyObject]] {
      for tempPushObj in tempPushArray {
        var pushParamsDict = [String : AnyObject]()
        pushParamsDict[pushNotificationText] = tempPushObj[pushNotificationText]
        pushParamsDict[pushTime] = tempPushObj[pushTime]
        pushParamsDict[surveyId] = tempPushObj[surveyId]
        self.insertForBasicPushDetails(pushParamsDict)
      }
    }
    }
        self.fetchDataForSurvey(userID! as AnyObject)
        if self.isAnySurveyPresentWithLocalSubmitFlagFalse(userID! as AnyObject) == true {
//            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "NEED_TO_SUBMIT_CURRENT_SURVEY_\(userID!)")
        }
    }
  }
  
  func insertForSurveyDetails(_ details : [String : AnyObject]) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      
      //      let sql_stmt_survey_details = "CREATE TABLE IF NOT EXISTS SURVEY_RECORDS (User_ID Integer,Survey_Seq_Num Integer, Multiple_Time_Submit BOOL, Survey_ID Integer, Sensor_UUID TEXT, Sensor_Name TEXT, Survey_Name TEXT, Survey_Instruction TEXT, Survey_Brief_Description TEXT, Survey_End_Time TEXT, LocalPush_Time TEXT, Max_No_Push TEXT, Push_Interval TEXT, Push_Notification_Text TEXT, Survey_Introductory_Question TEXT)"
      
      
      let sql_stmt = "INSERT INTO SURVEY_RECORDS (User_ID, Survey_Seq_Num ,  IS_SUBMITTED_LOCALLY,Multiple_Time_Submit, Survey_ID, Survey_SerialNo, Sensor_UUID , Sensor_Name, Survey_Name , Survey_Instruction , Survey_Brief_Description , Survey_End_Time, Survey_Start_Time, LocalPush_Time, Max_No_Push, Push_Interval, Push_Notification_Text,Survey_Introductory_Question) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
      do {
        try database?.executeUpdate(sql_stmt, values: [userID!, details["surveySeqNum"]!,details[isSurveySubmittedLocally]!,details[multipleTimeSubmit]!,details[surveyId]!, details[surveySerialNo]!,details[uuid]!,details[sensorName]!,details[surveyName]!,details[surveyInstruction]!,details[surveyBriefDescription]!,details[surveyEndTime]!,details[surveyStartTime]!,details[localPushTime]!,details[maxPushNo]!,details[pushInterval]!,details[pushNotificationText]!,details[surveyIntroductoryQuestion]!])
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
    func updateFlagOfLocalSubmit(_ surveyID: AnyObject, userID: AnyObject, flag: Bool) {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            let sql_stmt = "UPDATE SURVEY_RECORDS SET IS_SUBMITTED_LOCALLY =? WHERE User_ID =? AND Survey_ID =?"
            do {
                try database?.executeUpdate(sql_stmt, values: [flag,userID,surveyID])
            } catch let error as NSError {
                print("failed : \(error.localizedDescription)")
            }
        }
    }
    
    
  func insertForBasicPushDetails(_ details : [String : AnyObject]) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      
      //      let sql_stmt_survey_basic_push = "CREATE TABLE IF NOT EXISTS SURVEY_BASIC_PUSH_RECORDS (User_ID Integer, Survey_ID Integer,Push_Time TEXT, Push_Notification_Text TEXT)"
      
      
      let sql_stmt = "INSERT INTO SURVEY_BASIC_PUSH_RECORDS (User_ID, Survey_ID, Push_Time , Push_Notification_Text) values (?,?,?,?)"
      do {
        try database?.executeUpdate(sql_stmt, values: [userID!, details[surveyId]!,details[pushTime]!,details[pushNotificationText]!])
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
    func clearUD() {
        let userID = UserDefaults.standard.value(forKey: currentUserId)
        UserDefaults.standard.removeObject(forKey: MULTIPLE_FLAG_UD)
        UserDefaults.standard.removeObject(forKey: surveyId)
        UserDefaults.standard.removeObject(forKey: surveyName)
        UserDefaults.standard.removeObject(forKey: surveyInstruction)
        UserDefaults.standard.removeObject(forKey: surveyBriefDescription)
        UserDefaults.standard.removeObject(forKey: surveyEndTime)
        UserDefaults.standard.removeObject(forKey: surveyStartTime)
        UserDefaults.standard.removeObject(forKey: surveyIntroductoryQuestion)
    }
  func fetchDataForSurvey( _ userID : AnyObject) -> Bool {
    var returnResult = false
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * FROM SURVEY_RECORDS WHERE User_ID =? AND IS_SUBMITTED_LOCALLY =? ORDER BY Survey_SerialNo ASC", values: [userID,false])
      while (rs?.next())! {
        let suySeqNum = rs?.int(forColumn: "Survey_Seq_Num")
        let multipleFlag = rs?.bool(forColumn: "Multiple_Time_Submit")
        let surveyID = rs?.int(forColumn: "Survey_ID")
        let sensoruuid = rs?.string(forColumn: "Sensor_UUID")
        let sensorname = rs?.string(forColumn: "Sensor_Name")
        let surveyname = rs?.string(forColumn: "Survey_Name")
        let surveyinstruction = rs?.string(forColumn: "Survey_Instruction")
        let surveybriefdesc = rs?.string(forColumn: "Survey_Brief_Description")
        let surveyendtime = rs?.string(forColumn: "Survey_End_Time")
        let surveystarttime = rs?.string(forColumn: "Survey_Start_Time")
        let localpushtime = rs?.string(forColumn: "LocalPush_Time")
        let maxpush = rs?.int(forColumn: "Max_No_Push")
        let pushinterval = rs?.string(forColumn: "Push_Interval")
        let pushnotifytext = rs?.string(forColumn: "Push_Notification_Text")
        let surveyintroques = rs?.string(forColumn: "Survey_Introductory_Question")
        
        UserDefaults.standard.set(Int(suySeqNum!), forKey: "SURVEY_SEQ_NUM_\(userID)")
        UserDefaults.standard.set(multipleFlag, forKey: MULTIPLE_FLAG_UD)
        UserDefaults.standard.setValue(Int(surveyID!), forKey: surveyId)
        if sensoruuid != "" {
          UserDefaults.standard.setValue(sensoruuid, forKey: CURRENT_SENSOR_UUID_UD)
        }
        if sensorname != "" {
          UserDefaults.standard.setValue(sensorname, forKey: CURRENT_SENSOR_NAME_UD)
        }
        UserDefaults.standard.setValue(surveyname, forKey: surveyName)
        UserDefaults.standard.setValue(surveyinstruction, forKey: surveyInstruction)
        UserDefaults.standard.setValue(surveybriefdesc, forKey: surveyBriefDescription)
        UserDefaults.standard.setValue(surveyendtime, forKey: surveyEndTime)
        UserDefaults.standard.setValue(surveystarttime, forKey: surveyStartTime)
        if localpushtime != "" {
          UserDefaults.standard.setValue(localpushtime, forKey: localPushTime)
        }
        if maxpush != 0 {
          UserDefaults.standard.setValue(Int(maxpush!), forKey: maxPushNo)
        }
        if pushinterval != "" {
          UserDefaults.standard.setValue(pushinterval, forKey: pushInterval)
        }
        if pushnotifytext != "" {
          UserDefaults.standard.setValue(pushnotifytext, forKey: pushNotificationText)
        }
        UserDefaults.standard.setValue(surveyintroques, forKey: surveyIntroductoryQuestion)
        UserDefaults.standard.synchronize()
        returnResult = true
        break
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return returnResult
  }
  
    func isAnySurveyPresent(_ userID : AnyObject) -> Bool {
        var returnResult = false
        var count = 0
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM SURVEY_RECORDS WHERE User_ID =?", values: [userID])
            while (rs?.next())! {
                count  = count + 1
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if count > 0 {
            returnResult = true
        } else {
            returnResult = false
        }
        return returnResult
    }
    
    func isTheSurveyPresentWithLocalSubmitFlagTrue(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
        var returnResult = false
        var count = 0
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM SURVEY_RECORDS WHERE User_ID =? AND Survey_ID =? AND IS_SUBMITTED_LOCALLY =?", values: [userID,surveyID,true])
            while (rs?.next())! {
                count  = count + 1
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if count > 0 {
            returnResult = true
        } else {
            returnResult = false
        }
        return returnResult
    }
    
    func isAnySurveyPresentWithLocalSubmitFlagFalse(_ userID : AnyObject) -> Bool {
        var returnResult = false
        var count = 0
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM SURVEY_RECORDS WHERE User_ID =? AND IS_SUBMITTED_LOCALLY =?", values: [userID,false])
            while (rs?.next())! {
                count  = count + 1
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if count > 0 {
            returnResult = true
        } else {
            returnResult = false
        }
        return returnResult
    }
    
    func isTheSurveyPresentThenGetSeqNum(_ userID : AnyObject, surveyID: AnyObject) -> String {
        var returnResult = ""
        var count = 0
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        do {
            let rs = try database?.executeQuery("SELECT * FROM SURVEY_RECORDS WHERE User_ID =? AND Survey_ID =?", values: [userID,surveyID])
            while (rs?.next())! {
                count  = count + 1
                let val1 = String(format: "%d", (rs?.int(forColumn: "Survey_Seq_Num"))!)
                returnResult = "\(val1)"
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        if count == 0 {
            returnResult = ""
        }
        return returnResult
    }
  
    func fetchDataForBasicPush( _ userID : AnyObject, surveyID: AnyObject) {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    do {
      let rs = try database?.executeQuery("SELECT * FROM SURVEY_BASIC_PUSH_RECORDS WHERE User_ID =? AND Survey_ID =?", values: [userID,surveyID])
      while (rs?.next())! {
        let timeforPush = rs?.string(forColumn: "Push_Time")
        let notifyText = rs?.string(forColumn: "Push_Notification_Text")
        let tempPushObj = [pushTime : timeforPush, pushNotificationText : notifyText]
        self.basicLocalPushHandler(tempPushObj as [String : AnyObject])
      }
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
  }
  
  
  func basicLocalPushHandler(_ pushObj : [String:AnyObject]) {
    //    surveyLocalPush =             (
    //      {
    //        id = 0;
    //        pushNotificationText = "test push";
    //        pushTime = "2017-06-13T22:10:48.0+0000";
    //        surveyId = 527;
    //      }
    //    );
    
    if let temppushtime = pushObj[pushTime] as? String {
      
      if QuestionSharedClass.sharedInstance.compareTwoDatesforPush(temppushtime)  && QuestionSharedClass.sharedInstance.compareBeforeDatesforPush(temppushtime) {
        
        let pushTime = temppushtime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let actualPushdate = dateFormatter.date(from: pushTime)
        
        if let tempPushText = pushObj[pushNotificationText] as? String {
          let pushText = tempPushText
          
          let localNotification = UILocalNotification()
          localNotification.alertBody = pushText
            self.logger(msg: "\n\(pushText) : \(actualPushdate)\n")
          if #available(iOS 8.2, *) {
            localNotification.alertTitle = "MMR"
          } else {
            // Fallback on earlier versions
          }
          let diff = actualPushdate!.timeIntervalSince(Date())
          localNotification.fireDate = Date(timeIntervalSinceNow: Double(diff))
          UIApplication.shared.scheduleLocalNotification(localNotification)
        }
      }
    }
  }
  
  func updateSeqNumOfSurvey(_ seqNum : AnyObject, surveyID : AnyObject, userID : AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    if (database?.open())! {
      let sql_stmt = "UPDATE SURVEY_RECORDS SET Survey_Seq_Num = ? WHERE Survey_ID = ? AND User_ID =?"
      do {
        try database?.executeUpdate(sql_stmt, values: [seqNum,surveyID,userID])
        return true
      } catch let error as NSError {
        print("failed : \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func updateStartTimeOfSurvey(_ stTime: AnyObject, surveyID: AnyObject, userID: AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            let sql_stmt = "UPDATE SURVEY_RECORDS SET Survey_Start_Time = ? WHERE Survey_ID = ? AND User_ID =?"
            do {
                try database?.executeUpdate(sql_stmt, values: [stTime,surveyID,userID])
                return true
            } catch let error as NSError {
                print("failed : \(error.localizedDescription)")
            }
        }
        return false
    }
  
    func deleteSurveyDetails(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    if (database?.open())! {
        var sql_stmt = "DELETE FROM SURVEY_RECORDS WHERE User_ID =? AND Survey_ID =?"

        if surveyID as? String == "" {
            sql_stmt = "DELETE FROM SURVEY_RECORDS WHERE User_ID =? AND Survey_ID =?"
        }
      do {
        if surveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
    
    func deleteSurveyDetailsWithLocalSubmitFlagTrue(_ userID : AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            let sql_stmt = "DELETE FROM SURVEY_RECORDS WHERE User_ID =? AND IS_SUBMITTED_LOCALLY =?"
            do {
                try database?.executeUpdate(sql_stmt, values: [userID,true])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func deleteSurveyBasicPushDetails(_ userID : AnyObject, surveyID: AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        if (database?.open())! {
            var sql_stmt = "DELETE FROM SURVEY_BASIC_PUSH_RECORDS WHERE User_ID =? AND Survey_ID =?"
            if surveyID as? String == "" {
                sql_stmt = "DELETE FROM SURVEY_BASIC_PUSH_RECORDS WHERE User_ID =?"
            }
            do {
                if surveyID as? String == "" {
                try database?.executeUpdate(sql_stmt, values: [userID])
                } else {
                    try database?.executeUpdate(sql_stmt, values: [userID,surveyID])
                }
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
    
    func deleteSurveyBasicPushDetailsExcludingCurrentSurvey(_ userID : AnyObject) -> Bool {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        let surveyID = UserDefaults.standard.value(forKey: surveyId)
        if (database?.open())! {
            let sql_stmt = "DELETE FROM SURVEY_BASIC_PUSH_RECORDS WHERE User_ID =? AND Survey_ID !=?"
            do {
                    try database?.executeUpdate(sql_stmt, values: [userID,surveyID!])
                return true
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        return false
    }
  
    func compareTwoDatesAndGetOInterval(_ pushdate : String) -> TimeInterval {
        
//        let date0 = NSUserDefaults.standardUserDefaults().valueForKey(surveyEndTime) as! String
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
//        dateFormatter.timeZone = NSTimeZone(name: "UTC")
//        let date1 = dateFormatter.dateFromString(date0)
        let currentTime = Date()
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter2.timeZone = TimeZone(identifier: "UTC")
        let date2 = dateFormatter2.date(from: pushdate)
        
        let interval = date2!.timeIntervalSince(currentTime)
        
        print ("Interval : \(interval)")
        return interval
//        if interval > 0 {
//            return true
//        }
//        return false
    }
    
    func logger(msg: String) {
        //Get the file path
        var documentsDirectory: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        var fileName: String = (URL(string: "\(NSHomeDirectory())/Documents/mmrLog.txt")?.absoluteString)!
        //create file if it doesn't exist
        if !FileManager.default.fileExists(atPath: fileName) {
            FileManager.default.createFile(atPath: fileName, contents: nil, attributes: nil)
        }
        //append text to file (you'll probably want to add a newline every write)
        var file: FileHandle? = FileHandle(forUpdatingAtPath: fileName)
        file?.seekToEndOfFile()
        file?.write(msg.data(using: String.Encoding.utf8)!)
        file?.closeFile()
    }
}
