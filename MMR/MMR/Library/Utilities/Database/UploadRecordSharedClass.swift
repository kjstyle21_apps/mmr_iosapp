//
//  UploadRecordSharedClass.swift
//  MMR
//
//  Created by Mindbowser on 4/12/17.
//  Copyright © 2017 Mindbowser. All rights reserved.
//

import UIKit

class UploadRecordSharedClass: NSObject {
  class var sharedInstance :UploadRecordSharedClass {
    struct Singleton {
      static let instance = UploadRecordSharedClass()
    }
    return Singleton.instance
  }
  
  func insertuploadData(_ details : NSMutableDictionary, counter : Int, fetchedSurveyID: AnyObject) -> Bool
  {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
      
      if userID != nil{
        
//        let sql_stmt5 = "CREATE TABLE IF NOT EXISTS UPLOAD_RECORDS ( Question_ID INTEGER, User_ID Integer Question_Type TEXT, Answer_Text TEXT, SerialNo INTEGER, SequenceNum INTEGER, Upload_Flag TEXT)"

        
        let sql_stmt = "INSERT INTO UPLOAD_RECORDS (Question_ID, User_ID , Question_Type, Answer_Text, SerialNo, SequenceNum, Upload_Flag, Survey_ID) values (?,?,?,?,?,?,?,?)"
        do {
          try database?.executeUpdate(sql_stmt, values: [details["Question_ID"]!, userID!, details["Question_Type"]!, details[answer_text]!,"\(details[serial_id]!)_\(counter)", details["SeqNum"]!, details["Upload_Flag"]!, fetchedSurveyID])
          return true
        } catch let error as NSError {
          print("failed: \(error.localizedDescription)")
        }
      }
    }
    return false
  }
    
    func insertuploadDataUpdated(_ details : NSMutableDictionary, fetchedSurveyID: AnyObject) -> Bool
    {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        
        if (database?.open())! {
            let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
            
            if userID != nil{
                
                //        let sql_stmt5 = "CREATE TABLE IF NOT EXISTS UPLOAD_RECORDS ( Question_ID INTEGER, User_ID Integer Question_Type TEXT, Answer_Text TEXT, SerialNo INTEGER, SequenceNum INTEGER, Upload_Flag TEXT)"
                
                
                let sql_stmt = "INSERT INTO UPLOAD_RECORDS (Question_ID, User_ID , Question_Type, Answer_Text, SerialNo, SequenceNum, Upload_Flag, Survey_ID) values (?,?,?,?,?,?,?,?)"
                do {
                    try database?.executeUpdate(sql_stmt, values: [details["Question_ID"]!, userID!, details["Question_Type"]!, details[answer_text]!,details[serial_id]!, details["SeqNum"]!, details["Upload_Flag"]!, fetchedSurveyID])
                    return true
                } catch let error as NSError {
                    print("failed: \(error.localizedDescription)")
                }
            }
        }
        return false
    }
  
  func fetchVideoOrImageURLsFromUploadRecords(_ userID : AnyObject, type : String) -> [NSMutableDictionary] {
    
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var dictInfoArray = [NSMutableDictionary]()
    
    do {
      let rs = try database?.executeQuery("SELECT * FROM UPLOAD_RECORDS WHERE USER_ID = ? AND Question_Type = ? OR Question_Type = ?", values: [userID, "videoChoice", "pictureChoice"])
      while (rs?.next())! {
        let dictInfo = NSMutableDictionary()
        if rs?.string(forColumn: "Upload_Flag") == "FALSE" {
        let val1 = String(format: "%d", (rs?.int(forColumn: "Question_ID"))!)
        dictInfo.setValue("\(val1)", forKey:ques_id)
        let val2 = String(format: "%d", (rs?.int(forColumn: "User_ID"))!)
        dictInfo.setValue("\(val2)", forKey:"USER_ID")
        dictInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey: quest_type)
        dictInfo.setValue(rs?.string(forColumn: "Answer_Text"), forKey: answer_text)
        dictInfo.setValue(rs?.string(forColumn: "SerialNo"), forKey: serial_id)
        let val3 = String(format: "%d", (rs?.int(forColumn: "SequenceNum"))!)
        dictInfo.setValue("\(val3)", forKey: "SeqNum")
        let val4 = String(format: "%d", (rs?.int(forColumn: "Survey_ID"))!)
        dictInfo.setValue("\(val4)", forKey: surveyId)
        dictInfoArray.append(dictInfo)
      }
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return dictInfoArray
  }
    
    func getCountOfNotUploadedRecords(_ userID : AnyObject, type : String) -> Int {
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        var returnCount = 0
        do {
            let rs = try database?.executeQuery("SELECT * FROM UPLOAD_RECORDS WHERE USER_ID = ? AND Question_Type = ? OR Question_Type = ?", values: [userID, "videoChoice", "pictureChoice"])
            while (rs?.next())! {
                if rs?.string(forColumn: "Upload_Flag") == "FALSE" {
                    returnCount = returnCount + 1
                }
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
        return returnCount
    }
    
    func fetchVideoOrImageURLsFromUploadRecordsToVerify(_ userID : AnyObject) {
        
        let database = FMDatabase(path : CreateDB.getDataBasePath())
        database?.open()
        
        do {
            let rs = try database?.executeQuery("SELECT * FROM UPLOAD_RECORDS WHERE USER_ID = ? AND Question_Type = ? OR Question_Type = ?", values: [userID, "videoChoice", "pictureChoice"])
            while (rs?.next())! {
                let dictInfo = NSMutableDictionary()
                let val1 = String(format: "%d", (rs?.int(forColumn: "Question_ID"))!)
                    dictInfo.setValue("\(val1)", forKey:ques_id)
                let val2 = String(format: "%d", (rs?.int(forColumn: "User_ID"))!)
                dictInfo.setValue("\(val2)", forKey:"USER_ID")
                    dictInfo.setValue(rs?.string(forColumn: "Question_Type"), forKey: quest_type)
                    dictInfo.setValue(rs?.string(forColumn: "Answer_Text"), forKey: answer_text)
                    dictInfo.setValue(rs?.string(forColumn: "SerialNo"), forKey: serial_id)
                let val3 = String(format: "%d", (rs?.int(forColumn: "SequenceNum"))!)
                dictInfo.setValue("\(val3)", forKey: "SeqNum")
                let val4 = String(format: "%d", (rs?.int(forColumn: "Survey_ID"))!)
                    dictInfo.setValue("\(val4)", forKey: surveyId)
                    dictInfo.setValue(rs?.string(forColumn: "Upload_Flag"), forKey: "Upload_Flag")
                print(dictInfo)
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        database?.close()
    }
  
  
  func fetchDataToMerge(_ userID : AnyObject, paramsdict : NSMutableDictionary) -> [String] {
    
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    database?.open()
    var strArray = [String]()
    
    do {
      let sr = paramsdict.value(forKey: serial_id)!
      let rs = try database?.executeQuery("SELECT Answer_Text FROM UPLOAD_RECORDS WHERE USER_ID = ? AND Question_Type = ? AND SequenceNum = ? AND SerialNo LIKE ? AND Survey_ID =?", values: [userID, paramsdict.value(forKey: quest_type)!, paramsdict.value(forKey: "SeqNum")!,"\(sr)_%",paramsdict.value(forKey: surveyId)!])
      while (rs?.next())! {
        var dictInfo = NSMutableDictionary()
//          dictInfo.setValue("\(rs.intForColumn("Question_ID"))", forKey:ques_id)
//          dictInfo.setValue("\(rs.intForColumn("User_ID"))", forKey:"USER_ID")
//          dictInfo.setValue(rs.stringForColumn("Question_Type"), forKey: quest_type)
//          dictInfo.setValue(rs.stringForColumn("Answer_Text"), forKey: answer_text)
//          dictInfo.setValue(rs.stringForColumn("SerialNo"), forKey: serial_id)
//          dictInfo.setValue("\(rs.intForColumn("SequenceNum"))", forKey: "SeqNum")
//          dictInfoArray.append(dictInfo)
            strArray.append((rs?.string(forColumn: "Answer_Text"))!)
      }
      
    } catch let error as NSError {
      print("failed: \(error.localizedDescription)")
    }
    database?.close()
    return strArray
  }
  
  func updateUploadStatusForRecord(_ serialNo : String, userID : AnyObject, sequenceNum : String, answerText : String, uploadFlag : String, fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    if (database?.open())! {
        print(fetchedSurveyID)
      let sql_stmt = "UPDATE UPLOAD_RECORDS SET Answer_Text = ?,Upload_Flag = ?  WHERE SerialNo = ? AND User_ID =? AND SequenceNum =? AND Survey_ID =?"
      do {
        try database?.executeUpdate(sql_stmt, values: [answerText, uploadFlag, serialNo, userID, sequenceNum,fetchedSurveyID])
        return true
      } catch let error as NSError {
        print("failed : \(error.localizedDescription)")
      }
    }
    return false
  }
  
  func deleteUploadRecord(_ userID : AnyObject, fetchedSurveyID: AnyObject) -> Bool {
    let database = FMDatabase(path : CreateDB.getDataBasePath())
    
    if (database?.open())! {
      var sql_stmt = "DELETE FROM UPLOAD_RECORDS WHERE User_ID =? AND Survey_ID =?"
        if fetchedSurveyID as? String == "" {
            sql_stmt = "DELETE FROM UPLOAD_RECORDS WHERE User_ID =?"
        }
      do {
        if fetchedSurveyID as? String == "" {
            try database?.executeUpdate(sql_stmt, values: [userID])
        } else {
            try database?.executeUpdate(sql_stmt, values: [userID,fetchedSurveyID])
        }
        return true
      } catch let error as NSError {
        print("failed: \(error.localizedDescription)")
      }
    }
    return false
  }
  
  func deleteImagesAndVideosUploadRecord(_ userID : String) {
    
    var urlArray = [String]()
    let videoArray = UploadRecordSharedClass.sharedInstance.fetchVideoOrImageURLsFromUploadRecords(userID as AnyObject, type: "")
    print(videoArray)
    for video in videoArray {
      let ansText = video.value(forKey: answer_text)
      let ansArr = (ansText as! String).components(separatedBy: "^]`,")
      for ans in ansArr {
        if ans != "" && ans.contains("https") == false {
              urlArray.append(ans)
        }
      }
    }
    
    for url in urlArray {
      do {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath1 = URL(fileURLWithPath: "\(documentsPath)/\(url)")
        _ = try FileManager.default.removeItem(at: dataPath1)
        print("Item deleted Succesfully")
      } catch {
        print(error)
      }
    }
    UploadRecordSharedClass.sharedInstance.deleteUploadRecord(userID as AnyObject,fetchedSurveyID: "" as AnyObject)
  }
  
}
