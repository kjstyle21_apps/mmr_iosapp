//
//  Beacon.swift
//  MMR
//
//  Created by admin on 09/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import Foundation

class Beacon
{
     /* Beacon model */
    var UUID = String();
    var updatedTime = String();
    var weight = String();
    var historyCount = uint();
    var batteryLevel = String();
    var dataReceivedTime = String();
    var createdTime = String()
}
