//
//  CBManager.swift
//  MMR
//
//  Created by admin on 08/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import UIKit
import CoreBluetooth

class CBManager : UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate, CBPeripheralManagerDelegate
{
    
    /* Variables declaration and initialization */
    static let instance = CBManager();
    var centralManager = CBCentralManager();
    var peripheralManager = CBPeripheralManager();
    var discoveredPeripheral : CBPeripheral?
    
    
    /* Callback after central manager initialized - and start scanning peripherals */
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        // check Bluetooth is currently powered on and available to use
        if(central.state == .poweredOn)
        {
            centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey : false])
//            centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:false]);
            print("SCAN STARTED");
        }
        else
        {
            print("Make sure that bluetooth is powered on");
        }
    }
    
    /* Callback when any peripheral discovered */
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        print(peripheral.name);
        
        /* Connect peripheral only - having defined name */
        if(peripheral.name != nil && peripheral.name == DEVICE_NAME)
        {
            self.discoveredPeripheral = peripheral;
            
            //Call to funtion to parse the data
            Parser.instance.parseAndLoadDeviceDetails(peripheral,advertisementData: advertisementData,RSSI: RSSI);
            
            let discoveredPeripheralInformation = "PERIPHERAL NAME: \(peripheral.name)\n AdvertisementData: \(advertisementData)\n RSSI: \(RSSI)\n UUID DESCRIPTION: \(peripheral.identifier.uuidString)\n IDENTIFIER: \(peripheral.identifier)"
            
            print("PERIPHERAL NAME: \(peripheral.name)\n AdvertisementData: \(advertisementData)\n RSSI: \(RSSI)\n")
            print(discoveredPeripheralInformation);
        }
    }
    
    /* Initialize central manager and peripheral manager */
    func startDiscovery()
    {
        centralManager = CBCentralManager(delegate: self, queue: nil);
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil);
        centralManager.scanForPeripherals(withServices: nil, options: nil);
    }
    
    /* Stop scanning */
    func stopScanning()
    {
        centralManager.stopScan();
        print("SCAN STOPED")
    }
    
    /*Connect to peripheral*/
    func connectDevice(_ peripheral: CBPeripheral)
    {
        stopScanning();
        centralManager.connect(peripheral, options: nil);
    }
    
    /*Disconnect to peripheral*/
    func disconnectDevice(_ peripheral: CBPeripheral)
    {
        centralManager.cancelPeripheralConnection(peripheral)
        startDiscovery();
    }
    
    /* Write data bytes to specified characteristics */
    func writeData(_ peripheral: CBPeripheral, data: Data, characteristic: CBCharacteristic)
    {
        peripheral.writeValue(data, for: characteristic, type: CBCharacteristicWriteType.withResponse);
    }
    
    /* Update device time - 10 bytes */
    func updateDeviceTime(_ peripheral: CBPeripheral, time: Data, characteristic: CBCharacteristic)
    {
        peripheral.writeValue(time, for: characteristic, type: CBCharacteristicWriteType.withResponse);
    }
    
    /* Read value from specified characteristics */
    func readData(_ peripheral: CBPeripheral, characteristic: CBCharacteristic)
    {
        peripheral.readValue(for: characteristic);
    }
    
    /* Set notification value of history characteristic - true */
    func readHistory(_ peripheral: CBPeripheral, characteristic: CBCharacteristic)
    {
        peripheral.setNotifyValue(true, for: characteristic);
    }
    
    /* Disable notification of history characteristic - false */
    func disableHistoryNotification(_ peripheral: CBPeripheral, characteristic: CBCharacteristic)
    {
        peripheral.setNotifyValue(false, for: characteristic);
    }
    
    
    /* Set notification value of specified characteristic - true */
    func readCharacteristics(_ peripheral: CBPeripheral, characteristic: CBCharacteristic)
    {
        peripheral.setNotifyValue(true, for: characteristic);
    }
    
    
    /* Callback after connecting peripheral */
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral){
        print("Connected to peripheral");
        peripheral.delegate = self;
        peripheral.discoverServices(nil);
    }
    
    /* Callback when failed to connect peripheral */
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: NSError?) {
        print("Failed to connect peripheral");
    }
    
    /* Callback when peripheral disconnected */
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected from peripheral");
    }
    
    /* Callback when peripheral has invalid services */
    func peripheralDidInvalidateServices(_ peripheral: CBPeripheral!) {
        print("Peripheral has invalid services");
    }
    
    /* Callback after discovered services of peripheral */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("Discovered services");
        
        /* For each discovered service */
        for service in peripheral.services! {
            let eachService = service 
            
            if(service.uuid == TIME_SERVICE_UUID)
            {
                peripheral.discoverCharacteristics(nil, for: eachService);
            }
            
            if service.uuid == REAL_TIME_DATA_SERVICE_UUID
            {
                peripheral.discoverCharacteristics(nil, for: eachService);
            }
            
            if service.uuid == HISTORY_DATA_SERVICE_UUID
            {
                peripheral.discoverCharacteristics(nil, for: eachService);
            }
            
            print("Services: " + String(stringInterpolationSegment: eachService.uuid));
        }
    }
    
    
    /* Callback after discovered characteristics of service */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: NSError?) {
        
        /* For each discovered characteristic */
        for charateristic in service.characteristics! {
            let eachCharacteristic = charateristic ;
            
            if eachCharacteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID {
                peripheral.readValue(for: eachCharacteristic);
            }
            
            if eachCharacteristic.uuid == BATTERY_CHARACTERISTICS_UUID {
                peripheral.setNotifyValue(true, for: eachCharacteristic);
            }
            
            if eachCharacteristic.uuid == REAL_TIME_WEIGHT_CHARACTERISTICS_UUID{
                peripheral.setNotifyValue(true, for: eachCharacteristic);
            }
            
            if eachCharacteristic.uuid == UUID_CHARACTERISTICS_UUID {
                peripheral.readValue(for: eachCharacteristic);
            }
            
            //Store in cache
            CacheManager.instance.setDiscoveredCharaListInCache(eachCharacteristic);
        }
    }
    
    
    /* Callback after reading/writing of characteristic */
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: NSError?) {
        
        //println("Updated characteristic value:" + String(stringInterpolationSegment: characteristic.value));
        
        let selectedDevice = CacheManager.instance.getSelectedDeviceFromCache();
        
        if (characteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID)
        {
            //Check for current time with received device time - update time if not matching
            Parser.instance.parseReceivedDeviceTime(characteristic.value!);
        }
        else if(characteristic.uuid == REAL_TIME_WEIGHT_CHARACTERISTICS_UUID)
        {
            Parser.instance.parseWeight(characteristic.value!);
        }
        else if (characteristic.uuid == BATTERY_CHARACTERISTICS_UUID)
        {
            Parser.instance.parseBattery(characteristic.value!);
        }
        else if (characteristic.uuid == UUID_CHARACTERISTICS_UUID)
        {

        }
        else if(characteristic.uuid == HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID)
        {
            Parser.instance.parseHistoryData(characteristic.value!);
        }
        CacheManager.instance.setSelectedDeviceInCache(selectedDevice);
    }
    
    /* Callback after write characteristic  */
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: NSError?) {
    
        if(characteristic.uuid == UUID_CHARACTERISTICS_UUID)
        {

        }
        else if(characteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID)
        {

        }
        else if(characteristic.uuid == REAL_TIME_MOTION_CHARACTERISTICS_UUID)
        {
           
        }
    }
    
    
    /* Callback after peripheral manager initialized - checking state */
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        switch peripheralManager.state {
        case .poweredOff:
            print("Peripheral - CoreBluetooth BLE hardware is powered off");
            break;
            
        case .poweredOn:
            print("Peripheral - CoreBluetooth BLE hardware is powered on and ready");
            break;
            
        case .resetting:
            print("Peripheral - CoreBluetooth BLE hardware is resetting");
            break;
            
        case .unauthorized:
            print("Peripheral - CoreBluetooth BLE state is unauthorized");
            break;
            
        case .unknown:
            print("Peripheral - CoreBluetooth BLE state is unknown");
            break;
            
        case .unsupported:
            print("Peripheral - CoreBluetooth BLE hardware is unsupported on this platform");
            break;
            
        default:
            break;
        }
    }
}

