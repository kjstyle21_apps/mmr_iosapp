//
//  CacheManager.swift
//  MMR
//
//  Created by admin on 08/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import Foundation
import CoreBluetooth

/*This class would be singletone class which would hold the objects*/
class CacheManager
{
    /* Variables declaration and initialization */
    static let instance = CacheManager();
    var selectedDevice = Device();
    var selectedPeripheral : CBPeripheral?
    var deviceList = [Device]();
    var beaconList = [Beacon]();
    var historyList = [History]();
    var discoveredPeripheralList = [CBPeripheral]();
    var characteristicList = [AnyObject]();
    var enteredUUIDString = String();
    var deviceTimeString = String();
    var selectedHistoryRange = COMPLETE_HISTORY; // 1 for complete history , 2 for specific date range.
    var beaconDataPacket = NSMutableData();
    var currentState = STATE_1;
    var beaconDataDictionary = NSMutableDictionary();

    
    /* Functions to set and get the beacon and device details in cache */
    func setSelectedDeviceInCache(_ device: Device)
    {
        self.selectedDevice = device;
    }
    
    func getSelectedDeviceFromCache() -> Device
    {
        return self.selectedDevice;
    }
    
    func setSelectedPeripheralInCache(_ peripheral: CBPeripheral)
    {
        self.selectedPeripheral = peripheral;
    }
    
    func getSelectedPeripheralFromCache() -> CBPeripheral
    {
        return self.selectedPeripheral!;
    }
    
    func setSelectedDeviceListInCache(_ devices: [Device])
    {
        self.deviceList = devices;
    }
    
    func getSelectedDeviceListFromCache() -> [Device]
    {
        return self.deviceList;
    }
    
    func setSelectedPeripheralListInCache(_ peripherals: [CBPeripheral])
    {
        self.discoveredPeripheralList = peripherals;
    }
    
    func getSelectedPeripheralListFromCache() -> [CBPeripheral]
    {
        return self.discoveredPeripheralList;
    }
    
    func setSelectedBeaconListInCache(_ beacons: [Beacon])
    {
        self.beaconList = beacons;
    }
    
    func getSelectedBeaconListFromCache() -> [Beacon]
    {
        return self.beaconList;
    }
    
    func setDiscoveredCharaListInCache(_ characteristic: AnyObject)
    {
        self.characteristicList.append(characteristic);
    }
    
    func getDiscoveredCharaListFromCache() -> [AnyObject]
    {
        return self.characteristicList;
    }
    
    func setSelectedUUIDInCache(_ uuidString: String)
    {
        self.enteredUUIDString = uuidString;
    }
    
    func getSelectedUUIDFromCache() -> String
    {
        return self.enteredUUIDString;
    }
    
    func setSelectedHistoryListInCache(_ history: [History])
    {
        self.historyList = history;
    }
    
    func getSelectedHistoryListFromCache() -> [History]
    {
        return self.historyList;
    }
    
    func setCurrentTimeInCache(_ time: String)
    {
        self.deviceTimeString = time;
    }
    
    func getCurrentTimeFromCache() -> String
    {
        return self.deviceTimeString;
    }
    
}
