//
//  Device.swift
//  MMR
//
//  Created by admin on 08/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import Foundation


class Device
{
    /* Device model */
    var deviceName = String();
    var identifier = String();
    var RSSI = NSNumber();
    var weight = String();
    var weightTime = String();
    var batteryLevel = String();
    var batteryLevelTime = String();
    var uuid = String();
    var time = String();
    var dataReceivedTime = String();
}