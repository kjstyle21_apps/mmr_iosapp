//
//  Helper.swift
//  MMR
//
//  Created by admin on 10/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import UIKit
import CoreBluetooth


class Helper
{
    static let instance = Helper();
    
    /* Convert string to hex :: currently it is hardcoded */
    func convertStrToHex() -> [UInt8]
    {
         // "55AA55AA800000800000010000005555";
        let array : [UInt8] = [0x55, 0xAA, 0x55, 0xAA, 0x80, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01,0x00, 0x00,0x00, 0x55, 0x55];
        
        return array;
    }
    
    /* Write UUID data bytes to characteristic */
    func writeUUID()
    {
        var selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        var array = convertStrToHex();
        var uuidBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 16);
        var charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == UUID_CHARACTERISTICS_UUID {
               
                CBManager.instance.writeData(selectedPeripheral, data: uuidBytes, characteristic: eachCharacteristic);
                break;
            }
        }
    }
    
    /* Write current date time to device - 10 bytes */
    func checkAndUpdateDeviceTime()
    {
        var selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let date = subtractMonth(Date());
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
      let components = (calendar as NSCalendar).components([.hour , .minute , .month , .year , .day], from: date);
      
        //Define each date time components
        //var year = Data(bytes: UnsafePointer<UInt8>([components.year! % 256]), count: 1);
        var yearR = [components.year! % 256].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var year = Data(bytes: &yearR, count: 1)
        //var year1 = Data(bytes: UnsafePointer<UInt8>([components.year! / 256]), count: 1);
        var year1R = [components.year! % 256].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var year1 = Data(bytes: &year1R, count: 1)

        //var month = Data(bytes: UnsafePointer<UInt8>([components.month]), count: 1);
        var month1 = [components.month].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var month = Data(bytes: &month1, count: 1)

        //var day = Data(bytes: UnsafePointer<UInt8>([components.day]), count: 1);
        var day1 = [components.day].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var day = Data(bytes: &day1, count: 1)

        //var hour = Data(bytes: UnsafePointer<UInt8>([components.hour]), count: 1);
        var hour1 = [components.hour].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var hour = Data(bytes: &hour1, count: 1)

        //var minute = Data(bytes: UnsafePointer<UInt8>([components.minute]), count: 1);
        var minute1 = [components.minute].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var minute = Data(bytes: &minute1, count: 1)

        //var second = Data(bytes: UnsafePointer<UInt8>([components.second]), count: 1);
        var second1 = [components.second].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var second = Data(bytes: &second1, count: 1)

        //var weekday = Data(bytes: UnsafePointer<UInt8>([components.weekday]), count: 1);
        var weekday1 = [components.weekday].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var weekday = Data(bytes: &weekday1, count: 1)

        var zero = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(bitPattern: 0)!), count: 1);
        
        //Append bytes
        var totalTime = NSData(data: year) as Data;
        totalTime.append(year1);
        totalTime.append(month);
        totalTime.append(day);
        totalTime.append(hour);
        totalTime.append(minute);
        totalTime.append(second);
        totalTime.append(weekday);
        totalTime.append(zero);
        totalTime.append(zero);
        
        var charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID {
                
                CacheManager.instance.setCurrentTimeInCache(getCurrentTime());
                CBManager.instance.updateDeviceTime(selectedPeripheral, time: totalTime, characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Clear all history */
    func clearHistory()
    {
        var selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        var charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == CLEAR_HISTORY_CHARACTERISTICS_UUID {
                
                var clearFlag = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([CLEAR_ALL_HISTORY])), count: 1)
                CBManager.instance.writeData(selectedPeripheral, data: clearFlag, characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Fetch all history */
    func fetchHistory()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID {
                                
                CBManager.instance.readHistory(selectedPeripheral,  characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Fetch history of specified date range - currently it is of - last 1 hour  */
    func fetchDateRangeHistory()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        //End date time
        let endDateTimestamp = Date().timeIntervalSince1970;
        var intValue = Int32(endDateTimestamp);
      
//      return ImageProcessor.imageFromARGB32Bitmap(Data(bytes: pixelBuffer), width: framebufferwidth, height: framebufferheight)
//      var eD = NSData(bytes: <#T##UnsafePointer<Void>#>, length: <#T##Int#>)
      
        var endDate1 = [intValue].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var endDate = Data(bytes: &endDate1, count: MemoryLayout<Int32>.size)

//      var endDate = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([intValue])), count: MemoryLayout<Int32>.size)
//         var endDate = NSData(bytes: UnsafePointer<UInt8>(&intValue), length: sizeof(Int32));
//        var arr1 = [UInt8](repeating: 0, count: 4);
        var arr1 = [UInt8](repeating: 0, count: 4)
        (endDate as NSData).getBytes(&arr1);
        var array  = [arr1[3] as UInt8, arr1[2] as UInt8, arr1[1] as UInt8,arr1[0] as UInt8];
        endDate = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
        
        //Start date time
        let startDateTimestamp = (subtractHour(Date())).timeIntervalSince1970;
        intValue = Int32(startDateTimestamp);
        
        var startDate1 = [intValue].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var startDate = Data(bytes: &startDate1, count: MemoryLayout<Int32>.size)
        
        //var startDate = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([intValue])), count: MemoryLayout<Int32>.size);
        (startDate as NSData).getBytes(&arr1);
        array  = [arr1[3] as UInt8, arr1[2] as UInt8, arr1[1] as UInt8,arr1[0] as UInt8];
        startDate = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
        
        //Append bytes
        let dateRange = (NSData(data: startDate) as Data) as! NSMutableData;
        dateRange.append(endDate);
        
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == HISTORY_RANGE_WEIGHT_DATA_CHARACTERISTICS_UUID {
                
                CBManager.instance.writeData(selectedPeripheral, data: dateRange as Data, characteristic: eachCharacteristic);
                 break;
            }
        }
        
        sleep(1);
        
        /* For each characteristic */
//        for charateristic in charaList {
//            let eachCharacteristic = charateristic as! CBCharacteristic;
//            
//            //If UUID characteristic found
//            if eachCharacteristic.UUID == HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID {
//                
//                CBManager.instance.readHistory(selectedPeripheral,  characteristic: eachCharacteristic);
//                 break;
//            }
//        }
        fetchHistory();
        
    }
    
    /* Retrive current date time in specified format */
    func getCurrentTime() -> String
    {
        //Current date time
        let currentDateTime: Date = Date();
        let dateFormatter:DateFormatter = DateFormatter();
        dateFormatter.dateFormat = DATE_TIME_FORMAT; //Format
        let currentDateTimeString:String = dateFormatter.string(from: currentDateTime);
        return currentDateTimeString;
    }

    /* Subtract 1 month for each received date time from device to get exact date time(as per standard) */
    func subtractMonth(_ date:  Date) -> Date
    {
        var dateComponent = DateComponents();
        dateComponent.month = -1;
        let cal = Calendar.current
      return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options.init(rawValue: 0))!
      
//        return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    /* Add 1 month from date time device to get exact date time(as per standard) */
    func addMonth(_ date:  Date) -> Date
    {
        var dateComponent = DateComponents();
        dateComponent.month = 1;
        let cal = Calendar.current
      return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options.init(rawValue: 0))!
//        return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    /* Subctract 1 hour from specified date time */
    func subtractHour(_ date:  Date) -> Date
    {
        var dateComponent = DateComponents();
        dateComponent.hour = -1;
        let cal = Calendar.current
      return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options.init(rawValue: 0))!
//        return (cal as NSCalendar).date(byAdding: dateComponent, to: date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    /* Read battery level from device - set noitification true */
    func readBatteryLevel()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == BATTERY_CHARACTERISTICS_UUID {
                
                CBManager.instance.readCharacteristics(selectedPeripheral, characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Read weight from device - set noitification true */
    func readWeight()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == REAL_TIME_WEIGHT_CHARACTERISTICS_UUID {
                
                CBManager.instance.readCharacteristics(selectedPeripheral, characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Read current device time from device */
    func readDeviceTime()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
            //If UUID characteristic found
            if eachCharacteristic.uuid == CURRENT_TIME_CHARACTERISTIC_UUID {
                
                CBManager.instance.readData(selectedPeripheral, characteristic: eachCharacteristic);
                 break;
            }
        }
    }
    
    /* Disable history notification */
    func DisableHistoryNotification()
    {
        let selectedPeripheral = CacheManager.instance.getSelectedPeripheralFromCache();
        let charaList = CacheManager.instance.getDiscoveredCharaListFromCache();
        
        /* For each characteristic */
        for charateristic in charaList {
            let eachCharacteristic = charateristic as! CBCharacteristic;
            
                //If UUID characteristic found
                if(eachCharacteristic.uuid == HISTORY_WEIGHT_DATA_CHARACTERISTICS_UUID) {
                        
                        CBManager.instance.disableHistoryNotification(selectedPeripheral,  characteristic: eachCharacteristic);
                }

        }
    }
    
}
