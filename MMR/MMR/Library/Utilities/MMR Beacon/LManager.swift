//
//  LManager.swift
//  MMR
//
//  Created by admin on 09/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import UIKit
import CoreLocation

class LManager : UIViewController, CLLocationManagerDelegate
{
    /* Variables declaration and initialization */
    static let instance = LManager();
    var beaconRegion: CLBeaconRegion!
    var locationManager: CLLocationManager!
    var lastFoundBeacon: CLBeacon! = CLBeacon();
    var lastProximity: CLProximity! = CLProximity.unknown;
    
    /* Initialize location manager */
    func startDiscovery()
    {
        locationManager = CLLocationManager();
        locationManager.delegate = self;
        if #available(iOS 8.0, *) {
            if(locationManager!.responds(to: #selector(CLLocationManager.requestAlwaysAuthorization))){
                if #available(iOS 8.0, *) {
                    locationManager!.requestAlwaysAuthorization()
                } else {
                    // Fallback on earlier versions
                };
            }
        } else {
            // Fallback on earlier versions
        }
        
        discoverBeacons();
    }
    
    /* Stop discovery */
    func stopBeacon()
    {
        if(locationManager != nil)
        {
            locationManager.stopMonitoring(for: beaconRegion);
            locationManager.stopRangingBeacons(in: beaconRegion);
            locationManager.stopUpdatingLocation();
        }
    }
    
    /* Start discovery of beacons */
    func discoverBeacons()
    {
        // let uuid = NSUUID(UUIDString: "F7826DA6-4FA2-4E98-8024-BC5B71E0893E");
//        let uuid = UUID(uuidString: "55AA55AA-8000-0080-0000-010000005555");
      let uuid = UUID(uuidString: "00005100-0000-1000-8000-008055aa55aa")
        //let uuid = NSUUID(UUIDString: "FFFFAAAA-FFFF-AAAA-FFFF-AAAAFFFFAAAA");
        //let uuid = NSUUID(UUIDString: "55AA55AA-1000-0000-0000-000000000000");
        
        beaconRegion = CLBeaconRegion(proximityUUID: uuid!, identifier: "com.appcoda.beacondemo");
        beaconRegion.notifyOnEntry = true;
        beaconRegion.notifyOnExit = true;
        locationManager.startMonitoring(for: beaconRegion);
        locationManager.startUpdatingLocation();
    }
    
    /* Callback received after - monitoring region - request state of region */
    func locationManager(_ manager: CLLocationManager!, didStartMonitoringFor region: CLRegion!) {
        locationManager.requestState(for: region);
    }
    
    /* Callback received - mentioning region state */
    func locationManager(_ manager: CLLocationManager!, didDetermineState state: CLRegionState, for region: CLRegion!) {
        if state == CLRegionState.inside {
            if(beaconRegion != nil)
            {
                locationManager.startRangingBeacons(in: beaconRegion);
            }
        }
        else {
            if(beaconRegion != nil)
            {
                locationManager.stopRangingBeacons(in: beaconRegion);
            }
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
    }
    
    
    func locationManager(_ manager: CLLocationManager!, didExitRegion region: CLRegion!) {
    }
    
    
    /* Callback - if any beacon found  */
    /* func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion)
    {
        var shouldHideBeaconDetails = true;
        if let foundBeacons = beacons as? [CLBeacon]
        {
            if foundBeacons.count > 0
            {
                if let closestBeacon = foundBeacons[0] as? CLBeacon
                {
                    if closestBeacon != lastFoundBeacon || lastProximity != closestBeacon.proximity
                    {
                        lastFoundBeacon = closestBeacon;
                        print(closestBeacon.proximityUUID);
                        var major = closestBeacon.major;
                        var minor = closestBeacon.minor;
                        var RSSI = closestBeacon.rssi;
                        var proximityMessage: String!
                        
                        switch lastFoundBeacon.proximity
                        {
                        case CLProximity.Immediate:
                            proximityMessage = "Very close";
                            
                        case CLProximity.Near:
                            proximityMessage = "Near";
                            
                        case CLProximity.Far:
                            proximityMessage = "Far";
                            
                        default:
                            proximityMessage = "Where's the iBeacon?"; //Unknown
                        }
                        
                        //Parse major and minor values
                    Parser.instance.parseAndLoadBeaconDetails(closestBeacon.proximityUUID,major: closestBeacon.major,minor: closestBeacon.minor,proximity: proximityMessage);
                    }
                }
            }
        }
    }*/
  
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion)
  {
    _ = true
    
    print("beacons= \(beacons)")
    
    if beacons.count > 0
    {
      print(beacons.first)
      lastFoundBeacon = beacons.first
      //      lastFoundBeacon = beacons[0]
      var proximityMessage: String!
      
      switch lastFoundBeacon.proximity
      {
      case CLProximity.immediate:
        
        proximityMessage = "Very close"
        
      case CLProximity.near:
        proximityMessage = "Near"
        
      case CLProximity.far:
        proximityMessage = "Far"
        
      default:
        proximityMessage = "Where's the iBeacon?" //Unknown
        
      }
      
      //Parse major and minor values
      Parser.instance.parseAndLoadBeaconDetails(beacons[0].proximityUUID,major: beacons[0].major,minor: beacons[0].minor,proximity: proximityMessage)
      
      // if beacons[0] != lastFoundBeacon || lastProximity != beacons[0].proximity
      //            {
      //                lastFoundBeacon = beacons[0]
      //
      //                print(beacons[0].proximityUUID);
      //
      //                _ = beacons[0].major;
      //                _ = beacons[0].minor;
      //                _ = beacons[0].rssi;
      //
      //                var proximityMessage: String!
      //                switch lastFoundBeacon.proximity
      //                {
      //                case CLProximity.Immediate:
      //
      //                    proximityMessage = "Very close"
      //
      //                case CLProximity.Near:
      //                    proximityMessage = "Near"
      //
      //                case CLProximity.Far:
      //                    proximityMessage = "Far"
      //
      //                default:
      //                    proximityMessage = "Where's the iBeacon?" //Unknown
      //
      //                }
      //
      //                //Parse major and minor values
      //                Parser.instance.parseAndLoadBeaconDetails(beacons[0].proximityUUID,major: beacons[0].major,minor: beacons[0].minor,proximity: proximityMessage)
      //            }
    }
  }
    
    func locationManager(_ manager: CLLocationManager!, didFailWithError error: Error) {
      print("Did Fail With Error : \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager!, monitoringDidFailFor region: CLRegion!, withError error: NSError) {
      print("Monitoring fail error : \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager!, rangingBeaconsDidFailFor region: CLBeaconRegion!, withError error: NSError) {
      print("Ranging fail error : \(error)")
    }
    
}


