//
//  PacketParser.swift
//  MMR
//
//  Created by admin on 09/12/16.
//  Copyright (c) 2016 maven. All rights reserved.
//

import CoreBluetooth

class Parser
{
    
    /* Variables declaration and initialization */
    static let instance = Parser();
    var deviceList = [Device]();
    var beaconList = [Beacon]();
    var discoveredPeripheralList = [CBPeripheral]();
    var deviceObj = Device();
    var beaconObj = Beacon();
    var localNotification:UILocalNotification?

  
    /* Parse received device details and add/update device model contents accordingly */
  
    func parseAndLoadDeviceDetails(_ peripheral: CBPeripheral!, advertisementData: [String : Any]!, RSSI: NSNumber!)
    {
        deviceList = CacheManager.instance.getSelectedDeviceListFromCache();
        discoveredPeripheralList = CacheManager.instance.getSelectedPeripheralListFromCache();
        
        var deviceAlreadyFound = false;
        for device in 0..<deviceList.count
        {
            let selectedDevice = deviceList[device];
            if(selectedDevice.identifier == peripheral.identifier.uuidString)
            {
                deviceAlreadyFound = true;
                selectedDevice.deviceName = peripheral.name!;
                selectedDevice.RSSI = RSSI;
                selectedDevice.dataReceivedTime = Helper.instance.getCurrentTime();
            }
        }
        
        if(!deviceAlreadyFound)
        {
            deviceObj = Device();
            deviceObj.deviceName = peripheral.name!;
            deviceObj.RSSI = RSSI;
            deviceObj.identifier = peripheral.identifier.uuidString;
            deviceObj.dataReceivedTime = Helper.instance.getCurrentTime();
            deviceList.append(deviceObj);
            discoveredPeripheralList.append(peripheral);
        }
        
        CacheManager.instance.setSelectedDeviceListInCache(deviceList);
        CacheManager.instance.setSelectedPeripheralListInCache(discoveredPeripheralList);
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshDeviceTableView"), object: deviceList);
    }
    
    
    /* Parse received beacon details and add/update beacon model contents accordingly */
    func parseAndLoadBeaconDetails(_ uuid: UUID, major: NSNumber, minor: NSNumber,
        proximity: String)
    {
        
        beaconList = CacheManager.instance.getSelectedBeaconListFromCache();
        
        var beaconAlreadyFound = false;
        for beacon in 0..<beaconList.count
        {
            var selectedBeacon = beaconList[beacon];
            if(selectedBeacon.UUID == uuid.uuidString)
            {
                beaconAlreadyFound = true;
                selectedBeacon.dataReceivedTime = Helper.instance.getCurrentTime();
                selectedBeacon =  parseAndExtractParameters_New(major, minor: minor, beaconObj: selectedBeacon);
            }
        }
        
        if(!beaconAlreadyFound)
        {
            beaconObj = Beacon();
            beaconObj.UUID = uuid.uuidString;
            beaconObj.updatedTime = "-";
            beaconObj.weight = DEFUALT_VALUE + UNIT_GRAM;
            beaconObj.batteryLevel = DEFUALT_VALUE + UNIT_PER;
            beaconObj.historyCount = 0;
            beaconObj.dataReceivedTime = Helper.instance.getCurrentTime();
            beaconObj = parseAndExtractParameters_New(major, minor: minor, beaconObj: beaconObj);
            beaconList.append(beaconObj);
        }
        
        CacheManager.instance.setSelectedBeaconListInCache(beaconList);
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshBeaconTableView"), object: beaconList);
      
    }
    
    
    /* Parse major and minor value - and define parameters - old strategy(key-value pair) */
    func parseAndExtractParameters(_ major: NSNumber, minor: NSNumber, beaconObj : Beacon) -> Beacon
    {
        var calculatedWeight : Float;
        var parameterId : uint = 0;
        var parameterValue : uint = 0;
        
        var firstTwoBytes : ushort = major.uint16Value //major.uint16Value;
        var secondTwoBytes : ushort = minor.uint16Value;
        
        firstTwoBytes = UInt16(bigEndian: firstTwoBytes);
        secondTwoBytes = UInt16(bigEndian:secondTwoBytes);
        
        //var data1 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([firstTwoBytes])), count: 2);
       
        var dataR1 = [firstTwoBytes].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var data1 = Data(bytes: &dataR1, count: 2)
        
       // var data2 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([secondTwoBytes])), count: 2);
        
        var dataR2 = [secondTwoBytes].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var data2 = Data(bytes: &dataR2, count: 2)
        
        var data = NSData(data: data1) as Data;
        data.append(data2);
        
        let range = Range(0 ... 1)

        var parameterIdTemp = data.subdata(in: range)
        (parameterIdTemp as NSData).getBytes(&parameterId, length: 1)
      
        let range1 = Range(1 ... 3)

        var parameterValueTemp = data.subdata(in: range1);
//        var arr1 = [UInt8](repeating: 0, count: 4);
        var arr1 = [UInt8](repeating: 0, count: 4)
        (parameterValueTemp as NSData).getBytes(&arr1);
        
        print("****************");
        print(data);
        print(parameterIdTemp);
        print(parameterValueTemp);
        
        
        if(parameterId == WEIGHT_PARAMETER_ID)
        {
            var array : [UInt8] = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,0 as UInt8];
            var data22 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
            (data22 as NSData).getBytes(&parameterValue, length: 4);
            calculatedWeight = Float(parameterValue / SCALING_FACTOR);
            beaconObj.weight = String(stringInterpolationSegment: calculatedWeight) + UNIT_GRAM;
        }
        else if(parameterId == HISTORY_COUNT_PARAMETER_ID)
        {
            var array : [UInt8] = [  arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,0 as UInt8];
            var data22 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
            (data22 as NSData).getBytes(&parameterValue, length: 4);
            beaconObj.historyCount = parameterValue;
        }
        else if(parameterId == BATTERY_LEVEL_PARAMETER_ID)
        {
            var array : [UInt8] = [arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,0 as UInt8];
            var data22 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 3);
            (data22 as NSData).getBytes(&parameterValue, length: 4);
            beaconObj.batteryLevel = String(parameterValue) + UNIT_PER;
        }
      
        print("para id:\(parameterId)");
        print("para value:\(parameterValue)");
        
        return  beaconObj;
    }
    
    /* Parse major and minor value - and define parameters - new strategy(buffering) */
    func parseAndExtractParameters_New(_ major: NSNumber, minor: NSNumber, beaconObj : Beacon) -> Beacon
    {
        
        var packetId : uint = 0;
        
        var firstTwoBytes : ushort = major.uint16Value;
        var secondTwoBytes : ushort = minor.uint16Value;
        
        firstTwoBytes = UInt16(bigEndian: firstTwoBytes);
        secondTwoBytes = UInt16(bigEndian:secondTwoBytes);
        
        
        //var data1 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([firstTwoBytes])), count: 2);
        var dataR1 = [firstTwoBytes].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var data1 = Data(bytes: &dataR1, count: 2)
        
        //var data2 = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>([secondTwoBytes])), count: 2);
        var dataR2 = [secondTwoBytes].withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: UInt8.self)
        }
        var data2 = Data(bytes: &dataR2, count: 2)
        
        var data = NSData(data: data1) as Data;
        data.append(data2);
        let packetIdTemp: NSData = data.scanValue(start: 0, length: 1)
//        var packetIdTemp = data.subdata(in: NSMakeRange(0, 1));
        (packetIdTemp as NSData).getBytes(&packetId, length: 1);
        
        let packetData: NSData = data.scanValue(start: 1, length: 3)
        //var packetData = data.subdata(in: NSMakeRange(1, 3))
        
        
        print("****************");
        print(Int64(Date().timeIntervalSince1970 * 1000));
        print(data)
        //        println(packetIdTemp);
        //        println(packetData);
      
        print("Packet ID : \(packetId)")
        print("Packet Data : \(packetData)")
      
        switch(packetId)
        {
            case STATE_1:
                CacheManager.instance.beaconDataDictionary.setValue(packetData, forKey: STATE_1_STR);
                break;
            case STATE_2:
                CacheManager.instance.beaconDataDictionary.setValue(packetData, forKey: STATE_2_STR);
                break;
            case STATE_3:
                CacheManager.instance.beaconDataDictionary.setValue(packetData, forKey: STATE_3_STR);
                break;
            case STATE_4:
                CacheManager.instance.beaconDataDictionary.setValue(packetData, forKey: STATE_4_STR);
                break;
            case STATE_5:
                CacheManager.instance.beaconDataDictionary.setValue(packetData, forKey: STATE_5_STR);
            default:
                print("Invalid packet id");
                break;
        }
        if(CacheManager.instance.beaconDataDictionary.count == BEACON_NO_OF_PACKETS)
        {
            return ParseBeaconData(beaconObj);
        }
        
        return  beaconObj;
    }
    
    /* Wait until complete packet data received - parse and define/extract parameters */
    func ParseBeaconData(_ beaconObj : Beacon) -> Beacon
    {
        var version : UInt8 = 0;
        var lenght : UInt8 = 0;
        var deviceType : UInt8 = 0;
        var batteryLevel : uint = 0;
        var historyCount : uint = 0;
        var weight : uint = 0;
        var timestamp : uint = 0;
        
        CacheManager.instance.beaconDataPacket = NSMutableData();
        
        //Packet id(key), packet data(value)
      
        CacheManager.instance.beaconDataPacket.append(CacheManager.instance.beaconDataDictionary.value(forKey: STATE_1_STR) as! Data);
        CacheManager.instance.beaconDataPacket.append(CacheManager.instance.beaconDataDictionary.value(forKey: STATE_2_STR) as! Data);
        CacheManager.instance.beaconDataPacket.append(CacheManager.instance.beaconDataDictionary.value(forKey: STATE_3_STR) as! Data);
        CacheManager.instance.beaconDataPacket.append(CacheManager.instance.beaconDataDictionary.value(forKey: STATE_4_STR) as! Data);
        CacheManager.instance.beaconDataPacket.append(CacheManager.instance.beaconDataDictionary.value(forKey: STATE_5_STR) as! Data);
        
        //Version - 1 byte
        let dataPacket =  CacheManager.instance.beaconDataPacket;
        var extractedBytes = dataPacket.subdata(with: NSMakeRange(0, 1));
        (extractedBytes as NSData).getBytes(&version, length: 1);
        
        if(version != BEACON_PACKET_VERSION)
        {
            print("Invalid packet version");
            return beaconObj;
        }
        
        //Lenght - 1 byte
        extractedBytes = dataPacket.subdata(with: NSMakeRange(1, 1));
        (extractedBytes as NSData).getBytes(&lenght, length: 1);
        
        if(lenght != BEACON_PACKET_LENGHT)
        {
            print("Invalid packet lenght");
            return beaconObj;
        }
        
        //Device type - 1 byte
        extractedBytes = dataPacket.subdata(with: NSMakeRange(2, 1));
        (extractedBytes as NSData).getBytes(&deviceType, length: 1);
        
        if(deviceType == MOTION_DEVICE)
        {
            print("Motion device");
        }
            //For weight device only
        else if(deviceType == WEIGHT_200_DEVICE || deviceType == WEIGHT_1000_DEVICE)
        {
          
          var arr1 = [UInt8](repeating: 0, count: 4)
//            var arr1 = [UInt8](repeating: 0, count: 4);
          
            //Battery level - 1 byte
            extractedBytes = dataPacket.subdata(with: NSMakeRange(3, 1));
            (extractedBytes as NSData).getBytes(&arr1, length: 4);
            
            var  array  = [arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,0 as UInt8];
            var tempBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 3);
            (tempBytes as NSData).getBytes(&batteryLevel, length: 4);
            beaconObj.batteryLevel = String(batteryLevel) + UNIT_PER;
            print("Battery Level : \(beaconObj.batteryLevel)")
            
            //History count - 2 bytes
            extractedBytes = dataPacket.subdata(with: NSMakeRange(4, 2));
            (extractedBytes as NSData).getBytes(&arr1, length: 4);
            
            array  = [arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,0 as UInt8];
            tempBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
            (tempBytes as NSData).getBytes(&historyCount, length: 4);
            beaconObj.historyCount = historyCount;
            print("History Count : \(beaconObj.historyCount)")
            if beaconObj.historyCount > 0 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: BLE_HISTORY_AVAILABLE_NOTIFY), object: nil)
            }
          
            //Weight - 4 bytes
            extractedBytes = dataPacket.subdata(with: NSMakeRange(6, 4));
            (extractedBytes as NSData).getBytes(&arr1, length: 4);
            
            array = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
            tempBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
            (tempBytes as NSData).getBytes(&weight, length: 4);
            beaconObj.weight = String(stringInterpolationSegment: Float(weight / SCALING_FACTOR)) + UNIT_GRAM;
          
//            self.fireUILocalnotificationWithMessage("You're inside region with weight \(beaconObj.weight)")
//            let notification:UILocalNotification = UILocalNotification()
//            notification.alertBody = "You're inside region with weight \(beaconObj.weight)"
//            notification.fireDate = NSDate(timeIntervalSinceNow: 0)
//            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
          
            print("Weight : \(beaconObj.weight)")
            
            //Timestamp - 4 bytes
            extractedBytes = dataPacket.subdata(with: NSMakeRange(10, 4));
            (extractedBytes as NSData).getBytes(&arr1, length: 4);
            
            array = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
            tempBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
            (tempBytes as NSData).getBytes(&timestamp, length: 4);
              print("Time Stamp for Motion Sensor : \(timestamp)")
          
            let receivedDate = Date(timeIntervalSince1970: Double(timestamp));
            let dateFormatter:DateFormatter = DateFormatter();
            dateFormatter.dateFormat = DATE_TIME_FORMAT;
            beaconObj.updatedTime = dateFormatter.string(from: receivedDate);
            print("Updated Time : \(beaconObj.updatedTime)")
          
            let details = [bleWeight : beaconObj.weight, bleUpdatedDate : beaconObj.updatedTime, bleCreatedDate : beaconObj.updatedTime, bleBatteryLevel : beaconObj.batteryLevel]
          
          let status = BLERecordsSharedClass.sharedInstance.isRecordAvailable(beaconObj.updatedTime)
          if status == "No_Exists" {
            if UserDefaults.standard.value(forKey: surveyId) != nil {
              BLERecordsSharedClass.sharedInstance.insertbleData(details as [String : AnyObject])
            }
          }
          
//            BLERecordsSharedClass.sharedInstance.insertbleData(details)

        }
        else
        {
            print("Invalid device type");
        }
        CacheManager.instance.beaconDataPacket = NSMutableData();
        return beaconObj;
        
    }

    
    /* Parse received date time from device - 10 bytes */
    func parseReceivedDeviceTime(_ receiveTimeBytes: Data)
    {
        var selectedDevice = CacheManager.instance.getSelectedDeviceFromCache();
        var tempByte = Data();
        var year : Int = 0;
        var year1 : Int = 0;
        var month : Int = 0;
        var day : Int = 0;
        var hour : Int = 0;
        var minute : Int = 0;
        var second : Int = 0;
      
      var arr1 = [UInt8](repeating: 0, count: 10)
//        var arr1 = [UInt8](repeating: 0, count: 10);
        (receiveTimeBytes as NSData).getBytes(&arr1);
        var array : [UInt8] = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8,arr1[4] as UInt8, arr1[5] as UInt8, arr1[6] as UInt8,arr1[7] as UInt8,arr1[8] as UInt8,arr1[9] as UInt8];
        var time = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 10);
      

//        tempByte = time.subdata(in: NSMakeRange(0, 1));
        tempByte = time.scanValue(start: 0, length: 1)
        (tempByte as NSData).getBytes(&year, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(1, 1));
        tempByte = time.scanValue(start: 1, length: 1)
        (tempByte as NSData).getBytes(&year1, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(2, 1));
        tempByte = time.scanValue(start: 2, length: 1)
        (tempByte as NSData).getBytes(&month, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(3, 1));
        tempByte = time.scanValue(start: 3, length: 1)
        (tempByte as NSData).getBytes(&day, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(4, 1));
        tempByte = time.scanValue(start: 4, length: 1)
        (tempByte as NSData).getBytes(&hour, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(5, 1));
        tempByte = time.scanValue(start: 5, length: 1)
        (tempByte as NSData).getBytes(&minute, length: 1);
        
//        tempByte = time.subdata(in: NSMakeRange(6, 1));
        tempByte = time.scanValue(start: 6, length: 1)
        (tempByte as NSData).getBytes(&second, length: 1);

        var calendar = Calendar.current
        var components = DateComponents();
        components.year = (256 * year1) + year;
        components.month = month;
        components.day = day;
        components.hour = hour;
        components.minute = minute;
        components.second = second;
        
        //Convert received date time components to date time
        calendar.timeZone = TimeZone(abbreviation: "UTC")! //TODO
        var receivedDateTime = calendar.date(from: components)!
        receivedDateTime = Helper.instance.addMonth(receivedDateTime);
        var dateFormatter:DateFormatter = DateFormatter();
        dateFormatter.dateFormat = DATE_TIME_FORMAT;
        selectedDevice.time = dateFormatter.string(from: receivedDateTime);
        
        CacheManager.instance.setSelectedDeviceInCache(selectedDevice);
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshDeviceContents"), object: nil);
    }
    
    /* Parse battery and its timestamp - 5 bytes */
    func parseBattery(_ batteryBytes : Data)
    {
        var selectedDevice = CacheManager.instance.getSelectedDeviceFromCache();
        var batteryLevel : Int = 0;
        var timeStamp : Int = 0;
        
        //Battery level - 1 byte
//        var batteryExtractedBytes = batteryBytes.subdata(in: NSMakeRange(0, 1));
        var batteryExtractedBytes: NSData = batteryBytes.scanValue(start: 0, length: 1)
        (batteryExtractedBytes as NSData).getBytes(&batteryLevel, length: 1);
        selectedDevice.batteryLevel = String(batteryLevel) + UNIT_PER;
        
        //Timestamp - 4 byte
//        var timeExtractedBytes = batteryBytes.subdata(in: NSMakeRange(1, 4));
        var timeExtractedBytes: NSData = batteryBytes.scanValue(start: 1, length: 4)
        
//        var arr1 = [UInt8](repeating: 0, count: 4);
      var arr1 = [UInt8](repeating: 0, count: 4)
        (timeExtractedBytes as NSData).getBytes(&arr1);
        var array : [UInt8] = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
        var dataTemp = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
        (dataTemp as NSData).getBytes(&timeStamp);
        
        //Convert timestamp to date time.
        var receivedDate = Date(timeIntervalSince1970: Double(timeStamp));
        var dateFormatter:DateFormatter = DateFormatter();
        dateFormatter.dateFormat = DATE_TIME_FORMAT;
        selectedDevice.batteryLevelTime = dateFormatter.string(from: receivedDate);
        
        CacheManager.instance.setSelectedDeviceInCache(selectedDevice);
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshDeviceContents"), object: nil);
    }
    
    /* Parse weight and its timestamp - 9 bytes */
    func parseWeight(_ weightBytes : Data)
    {
        var calibrationFlag : Int = 0;
        var weight : uint = 0;
        var timeStamp : Int = 0;
        var selectedDevice = CacheManager.instance.getSelectedDeviceFromCache();
        
        //Calibration - 1 byte
//        let calibrationExtractedBytes = weightBytes.subdata(in: NSMakeRange(0, 1));
        let calibrationExtractedBytes: NSData = weightBytes.scanValue(start: 0, length: 1)
        (calibrationExtractedBytes as NSData).getBytes(&calibrationFlag, length: 1);
        
        //Weight - 4 bytes
//        let weightExtractedBytes = weightBytes.subdata(in: NSMakeRange(1, 4));
        let weightExtractedBytes: NSData = weightBytes.scanValue(start: 1, length: 4)
//        var arr1 = [UInt8](repeating: 0, count: 4);
        var arr1 = [UInt8](repeating: 0, count: 4)
        (weightExtractedBytes as NSData).getBytes(&arr1);
        var array : [UInt8] = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
        var dataTemp = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
        (dataTemp as NSData).getBytes(&weight, length: 4);
        let calculatedWeight = Float(weight / SCALING_FACTOR);
        selectedDevice.weight = String(stringInterpolationSegment: calculatedWeight) + UNIT_GRAM;
        
        //Timestamp - 4 bytes
//        let timeExtractedBytes = weightBytes.subdata(in: NSMakeRange(5, 4));
        let timeExtractedBytes: NSData = weightBytes.scanValue(start: 5, length: 4)
//        arr1 = [UInt8](repeating: 0, count: 4);
        arr1 = [UInt8](repeating: 0, count: 4)
        (timeExtractedBytes as NSData).getBytes(&arr1);
        array  = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
        dataTemp = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4);
        (dataTemp as NSData).getBytes(&timeStamp, length: 4);
        
        //Convert timestamp to date time
        let receivedDate = Date(timeIntervalSince1970: Double(timeStamp));
        let dateFormatter:DateFormatter = DateFormatter();
        dateFormatter.dateFormat = DATE_TIME_FORMAT;
        selectedDevice.weightTime = dateFormatter.string(from: receivedDate);
        
        CacheManager.instance.setSelectedDeviceInCache(selectedDevice);
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshDeviceContents"), object: nil);
    }
    
    /* Parse weight history data - 8 bytes */
    func parseHistoryData(_ historyData : Data)
    {
        var weight : uint = 0;
        var historyList = CacheManager.instance.getSelectedHistoryListFromCache();
        let historyObj = History();
        var timeTemp : Int = 0 ;
        
        //Weight - 4 bytes
//        var weightTemp = historyData.subdata(in: NSMakeRange(0, 4));
        var weightTemp: NSData = historyData.scanValue(start: 0, length: 4)
        
//        var arr1 = [UInt8](repeating: 0, count: 4);
        var arr1 = [UInt8](repeating: 0, count: 4)
        (weightTemp as NSData).getBytes(&arr1);
        var array : [UInt8] = [ arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
        weightTemp = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4) as NSData;
        (weightTemp as NSData).getBytes(&weight, length: 4);
        let calculatedWeight = Float(weight / SCALING_FACTOR);
        historyObj.weight = String(stringInterpolationSegment: calculatedWeight) + UNIT_GRAM;
        
        //Timestamp - 4 bytes
//        var timeBytes = historyData.subdata(in: NSMakeRange(4, 4));
        var timeBytes: NSData = historyData.scanValue(start: 4, length: 4)
//        arr1 = [UInt8](repeating: 0, count: 4);
        arr1 = [UInt8](repeating: 0, count: 4)
        (timeBytes as NSData).getBytes(&arr1);
        array  = [arr1[0] as UInt8, arr1[1] as UInt8, arr1[2] as UInt8,arr1[3] as UInt8];
        timeBytes = Data(bytes: UnsafePointer<UInt8>(UnsafePointer<UInt8>(array)), count: 4) as NSData;
        (timeBytes as NSData).getBytes(&timeTemp, length: 4);
        
        //Convert timestamp to date time
        let receivedHistoryDate = Date(timeIntervalSince1970: Double(timeTemp));
        let dateFormatter:DateFormatter = DateFormatter();
        dateFormatter.dateFormat = DATE_TIME_FORMAT;
        historyObj.dateTime = dateFormatter.string(from: receivedHistoryDate);
        historyList.append(historyObj);
        
        print("History DATA:")
        print(historyObj)
        
        CacheManager.instance.setSelectedHistoryListInCache(historyList);
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshHistoryContents"), object: historyList);
    }
  
  func fireUILocalnotificationWithMessage(_ message:String) {
    let previousNotification = localNotification
    if previousNotification != nil {
      // UIApplication.sharedApplication().cancelLocalNotification(localNotification!)
    }
    localNotification = UILocalNotification()
    localNotification?.alertBody = message
    if #available(iOS 8.2, *) {
      localNotification?.alertTitle = "MMR"
    } else {
      // Fallback on earlier versions
    }
    localNotification?.fireDate = Date(timeIntervalSinceNow: 0)
    UIApplication.shared.scheduleLocalNotification(localNotification!)
  }
}

//extension Data {
//    func subdata(in range: ClosedRange<Index>) -> Data {
//        return subdata(in: range.lowerBound ..< range.upperBound + 1)
//    }
//}
extension Data {
    func scanValue<T>(start: Int, length: Int) -> T {
        return self.subdata(in: start..<start+length).withUnsafeBytes { $0.pointee }
    }
}

