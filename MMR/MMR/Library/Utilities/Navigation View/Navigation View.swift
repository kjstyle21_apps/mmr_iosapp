//
//  Navigation View.swift
//  MMR
//
//  Created by Mindbowser on 10/24/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit
private var once = Int()
class Navigation_View: UIView {
    
    var leftButton: UIButton?
    var rightButton =  UIButton()
    var titleLabel  =  UILabel ()
    var statusLabel = UILabel()
    var progressView = UIProgressView()
    
    convenience init() {
        self.init()
        commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        
    }
    
    func  commonInit () {
        self.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 64)
        self.backgroundColor = NAVIGATION_BAR_COLOR
        self.isUserInteractionEnabled = true
        
        
        
        
        // setUp title
        self.titleLabelSetUp()
        
        // setUp left button
        self.leftButtonSetUp()
        
        // setUp right button
        self.rightButtonSetUp()
        
        self.bluetoothStatusBar()
        
        self.setUpProgress()
        
    }
    
    func bluetoothStatusBar() {
        self.statusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 12))
        self.statusLabel.textAlignment = NSTextAlignment.center
        self.statusLabel.text = "Please Turn On Bluetooth"
        self.statusLabel.textColor = UIColor.white
        self.statusLabel.backgroundColor = UIColor.red
        self.statusLabel.font = UIFont(name: TAHOMA_REGULAR, size: 12);
        self.statusLabel.isHidden = true
        addSubview(self.statusLabel)
    }
    
    func titleLabelSetUp(){
        self.titleLabel = UILabel(frame: CGRect(x: 0, y: 40, width: 250, height: 44))
        self.titleLabel.center = self.center
        self.titleLabel.center.x = self.center.x
        self.titleLabel.center.y = self.center.y+10
        
        self.titleLabel.textAlignment = NSTextAlignment.center
        self.titleLabel.font = UIFont(name: VAGTRUNDSCHRIFT, size: 20);
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.text = "";
        addSubview(self.titleLabel)
    }
    
    // MARK: - *** Left Button ***
    
    func leftButtonSetUp(){
        leftButton = UIButton ()
        leftButton!.frame = CGRect( x: 1, y: 20, width: 44, height: 44)
        // self.leftButton.addTarget(self, action: #selector(test) , forControlEvents: .TouchUpInside)
        addSubview(self.leftButton!)
    }
    
    
    // MARK: - *** Right Button ***
    func rightButtonSetUp (){
        self.rightButton.frame = CGRect(x: SCREEN_WIDTH-45, y: 20, width: 44, height: 44)
        addSubview(self.rightButton)
    }
    
    func setUpProgress() {
        let userID = UserDefaults.standard.value(forKey: CURRENT_USER_ID_UD)
        if (userID != nil) {
            if let serialID = UserDefaults.standard.value(forKey: serialNoID) as? Int {
                let currentSurveyID = UserDefaults.standard.value(forKey: surveyId)
                //@kj
                //            let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as! Int
                var newSeqNo = Int()
                if let seqNum = UserDefaults.standard.value(forKey: "SURVEY_SEQ_NUM_\(userID!)") as? Int{
                    newSeqNo = seqNum
                }
                let totalCnt = QuestionSharedClass().fetchQuestionCount(userID as AnyObject, seqNum: newSeqNo as AnyObject, surveyId: currentSurveyID as AnyObject)
                print((Float(serialID - 1) / Float(totalCnt)) * 100)
                self.progressView.frame = CGRect(x: 54.0, y: self.bounds.size.height / 2 + 8, width: SCREEN_WIDTH - 98, height: 20)
                self.progressView.progressViewStyle = .bar
                self.progressView.progress = ((Float(serialID - 1) / Float(totalCnt)) * 100) / 100
                self.progressView.trackTintColor = UIColor.gray
                self.progressView.tintColor = UIColor.white
                addSubview(self.progressView)
            }
        }
    }
    
    func test()
    {
        print("parent")
    }
}
