//
//  NVActivityIndicatorView.swift
//  NVActivityIndicatorViewDemo
//
//  Created by Nguyen Vinh on 7/21/15.
//  Copyright (c) 2015 Nguyen Vinh. All rights reserved.
//

import UIKit

public enum NVActivityIndicatorType: Int {
    case ballTrianglePath


    fileprivate func animation() -> NVActivityIndicatorAnimationDelegate {
        
        return NVActivityIndicatorAnimationBallTrianglePath()
    }
}

/// Activity indicator view with nice animations
open class NVActivityIndicatorView: UIView {
    /// Default type. Default value is .BallSpinFadeLoader.
    open static var DEFAULT_TYPE: NVActivityIndicatorType = .ballTrianglePath
    
    /// Default color. Default value is UIColor.whiteColor().
    open static var DEFAULT_COLOR = UIColor.white
    
    /// Default padding. Default value is 0.
    open static var DEFAULT_PADDING: CGFloat = 0
    
    /// Default size of activity indicator view in UI blocker. Default value is 60x60.
    open static var DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
    
    /// Animation type, value of NVActivityIndicatorType enum.
    open var type: NVActivityIndicatorType = NVActivityIndicatorView.DEFAULT_TYPE

    @available(*, unavailable, message: "This property is reserved for Interface Builder. Use 'type' instead.")
    @IBInspectable var typeName: String {
        get {
            return String(describing: self.type)
        }
        set (typeString) {
            self.type = .ballTrianglePath
        }
    }

    /// Color of activity indicator view.
    @IBInspectable open var color: UIColor = NVActivityIndicatorView.DEFAULT_COLOR

    /// Padding of activity indicator view.
    @IBInspectable open var padding: CGFloat = NVActivityIndicatorView.DEFAULT_PADDING

    /// Current status of animation, this is not used to start or stop animation.
    open var animating: Bool = false
    
    /// Specify whether activity indicator view should hide once stopped.
    @IBInspectable open var hidesWhenStopped: Bool = true
    
    /**
     Create a activity indicator view with default type, color and padding.
     This is used by storyboard to initiate the view.
     
     - Default type is BallSpinFadeLoader.
     - Default color is white.
     - Default padding is 0.
     
     - parameter decoder:
     
     - returns: The activity indicator view.
     */
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    
    /**
     Create a activity indicator view with specified frame, type, color and padding.
     
     - parameter frame: view's frame.
     - parameter type: animation type, value of NVActivityIndicatorType enum. Default type is BallSpinFadeLoader.
     - parameter color: color of activity indicator view. Default color is white.
     - parameter padding: view's padding. Default padding is 0.
     
     - returns: The activity indicator view.
     */
    public init(frame: CGRect, type: NVActivityIndicatorType? = nil, color: UIColor? = nil, padding: CGFloat? = nil) {
        self.type = type ?? NVActivityIndicatorView.DEFAULT_TYPE
        self.color = color ?? NVActivityIndicatorView.DEFAULT_COLOR
        self.padding = padding ?? NVActivityIndicatorView.DEFAULT_PADDING
        super.init(frame: frame)
    }
    
    /**
     Start animation.
     */
    open func startAnimation() {
        if hidesWhenStopped && isHidden {
            isHidden = false
        }
        if (self.layer.sublayers == nil) {
            setUpAnimation()
        }
        self.layer.speed = 1
        self.animating = true
    }
    
    /**
     Stop animation.
     */
    open func stopAnimation() {
        self.layer.sublayers = nil
        self.animating = false
        if hidesWhenStopped && !isHidden {
            isHidden = true
        }
    }
    
    // MARK: Privates
    
    fileprivate func setUpAnimation() {
        let animation: NVActivityIndicatorAnimationDelegate = self.type.animation()
        var animationRect = UIEdgeInsetsInsetRect(self.frame, UIEdgeInsetsMake(padding, padding, padding, padding))
        let minEdge = min(animationRect.width, animationRect.height)
        
        self.layer.sublayers = nil
        animationRect.size = CGSize(width: minEdge, height: minEdge)
        animation.setUpAnimationInLayer(self.layer, size: animationRect.size, color: self.color)
    }
}
