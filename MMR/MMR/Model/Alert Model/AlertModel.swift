//
//  AlertModel.swift
//  MMR
//
//  Created by Mindbowser on 10/13/16.
//  Copyright © 2016 Mindbowser. All rights reserved.
//

import UIKit

class AlertModel: NSObject {
    typealias AlertCompletionHandler = (_ clickedButtonTitle:String, _ success:Bool?) -> Void
    var completionHandler: AlertCompletionHandler?
    
    class var sharedInstance :AlertModel {
        struct Singleton {
            static let instance = AlertModel()
        }
        return Singleton.instance
    }
    
    func showMessage(_ title: String, message: String) {
        DispatchQueue.main.async(execute: {
            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK".localized(), style: .default) { (action) in }
                alertController.addAction(okAction)
                
                var viewController = UIApplication.shared.delegate?.window??.rootViewController
                while ((viewController?.presentedViewController) != nil) {
                    viewController = viewController?.presentedViewController;
                }
                
                viewController?.present(alertController, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
                
                let alertView = UIAlertView()
                alertView.title = title
                alertView.message = message
                alertView.addButton(withTitle: "OK".localized())
                alertView.alertViewStyle = .default
                alertView.show()
            }
        })
    }
    
    func showErrorMessage(_ message:String) {
        showMessage("MMR", message: message)
    }
    
    func showAlert(_ title: String, message: String, buttonTitles: [String], viewController: UIViewController, completionHandler: @escaping AlertCompletionHandler) {
        
        self.completionHandler = completionHandler
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            var buttonIndex = 0
            for buttonTitle in buttonTitles {
                let cancelAction = UIAlertAction(title: buttonTitle, style: .default) { (action) in
                    self.completionHandler!(buttonTitle, true)
                }
                alertController.addAction(cancelAction)
                buttonIndex += 1
            }
            
            viewController.present(alertController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let alertView = UIAlertView()
            alertView.title = title
            alertView.message = message
            
            for buttonTitle in buttonTitles {
                alertView.addButton(withTitle: buttonTitle)
            }
            
            alertView.alertViewStyle = .default
            alertView.delegate = self
            alertView.show()
        }
    }
    
    // MARK: UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        let buttonTitle = alertView.buttonTitle(at: buttonIndex)
        self.completionHandler!(buttonTitle!, true)
    }

}
