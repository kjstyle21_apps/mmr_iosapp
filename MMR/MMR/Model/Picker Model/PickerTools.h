//
//  PickerTools.h
//  PickerViewUtil
//
//  Created by Nilam on 10/23/13.
//  Copyright (c) 2013 Nilam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_INLINE NSException * _Nullable tryBlock(void(^_Nonnull tryBlock)(void)) {
    @try {
        tryBlock();
    }
    @catch (NSException *exception) {
        return exception;
    }
    return nil;
}
typedef void (^PickerViewCompletionHandler) ( NSString *value, BOOL valueChanged);

@interface PickerTools : NSObject<UIPickerViewDelegate,UIPickerViewDataSource>

+(PickerTools *)shareModel;

- (void)showPickerViewWith:(UIViewController *)myViewController withContent:(NSArray *)contentArray andSelectedString:(NSString *)selectedOption
         CompletionHandler:(PickerViewCompletionHandler)completionHandler;


-(void)removePickerView;
@end
